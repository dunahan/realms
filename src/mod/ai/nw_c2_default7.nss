//:://////////////////////////////////////////////////
//:: NW_C2_DEFAULT7
/*
  Default OnDeath event handler for NPCs.

  Adjusts killer's alignment if appropriate and
  alerts allies to our death.
*/
//:://////////////////////////////////////////////////
//:: Copyright (c) 2002 Floodgate Entertainment
//:: Created By: Naomi Novik
//:: Created On: 12/22/2002
//:://////////////////////////////////////////////////
//:://////////////////////////////////////////////////
//:: Modified By: Deva Winblood
//:: Modified On: April 1st, 2008
//:: Added Support for Dying Wile Mounted
//:://///////////////////////////////////////////////

#include "hench_i0_ai"
#include "x2_inc_compon"
#include "x0_i0_spawncond"
#include "x3_inc_horse"

void main()
{
    int nClass = GetLevelByClass(CLASS_TYPE_COMMONER);
    int nAlign = GetAlignmentGoodEvil(OBJECT_SELF);
    object oKiller = GetLastKiller();

    if (GetLocalInt(GetModule(),"X3_ENABLE_MOUNT_DB")&&GetIsObjectValid(GetMaster(OBJECT_SELF))) SetLocalInt(GetMaster(OBJECT_SELF),"bX3_STORE_MOUNT_INFO",TRUE);


    // If we're a good/neutral commoner,
    // adjust the killer's alignment evil
    if(nClass > 0 && (nAlign == ALIGNMENT_GOOD || nAlign == ALIGNMENT_NEUTRAL))
    {
        AdjustAlignment(oKiller, ALIGNMENT_EVIL, 5);
    }

    if (GetLocalInt(OBJECT_SELF,"GaveHealing"))
    {
        // Pausanias: destroy potions of healing
        object oItem = GetFirstItemInInventory();
        while (GetIsObjectValid(oItem))
        {
            if (GetTag(oItem) == "NW_IT_MPOTION003")
                DestroyObject(oItem);
            oItem = GetNextItemInInventory();
        }
    }

    if (GetLocalInt(OBJECT_SELF, sHenchSummonedFamiliar))
    {
        object oFam = GetLocalObject(OBJECT_SELF, sHenchSummonedFamiliar);
        if (GetIsObjectValid(oFam))
        {
//            Jug_Debug(GetName(OBJECT_SELF) + " destroy familiar");
            DestroyObject(oFam, 0.1);
            ApplyEffectAtLocation(DURATION_TYPE_TEMPORARY, EffectVisualEffect(VFX_IMP_UNSUMMON), GetLocation(oFam));
        }
    }
    if (GetLocalInt(OBJECT_SELF, sHenchSummonedAniComp))
    {
        object oAni = GetLocalObject(OBJECT_SELF, sHenchSummonedAniComp);
        if (GetIsObjectValid(oAni))
        {
//            Jug_Debug(GetName(OBJECT_SELF) + " destroy ani comp");
            DestroyObject(oAni, 0.1);
            ApplyEffectAtLocation(DURATION_TYPE_TEMPORARY, EffectVisualEffect(VFX_IMP_UNSUMMON), GetLocation(oAni));
        }
    }

    ClearEnemyLocation();

    // Call to allies to let them know we're dead
    SpeakString("NW_I_AM_DEAD", TALKVOLUME_SILENT_TALK);

    //Shout Attack my target, only works with the On Spawn In setup
    SpeakString("NW_ATTACK_MY_TARGET", TALKVOLUME_SILENT_TALK);

    // NOTE: the OnDeath user-defined event does not
    // trigger reliably and should probably be removed
    if(GetSpawnInCondition(NW_FLAG_DEATH_EVENT))
    {
        SignalEvent(OBJECT_SELF, EventUserDefined(1007));
    }
    craft_drop_items(oKiller);
}
