#include "inc_crafting"

void BeginConversationVoid(string sResRef, object PC)
{
    BeginConversation("x0_skill_ctrap", PC);
}

void main()
{
    object PC = GetPCSpeaker();
    SetCrafterCreature(PC, OBJECT_SELF);
    AssignCommand(PC, BeginConversationVoid("x0_skill_ctrap", PC));
}
