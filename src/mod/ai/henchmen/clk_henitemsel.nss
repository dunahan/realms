#include "inc_language"
#include "inc_targeting"

void main()
{
    object item = GetTargetingModeSelectedObject();
    object PC = GetLastPlayerToSelectTarget();
    if (item != OBJECT_INVALID)
    {
        SetLocalObject(PC, "LastItemOrderedToBeUsed", item);

        int baseItem = GetBaseItemType(item);
        if (baseItem == BASE_ITEM_POTIONS || baseItem == BASE_ITEM_ENCHANTED_POTION)
        {
            ExecuteScript("clk_henitemtar");
            return;
        }
        int targetType, validCursor, invalidCursor;
        if (baseItem == BASE_ITEM_HEALERSKIT)
        {
            targetType = OBJECT_TYPE_CREATURE;
            validCursor = MOUSECURSOR_HEAL;
            invalidCursor = MOUSECURSOR_NOHEAL;
        }
        else if (baseItem == BASE_ITEM_THIEVESTOOLS)
        {
            targetType = OBJECT_TYPE_PLACEABLE | OBJECT_TYPE_DOOR;
            validCursor = MOUSECURSOR_LOCK;
            invalidCursor = MOUSECURSOR_NOLOCK;
        }
        else if (baseItem == BASE_ITEM_TRAPKIT)
        {
            targetType = OBJECT_TYPE_ALL;
            validCursor = MOUSECURSOR_ACTION;
            invalidCursor = MOUSECURSOR_NOACTION;
        }
        else
        {
            targetType = OBJECT_TYPE_ALL;
            validCursor = MOUSECURSOR_MAGIC;
            invalidCursor = MOUSECURSOR_NOMAGIC;
        }

        EnterTargetingModeToExecuteScript(PC, "clk_henitemtar", targetType, validCursor, invalidCursor);
        string msg = GetLocalizedString("Select a target.", "Wybierz cel.");
        FloatingTextStringOnCreature(msg, PC, FALSE);
    }
}