#include "inc_epicspells"

// the henchman targets PC with the spell

void main()
{
    int spell = GetLocalInt(OBJECT_SELF, "Selected_Spell");
    ClearAllActions();
    object PC = GetPCSpeaker();
    ActionCastSpellAtObjectIncludingEpic(spell, PC);
}
