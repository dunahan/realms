#include "inc_convoptions"
#include "inc_common"
#include "inc_colors"
#include "inc_crafting"
#include "inc_debug"
#include "inc_dftokens"
#include "inc_epicspells"

int GetIsSpellValidForPC(int spellId)
{
    switch (spellId)
    {
        case SPELL_EPIC_MUMMY_DUST:
        case SPELL_EPIC_HELLBALL:
        case SPELL_EPIC_MAGE_ARMOR:
        case SPELL_EPIC_RUIN:
        case SPELL_EPIC_DRAGON_KNIGHT:
        case 695: //epic warding, couldn't find the constant...
        return TRUE;
    }
    string bard = Get2DAString("spells", "Bard", spellId);
    string cleric = Get2DAString("spells", "Cleric", spellId);
    string druid = Get2DAString("spells", "Druid", spellId);
    string paladin = Get2DAString("spells", "Paladin", spellId);
    string ranger = Get2DAString("spells", "Ranger", spellId);
    string wizSorc = Get2DAString("spells", "Wiz_Sorc", spellId);
    return (bard != "" || cleric != "" || druid != "" || paladin != "" || ranger != "" || wizSorc != "");
}

void main()
{
    if (GetConversationOptionsInitialized())
        return;

    string optionsArray = "optionsArray";
    string valuesArray = "valuesArray";

    CreateStringArray(optionsArray);
    CreateStringArray(valuesArray);

    int spellId = 0;
    for (spellId = 0; spellId < 1000; spellId++)
    {
        string nameStringRef = Get2DAString("spells", "Name", spellId);
        string masterSpell = Get2DAString("spells", "Master", spellId);
        int isSubSpell = masterSpell != "";
        int hasSubSpells = Get2DAString("spells", "SubRadSpell1", spellId) != "";
        int isValidPCSpell = GetIsSpellValidForPC(spellId);
        int masterSpellId = StringToInt(masterSpell);
        //Check for 50, 137 and 150 master spells is due to deleted subspells of Resist Elements and Protection from Elements
        int isMasterValidPCSpell = isSubSpell && masterSpellId != 50 && masterSpellId != 137 && masterSpellId != 150 ? GetIsSpellValidForPC(masterSpellId) : FALSE;

        if (GetIsStringInt(nameStringRef) && (isValidPCSpell || isMasterValidPCSpell) && (isSubSpell || !hasSubSpells) && GetHasSpellIncludingEpic(spellId, FALSE))
        {
            string spellName = GetStringByStrRef(StringToInt(nameStringRef));

            string value = IntToString(spellId);
            string option = spellName+".";
            AddStringArrayElement(optionsArray, option);
            AddStringArrayElement(valuesArray, value);
        }
    }

    LoadConversationOptions(optionsArray, valuesArray);
}
