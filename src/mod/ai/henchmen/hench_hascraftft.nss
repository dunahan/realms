int StartingConditional()
{
    return GetHasFeat(FEAT_SCRIBE_SCROLL) || GetHasFeat(FEAT_CRAFT_WAND) || GetHasFeat(FEAT_BREW_POTION);
}
