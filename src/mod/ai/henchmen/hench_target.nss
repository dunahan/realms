#include "inc_targeting"

void main()
{
    object PC = GetPCSpeaker();
    SetLocalObject(PC, "SpellCasterCompanion", OBJECT_SELF);
    EnterTargetingModeToExecuteScript(PC, "clk_spelltarget", OBJECT_TYPE_ALL);
}
