#include "inc_language"
#include "inc_debug"

itemproperty _GetIPToUse(object oItem)
{
    itemproperty ip = GetFirstItemProperty(oItem);
    while (GetIsItemPropertyValid(ip))
    {
        int type = GetItemPropertyType(ip);
        if (type == ITEM_PROPERTY_CAST_SPELL
            || type == ITEM_PROPERTY_TRAP
            || type == ITEM_PROPERTY_HEALERS_KIT
            || type == ITEM_PROPERTY_THIEVES_TOOLS)
            return ip;
        ip = GetNextItemProperty(oItem);
    }
    itemproperty invalid;
    return invalid;
}

void main()
{
    object target = GetTargetingModeSelectedObject();
    object PC = GetLastPlayerToSelectTarget();
    object item = GetLocalObject(PC, "LastItemOrderedToBeUsed");
    object companion = GetLocalObject(PC, "LastCompanionOrderedToUseItem");
    itemproperty ip = _GetIPToUse(item);

    if (GetObjectType(target) == 0)
    {
        vector position = GetTargetingModeSelectedPosition();
        location loc = Location(target, position, 0.0f);       

        AssignCommand(companion, ClearAllActions());
        AssignCommand(companion, ActionUseItemAtLocation(item, ip, loc));
        return;
    } 

    int baseItem = GetBaseItemType(item);
    if (baseItem == BASE_ITEM_POTIONS || baseItem == BASE_ITEM_ENCHANTED_POTION)
        target = companion;

    AssignCommand(companion, ClearAllActions());
    AssignCommand(companion, ActionUseItemOnObject(item, ip, target));

    string msg = GetLocalizedString("I'm on it.", "Ju� si� robi.");
    AssignCommand(companion, SpeakString(msg));
}