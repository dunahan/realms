//:://////////////////////////////////////////////////
//:: X0_CH_HEN_HEART
/*

  OnHeartbeat event handler for henchmen/associates.

 */
//:://////////////////////////////////////////////////
//:: Copyright (c) 2002 Floodgate Entertainment
//:: Created By: Naomi Novik
//:: Created On: 01/05/2003
//:://////////////////////////////////////////////////
#include "x0_inc_henai"
#include "hench_i0_generic"
#include "inc_companions"
#include "inc_itemspells"

void TakeAndUseSoulstone(object master)
{
    object soulstone = GetItemPossessedBy(master, "it_soulstone");
    SetItemStackSize(soulstone, GetItemStackSize(soulstone)-1);
    soulstone = CreateItemOnObject("it_soulstone");
    ActionUseItemOnObject(soulstone, GetFirstItemProperty(soulstone), master);
}

object GetClosestCompanionFromSameFaction(object oCreature)
{
    object factionMember = GetFirstFactionMember(oCreature, FALSE);
    object minDistanceFactionMember = OBJECT_INVALID;
    float minDistance = 999.0f;
    while (GetIsObjectValid(factionMember))
    {
        if (factionMember != oCreature && GetLocalString(factionMember, "ID") != "")
        {
            float distance = GetDistanceBetween(factionMember, oCreature);
            if (distance > 0.0f && distance < minDistance)
            {
                minDistance = distance;
                minDistanceFactionMember = factionMember;
            }
        }
        factionMember = GetNextFactionMember(oCreature, FALSE);
    }
    return minDistanceFactionMember;
}

void main()
{
//    Jug_Debug("*****" + GetName(OBJECT_SELF) + " heartbeat " + IntToString(GetCurrentAction()) + " busy " + IntToString(GetAssociateState(NW_ASC_IS_BUSY)));

    // If the henchman is in dying mode, make sure
    // they are non commandable. Sometimes they seem to
    // 'slip' out of this mode
    int bDying = GetIsHenchmanDying();

    if (bDying)
    {
        int bCommandable = GetCommandable();
        if (bCommandable == TRUE)
        {
            // lie down again
            ActionPlayAnimation(ANIMATION_LOOPING_DEAD_FRONT,
                                1.0, 65.0);
            SetCommandable(FALSE);
        }
    }

    // If we're dying or busy, we return
    // (without sending the user-defined event)
    if(GetAssociateState(NW_ASC_IS_BUSY) || bDying)
    {
        return;
    }

    //If any companion needs resurrecting and we have a means to do it, do it
    object soulstone = GetItemPossessedBy(OBJECT_SELF, "it_soulstone");
    object master = GetMaster(OBJECT_SELF);
    if (GetIsObjectValid(soulstone) || GetHasSpellIncludingItems(SPELL_RAISE_DEAD) || GetHasSpellIncludingItems(SPELL_RESURRECTION))
    {
        object ally = GetFirstFactionMember(OBJECT_SELF, FALSE);
        while (GetIsObjectValid(ally))
        {
            if (GetIsDead(ally) && GetDistanceToObject(ally) < 20.0f && !GetLocalInt(ally, "IsBeingRaised") && GetClosestCompanionFromSameFaction(ally) == OBJECT_SELF)
            {
                SetLocalInt(ally, "IsBeingRaised", TRUE);
                DelayCommand(10.0f, DeleteLocalInt(ally, "IsBeingRaised"));
                ClearAllActions();
                if (GetHasSpellIncludingItems(SPELL_RESURRECTION))
                    ActionCastSpellAtObjectIncludingItems(SPELL_RESURRECTION, ally);
                else if (GetHasSpellIncludingItems(SPELL_RAISE_DEAD))
                    ActionCastSpellAtObjectIncludingItems(SPELL_RAISE_DEAD, ally);
                else
                    ActionUseItemOnObject(soulstone, GetFirstItemProperty(soulstone), ally);
                return;
            }
            ally = GetNextFactionMember(OBJECT_SELF, FALSE);
        }
    }
    else if (GetIsObjectValid(master) && GetIsDead(master) 
        && GetIsObjectValid(GetItemPossessedBy(master, "it_soulstone")) 
        && GetDistanceToObject(master) < 20.0f 
        && !GetLocalInt(master, "IsBeingRaised") 
        && GetClosestCompanionFromSameFaction(master) == OBJECT_SELF)
    {
        SetLocalInt(master, "IsBeingRaised", TRUE);
        DelayCommand(10.0f, DeleteLocalInt(master, "IsBeingRaised"));
        ClearAllActions();
        ActionMoveToLocation(GetLocation(master), TRUE);
        ActionDoCommand(TakeAndUseSoulstone(master));
        return;
    }

    SetCombatMode();
    ExecuteScript("nw_ch_ac1", OBJECT_SELF);
}



