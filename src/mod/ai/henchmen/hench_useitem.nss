#include "inc_targeting"
#include "inc_language"

void main()
{
    object PC = GetPCSpeaker();
    object companion = OBJECT_SELF;
    OpenInventory(OBJECT_SELF, PC);
    SetLocalObject(PC, "LastCompanionOrderedToUseItem", OBJECT_SELF);
    EnterTargetingModeToExecuteScript(PC, "clk_henitemsel", OBJECT_TYPE_ITEM, MOUSECURSOR_ACTION, MOUSECURSOR_NOACTION);
    string msg = GetLocalizedString("Select a companion's item.", "Wybierz przedmiot towarzysza.");
    FloatingTextStringOnCreature(msg, PC, FALSE);
}
