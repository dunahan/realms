#include "inc_epicspells"
// the henchman targets himself with the spell

void main()
{
    int spell = GetLocalInt(OBJECT_SELF, "Selected_Spell");
    ClearAllActions();
    ActionCastSpellAtObjectIncludingEpic(spell, OBJECT_SELF);
}
