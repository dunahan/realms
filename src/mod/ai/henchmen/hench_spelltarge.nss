#include "inc_convoptions"
#include "inc_common"

int StartingConditional()
{
    string spell = GetSelectedOptionValue();
    int spellId = StringToInt(spell);
    SetLocalInt(OBJECT_SELF, "Selected_Spell", spellId);

    string targetTypeString = Get2DAString("spells", "TargetType", spellId);
    int targetType = HexStringToInt(targetTypeString);
    if (targetType != 1 && targetType != 9)
        return TRUE;
    return FALSE;
}
