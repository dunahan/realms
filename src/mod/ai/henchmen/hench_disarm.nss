#include "inc_language"

void main()
{
    float shortestDistance = 100.0f;
    object closestTrap = OBJECT_INVALID;
    object trap = GetFirstObjectInShape(SHAPE_SPHERE, 10.0f, GetLocation(OBJECT_SELF), TRUE, OBJECT_TYPE_TRIGGER);
    while (GetIsObjectValid(trap))
    {
        if (GetFactionEqual(trap, OBJECT_SELF))
        {
            float distance = GetDistanceBetween(trap, OBJECT_SELF);
            if (distance < shortestDistance)
            {
                shortestDistance = distance;
                closestTrap = trap;
            }
        }
        trap = GetNextObjectInShape(SHAPE_SPHERE, 10.0f, GetLocation(OBJECT_SELF), TRUE, OBJECT_TYPE_TRIGGER);
    }

    if (!GetIsObjectValid(closestTrap))
    {
        string msg = GetLocalizedString("I don't think there are any of our traps lying around here.", "Nie widz� w pobli�u �adnej naszej pu�apki.");
        SpeakString(msg);
        return;
    }

    ClearAllActions();
    ActionUseSkill(SKILL_DISABLE_TRAP, closestTrap);
}
