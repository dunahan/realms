// This master event handler processes custom event logic in a modular manner (see inc_scriptevents for details).
// This one is for every creature's OnHeartbeat event.

#include "inc_scriptevents"
#include "inc_ruleset"
#include "inc_horses"
#include "inc_debug"
#include "inc_darkareas"
#include "inc_convabort"

void main()
{
    //Run the conversation abort script if didn't run for some reason
    EnforceConversationAbortScript();

    //Very Difficult: check if the difficuly has changed and apply extra buffs on Very Difficulty or remove them on lower ones
    string difficultyVarName = "RememberedGameDifficulty";
    if (GetLocalInt(OBJECT_SELF, difficultyVarName) != GetGameDifficulty() && !GetIsPC(OBJECT_SELF))
    {
        int oldDifficulty = GetLocalInt(OBJECT_SELF, difficultyVarName);
        int newDifficulty = GetGameDifficulty();
        if (oldDifficulty == GAME_DIFFICULTY_DIFFICULT || newDifficulty == GAME_DIFFICULTY_DIFFICULT)
            RecalculateDifficultyBuffs(OBJECT_SELF);
        SetLocalInt(OBJECT_SELF, difficultyVarName, newDifficulty);
    }

    //Horse riding effects
    RecalculateRidingEffects(OBJECT_SELF);

    //Dark areas system
    RecalculateDarkAreaPenalty(OBJECT_SELF);
    VerifyLightOrContinualFlameSpellEffects(OBJECT_SELF);
    if (!GetIsPC(OBJECT_SELF))
        RecalculateEquippedLightSourcesOnNPCIfNeeded(OBJECT_SELF);

    //Run preset script for that event from a variable if it exists
    string presetScript = GetLocalString(OBJECT_SELF, SUBEVENT_CREATURE_ON_HEARTBEAT);
    if (presetScript != "")
    {
        if (presetScript == "evn_crehb")
            LogWarning("Creature with ResRef "+ GetResRef(OBJECT_SELF) +" attempted to call a looped master event script "+ presetScript);
        else
            ExecuteScript(presetScript);
    }

    //Run standard summon script if creature is marked as a summon with a variable (and didn't have any specific preset script)
    else if (GetLocalInt(OBJECT_SELF, "StandardSummon") == TRUE)
        ExecuteScript("nw_ch_ac1");

    RunEventScripts(SUBEVENT_CREATURE_ON_HEARTBEAT);
}