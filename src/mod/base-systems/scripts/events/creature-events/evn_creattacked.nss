// This master event handler processes custom event logic in a modular manner (see inc_scriptevents for details).
// This one is for every creature's OnPhysicalAttacked event.

#include "inc_scriptevents"
#include "inc_convabort"
#include "inc_debug"

void main()
{
    //Run the conversation abort script if didn't run for some reason
    EnforceConversationAbortScript();

    //Run preset script for that event from a variable if it exists
    string presetScript = GetLocalString(OBJECT_SELF, SUBEVENT_CREATURE_ON_PHYSICAL_ATTACKED);
    if (presetScript != "")
    {
        if (presetScript == "evn_creattacked")
            LogWarning("Creature with ResRef "+ GetResRef(OBJECT_SELF) +" attempted to call a looped master event script "+ presetScript);
        else
            ExecuteScript(presetScript);
    }

    //Run standard summon script if creature is marked as a summon with a variable (and didn't have any specific preset script)
    else if (GetLocalInt(OBJECT_SELF, "StandardSummon") == TRUE)
        ExecuteScript("nw_ch_ac5");

    RunEventScripts(SUBEVENT_CREATURE_ON_PHYSICAL_ATTACKED);
}