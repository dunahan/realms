#include "inc_onexitfix"
#include "inc_scriptevents"

// This master event handler processes custom event logic in a modular manner (see inc_scriptevents for details).
// This one is for every trigger's OnExit event.
// This script also fixes the OnExit bug (see inc_onexitfix for details)
void main()
{
    RemoveTrigger(GetExitingObject(), OBJECT_SELF);
    RunEventScripts(SUBEVENT_TRIGGER_ON_EXIT);
}
