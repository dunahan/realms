#include "inc_onexitfix"
#include "inc_scriptevents"
#include "inc_horses"
#include "inc_debug"

// This master event handler processes custom event logic in a modular manner (see inc_scriptevents for details).
// This one is for every area's OnEnter event.
// This script also fixes the OnExit bug (see inc_onexitfix for details)
void main()
{
    SetCurrentArea(GetEnteringObject(), OBJECT_SELF);
    ReaddHorsesInTheAreaToParty(OBJECT_SELF);
    RunEventScripts(SUBEVENT_AREA_ON_ENTER);
}
