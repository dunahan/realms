#include "inc_onexitfix"
#include "inc_scriptevents"

// This master event handler processes custom event logic in a modular manner (see inc_scriptevents for details).
// This one is for every area's OnExit event.
// This script also fixes the OnExit bug (see inc_onexitfix for details)
void main()
{
    UnsetCurrentArea(GetExitingObject(), OBJECT_SELF);
    RunEventScripts(SUBEVENT_AREA_ON_EXIT);
}
