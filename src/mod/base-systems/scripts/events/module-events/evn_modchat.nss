// This master event handler has three functions:
// - Process chat commands (defined by default as messages beginning with '/') by executing associated scripts
// - Registers other messages to enable GetLastMessage function in the rest of the module
// - Processes custom messages in a modular manner (see inc_scriptevents for details)

#include "inc_debug"
#include "inc_chat"
#include "inc_switches"
#include "inc_scriptevents"

void main()
{
    //Process chat commands
    if (RunChatCommandScript())
    {
        if (GetSwitch(MODULE_SWITCH_DEBUG_MODE))
            RunChatCommandScript("dbg_");
        SetPCChatMessage("");
        return;
    }

    //Register chat message for GetLastMessage function
    RegisterMessage();

    //Custom message processing
    RunEventScripts("OnChat");
}
