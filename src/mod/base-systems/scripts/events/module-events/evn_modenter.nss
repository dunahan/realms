// This master event handler has two functions:
// - ensure the PC has a skin and creates and equips it if necessary
// - processes custom event logic in a modular manner (see inc_scriptevents for details)

#include "inc_scriptevents"
#include "inc_debug"

void main()
{
    //Ensure skin possession
    string skinResRef = "x3_it_pchide";
    object PC = GetEnteringObject();
    object skin = GetItemInSlot(INVENTORY_SLOT_CARMOUR, PC);
    if (!GetIsObjectValid(skin) || GetResRef(skin) != skinResRef)
    {
        DestroyObject(skin);
        skin = CreateItemOnObject(skinResRef, PC);
        AssignCommand(PC, ActionEquipItem(skin, INVENTORY_SLOT_CARMOUR));
    }

    //Custom processing
    DelayCommand(0.1f, RunEventScripts(SUBEVENT_MODULE_ON_CLIENT_ENTER));
}