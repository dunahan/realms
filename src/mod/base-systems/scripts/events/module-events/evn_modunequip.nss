// This master event handler has three functions:
// - enforces reequip of PC skin if it's unequipped due to a bug
// - executes default BioWare event logic (content of x2_mod_def_unequ)
// - processes custom event logic in a modular manner (see inc_scriptevents for details)

#include "x2_inc_switches"
#include "x2_inc_intweapon"
#include "x3_inc_horse"
#include "inc_horses"
#include "inc_scriptevents"

void main()
{
    object oItem = GetPCItemLastUnequipped();
    object oPC = GetPCItemLastUnequippedBy();

    // Failsafe for when PC skin is unequipped by accident
    if (GetBaseItemType(oItem) == BASE_ITEM_CREATUREITEM)
    {
        AssignCommand(oPC, ActionEquipItem(oItem, INVENTORY_SLOT_CARMOUR));
        return;
    }

    // -------------------------------------------------------------------------
    //  Intelligent Weapon System
    // -------------------------------------------------------------------------
    if (IPGetIsIntelligentWeapon(oItem))
    {
            IWSetIntelligentWeaponEquipped(oPC,OBJECT_INVALID);
            IWPlayRandomUnequipComment(oPC,oItem);
    }

    // Mounted archery penalty
    RecalculateHorseRangedPenalty(oPC, oItem);

    // -------------------------------------------------------------------------
    // Generic Item Script Execution Code
    // If MODULE_SWITCH_EXECUTE_TAGBASED_SCRIPTS is set to TRUE on the module,
    // it will execute a script that has the same name as the item's tag
    // inside this script you can manage scripts for all events by checking against
    // GetUserDefinedItemEventNumber(). See x2_it_example.nss
    // -------------------------------------------------------------------------
    if (GetModuleSwitchValue(MODULE_SWITCH_ENABLE_TAGBASED_SCRIPTS) == TRUE)
    {
        SetUserDefinedItemEventNumber(X2_ITEM_EVENT_UNEQUIP);
        int nRet = ExecuteScriptAndReturnInt(GetUserDefinedItemEventScriptName(oItem),OBJECT_SELF);
        if (nRet == X2_EXECUTE_SCRIPT_END)
           return;
    }

    //Custom processing
    RunEventScripts(SUBEVENT_MODULE_ON_PLAYER_UNEQUIP_ITEM);
}