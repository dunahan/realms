#include "inc_scriptevents"
#include "x2_inc_switches"
#include "inc_switches"

// This master event handler processes custom event logic in a modular manner (see inc_scriptevents for details).
// This one is for the module's OnModuleLoad event. The script:
// - registers master event handlers for all other module events
// - registers module-specific startup scripts (the only part of this script that's supposed to be customizable)
// - registers master event handlers for all areas and triggers in the module
// - runs whatever module-specific startup scripts were registered

void main()
{
    if (!GetLocalInt(OBJECT_SELF, "MOD_IS_LAUNCHED"))
    {
        //Registering master event handlers
        RegisterMasterModuleEvents();

        //Register module-specific OnModuleLoad script (scripts for other events should be registered in that module-specific OnModuleLoad script)
        AddEventScript(SUBEVENT_MODULE_ON_LOAD, "mod_dfstart");        

        //Iterate through all areas and all triggers in areas to set them up for the OnExit bug workaround
        RegisterMasterAreaAndTriggerEvents();
    }

    RunEventScripts("OnModuleLoad");

    //Mark the module as launched, so the initialization doesn't take place on savefile load
    SetLocalInt(OBJECT_SELF, "MOD_IS_LAUNCHED", TRUE);
}
