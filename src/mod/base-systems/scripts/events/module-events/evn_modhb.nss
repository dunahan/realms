// This master event handler processes custom event logic in a modular manner (see inc_scriptevents for details)

#include "inc_scriptevents"

void main()
{
    RunEventScripts(SUBEVENT_MODULE_ON_HEARTBEAT);
}
