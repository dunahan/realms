#include "inc_switches"
#include "inc_colors"
#include "x3_inc_string"

const int LOG_LEVEL_INFO = 1;
const int LOG_LEVEL_WARN = 2;
const int LOG_LEVEL_FATAL = 3;

// Logs a given message into the log file. Does nothing if MODULE_SWITCH_LOGGING
// is turned FALSE. If MODULE_SWITCH_DEBUG_MODE is turned TRUE, the function additionally
// prints the message into the PC's chat log.
// You can pass up to four integers as extra arguments to format them in the message string.
// Add "%n" in the message string as a number placeholder.
void LogInfo(string sMessage, int nNum1=0, int nNum2=0, int nNum3=0, int nNum4=0);

// Logs a given message preceded with "WARNING: " into the log file. Does nothing if MODULE_SWITCH_LOGGING
// is turned FALSE. If MODULE_SWITCH_DEBUG_MODE is turned TRUE, the function additionally
// prints the message into the PC's chat log.
// You can pass up to four integers as extra arguments to format them in the message string.
// Add "%n" in the message string as a number placeholder.
void LogWarning(string sMessage, int nNum1=0, int nNum2=0, int nNum3=0, int nNum4=0);

// Logs a given message preceded with "FATAL: " into the log file. Does nothing if MODULE_SWITCH_LOGGING
// is turned FALSE. If MODULE_SWITCH_DEBUG_MODE is turned TRUE, the function additionally
// prints the message into the PC's chat log.
// You can pass up to four integers as extra arguments to format them in the message string.
// Add "%n" in the message string as a number placeholder.
void LogFatal(string sMessage, int nNum1=0, int nNum2=0, int nNum3=0, int nNum4=0);

// Asserts that a correct string was received, logging an error message
// (with an optional custom message included in addition to the regular one)
int AssertStringEqual(string sExpected, string sValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN);

// Asserts that a correct string was received, logging an error message
// (with an optional custom message included in addition to the regular one)
int AssertStringNotEqual(string sNotExpected, string sValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN);

// Asserts that a correct integer was received, logging an error message
// (with an optional custom message included in addition to the regular one)
int AssertIntEqual(int nExpected, int nValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN);

// Asserts that a correct integer was received, logging an error message
// (with an optional custom message included in addition to the regular one)
int AssertIntNotEqual(int nNotExpected, int nValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN);

// Asserts that a correct integer was received, logging an error message
// (with an optional custom message included in addition to the regular one)
int AssertIntGreater(int nThreshold, int nValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN);

// Asserts that a correct integer was received, logging an error message
// (with an optional custom message included in addition to the regular one)
int AssertIntNotLower(int nThreshold, int nValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN);

// Asserts that a correct integer was received, logging an error message
// (with an optional custom message included in addition to the regular one)
int AssertIntLower(int nThreshold, int nValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN);

// Asserts that a correct integer was received, logging an error message
// (with an optional custom message included in addition to the regular one)
int AssertIntNotGreater(int nThreshold, int nValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN);

// A wrapper for SpawnScriptDebugger() that gives you more precise control over when the breakpoint can be hit (useful if spawning the debugger within loops)
// Methods of use:
// Breakpoint() - will simply call SpawnScriptDebugger
// Breakpoint(i, 3) - will only call SpawnScriptDebugger when i == 3
// Breakpoint(0, 0, "break1") - will only call SpawnScriptDebugger if it's the first time a breakpoint tagged "break1" is hit in the game
void Breakpoint(int nCurrentLoopIteration=0, int nIterationOfBreak=0, string sBreakpointTag="");

//Public functions

int AssertStringNotEqual(string sNotExpected, string sValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN)
{
    if (sNotExpected != sValue)
        return TRUE;
    string fullMessage = "expected different than \"" + sNotExpected + "\", got \"" + sValue + "\"";
    if (sErrorMsg != "")
        fullMessage += " (" + sErrorMsg + ")";

    switch (nLogLevel)
    {
        case LOG_LEVEL_INFO:
            LogInfo(fullMessage);
            break;
        case LOG_LEVEL_WARN:
            LogWarning(fullMessage);
            break;
        case LOG_LEVEL_FATAL:
            LogFatal(fullMessage);
            break;
    }

    return FALSE;
}

int AssertStringEqual(string sExpected, string sValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN)
{
    if (sExpected == sValue)
        return TRUE;
    string fullMessage = "expected \"" + sExpected + "\", got \"" + sValue + "\"";
    if (sErrorMsg != "")
        fullMessage += " (" + sErrorMsg + ")";

    switch (nLogLevel)
    {
        case LOG_LEVEL_INFO:
            LogInfo(fullMessage);
            break;
        case LOG_LEVEL_WARN:
            LogWarning(fullMessage);
            break;
        case LOG_LEVEL_FATAL:
            LogFatal(fullMessage);
            break;
    }

    return FALSE;
}

int AssertIntEqual(int nExpected, int nValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN)
{
    if (nExpected == nValue)
        return TRUE;
    string fullMessage = "expected " + IntToString(nExpected) + ", got " + IntToString(nValue);
    if (sErrorMsg != "")
        fullMessage += " (" + sErrorMsg + ")";

    switch (nLogLevel)
    {
        case LOG_LEVEL_INFO:
            LogInfo(fullMessage);
            break;
        case LOG_LEVEL_WARN:
            LogWarning(fullMessage);
            break;
        case LOG_LEVEL_FATAL:
            LogFatal(fullMessage);
            break;
    }

    return FALSE;
}

int AssertIntNotEqual(int nNotExpected, int nValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN)
{
    if (nNotExpected != nValue)
        return TRUE;
    string fullMessage = "expected value different than " + IntToString(nNotExpected) + ", got " + IntToString(nValue);
    if (sErrorMsg != "")
        fullMessage += " (" + sErrorMsg + ")";

    switch (nLogLevel)
    {
        case LOG_LEVEL_INFO:
            LogInfo(fullMessage);
            break;
        case LOG_LEVEL_WARN:
            LogWarning(fullMessage);
            break;
        case LOG_LEVEL_FATAL:
            LogFatal(fullMessage);
            break;
    }

    return FALSE;
}

int AssertIntGreater(int nThreshold, int nValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN)
{
    if (nValue > nThreshold)
        return TRUE;
    string fullMessage = "expected value greater than " + IntToString(nThreshold) + ", got " + IntToString(nValue);
    if (sErrorMsg != "")
        fullMessage += " (" + sErrorMsg + ")";

    switch (nLogLevel)
    {
        case LOG_LEVEL_INFO:
            LogInfo(fullMessage);
            break;
        case LOG_LEVEL_WARN:
            LogWarning(fullMessage);
            break;
        case LOG_LEVEL_FATAL:
            LogFatal(fullMessage);
            break;
    }

    return FALSE;
}

int AssertIntNotLower(int nThreshold, int nValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN)
{
    if (nValue >= nThreshold)
        return TRUE;
    string fullMessage = "expected value not lower than " + IntToString(nThreshold) + ", got " + IntToString(nValue);
    if (sErrorMsg != "")
        fullMessage += " (" + sErrorMsg + ")";

    switch (nLogLevel)
    {
        case LOG_LEVEL_INFO:
            LogInfo(fullMessage);
            break;
        case LOG_LEVEL_WARN:
            LogWarning(fullMessage);
            break;
        case LOG_LEVEL_FATAL:
            LogFatal(fullMessage);
            break;
    }

    return FALSE;
}

int AssertIntLower(int nThreshold, int nValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN)
{
    if (nValue < nThreshold)
        return TRUE;
    string fullMessage = "expected value lower than " + IntToString(nThreshold) + ", got " + IntToString(nValue);
    if (sErrorMsg != "")
        fullMessage += " (" + sErrorMsg + ")";

    switch (nLogLevel)
    {
        case LOG_LEVEL_INFO:
            LogInfo(fullMessage);
            break;
        case LOG_LEVEL_WARN:
            LogWarning(fullMessage);
            break;
        case LOG_LEVEL_FATAL:
            LogFatal(fullMessage);
            break;
    }

    return FALSE;
}

int AssertIntNotGreater(int nThreshold, int nValue, string sErrorMsg="", int nLogLevel=LOG_LEVEL_WARN)
{
    if (nValue <= nThreshold)
        return TRUE;
    string fullMessage = "expected value not greater than " + IntToString(nThreshold) + ", got " + IntToString(nValue);
    if (sErrorMsg != "")
        fullMessage += " (" + sErrorMsg + ")";

    switch (nLogLevel)
    {
        case LOG_LEVEL_INFO:
            LogInfo(fullMessage);
            break;
        case LOG_LEVEL_WARN:
            LogWarning(fullMessage);
            break;
        case LOG_LEVEL_FATAL:
            LogFatal(fullMessage);
            break;
    }

    return FALSE;
}

//Small modification of StringReplace function from x3_inc_string
string _StringReplaceFirst(string sSource, string sFind, string sReplace)
{
    int nFindLength = GetStringLength(sFind);
    int nPosition = 0;
    string sRetVal = "";

    // Locate all occurences of sFind.
    int nFound = FindSubString(sSource, sFind);
    if ( nFound >= 0 )
    {
        // Build the return string, replacing this occurence of sFind with sReplace.
        sRetVal += GetSubString(sSource, nPosition, nFound - nPosition) + sReplace;
        nPosition = nFound + nFindLength;
        nFound = FindSubString(sSource, sFind, nPosition);
    }
    // Tack on the end of sSource and return.
    return sRetVal + GetStringRight(sSource, GetStringLength(sSource) - nPosition);
}

void LogInfo(string sMessage, int nNum1=0, int nNum2=0, int nNum3=0, int nNum4=0)
{
    if (GetSwitch(MODULE_SWITCH_LOGGING) == FALSE)
        return;

    string numPlaceholder = "%n";
    sMessage = _StringReplaceFirst(sMessage, numPlaceholder, IntToString(nNum1));
    sMessage = _StringReplaceFirst(sMessage, numPlaceholder, IntToString(nNum2));
    sMessage = _StringReplaceFirst(sMessage, numPlaceholder, IntToString(nNum3));
    sMessage = _StringReplaceFirst(sMessage, numPlaceholder, IntToString(nNum4));

    if (GetSwitch(MODULE_SWITCH_DEBUG_MODE) == TRUE)
    {
        string coloredMessage = ColorString("INFO: ", 255, 255, 255) + sMessage;
        SendMessageToPC(GetFirstPC(), coloredMessage);
    }

    sMessage = "INFO: " + sMessage;
    WriteTimestampedLogEntry(sMessage);
}

void LogWarning(string sMessage, int nNum1=0, int nNum2=0, int nNum3=0, int nNum4=0)
{
    if (GetSwitch(MODULE_SWITCH_LOGGING) == FALSE)
        return;

    string numPlaceholder = "%n";
    sMessage = _StringReplaceFirst(sMessage, numPlaceholder, IntToString(nNum1));
    sMessage = _StringReplaceFirst(sMessage, numPlaceholder, IntToString(nNum2));
    sMessage = _StringReplaceFirst(sMessage, numPlaceholder, IntToString(nNum3));
    sMessage = _StringReplaceFirst(sMessage, numPlaceholder, IntToString(nNum4));

    if (GetSwitch(MODULE_SWITCH_DEBUG_MODE) == TRUE || GetSwitch(MODULE_SWITCH_PRINT_WARN) == TRUE)
    {
        string coloredMessage = ColorString("WARNING: ", 255, 102, 0) + sMessage;
        SendMessageToPC(GetFirstPC(), coloredMessage);
    }

    sMessage = "WARNING: " + sMessage;
    WriteTimestampedLogEntry(sMessage);
}

void LogFatal(string sMessage, int nNum1=0, int nNum2=0, int nNum3=0, int nNum4=0)
{
    if (GetSwitch(MODULE_SWITCH_LOGGING) == FALSE)
        return;

    string numPlaceholder = "%n";
    sMessage = _StringReplaceFirst(sMessage, numPlaceholder, IntToString(nNum1));
    sMessage = _StringReplaceFirst(sMessage, numPlaceholder, IntToString(nNum2));
    sMessage = _StringReplaceFirst(sMessage, numPlaceholder, IntToString(nNum3));
    sMessage = _StringReplaceFirst(sMessage, numPlaceholder, IntToString(nNum4));


    if (GetSwitch(MODULE_SWITCH_DEBUG_MODE) == TRUE || GetSwitch(MODULE_SWITCH_PRINT_FATAL) == TRUE)
    {
        string coloredMessage = ColorString("FATAL: ", 255, 0, 0) + sMessage;
        SendMessageToPC(GetFirstPC(), coloredMessage);
    }

    sMessage = "FATAL: " + sMessage;
    WriteTimestampedLogEntry(sMessage);
}

void Breakpoint(int nCurrentLoopIteration=0, int nIterationOfBreak=0, string sBreakpointTag="")
{
    if (sBreakpointTag != "")
    {
        if (GetLocalInt(GetModule(), sBreakpointTag+"_breakpoint") == TRUE)
            return;
        SetLocalInt(GetModule(), sBreakpointTag+"_breakpoint", TRUE);
    }

    if (nIterationOfBreak != nCurrentLoopIteration)
        return;

    SpawnScriptDebugger();
}