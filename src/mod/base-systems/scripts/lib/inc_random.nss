#include "inc_debug"
#include "inc_common"

//Sets a seed for the RNG of a given name (which acts as its descriptor);
//Sets a random seed if nSeed=0
int SetRandomSeed(int nSeed=0, string sSeedName="default");

//Generates any integer, including negative numbers,
//from the RNG of the provided seed name
int RandomNextAny(string sSeedName="default");

//Generates an integer from the range of [0,nMaxInteger)
//from the RNG of the provided seed name
int RandomNext(int nMaxInteger, string sSeedName="default");

// Converts a string to text so that a text value can be used in lieu of a more difficult to remember number.
// A specific string will always generate the same number.
// Returns 0 on error (if an unsupported character was provided)
int GetSeedFromString(string sText);

int _RandomSeed()
{
    int windowsRandMax = 32768; //Max integer returned by rand() (and also NWScript Random()) is 32767, so this function is needed to unify Linux and Windows behavior
    int seed = Random(windowsRandMax) << 16 | Random(windowsRandMax) << 1 | Random(2);
    return seed;
}

int SetRandomSeed(int nSeed=0, string sSeedName="default")
{
    if (nSeed == 0)
        nSeed = _RandomSeed();
    sSeedName = "_custom_seed_" + sSeedName;
    SetLocalInt(GetModule(), sSeedName, nSeed);
    LogInfo("Set seed named '" + sSeedName + "' to " + IntToString(nSeed));
    return nSeed;
}

int RandomNextAny(string sSeedName="default")
{
    sSeedName = "_custom_seed_" + sSeedName;
    int seed = GetLocalInt(GetModule(), sSeedName);
    seed = (seed * 279470273) % 429496729;
    SetLocalInt(GetModule(), sSeedName, seed);
    return seed;
}

int RandomNext(int nMaxInteger, string sSeedName="default")
{
    int random = RandomNextAny(sSeedName);
    int result = abs(random % nMaxInteger);
    return result;
}

int _IndexToPrimeNumber(int index)
{
    switch (index)
    {
        case 0:
            return 2;
        case 1:
            return 3;
        case 2:
            return 5;
        case 3:
            return 7;
        case 4:
            return 11;
        case 5:
            return 13;
        case 6:
            return 17;
        case 7:
            return 19;
        case 8:
            return 23;
        case 9:
            return 29;
        case 10:
            return 31;
        case 11:
            return 37;
        case 12:
            return 41;
        case 13:
            return 43;
        case 14:
            return 47;
        case 15:
            return 53;
        case 16:
            return 59;
        case 17:
            return 61;
        case 18:
            return 67;
        case 19:
            return 71;
        case 20:
            return 73;
        case 21:
            return 79;
        case 22:
            return 83;
        case 23:
            return 89;
        case 24:
            return 97;
        case 25:
            return 101;
        case 26:
            return 103;
        case 27:
            return 107;
        case 28:
            return 109;
        case 29:
            return 113;
        case 30:
            return 127;
        case 31:
            return 131;
        case 32:
            return 137;
        case 33:
            return 139;
        case 34:
            return 149;
        case 35:
            return 151;
        case 36:
            return 157;
        case 37:
            return 163;
        case 38:
            return 167;
        case 39:
            return 173;
        case 40:
            return 179;
        case 41:
            return 181;
        case 42:
            return 191;
        case 43:
            return 193;
        case 44:
            return 197;
        case 45:
            return 199;
        case 46:
            return 211;
        case 47:
            return 223;
        case 48:
            return 227;
        case 49:
            return 229;
        case 50:
            return 233;
        case 51:
            return 239;
        case 52:
            return 241;
        case 53:
            return 251;
        case 54:
            return 257;
        case 55:
            return 263;
        case 56:
            return 269;
        case 57:
            return 271;
        case 58:
            return 277;
        case 59:
            return 281;
        case 60:
            return 283;
        case 61:
            return 293;
        case 62:
            return 307;
        case 63:
            return 311;
        case 64:
            return 313;
        case 65:
            return 317;
        case 66:
            return 331;
        case 67:
            return 337;
        case 68:
            return 347;
        case 69:
            return 349;
        case 70:
            return 353;
        case 71:
            return 359;
        case 72:
            return 367;
        case 73:
            return 373;
        case 74:
            return 379;
        case 75:
            return 383;
        case 76:
            return 389;
        case 77:
            return 397;
        case 78:
            return 401;
        case 79:
            return 409;
        case 80:
            return 419;
        case 81:
            return 421;
        case 82:
            return 431;
        case 83:
            return 433;
    }
    return 0;
}

int _CharToInt(string sChar, int nPosition)
{
    string chars = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTWXYZ -_=+!@#$%^&*()[]{}\\|\"';:`~,<.>/?";
    int length = GetStringLength(chars);
    int index = FindSubString(chars, sChar);
    if (index == -1)
        return 0;

    return nPosition + _IndexToPrimeNumber(index);
}

int _SanitizeZero(int nNum)
{
    if (nNum == 0)
        return 1;
    return nNum;
}

int GetSeedFromString(string sText)
{
    int length = GetStringLength(sText);
    string currentNumber = "";
    int seed = 1;
    int i;
    for (i = 0; i < length; i++)
    {
        string char = GetSubString(sText, i, 1);
        string nextChar = GetSubString(sText, i+1, 1);
        if (GetIsStringInt(char) || (char == "-" && currentNumber == "" && GetIsStringInt(nextChar)))
        {
            currentNumber += char;
            //integer can at most have 10 digits, so if we have more than that, treat them as separate numbers to multiply
            if (GetStringLength(currentNumber) == 10 || (GetStringLength(currentNumber) == 11 && GetStringLeft(currentNumber, 1) == "-"))
            {
                seed *= _SanitizeZero(StringToInt(currentNumber));
                //just in case an overflow can result in a zero
                if (seed == 0)
                    seed = 1;
                currentNumber = "";
            }
        }
        else
        {
            if (currentNumber != "")
            {
                seed *= _SanitizeZero(StringToInt(currentNumber));
                if (seed == 0)
                    seed = 1;
                currentNumber = "";
            }
            int charValue = _CharToInt(char, i);
            if (charValue == 0)
                return 0;
            seed *= charValue;
            if (seed == 0)
                seed = 1;
        }
    }
    if (currentNumber != "")
    {
        seed *= _SanitizeZero(StringToInt(currentNumber));
        if (seed == 0)
            seed = 1;
        currentNumber = "";
    }
    return seed;
}