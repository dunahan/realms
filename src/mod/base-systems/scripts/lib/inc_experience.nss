// Library containing exp-related functions

// Returns the minimum experience points amount required to reach nLevel
int GetExperienceForLevel(int nLevel);

// Returns the level reachable with nExp experience points
int GetLevelForExperience(int nExp);

// Returns the experience required to gain on nCurrentLevel to level-up
int GetExperienceToNextLevel(int nCurrentLevel);

// Returns the percentage experience penalty for multiclassing that oCreature is subject to
int GetExperiencePenalty(object oCreature);

// Returns nOriginalExp after decreasing it by multiclassing penalty that oCreature is subject to
int GetPenaltyAdjustedExperience(object oCreature, int nOriginalExp);

// Returns the favored class of a creature
int GetFavoredClass(object oCreature);

// Returns TRUE if the provided class is a prestige class and FALSE if it is a base class
int GetIsClassPrestige(int nClass);

int GetExperienceForLevel(int nLevel)
{
    if (nLevel < 1)
        return 0;
    int row = nLevel - 1;
    string entry = Get2DAString("exptable", "XP", row);
    return StringToInt(entry);
}

int GetLevelForExperience(int nExp)
{
    int i;
    string s2da = "exptable";
    for (i = 0; TRUE; i++)
    {
        string entry = Get2DAString(s2da, "XP", i);
        int xp = StringToInt(entry);
        if (xp > nExp)
            return StringToInt(Get2DAString(s2da, "Level", i-1));
    }
    return 0;
}

int GetExperienceToNextLevel(int nCurrentLevel)
{
    int current = GetExperienceForLevel(nCurrentLevel);
    int next = GetExperienceForLevel(nCurrentLevel+1);
    return (next-current);
}

int _GetHighestLevelledClassPosition(object oCreature, int nHighestNonFavored=FALSE)
{
    int highestPosition = 0;
    int highestLevel = 0;
    int favoredClass = GetFavoredClass(oCreature);
    int i;
    for (i = 1; i < 4; i++)
    {
        int level = GetLevelByPosition(i, oCreature);
        if (level == 0)
            break;
        if (level > highestLevel && (!nHighestNonFavored || GetClassByPosition(i, oCreature) != favoredClass))
        {
            highestLevel = level;
            highestPosition = i;
        }
    }
    return highestPosition;
}

int GetFavoredClass(object oCreature)
{
    int race = GetRacialType(oCreature);
    string entry = Get2DAString("racialtypes", "Favored", race);
    if (GetStringLeft(entry, 1) != "*")
        return StringToInt(entry);

    int highestLevelPosition = _GetHighestLevelledClassPosition(oCreature);
    return GetClassByPosition(highestLevelPosition, oCreature);
}

int GetIsClassPrestige(int nClass)
{
    string entry = Get2DAString("classes", "PreReqTable", nClass);
    if (GetStringLeft(entry, 1) == "*")
        return FALSE;
    return TRUE;
}

int _GetPenaltyForClassInPosition(int nPosition, object oCreature, int highestClassLevel, int highestClass, int highestNonFavoredClassLevel, int highestNonFavoredClass)
{
    int class = GetClassByPosition(nPosition, oCreature);
    if (class == CLASS_TYPE_INVALID)
        return 0;
    int level = GetLevelByPosition(nPosition, oCreature);
    int prestige = GetIsClassPrestige(class);
    int favoredClass = GetFavoredClass(oCreature);

    //Penalty of the highest-levelled class is always 0%
    if (class == highestClass)
        return 0;

    //Penalty of the favored class is always 0%
    if (class == favoredClass)
        return 0;

    //Penalty of a prestige class at level 5 or above is always 0%
    if (prestige && level >= 5)
        return 0;

    //Penalty of a base class at or above level 5 is calculated according to vanilla multiclass penalty rules
    if (!prestige && level >=5)
    {
        if (level+1 < highestNonFavoredClassLevel)
            return 20;
        return 0;
    }

    //Penalty of a class below level 5 is calculated as follows
    int penalty = (highestClassLevel - level) * 10 - 60;
    if (penalty < 0)
        penalty = 0;
    return penalty;
}

int GetExperiencePenalty(object oCreature)
{
    int highestLevelPosition = _GetHighestLevelledClassPosition(oCreature);
    int highestClassLevel = GetLevelByPosition(highestLevelPosition, oCreature);
    int highestClass = GetClassByPosition(highestLevelPosition, oCreature);

    int highestLevelNonFavoredPosition = _GetHighestLevelledClassPosition(oCreature, TRUE);
    int highestNonFavoredClassLevel = highestLevelNonFavoredPosition == 0 ? 0 : GetLevelByPosition(highestLevelNonFavoredPosition, oCreature);
    int highestNonFavoredClass = highestLevelNonFavoredPosition == 0 ? CLASS_TYPE_INVALID : GetClassByPosition(highestLevelNonFavoredPosition, oCreature);

    int percentPenalty1 = _GetPenaltyForClassInPosition(1, oCreature, highestClassLevel, highestClass, highestNonFavoredClassLevel, highestNonFavoredClass);
    int percentPenalty2 = _GetPenaltyForClassInPosition(2, oCreature, highestClassLevel, highestClass, highestNonFavoredClassLevel, highestNonFavoredClass);
    int percentPenalty3 = _GetPenaltyForClassInPosition(3, oCreature, highestClassLevel, highestClass, highestNonFavoredClassLevel, highestNonFavoredClass);

    int percentPenaltyTotal = percentPenalty1 + percentPenalty2 + percentPenalty3;
    if (percentPenaltyTotal > 50)
        percentPenaltyTotal = 50;

    return percentPenaltyTotal;
}

int GetPenaltyAdjustedExperience(object oCreature, int nOriginalExp)
{
    int penalty = GetExperiencePenalty(oCreature);
    float penaltyMultiplier = 1.0f - (IntToFloat(penalty) / 100);
    float result = nOriginalExp * penaltyMultiplier;
    return FloatToInt(result);
}
