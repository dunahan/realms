#include "inc_darkareas"

void main()
{
    object PC = GetPCItemLastUnequippedBy();
    object item = GetPCItemLastUnequipped();
    RecalculateEquippedLightSources(PC, item);
    DelayCommand(0.1, RecalculateDarkAreaPenalty(PC));
}