///////////////////////
//  Chat processing  //
///////////////////////

// Executes an appropriate script based on a chat command received.
// Returns TRUE if message was identified as a chat command and FALSE otherwise.
// This function should only be called in the OnChat event.
// Example: a player sending a "/help" chat command will fire a "cmd_help" script.
int RunChatCommandScript(string scriptPrefix="cmd_", string commandPrefix="/");

// Returns chat parameters string of a last chat command.
// Returns an empty string if no parameters were supplied. Should be used in a chat command script.
// Example: a "/help some topic" command will fire (by default) a "cmd_help" script
// and a GetChatCommandParameters() call in that script will return "some topic".
string GetChatCommandParameters();

// A necessary function to be called at the start of the OnChat event
// to enable GetLastMessage.
void RegisterMessage();

// Returns the last chat message sent by a PC.
// This requires running a RegisterMessage function in the OnChat event.
string GetLastMessage(object oPC);

// Clears last message sent by a PC.
void ClearLastMessage(object oPC);


string LAST_MSG = "LAST_MSG";

int RunChatCommandScript(string scriptPrefix="cmd_", string commandPrefix="/")
{
    string command = GetPCChatMessage();
    int prefixLength = GetStringLength(commandPrefix);
    if (GetStringLeft(command, prefixLength) != commandPrefix)
        return FALSE;

    command = GetStringRight(command, GetStringLength(command)-prefixLength);
    int spaceIndex = FindSubString(command, " ");
    string parameters = spaceIndex == -1 ? "" : GetSubString(command, spaceIndex+1, 9999);
    SetLocalString(GetModule(), "CHAT_CMD_PARAMS", parameters);
    if (spaceIndex != -1)
        command = GetStringLeft(command, spaceIndex);
    string commandScript = scriptPrefix + command;
    ExecuteScript(commandScript, GetPCChatSpeaker());
    return TRUE;
}

string GetChatCommandParameters()
{
    return GetLocalString(GetModule(), "CHAT_CMD_PARAMS");
}

void RegisterMessage()
{
    object oPC = GetPCChatSpeaker();
    string message = GetPCChatMessage();
    SetLocalString(oPC, LAST_MSG, message);
}

string GetLastMessage(object oPC)
{
    return GetLocalString(oPC, LAST_MSG);
}

void ClearLastMessage(object oPC)
{
    DeleteLocalString(oPC, LAST_MSG);
}
