#include "inc_random"

// Trap functions library

const int TRAP_POWER_MINOR = 1;
const int TRAP_POWER_AVERAGE = 2;
const int TRAP_POWER_STRONG = 3;
const int TRAP_POWER_DEADLY = 4;
const int TRAP_POWER_EPIC = 5;
const int TRAP_TYPE_FIRE = 1;
const int TRAP_TYPE_FROST = 2;
const int TRAP_TYPE_ELECTRICAL = 3;
const int TRAP_TYPE_SONIC = 4;
const int TRAP_TYPE_ACID = 5;
const int TRAP_TYPE_NEGATIVE = 6;
const int TRAP_TYPE_HOLY = 7;
const int TRAP_TYPE_SPIKE = 8;
const int TRAP_TYPE_TANGLE = 9;
const int TRAP_TYPE_ACID_SPLASH = 10;
const int TRAP_TYPE_GAS = 11;

// Returns a trap constant based on TRAP_POWER_* and TRAP_TYPE_* constants from inc_traps;
// returns -1 if no trap for a given combination exists
int GetTrapConstant(int nTrapPower, int nTrapType);

int GetTrapConstant(int nTrapPower, int nTrapType)
{
    if (nTrapPower == TRAP_POWER_MINOR)
    {
        switch (nTrapType)
        {
            case TRAP_TYPE_ACID:
                return TRAP_BASE_TYPE_MINOR_ACID;
            case TRAP_TYPE_FIRE:
                return TRAP_BASE_TYPE_MINOR_FIRE;
            case TRAP_TYPE_FROST:
                return TRAP_BASE_TYPE_MINOR_FROST;
            case TRAP_TYPE_ELECTRICAL:
                return TRAP_BASE_TYPE_MINOR_ELECTRICAL;
            case TRAP_TYPE_SONIC:
                return TRAP_BASE_TYPE_MINOR_SONIC;
            case TRAP_TYPE_NEGATIVE:
                return TRAP_BASE_TYPE_MINOR_NEGATIVE;
            case TRAP_TYPE_HOLY:
                return TRAP_BASE_TYPE_MINOR_HOLY;
            case TRAP_TYPE_SPIKE:
                return TRAP_BASE_TYPE_MINOR_SPIKE;
            case TRAP_TYPE_TANGLE:
                return TRAP_BASE_TYPE_MINOR_TANGLE;
            case TRAP_TYPE_ACID_SPLASH:
                return TRAP_BASE_TYPE_MINOR_ACID_SPLASH;
            case TRAP_TYPE_GAS:
                return TRAP_BASE_TYPE_MINOR_GAS;
        }
    }
    else if (nTrapPower == TRAP_POWER_AVERAGE)
    {
        switch (nTrapType)
        {
            case TRAP_TYPE_ACID:
                return TRAP_BASE_TYPE_AVERAGE_ACID;
            case TRAP_TYPE_FIRE:
                return TRAP_BASE_TYPE_AVERAGE_FIRE;
            case TRAP_TYPE_FROST:
                return TRAP_BASE_TYPE_AVERAGE_FROST;
            case TRAP_TYPE_ELECTRICAL:
                return TRAP_BASE_TYPE_AVERAGE_ELECTRICAL;
            case TRAP_TYPE_SONIC:
                return TRAP_BASE_TYPE_AVERAGE_SONIC;
            case TRAP_TYPE_NEGATIVE:
                return TRAP_BASE_TYPE_AVERAGE_NEGATIVE;
            case TRAP_TYPE_HOLY:
                return TRAP_BASE_TYPE_AVERAGE_HOLY;
            case TRAP_TYPE_SPIKE:
                return TRAP_BASE_TYPE_AVERAGE_SPIKE;
            case TRAP_TYPE_TANGLE:
                return TRAP_BASE_TYPE_AVERAGE_TANGLE;
            case TRAP_TYPE_ACID_SPLASH:
                return TRAP_BASE_TYPE_AVERAGE_ACID_SPLASH;
            case TRAP_TYPE_GAS:
                return TRAP_BASE_TYPE_AVERAGE_GAS;
        }
    }
    else if (nTrapPower == TRAP_POWER_STRONG)
    {
        switch (nTrapType)
        {
            case TRAP_TYPE_ACID:
                return TRAP_BASE_TYPE_STRONG_ACID;
            case TRAP_TYPE_FIRE:
                return TRAP_BASE_TYPE_STRONG_FIRE;
            case TRAP_TYPE_FROST:
                return TRAP_BASE_TYPE_STRONG_FROST;
            case TRAP_TYPE_ELECTRICAL:
                return TRAP_BASE_TYPE_STRONG_ELECTRICAL;
            case TRAP_TYPE_SONIC:
                return TRAP_BASE_TYPE_STRONG_SONIC;
            case TRAP_TYPE_NEGATIVE:
                return TRAP_BASE_TYPE_STRONG_NEGATIVE;
            case TRAP_TYPE_HOLY:
                return TRAP_BASE_TYPE_STRONG_HOLY;
            case TRAP_TYPE_SPIKE:
                return TRAP_BASE_TYPE_STRONG_SPIKE;
            case TRAP_TYPE_TANGLE:
                return TRAP_BASE_TYPE_STRONG_TANGLE;
            case TRAP_TYPE_ACID_SPLASH:
                return TRAP_BASE_TYPE_STRONG_ACID_SPLASH;
            case TRAP_TYPE_GAS:
                return TRAP_BASE_TYPE_STRONG_GAS;
        }
    }
    else if (nTrapPower == TRAP_POWER_DEADLY)
    {
        switch (nTrapType)
        {
            case TRAP_TYPE_ACID:
                return TRAP_BASE_TYPE_DEADLY_ACID;
            case TRAP_TYPE_FIRE:
                return TRAP_BASE_TYPE_DEADLY_FIRE;
            case TRAP_TYPE_FROST:
                return TRAP_BASE_TYPE_DEADLY_FROST;
            case TRAP_TYPE_ELECTRICAL:
                return TRAP_BASE_TYPE_DEADLY_ELECTRICAL;
            case TRAP_TYPE_SONIC:
                return TRAP_BASE_TYPE_DEADLY_SONIC;
            case TRAP_TYPE_NEGATIVE:
                return TRAP_BASE_TYPE_DEADLY_NEGATIVE;
            case TRAP_TYPE_HOLY:
                return TRAP_BASE_TYPE_DEADLY_HOLY;
            case TRAP_TYPE_SPIKE:
                return TRAP_BASE_TYPE_DEADLY_SPIKE;
            case TRAP_TYPE_TANGLE:
                return TRAP_BASE_TYPE_DEADLY_TANGLE;
            case TRAP_TYPE_ACID_SPLASH:
                return TRAP_BASE_TYPE_DEADLY_ACID_SPLASH;
            case TRAP_TYPE_GAS:
                return TRAP_BASE_TYPE_DEADLY_GAS;
        }
    }
    else if (nTrapPower == TRAP_POWER_EPIC)
    {
        switch (nTrapType)
        {
            case TRAP_TYPE_ACID:
                return -1;
            case TRAP_TYPE_FIRE:
                return TRAP_BASE_TYPE_EPIC_FIRE;
            case TRAP_TYPE_FROST:
                return TRAP_BASE_TYPE_EPIC_FROST;
            case TRAP_TYPE_ELECTRICAL:
                return TRAP_BASE_TYPE_EPIC_ELECTRICAL;
            case TRAP_TYPE_SONIC:
                return TRAP_BASE_TYPE_EPIC_SONIC;
            case TRAP_TYPE_NEGATIVE:
                return -1;
            case TRAP_TYPE_HOLY:
                return -1;
            case TRAP_TYPE_SPIKE:
                return -1;
            case TRAP_TYPE_TANGLE:
                return -1;
            case TRAP_TYPE_ACID_SPLASH:
                return -1;
            case TRAP_TYPE_GAS:
                return -1;
        }
    }
    return -1;
}