// Returns TRUE if the database exists
int GetDatabaseExists(string sDbName);

int GetDatabaseExists(string sDbName)
{
    string sql = "SELECT COUNT(*) FROM sqlite_master WHERE type = 'table' AND name NOT IN ('db', 'meta', 'migrations');";
    sqlquery query = SqlPrepareQueryCampaign(sDbName, sql);
    SqlStep(query);
    int customTables = SqlGetInt(query, 0);
    if (customTables > 0)
        return TRUE;
    DestroyCampaignDatabase(sDbName);
    return FALSE;
}
