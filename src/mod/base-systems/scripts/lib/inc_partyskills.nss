#include "inc_common"
#include "nw_i0_plot"

// This library contains functions for party skill checks

// Performs the skill check for the party member (oTarget or one of their henchmen) with the highest skill rank and returns the result;
// Suggested party skills: persuade, bluff, intimidate, lore, craft weapon, craft armor, craft trap, appraise,
// possibly others in certain situations.
int GetIsPartySkillSuccessful(object oTarget, int nSkill, int nDifficulty);

// Opens a store using the highest party Appraise rank for the Appraise check (including oPC and their henchmen)
void OpenStoreWithPartyAppraise(object oStore, object oPC, int nBonusMarkUp=0, int nBonusMarkDown=0);

int GetIsPartySkillSuccessful(object oTarget, int nSkill, int nDifficulty)
{
    object finalTarget = oTarget;
    int highestSkillRank = GetSkillRank(nSkill, oTarget);
    int i = 1;
    object hench = GetHenchman(oTarget, i);
    while (GetIsObjectValid(hench))
    {
        int henchRank = GetSkillRank(nSkill, hench);
        if (henchRank > highestSkillRank)
        {
            highestSkillRank = henchRank;
            finalTarget = hench;
        }
        hench = GetHenchman(oTarget, ++i);
    }
    return GetIsSkillSuccessful(finalTarget, nSkill, nDifficulty);
}

void OpenStoreWithPartyAppraise(object oStore, object oPC, int nBonusMarkUp=0, int nBonusMarkDown=0)
{
    int pcRank = GetSkillRank(SKILL_APPRAISE, oPC);
    int highestSkillRank = pcRank;
    int i = 1;
    object hench = GetHenchman(oPC, i);
    while (GetIsObjectValid(hench))
    {
        int henchRank = GetSkillRank(SKILL_APPRAISE, hench);
        if (henchRank > highestSkillRank)
            highestSkillRank = henchRank;
        hench = GetHenchman(oPC, ++i);
    }

    if (pcRank == highestSkillRank)
    {
        gplotAppraiseOpenStore(oStore, oPC, nBonusMarkUp, nBonusMarkDown);
        return;
    }

    effect bonus = EffectSkillIncrease(SKILL_APPRAISE, highestSkillRank - pcRank);
    bonus = TagEffect(bonus, "PARTY_APPRAISE_BONUS");
    ApplyEffectToObject(DURATION_TYPE_TEMPORARY, bonus, oPC, 0.5f);
    gplotAppraiseOpenStore(oStore, oPC, nBonusMarkUp, nBonusMarkDown);
    RemoveEffectsByTag(oPC, "PARTY_APPRAISE_BONUS");
}