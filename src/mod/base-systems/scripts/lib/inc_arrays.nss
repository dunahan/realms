// ----------------------
// -- Universal arrays --
// --    by Taro94     --
// ----------------------

// This library is an implementation of pseudo-arrays in NWScript
// These can successfully be used as lists if desired.
// Features:
//  - resizing (deleting and adding elements)
//  - getting array size
//  - support for local and "global" arrays
//  - getting indexes by elements' values
//  - copying arrays
// Global arrays are technically stored locally on the module object,
// but are separate from arrays stored specifically on the module,
// even if they share the same name and type.
// I.e:
// CreateIntArray("arr", 10);
// CreateIntArray("arr", 15, GetModule());
// will create two different arrays,
// even though both will be stored on the module object.

// ---------------
// -- Constants --
// ---------------

const int ARRAY_INDEX_INVALID = -1;

// -------------------------
// -- Function prototypes --
// -------------------------

// Clears an integer array named sName stored on oObject;
// Calling this function is redundant when creating a new array
void ClearIntArray(string sName, object oObject=OBJECT_INVALID);

// Clears a string array named sName stored on oObject;
// Calling this function is redundant when creating a new array
void ClearStringArray(string sName, object oObject=OBJECT_INVALID);

// Clears a float array named sName stored on oObject;
// Calling this function is redundant when creating a new array
void ClearFloatArray(string sName, object oObject=OBJECT_INVALID);

// Clears an object array named sName stored on oObject;
// Calling this function is redundant when creating a new array
void ClearObjectArray(string sName, object oObject=OBJECT_INVALID);

// Clears a location array named sName stored on oObject;
// Calling this function is redundant when creating a new array
void ClearLocationArray(string sName, object oObject=OBJECT_INVALID);

// Creates an integer array named sName of size equal to nArraySize stored on oObject;
// The array will be global if oObject is invalid;
// A global array is technically stored on the module object, but remains separate
// from an array created specifically on the module object
void CreateIntArray(string sName, int nArraySize=0, object oObject=OBJECT_INVALID);

// Creates a string array named sName of size equal to nArraySize stored on oObject;
// The array will be global if oObject is invalid;
// A global array is technically stored on the module object, but remains separate
// from an array created specifically on the module object
void CreateStringArray(string sName, int nArraySize=0, object oObject=OBJECT_INVALID);

// Creates a float array named sName of size equal to nArraySize stored on oObject;
// The array will be global if oObject is invalid;
// A global array is technically stored on the module object, but remains separate
// from an array created specifically on the module object
void CreateFloatArray(string sName, int nArraySize=0, object oObject=OBJECT_INVALID);

// Creates an object array named sName of size equal to nArraySize stored on oObject;
// The array will be global if oObject is invalid;
// A global array is technically stored on the module object, but remains separate
// from an array created specifically on the module object
void CreateObjectArray(string sName, int nArraySize=0, object oObject=OBJECT_INVALID);

// Creates a location array named sName of size equal to nArraySize stored on oObject;
// The array will be global if oObject is invalid;
// A global array is technically stored on the module object, but remains separate
// from an array created specifically on the module object
void CreateLocationArray(string sName, int nArraySize=0, object oObject=OBJECT_INVALID);

// Returns size of an integer array named sName stored on oObject
int GetIntArraySize(string sName, object oObject=OBJECT_INVALID);

// Returns size of a string array named sName stored on oObject
int GetStringArraySize(string sName, object oObject=OBJECT_INVALID);

// Returns size of a float array named sName stored on oObject
int GetFloatArraySize(string sName, object oObject=OBJECT_INVALID);

// Returns size of an object array named sName stored on oObject
int GetObjectArraySize(string sName, object oObject=OBJECT_INVALID);

// Returns size of a location array named sName stored on oObject
int GetLocationArraySize(string sName, object oObject=OBJECT_INVALID);

// Sets an element at nIndex of an integer array named sName stored on oObject to nValue
void SetIntArrayElement(string sName, int nIndex, int nValue, object oObject=OBJECT_INVALID);

// Sets an element at nIndex of a string array named sName stored on oObject to nValue
void SetStringArrayElement(string sName, int nIndex, string sValue, object oObject=OBJECT_INVALID);

// Sets an element at nIndex of a float array named sName stored on oObject to nValue
void SetFloatArrayElement(string sName, int nIndex, float fValue, object oObject=OBJECT_INVALID);

// Sets an element at nIndex of an object array named sName stored on oObject to nValue
void SetObjectArrayElement(string sName, int nIndex, object oValue, object oObject=OBJECT_INVALID);

// Sets an element at nIndex of a location array named sName stored on oObject to nValue
void SetLocationArrayElement(string sName, int nIndex, location lValue, object oObject=OBJECT_INVALID);

// Gets an element at nIndex of an integer array named sName stored on oObject
int GetIntArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID);

// Gets an element at nIndex of a string array named sName stored on oObject
string GetStringArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID);

// Gets an element at nIndex of a float array named sName stored on oObject
float GetFloatArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID);

// Gets an element at nIndex of an object array named sName stored on oObject
object GetObjectArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID);

// Gets an element at nIndex of a location array named sName stored on oObject
// Note: this function returns the module's starting location on error
location GetLocationArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID);

// Adds an element to an integer array named sName stored on oObject;
// If nBeginning is TRUE, the element is added at the beginning, otherwise it is added at the end
void AddIntArrayElement(string sName, int nValue, int nBeginning=FALSE, object oObject=OBJECT_INVALID);

// Adds an element to a string array named sName stored on oObject;
// If nBeginning is TRUE, the element is added at the beginning, otherwise it is added at the end
void AddStringArrayElement(string sName, string sValue, int nBeginning=FALSE, object oObject=OBJECT_INVALID);

// Adds an element to a float array named sName stored on oObject;
// If nBeginning is TRUE, the element is added at the beginning, otherwise it is added at the end
void AddFloatArrayElement(string sName, float fValue, int nBeginning=FALSE, object oObject=OBJECT_INVALID);

// Adds an element to an object array named sName stored on oObject;
// If nBeginning is TRUE, the element is added at the beginning, otherwise it is added at the end
void AddObjectArrayElement(string sName, object oValue, int nBeginning=FALSE, object oObject=OBJECT_INVALID);

// Adds an element to a location array named sName stored on oObject;
// If nBeginning is TRUE, the element is added at the beginning, otherwise it is added at the end
void AddLocationArrayElement(string sName, location lValue, int nBeginning=FALSE, object oObject=OBJECT_INVALID);

// Deletes an element at nIndex of an integer array named sName stored on oObject;
// The array will be resized upon element deletion
void DeleteIntArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID);

// Deletes an element at nIndex of a string array named sName stored on oObject;
// The array will be resized upon element deletion
void DeleteStringArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID);

// Deletes an element at nIndex of a float array named sName stored on oObject;
// The array will be resized upon element deletion
void DeleteFloatArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID);

// Deletes an element at nIndex of an object array named sName stored on oObject;
// The array will be resized upon element deletion
void DeleteObjectArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID);

// Deletes an element at nIndex of a location array named sName stored on oObject;
// The array will be resized upon element deletion
void DeleteLocationArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID);

// Creates a copy named oDestName on oDestObject of an integer array named sSourceName on oSourceObject
void CopyIntArray(string sSourceName, string sDestName, object oSourceObject=OBJECT_INVALID, object oDestObject=OBJECT_INVALID);

// Creates a copy named oDestName on oDestObject of a string array named sSourceName on oSourceObject
void CopyStringArray(string sSourceName, string sDestName, object oSourceObject=OBJECT_INVALID, object oDestObject=OBJECT_INVALID);

// Creates a copy named oDestName on oDestObject of a float array named sSourceName on oSourceObject
void CopyFloatArray(string sSourceName, string sDestName, object oSourceObject=OBJECT_INVALID, object oDestObject=OBJECT_INVALID);

// Creates a copy named oDestName on oDestObject of an object array named sSourceName on oSourceObject
void CopyObjectArray(string sSourceName, string sDestName, object oSourceObject=OBJECT_INVALID, object oDestObject=OBJECT_INVALID);

// Creates a copy named oDestName on oDestObject of a location array named sSourceName on oSourceObject
void CopyLocationArray(string sSourceName, string sDestName, object oSourceObject=OBJECT_INVALID, object oDestObject=OBJECT_INVALID);

// Returns the index of nValue stored in an integer array named sName stored on oObject;
// nOccurence equal to 1 will return the index of the first occurence of the specified element in the array,
// nOccurence equal to 2 will return the index of the second occurence, etc;
// Returns ARRAY_INDEX_INVALID if the array does not contain the specified element or if nOccurence is lower than 1
int GetIntArrayIndexByValue(string sName, int nValue, int nOccurence=1, object oObject=OBJECT_INVALID);

// Returns the index of sValue stored in a string array named sName stored on oObject;
// nOccurence equal to 1 will return the index of the first occurence of the specified element in the array,
// nOccurence equal to 2 will return the index of the second occurence, etc;
// Returns ARRAY_INDEX_INVALID if the array does not contain the specified element or if nOccurence is lower than 1
int GetStringArrayIndexByValue(string sName, string sValue, int nOccurence=1, object oObject=OBJECT_INVALID);

// Returns the index of fValue stored in a float array named sName stored on oObject;
// nOccurence equal to 1 will return the index of the first occurence of the specified element in the array,
// nOccurence equal to 2 will return the index of the second occurence, etc;
// Returns ARRAY_INDEX_INVALID if the array does not contain the specified element or if nOccurence is lower than 1
int GetFloatArrayIndexByValue(string sName, float fValue, int nOccurence=1, object oObject=OBJECT_INVALID);

// Returns the index of oValue stored in an object array named sName stored on oObject;
// nOccurence equal to 1 will return the index of the first occurence of the specified element in the array,
// nOccurence equal to 2 will return the index of the second occurence, etc;
// Returns ARRAY_INDEX_INVALID if the array does not contain the specified element or if nOccurence is lower than 1
int GetObjectArrayIndexByValue(string sName, object oValue, int nOccurence=1, object oObject=OBJECT_INVALID);

// Returns the index of lValue stored in a location array named sName stored on oObject;
// nOccurence equal to 1 will return the index of the first occurence of the specified element in the array,
// nOccurence equal to 2 will return the index of the second occurence, etc;
// Returns ARRAY_INDEX_INVALID if the array does not contain the specified element or if nOccurence is lower than 1
int GetLocationArrayIndexByValue(string sName, location lValue, int nOccurence=1, object oObject=OBJECT_INVALID);

// -----------------------------
// -- Function implementation --
// -----------------------------

//Clearing arrays
void ClearIntArray(string sName, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    int i;
    for (i = 0; i < nArraySize; i++)
        DeleteLocalInt(oObject, sName+"[�"+IntToString(i)+"]"+global);
    DeleteLocalInt(oObject, sName+"[��]"+global);
}

void ClearStringArray(string sName, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    int i;
    for (i = 0; i < nArraySize; i++)
        DeleteLocalString(oObject, sName+"[�"+IntToString(i)+"]"+global);
    DeleteLocalInt(oObject, sName+"[��]"+global);
}

void ClearFloatArray(string sName, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    int i;
    for (i = 0; i < nArraySize; i++)
        DeleteLocalFloat(oObject, sName+"[�"+IntToString(i)+"]"+global);
    DeleteLocalInt(oObject, sName+"[��]"+global);
}

void ClearObjectArray(string sName, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    int i;
    for (i = 0; i < nArraySize; i++)
        DeleteLocalObject(oObject, sName+"[�"+IntToString(i)+"]"+global);
    DeleteLocalInt(oObject, sName+"[��]"+global);
}

void ClearLocationArray(string sName, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    int i;
    for (i = 0; i < nArraySize; i++)
        DeleteLocalLocation(oObject, sName+"[�"+IntToString(i)+"]"+global);
    DeleteLocalInt(oObject, sName+"[��]"+global);
}


//Creating arrays
void CreateIntArray(string sName, int nArraySize=0, object oObject=OBJECT_INVALID)
{
    string global = "";
    ClearIntArray(sName, oObject);
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize);
}

void CreateStringArray(string sName, int nArraySize=0, object oObject=OBJECT_INVALID)
{
    string global = "";
    ClearStringArray(sName, oObject);
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize);
}

void CreateFloatArray(string sName, int nArraySize=0, object oObject=OBJECT_INVALID)
{
    string global = "";
    ClearFloatArray(sName, oObject);
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize);
}

void CreateObjectArray(string sName, int nArraySize=0, object oObject=OBJECT_INVALID)
{
    string global = "";
    ClearObjectArray(sName, oObject);
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize);
}

void CreateLocationArray(string sName, int nArraySize=0, object oObject=OBJECT_INVALID)
{
    string global = "";
    ClearLocationArray(sName, oObject);
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize);
}

//Getting size
int GetIntArraySize(string sName, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    return GetLocalInt(oObject, sName+"[��]"+global);
}

int GetStringArraySize(string sName, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    return GetLocalInt(oObject, sName+"[��]"+global);
}

int GetFloatArraySize(string sName, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    return GetLocalInt(oObject, sName+"[��]"+global);
}

int GetObjectArraySize(string sName, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    return GetLocalInt(oObject, sName+"[��]"+global);
}

int GetLocationArraySize(string sName, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    return GetLocalInt(oObject, sName+"[��]"+global);
}

//Setting elements
void SetIntArrayElement(string sName, int nIndex, int nValue, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    if (nIndex >= GetLocalInt(oObject, sName+"[��]"+global) || nIndex < 0) return;
    SetLocalInt(oObject, sName+"[�"+IntToString(nIndex)+"]"+global, nValue);
}

void SetStringArrayElement(string sName, int nIndex, string sValue, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    if (nIndex >= GetLocalInt(oObject, sName+"[��]"+global) || nIndex < 0) return;
    SetLocalString(oObject, sName+"[�"+IntToString(nIndex)+"]"+global, sValue);
}

void SetFloatArrayElement(string sName, int nIndex, float fValue, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    if (nIndex >= GetLocalInt(oObject, sName+"[��]"+global) || nIndex < 0) return;
    SetLocalFloat(oObject, sName+"[�"+IntToString(nIndex)+"]"+global, fValue);
}

void SetObjectArrayElement(string sName, int nIndex, object oValue, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    if (nIndex >= GetLocalInt(oObject, sName+"[��]"+global) || nIndex < 0) return;
    SetLocalObject(oObject, sName+"[�"+IntToString(nIndex)+"]"+global, oValue);
}

void SetLocationArrayElement(string sName, int nIndex, location lValue, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    if (nIndex >= GetLocalInt(oObject, sName+"[��]"+global) || nIndex < 0) return;
    SetLocalLocation(oObject, sName+"[�"+IntToString(nIndex)+"]"+global, lValue);
}

//Getting elements
int GetIntArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    if (nIndex >= GetLocalInt(oObject, sName+"[��]"+global) || nIndex < 0) return 0;
    return GetLocalInt(oObject, sName+"[�"+IntToString(nIndex)+"]"+global);
}

string GetStringArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    if (nIndex >= GetLocalInt(oObject, sName+"[��]"+global) || nIndex < 0) return "";
    return GetLocalString(oObject, sName+"[�"+IntToString(nIndex)+"]"+global);
}

float GetFloatArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    if (nIndex >= GetLocalInt(oObject, sName+"[��]"+global) || nIndex < 0) return 0.0f;
    return GetLocalFloat(oObject, sName+"[�"+IntToString(nIndex)+"]"+global);
}

object GetObjectArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    if (nIndex >= GetLocalInt(oObject, sName+"[��]"+global) || nIndex < 0) return OBJECT_INVALID;
    return GetLocalObject(oObject, sName+"[�"+IntToString(nIndex)+"]"+global);
}

location GetLocationArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    if (nIndex >= GetLocalInt(oObject, sName+"[��]"+global) || nIndex < 0) return GetStartingLocation();
    return GetLocalLocation(oObject, sName+"[�"+IntToString(nIndex)+"]"+global);
}

//Add elements
void AddIntArrayElement(string sName, int nValue, int nBeginning=FALSE, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize+1);
    if (!nBeginning)
    {
        SetLocalInt(oObject, sName+"[�"+IntToString(nArraySize)+"]"+global, nValue);
        return;
    }
    int i;
    for (i = nArraySize-1; i >= 0; i--)
        SetLocalInt(oObject, sName+"[�"+IntToString(i+1)+"]"+global, GetLocalInt(oObject, sName+"[�"+IntToString(i)+"]"+global));
    SetLocalInt(oObject, sName+"[�0]"+global, nValue);
}

void AddStringArrayElement(string sName, string sValue, int nBeginning=FALSE, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize+1);
    if (!nBeginning)
    {
        SetLocalString(oObject, sName+"[�"+IntToString(nArraySize)+"]"+global, sValue);
        return;
    }
    int i;
    for (i = nArraySize-1; i >= 0; i--)
        SetLocalString(oObject, sName+"[�"+IntToString(i+1)+"]"+global, GetLocalString(oObject, sName+"[�"+IntToString(i)+"]"+global));
    SetLocalString(oObject, sName+"[�0]"+global, sValue);
}

void AddFloatArrayElement(string sName, float fValue, int nBeginning=FALSE, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize+1);
    if (!nBeginning)
    {
        SetLocalFloat(oObject, sName+"[�"+IntToString(nArraySize)+"]"+global, fValue);
        return;
    }
    int i;
    for (i = nArraySize-1; i >= 0; i--)
        SetLocalFloat(oObject, sName+"[�"+IntToString(i+1)+"]"+global, GetLocalFloat(oObject, sName+"[�"+IntToString(i)+"]"+global));
    SetLocalFloat(oObject, sName+"[�0]"+global, fValue);
}

void AddObjectArrayElement(string sName, object oValue, int nBeginning=FALSE, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize+1);
    if (!nBeginning)
    {
        SetLocalObject(oObject, sName+"[�"+IntToString(nArraySize)+"]"+global, oValue);
        return;
    }
    int i;
    for (i = nArraySize-1; i >= 0; i--)
        SetLocalObject(oObject, sName+"[�"+IntToString(i+1)+"]"+global, GetLocalObject(oObject, sName+"[�"+IntToString(i)+"]"+global));
    SetLocalObject(oObject, sName+"[�0]"+global, oValue);
}

void AddLocationArrayElement(string sName, location lValue, int nBeginning=FALSE, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize+1);
    if (!nBeginning)
    {
        SetLocalLocation(oObject, sName+"[�"+IntToString(nArraySize)+"]"+global, lValue);
        return;
    }
    int i;
    for (i = nArraySize-1; i >= 0; i--)
        SetLocalLocation(oObject, sName+"[�"+IntToString(i+1)+"]"+global, GetLocalLocation(oObject, sName+"[�"+IntToString(i)+"]"+global));
    SetLocalLocation(oObject, sName+"[�0]"+global, lValue);
}

//Deleting elements
void DeleteIntArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    if (nIndex >= nArraySize || nIndex < 0) return;
    int i;
    for (i = nIndex; i < nArraySize-1; i++)
        SetLocalInt(oObject, sName+"[�"+IntToString(i)+"]"+global, GetLocalInt(oObject, sName+"[�"+IntToString(i+1)+"]"+global));
    DeleteLocalInt(oObject, sName+"[�"+IntToString(nArraySize-1)+"]"+global);
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize-1);
}

void DeleteStringArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    if (nIndex >= nArraySize || nIndex < 0) return;
    int i;
    for (i = nIndex; i < nArraySize-1; i++)
        SetLocalString(oObject, sName+"[�"+IntToString(i)+"]"+global, GetLocalString(oObject, sName+"[�"+IntToString(i+1)+"]"+global));
    DeleteLocalString(oObject, sName+"[�"+IntToString(nArraySize-1)+"]"+global);
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize-1);
}

void DeleteFloatArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    if (nIndex >= nArraySize || nIndex < 0) return;
    int i;
    for (i = nIndex; i < nArraySize-1; i++)
        SetLocalFloat(oObject, sName+"[�"+IntToString(i)+"]"+global, GetLocalFloat(oObject, sName+"[�"+IntToString(i+1)+"]"+global));
    DeleteLocalFloat(oObject, sName+"[�"+IntToString(nArraySize-1)+"]"+global);
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize-1);
}

void DeleteObjectArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    if (nIndex >= nArraySize || nIndex < 0) return;
    int i;
    for (i = nIndex; i < nArraySize-1; i++)
        SetLocalObject(oObject, sName+"[�"+IntToString(i)+"]"+global, GetLocalObject(oObject, sName+"[�"+IntToString(i+1)+"]"+global));
    DeleteLocalObject(oObject, sName+"[�"+IntToString(nArraySize-1)+"]"+global);
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize-1);
}

void DeleteLocationArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);
    if (nIndex >= nArraySize || nIndex < 0) return;
    int i;
    for (i = nIndex; i < nArraySize-1; i++)
        SetLocalLocation(oObject, sName+"[�"+IntToString(i)+"]"+global, GetLocalLocation(oObject, sName+"[�"+IntToString(i+1)+"]"+global));
    DeleteLocalLocation(oObject, sName+"[�"+IntToString(nArraySize-1)+"]"+global);
    SetLocalInt(oObject, sName+"[��]"+global, nArraySize-1);
}

//Copying arrays
void CopyIntArray(string sSourceName, string sDestName, object oSourceObject=OBJECT_INVALID, object oDestObject=OBJECT_INVALID)
{
    int nSize = GetIntArraySize(sSourceName, oSourceObject);
    CreateIntArray(sDestName, nSize, oDestObject);

    int i;
    for (i = 0; i < nSize; i++)
        SetIntArrayElement(sDestName, i, GetIntArrayElement(sSourceName, i, oSourceObject), oDestObject);
}

void CopyStringArray(string sSourceName, string sDestName, object oSourceObject=OBJECT_INVALID, object oDestObject=OBJECT_INVALID)
{
    int nSize = GetStringArraySize(sSourceName, oSourceObject);
    CreateStringArray(sDestName, nSize, oDestObject);

    int i;
    for (i = 0; i < nSize; i++)
        SetStringArrayElement(sDestName, i, GetStringArrayElement(sSourceName, i, oSourceObject), oDestObject);
}

void CopyFloatArray(string sSourceName, string sDestName, object oSourceObject=OBJECT_INVALID, object oDestObject=OBJECT_INVALID)
{
    int nSize = GetFloatArraySize(sSourceName, oSourceObject);
    CreateFloatArray(sDestName, nSize, oDestObject);

    int i;
    for (i = 0; i < nSize; i++)
        SetFloatArrayElement(sDestName, i, GetFloatArrayElement(sSourceName, i, oSourceObject), oDestObject);
}

void CopyObjectArray(string sSourceName, string sDestName, object oSourceObject=OBJECT_INVALID, object oDestObject=OBJECT_INVALID)
{
    int nSize = GetObjectArraySize(sSourceName, oSourceObject);
    CreateObjectArray(sDestName, nSize, oDestObject);

    int i;
    for (i = 0; i < nSize; i++)
        SetObjectArrayElement(sDestName, i, GetObjectArrayElement(sSourceName, i, oSourceObject), oDestObject);
}

void CopyLocationArray(string sSourceName, string sDestName, object oSourceObject=OBJECT_INVALID, object oDestObject=OBJECT_INVALID)
{
    int nSize = GetLocationArraySize(sSourceName, oSourceObject);
    CreateLocationArray(sDestName, nSize, oDestObject);

    int i;
    for (i = 0; i < nSize; i++)
        SetLocationArrayElement(sDestName, i, GetLocationArrayElement(sSourceName, i, oSourceObject), oDestObject);
}

//Getting indexes
int GetIntArrayIndexByValue(string sName, int nValue, int nOccurence=1, object oObject=OBJECT_INVALID)
{
    if (nOccurence < 1) return ARRAY_INDEX_INVALID;

    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);

    int i;
    for (i = 0; i < nArraySize; i++)
    {
        if (GetLocalInt(oObject, sName+"[�"+IntToString(i)+"]"+global) == nValue)
            nOccurence--;
        if (nOccurence == 0) return i;
    }

    return ARRAY_INDEX_INVALID;
}

int GetStringArrayIndexByValue(string sName, string sValue, int nOccurence=1, object oObject=OBJECT_INVALID)
{
    if (nOccurence < 1) return ARRAY_INDEX_INVALID;

    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);

    int i;
    for (i = 0; i < nArraySize; i++)
    {
        if (GetLocalString(oObject, sName+"[�"+IntToString(i)+"]"+global) == sValue)
            nOccurence--;
        if (nOccurence == 0) return i;
    }

    return ARRAY_INDEX_INVALID;
}

int GetFloatArrayIndexByValue(string sName, float fValue, int nOccurence=1, object oObject=OBJECT_INVALID)
{
    if (nOccurence < 1) return ARRAY_INDEX_INVALID;

    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);

    int i;
    for (i = 0; i < nArraySize; i++)
    {
        if (GetLocalFloat(oObject, sName+"[�"+IntToString(i)+"]"+global) == fValue)
            nOccurence--;
        if (nOccurence == 0) return i;
    }

    return ARRAY_INDEX_INVALID;
}

int GetObjectArrayIndexByValue(string sName, object oValue, int nOccurence=1, object oObject=OBJECT_INVALID)
{
    if (nOccurence < 1) return ARRAY_INDEX_INVALID;

    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);

    int i;
    for (i = 0; i < nArraySize; i++)
    {
        if (GetLocalObject(oObject, sName+"[�"+IntToString(i)+"]"+global) == oValue)
            nOccurence--;
        if (nOccurence == 0) return i;
    }

    return ARRAY_INDEX_INVALID;
}

int GetLocationArrayIndexByValue(string sName, location lValue, int nOccurence=1, object oObject=OBJECT_INVALID)
{
    if (nOccurence < 1) return ARRAY_INDEX_INVALID;

    string global = "";
    if (oObject == OBJECT_INVALID)
    {
        oObject = GetModule();
        global = "g";
    }
    int nArraySize = GetLocalInt(oObject, sName+"[��]"+global);

    int i;
    for (i = 0; i < nArraySize; i++)
    {
        if (GetLocalLocation(oObject, sName+"[�"+IntToString(i)+"]"+global) == lValue)
            nOccurence--;
        if (nOccurence == 0) return i;
    }

    return ARRAY_INDEX_INVALID;
}

