#include "inc_arrays"
#include "inc_tokens"

// Store a token value on a creature to be used in conversations
void StoreCreatureConversationToken(object oCreature, int nToken, string sValue);

// Clears all tokens stored on a creature
void ClearCreatureConversationTokens(object oCreature);

// Sets token values of all the tokens stored on a creature to those specified in StoreCreatureConversationToken calls.
// Call this in the conditional script of every node in the creature's conversation in which a custom token is used
// to ensure the tokens are displayed.  
void SetCreatureConversationTokens();



void StoreCreatureConversationToken(object oCreature, int nToken, string sValue)
{
    string array = "CONVOTOKENS";
    if (GetIntArraySize(array, oCreature) == 0)
        CreateIntArray(array, 0, oCreature);

    string var = "CONVOTOKEN_"+IntToString(nToken);
    string setVar = var+"_SET";
    if (!GetLocalInt(oCreature, setVar))
    {
        AddIntArrayElement(array, nToken, FALSE, oCreature);
        SetLocalInt(oCreature, setVar, TRUE);
    }
    SetLocalString(oCreature, var, sValue);
}

void ClearCreatureConversationTokens(object oCreature)
{
    string array = "CONVOTOKENS";
    int size = GetIntArraySize(array, oCreature);
    int i;
    for (i = size-1; i >= 0; i--)
    {
        int token = GetIntArrayElement(array, i, oCreature);
        string var = "CONVOTOKEN_"+IntToString(token);
        string setVar = var+"_SET";
        DeleteLocalString(oCreature, var);
        DeleteLocalInt(oCreature, setVar);
        DeleteIntArrayElement(array, i, oCreature);
    }
    ClearIntArray(array, oCreature);
}

void SetCreatureConversationTokens()
{
    string array = "CONVOTOKENS";
    int size = GetIntArraySize(array, OBJECT_SELF);
    int i;
    for (i = 0; i < size; i++)
    {
        int token = GetIntArrayElement(array, i, OBJECT_SELF);
        string var = "CONVOTOKEN_"+IntToString(token);
        string value = GetLocalString(OBJECT_SELF, var);
        SetCustomTokenEx(token, value);
    }
}