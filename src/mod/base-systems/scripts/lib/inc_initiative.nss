// Returns the initiative bonuses a creature has from feats
int GetInitiativeFeatBonuses(object oCreature);

// Performs a manual initiative roll and returns the score
int InitiativeRoll(object oCreature);

int GetInitiativeFeatBonuses(object oCreature)
{
    int bonus = 0;

    if (GetHasFeat(FEAT_BLOODED, oCreature))
        bonus += 2;
    if (GetHasFeat(FEAT_THUG, oCreature))
        bonus += 2;
    if (GetHasFeat(FEAT_EPIC_SUPERIOR_INITIATIVE, oCreature))
        bonus += 8;
    else if (GetHasFeat(FEAT_IMPROVED_INITIATIVE, oCreature))
        bonus += 4;

    return bonus;
}

int InitiativeRoll(object oCreature)
{
    int dexBonus = GetAbilityModifier(ABILITY_DEXTERITY, oCreature);
    int roll = Random(20)+1 + GetInitiativeFeatBonuses(oCreature) + dexBonus;
    return roll;
}