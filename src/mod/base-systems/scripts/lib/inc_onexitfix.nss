// This library's sole purpose is providing functions for workarounding the following issue:
// The OnExit event for trigger nor area of effect doesn't fire for player exiting the game. Also, the area OnExit event doesn't fire for dead player exiting the game.

#include "inc_arrays"

// This function should be called at the beginning of every trigger's OnEnter event handler
void AddTrigger(object oPC, object oTrigger);

// This function should be called at the beginning of every trigger's OnExit event handler
void RemoveTrigger(object oPC, object oTrigger);

// This function should be called at the beginning of every area's OnEnter event handler
void SetCurrentArea(object oPC, object oArea);

// This function should be called at the beginning of every area's OnExit event handler
void UnsetCurrentArea(object oPC, object oArea);

// This function should be called at the beginning of module's OnClientLeave event handler
void ExecuteOnExitEvents();

string TRIGGER_ARRAY = "TRIGGER_ARRAY";
string CURRENT_AREA = "CURRENT_AREA";

void AddTrigger(object oPC, object oTrigger)
{
    if (!GetIsPC(oPC))
        return;
    if (GetObjectArraySize(TRIGGER_ARRAY, oPC) == 0)
        CreateObjectArray(TRIGGER_ARRAY, 0, oPC);

    AddObjectArrayElement(TRIGGER_ARRAY, oTrigger, FALSE, oPC);
    SetLocalInt(oTrigger, GetObjectUUID(oPC), TRUE);
}

void RemoveTrigger(object oPC, object oTrigger)
{
    if (!GetIsPC(oPC))
        return;
    int index = GetObjectArrayIndexByValue(TRIGGER_ARRAY, oTrigger, 1, oPC);
    DeleteObjectArrayElement(TRIGGER_ARRAY, index, oPC);
    DeleteLocalInt(oTrigger, GetObjectUUID(oPC));
}

void SetCurrentArea(object oPC, object oArea)
{
    SetLocalObject(oPC, CURRENT_AREA, oArea);
    SetLocalInt(oArea, GetObjectUUID(oPC), TRUE);
}

void UnsetCurrentArea(object oPC, object oArea)
{
    DeleteLocalObject(oPC, CURRENT_AREA);
    DeleteLocalInt(oArea, GetObjectUUID(oPC));
}

void ExecuteOnExitEvents()
{
    object PC = GetExitingObject();
    if (!GetIsDead(PC))
        return;

    //Execute area's OnExit event
    object area = GetLocalObject(PC, CURRENT_AREA);
    string areaScript = GetEventScript(area, EVENT_SCRIPT_AREA_ON_EXIT);
    if (GetLocalInt(area, GetObjectUUID(PC)) == TRUE) //condition ensures OnExit is run only once
        ExecuteScript(areaScript, area);

    //Execute OnExit event of every trigger the PC is standing in
    int i;
    int triggerNum = GetObjectArraySize(TRIGGER_ARRAY, PC);
    for (i = triggerNum-1; i >= 0; i--) //reverse order is more efficient for deletion of array elements
    {
        object trigger = GetObjectArrayElement(TRIGGER_ARRAY, i, PC);
        string triggerScript = GetEventScript(trigger, EVENT_SCRIPT_TRIGGER_ON_OBJECT_EXIT);
        if (GetLocalInt(trigger, GetObjectUUID(PC)) == TRUE)
            ExecuteScript(triggerScript, trigger);
    }
}
