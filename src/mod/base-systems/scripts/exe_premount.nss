#include "inc_horses"

// Script firing before a horse is mounted (see x3_inc_horse for details)

void main()
{
    object PC = OBJECT_SELF;
    object horse = GetLocalObject(PC, "oX3_TempHorse");

    SaveHorseVariablesOnRider(PC, horse);
}