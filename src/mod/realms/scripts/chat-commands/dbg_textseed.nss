#include "inc_chat"
#include "inc_random"

void main()
{
    string param = GetChatCommandParameters();
    SendMessageToPC(GetPCChatSpeaker(), param + ": " + IntToString(GetSeedFromString(param)));
}