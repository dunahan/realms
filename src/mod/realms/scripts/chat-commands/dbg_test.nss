#include "inc_companions"
#include "inc_debug"
#include "inc_realms"
#include "inc_ruleset"
#include "inc_scriptevents"
#include "inc_weather"
#include "inc_rngnames"
#include "inc_targeting"

void NextLevel(object companion)
{
    LogInfo("+28000 xp!");
    companion = SetCompanionXP(companion, GetXP(companion)+28000);
    //DelayCommand(15.0f, NextLevel(companion));
}

void main()
{
    //UpdateRealmWeather();
    //LogInfo("Weather updated");
    //EnterTargetingModeToExecuteScript(OBJECT_SELF, "clk_test");
    //SendMessageToPC(GetFirstPC(), RandomTavernName());
    //object companion = GetHenchman(OBJECT_SELF);
    //NextLevel(companion);
    CreateItemOnObject("it_battleaxe00a", OBJECT_SELF);
    SetIdentified(CreateItemOnObject("it_battleaxe01a", OBJECT_SELF), TRUE);
    SetIdentified(CreateItemOnObject("it_battleaxe02a", OBJECT_SELF), TRUE);
    SetIdentified(CreateItemOnObject("it_battleaxe03a", OBJECT_SELF), TRUE);
    //CreateCreature("cre_hwarhorse2", GetLocation(OBJECT_SELF));
    /*effect eDeath = EffectDeath(FALSE, FALSE);
    CreateItemOnObject("nw_it_spdvscr501");
    ApplyEffectToObject(DURATION_TYPE_INSTANT, eDeath, OBJECT_SELF);*/
}
