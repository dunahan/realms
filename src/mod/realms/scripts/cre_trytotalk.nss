// Put this in a creature's OnHeartbeat to make them attempt to talk to a PC
// stored in a variable using a conversation (also stored)
// until the "NoTalk" variable is set

void main()
{
    if (GetLocalInt(OBJECT_SELF, "NoTalk"))
        return;

    object PC = GetLocalObject(OBJECT_SELF, "PCToTalkTo");
    string convo = GetLocalString(OBJECT_SELF, "Conversation");
    ActionStartConversation(PC, convo);
}