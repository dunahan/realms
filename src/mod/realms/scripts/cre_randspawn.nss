#include "inc_random"
// This script randomizes appearance of the spawned NPC

void main()
{
    string seed = ObjectToString(OBJECT_SELF);

    //Randomize head
    int race = GetRacialType(OBJECT_SELF);
    int gender = GetGender(OBJECT_SELF);
    int head;
    if (race == RACIAL_TYPE_HUMAN)
    {
        if (gender == GENDER_MALE)
        {
            int rand = RandomNext(26, seed)+1;
            if (rand <= 18)
                head = rand;
            else if (rand <= 21)
                head = rand + 2;
            else if (rand == 22)
                head = rand + 6;
            else if (rand <= 25)
                head = rand + 7;
            else
                head = 155;
        }
        else
        {
            int rand = RandomNext(18, seed)+1;
            if (rand <= 13)
                head = rand;
            else if (rand <= 16)
                head = rand + 1;
            else if (rand == 17)
                head = 22;
            else
                head = 25;
        }
    }
    else if (race == RACIAL_TYPE_HALFLING)
    {
        if (gender == GENDER_MALE)
        {
            int rand = RandomNext(9, seed)+1;
            if (rand <= 8)
                head = rand;
            else
                head = rand+1;
        }
        else
        {
            int rand = RandomNext(10, seed)+1;
            if (rand <= 9)
                head = rand;
            else
                head = rand+1;
        }
    }
    SetCreatureBodyPart(CREATURE_PART_HEAD, head, OBJECT_SELF);

    //Randomize body size
    if (race == RACIAL_TYPE_HUMAN)
    {
        if (RandomNext(20, seed) == 0)
            SetPhenoType(PHENOTYPE_BIG);
    }
    else if (race == RACIAL_TYPE_HALFLING)
    {
        if (RandomNext(15, seed) == 0)
            SetPhenoType(PHENOTYPE_BIG);
    }

    //Randomize hair color
    if (race == RACIAL_TYPE_HUMAN || race == RACIAL_TYPE_HALFLING)
    {
        int hairColor;
        int rand = RandomNext(20, seed);
        if (rand < 16)
            hairColor = rand;
        else 
            hairColor = rand + 116;
        SetColor(OBJECT_SELF, COLOR_CHANNEL_HAIR, hairColor);
    }

    //Randomize skin color
    if (race == RACIAL_TYPE_HUMAN || race == RACIAL_TYPE_HALFLING)
    {
        int skinColor = RandomNext(4, seed);
        SetColor(OBJECT_SELF, COLOR_CHANNEL_SKIN, skinColor);
    }

    //Randomize clothes
    string predefined = GetLocalString(OBJECT_SELF, "Clothes");
    if (predefined != "")
    {
        object item = CreateItemOnObject(predefined);
        ClearAllActions();
        ActionEquipItem(item, INVENTORY_SLOT_CHEST);
    }
    else if (race == RACIAL_TYPE_HUMAN || race == RACIAL_TYPE_HALFLING)
    {
        string clothes;
        switch (RandomNext(10, seed))
        {
            case 0: clothes = "nw_cloth001"; break;
            case 1: clothes = "nw_cloth025"; break;
            case 2: clothes = "nw_cloth006"; break;
            case 3: clothes = "nw_cloth013"; break;
            case 4: clothes = "nw_cloth019"; break;
            case 5: clothes = "nw_cloth004"; break;
            case 6: clothes = "nw_cloth018"; break;
            case 7: clothes = "nw_cloth024"; break;
            case 8: clothes = "nw_cloth022"; break;
            case 9: clothes = "nw_cloth027"; break;
        }
        object item = CreateItemOnObject(clothes);
        ClearAllActions();
        ActionEquipItem(item, INVENTORY_SLOT_CHEST);
    }
}
