// This ensures that one can't place multiple times in the same spot by queuing them in an action que.

void main()
{
    object item = GetModuleItemLost();
    if (GetBaseItemType(item) != BASE_ITEM_TRAPKIT)
        return;
    object trapSetter = GetModuleItemLostBy();
    AssignCommand(trapSetter, ClearAllActions());
}