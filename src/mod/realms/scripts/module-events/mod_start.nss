#include "inc_scriptevents"
#include "inc_switches"
#include "inc_realms"
// Module-specific startup code

void main()
{
    if (GetIsModuleLoadedFromSaveFile())
    {
        return;
    }

    //Registering entities-specific event scripts
    AddEventScript(SUBEVENT_MODULE_ON_PLAYER_EQUIP_ITEM, "mod_equip");
    AddEventScript(SUBEVENT_MODULE_ON_HEARTBEAT, "mod_hb");
    AddEventScript(SUBEVENT_MODULE_ON_UNACQUIRE_ITEM, "mod_losetrap");

    //Extra switches
    SetSwitch(MODULE_SWITCH_DEBUG_MODE, FALSE);
    SetSwitch(MODULE_SWITCH_LOGGING, TRUE);
    SetSwitch(MODULE_SWITCH_PRINT_WARN, TRUE);
    SetSwitch(MODULE_SWITCH_PRINT_FATAL, TRUE);

    //We want to allow playing with any instruction limit in DEBUG mode to verify whether scripts break TMI
    if (GetSwitch(MODULE_SWITCH_DEBUG_MODE))
        SetLocalInt(OBJECT_SELF, "TMI_INCREASED", TRUE);

    //Choose a script implementing a realm template
    string realmScript = "rea_realm";
    SetRealmScript(realmScript);
}
