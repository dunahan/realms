#include "inc_factions"
#include "inc_common"

// Library with functions for triggering common NPC behavior routines, such as attacking after a delay or running away.

// Makes the creature and potentially its group start attacking the PCs after a delay.
// Call this in a conversation node when conflict is imminent, but you still want to give PCs a few seconds to read the conversation line.
// (without giving them infinite time by only starting the attack when they abort the conversation themselves).
// - nFactionToJoin - faction for the creatures to join after starting attacking
// - sTagOfGroup - creatures with this tag in the same area as oCreature will be considered part of oCreature's group and will also attack and change faction
// - sVarOfGroup - creatures with this integer variable set to TRUE will be considered part of oCreature's group and will also attack and change faction
// - fDelay - delay after which the attack should start
void PrepareCreaturesForAttack(object oCreature, int nFactionToJoin=FACTION_HOSTILE, string sTagOfGroup="", string sVarOfGroup="", float fDelay=6.0f);

// Returns TRUE if PrepareCreaturesForAttack has been called on oCreature or if oCreature was in a group of another creature it was called on.
int GetIsCreaturePreparingForAttack(object oCreature);

// Will be launched automatically after a PrepareCreaturesForAttack call after fDelay seconds.
// If called manually, stops the preparation delay and forces the creature and its group to attack immediately.
// Will NOT do anything if PrepareCreaturesForAttack has not been called on the creature or a member of its group.
// Call this on conversation aborting.
// - nFactionToJoin - faction for the creatures to join after starting attacking
// - sTagOfGroup - creatures with this tag in the same area as oCreature will be considered part of oCreature's group and will also attack and change faction
// - sVarOfGroup - creatures with this integer variable set to TRUE will be considered part of oCreature's group and will also attack and change faction
void LaunchAttackIfPrepared(object oCreature, int nFactionToJoin=FACTION_HOSTILE, string sTagOfGroup="", string sVarOfGroup="");

// Makes the creature and potentially its group run away and fade away (be destroyed).
// The creatures affected will be set to plot and their faction will become FACTION_TEMPORARY_NEUTRAL.
// Creatures in oCreature's group will follow oCreature in their running away.
// - lRunAwayFrom - location oCreature should run away from
// - sTagOfGroup - creatures with this tag in the same area as oCreature will be considered part of oCreature's group and will also run away
// - sVarOfGroup - creatures with this integer variable set to TRUE will be considered part of oCreature's group and will also run away
// - nRun - TRUE if the creatures are supposed to run, FALSE if they should just walk away
// - fDelay - delay after which the creatures will fade away
void RunAway(object oCreature, location lRunAwayFrom, string sTagOfGroup="", string sVarOfGroup="", int nRun=TRUE, float fDelay=3.0f);

void PrepareCreaturesForAttack(object oCreature, int nFactionToJoin=FACTION_HOSTILE, string sTagOfGroup="", string sVarOfGroup="", float fDelay=6.0f)
{
    SetLocalInt(OBJECT_SELF, "ATK_READY_TO_ATTACK", TRUE);
    DelayCommand(fDelay, LaunchAttackIfPrepared(oCreature, nFactionToJoin, sTagOfGroup, sVarOfGroup));

    if (sVarOfGroup != "")
    {
        object area = GetArea(oCreature);
        object hostile = GetFirstObjectInArea(area);
        while (GetIsObjectValid(hostile))
        {
            if (hostile != OBJECT_SELF && (GetTag(hostile) == sTagOfGroup || GetLocalInt(hostile, sVarOfGroup)))
            {
                SetLocalInt(OBJECT_SELF, "ATK_READY_TO_ATTACK", TRUE);
                DelayCommand(fDelay, LaunchAttackIfPrepared(oCreature, nFactionToJoin, sTagOfGroup, sVarOfGroup));
            }
            hostile = GetNextObjectInArea(area);
        }
    }
    else if (sTagOfGroup != "")
    {
        int i = 1;
        object hostile = GetNearestObjectByTag(sTagOfGroup, oCreature, i++);
        while (GetIsObjectValid(hostile))
        {
            if (hostile != OBJECT_SELF)
            {
                SetLocalInt(OBJECT_SELF, "ATK_READY_TO_ATTACK", TRUE);
                DelayCommand(fDelay, LaunchAttackIfPrepared(oCreature, nFactionToJoin, sTagOfGroup, sVarOfGroup));
            }
            hostile = GetNearestObjectByTag(sTagOfGroup, oCreature, i++);
        }
    }
}

int GetIsCreaturePreparingForAttack(object oCreature)
{
    return GetLocalInt(OBJECT_SELF, "ATK_READ_TO_ATTACK");
}

void LaunchAttackIfPrepared(object oCreature, int nFactionToJoin=FACTION_HOSTILE, string sTagOfGroup="", string sVarOfGroup="")
{
    if (!GetLocalInt(OBJECT_SELF, "ATK_STARTED_ATTACKING"))
    {
        SetLocalInt(OBJECT_SELF, "ATK_STARTED_ATTACKING", TRUE);
        SetFaction(OBJECT_SELF, nFactionToJoin);
        AssignCommand(OBJECT_SELF, StartCombat());
    }

    if (sVarOfGroup != "")
    {
        object area = GetArea(oCreature);
        object hostile = GetFirstObjectInArea(area);
        while (GetIsObjectValid(hostile))
        {
            if (!GetLocalInt(hostile, "ATK_STARTED_ATTACKING") && (GetTag(hostile) == sTagOfGroup || GetLocalInt(hostile, sVarOfGroup)))
            {
                SetLocalInt(hostile, "ATK_STARTED_ATTACKING", TRUE);
                SetFaction(hostile, nFactionToJoin);
                AssignCommand(hostile, StartCombat());
            }
            hostile = GetNextObjectInArea(area);
        }
    }
    else if (sTagOfGroup != "")
    {
        int i = 1;
        object hostile = GetNearestObjectByTag(sTagOfGroup, oCreature, i++);
        while (GetIsObjectValid(hostile))
        {
            if (!GetLocalInt(hostile, "ATK_STARTED_ATTACKING"))
            {
                SetLocalInt(hostile, "ATK_STARTED_ATTACKING", TRUE);
                SetFaction(hostile, nFactionToJoin);
                AssignCommand(hostile, StartCombat());
            }
            hostile = GetNearestObjectByTag(sTagOfGroup, oCreature, i++);
        }
    }
}

void _RunningAwayPseudoHb(int nRun, object oMaster, location lRunningAwayFrom, float fDelay)
{
    SetFaction(OBJECT_SELF, FACTION_TEMPORARY_NEUTRAL);
    SetPlotFlag(OBJECT_SELF, TRUE);
    if (fDelay >= 0.0f)
        DestroyObject(OBJECT_SELF, fDelay);
    ClearAllActions(TRUE);

    if (GetIsObjectValid(oMaster))
    {
        ActionForceMoveToObject(oMaster, nRun, 0.2f);
        DelayCommand(1.0f, _RunningAwayPseudoHb(nRun, oMaster, lRunningAwayFrom, -1.0f));
    }
    else
    {
        ActionMoveAwayFromLocation(lRunningAwayFrom, nRun, 40.0f);
    }
}

void RunAway(object oCreature, location lRunAwayFrom, string sTagOfGroup="", string sVarOfGroup="", int nRun=TRUE, float fDelay=3.0f)
{
    _RunningAwayPseudoHb(nRun, OBJECT_INVALID, lRunAwayFrom, fDelay);
    object main = OBJECT_SELF;

    if (sVarOfGroup != "")
    {
        object area = GetArea(oCreature);
        object hostile = GetFirstObjectInArea(area);
        while (GetIsObjectValid(hostile))
        {
            if (hostile != OBJECT_SELF && (GetTag(hostile) == sTagOfGroup || GetLocalInt(hostile, sVarOfGroup)))
            {
                AssignCommand(hostile, _RunningAwayPseudoHb(nRun, main, lRunAwayFrom, fDelay));
            }
            hostile = GetNextObjectInArea(area);
        }
    }
    else if (sTagOfGroup != "")
    {
        int i = 1;
        object hostile = GetNearestObjectByTag(sTagOfGroup, oCreature, i++);
        while (GetIsObjectValid(hostile))
        {
            if (hostile != OBJECT_SELF)
            {
                AssignCommand(hostile, _RunningAwayPseudoHb(nRun, main, lRunAwayFrom, fDelay));
            }
            hostile = GetNearestObjectByTag(sTagOfGroup, oCreature, i++);
        }
    }
}