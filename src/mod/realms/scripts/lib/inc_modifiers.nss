#include "inc_random"
#include "inc_language"
#include "x2_inc_itemprop"

// Library containing functions pertaining to champion modifiers

//Simple modifiers
const int CHAMPION_AC_BOOST             = 1;
const int CHAMPION_ATK_BOOST            = 2;
const int CHAMPION_HASTE                = 3;
const int CHAMPION_DMG_BOOST_MAGICAL    = 4;
const int CHAMPION_DMG_BOOST_FIRE       = 5;
const int CHAMPION_DMG_BOOST_ELECTRICAL = 6;
const int CHAMPION_DMG_BOOST_COLD       = 7;
const int CHAMPION_DMG_BOOST_ACID       = 8;
const int CHAMPION_DMG_BOOST_NEGATIVE   = 9;
const int CHAMPION_DMG_BOOST_DIVINE     = 10;
const int CHAMPION_DMG_BOOST_SONIC      = 11;
const int CHAMPION_FORTITUDE_BOOST      = 12;
const int CHAMPION_REFLEX_BOOST         = 13;
const int CHAMPION_WILL_BOOST           = 14;
const int CHAMPION_SAVES_BOOST          = 36;
const int CHAMPION_IMMUNITY_PIERCING    = 15;
const int CHAMPION_IMMUNITY_SLASHING    = 16;
const int CHAMPION_IMMUNITY_BLUDGEONING = 17;
const int CHAMPION_IMMUNITY_PHYSICAL    = 37;
const int CHAMPION_REDUCTION            = 49;
const int CHAMPION_IMMUNITY_MAGICAL     = 18;
const int CHAMPION_IMMUNITY_FIRE        = 19;
const int CHAMPION_IMMUNITY_ELECTRICAL  = 20;
const int CHAMPION_IMMUNITY_COLD        = 21;
const int CHAMPION_IMMUNITY_ACID        = 22;
const int CHAMPION_IMMUNITY_NEGATIVE    = 23;
const int CHAMPION_IMMUNITY_DIVINE      = 24;
const int CHAMPION_IMMUNITY_SONIC       = 25;
const int CHAMPION_REGENERATION         = 26;
const int CHAMPION_SPELL_RESISTANCE     = 46;

//Modifiers with weaknesses
const int CHAMPION_FORTITUDE_BOOST_REFLEX_PENALTY           = 27;
const int CHAMPION_FORTITUDE_BOOST_WILL_PENALTY             = 28;
const int CHAMPION_WILL_BOOST_FORTITUDE_PENALTY             = 29;
const int CHAMPION_WILL_BOOST_REFLEX_PENALTY                = 30;
const int CHAMPION_REFLEX_BOOST_FORTITUDE_PENALTY           = 31;
const int CHAMPION_REFLEX_BOOST_WILL_PENALTY                = 32;
const int CHAMPION_FORTITUDE_BOOST_OTHERS_PENALTY           = 33;
const int CHAMPION_REFLEX_BOOST_OTHERS_PENALTY              = 34;
const int CHAMPION_WILL_BOOST_OTHERS_PENALTY                = 35;
const int CHAMPION_IMMUNITY_FIRE_VULNERABILITY_COLD         = 38;
const int CHAMPION_IMMUNITY_COLD_VULNERABILITY_FIRE         = 39;
const int CHAMPION_IMMUNITY_PHYSICAL_VULNERABILITY_SONIC    = 40;
const int CHAMPION_IMMUNITY_DIVINE_VULNERABILITY_NEGATIVE   = 41;
const int CHAMPION_IMMUNITY_NEGATIVE_VULNERABILITY_DIVINE   = 42;
const int CHAMPION_IMMUNITY_ACID_VULNERABILITY_ELECTRICAL   = 43;
const int CHAMPION_IMMUNITY_ELECTRICAL_VULNERABILITY_ACID   = 44;
const int CHAMPION_RESISTANCE_PHYSICAL_AC_PENALTY           = 45;
const int CHAMPION_ATTACK_BOOST_AC_PENALTY                  = 47;
const int CHAMPION_AC_BOOST_ATTACK_PENALTY                  = 48;


// Returns a new name of a champion after adding a modifier-related title to it
string AddModifierTitleToName(string sName, int nModifier, string sSeedName, int nGender=GENDER_MALE);

// Returns a part of the rumor that explains the modifier's strength (and weakness, if applicable)
string GetModifierRumorString(string sName, int nModifier, int nGender=GENDER_MALE);

// Returns a new name of a champion after adding a random generic title to it
string AddRandomTitleToName(string sName, int nModifier, string sSeedName, int nGender=GENDER_MALE);

// Applies the effects of the given modifier to oChampion
void ApplyModifierToChampion(object oChampion, int nModifier, int nLevel);

string AddModifierTitleToName(string sName, int nModifier, string sSeedName, int nGender=GENDER_MALE)
{
    string newName;
    int rand = RandomNext(100, sSeedName);
    int lang = GetServerLanguage();

    switch (nModifier)
    {
        case CHAMPION_AC_BOOST:
        case CHAMPION_AC_BOOST_ATTACK_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 25)
                    newName = "The Unhittable "+sName;
                else if (rand < 50)
                    newName = "The Dodging "+sName;
                else if (rand < 75)
                    newName = sName+" the Lucky";
                else
                    newName = sName+" the Invincible";
            }
            else
            {
                if (rand < 25)
                    newName = sName + GenderedString(" Nietrafialny", " Nietrafialna", nGender);
                else if (rand < 50)
                    newName = sName + GenderedString(" Unikaj�cy", " Unikaj�ca", nGender);
                else if (rand < 75)
                    newName = GenderedString("Fortunny ", "Fortunna ", nGender) + sName;
                else
                    newName = GenderedString("Nietykalny ", "Nietykalna ", nGender) + sName;
            }
            break;
        case CHAMPION_ATK_BOOST:
        case CHAMPION_ATTACK_BOOST_AC_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 25)
                    newName = sName+" the Striker";
                else if (rand < 50)
                    newName = sName+" the Unstoppable";
                else if (rand < 75)
                    newName = "The Sure-Hit "+sName;
                else
                    newName = sName+" the Eagle Eye";
            }
            else
            {
                if (rand < 25)
                    newName = sName + GenderedString(" Precyzyjny", " Precyzyjna", nGender);
                else if (rand < 50)
                    newName = sName + GenderedString(" Niepowstrzymany", " Niepowstrzymana", nGender);
                else if (rand < 75)
                    newName = GenderedString("Trafiaj�cy ", "Trafiaj�ca ", nGender) + sName;
                else
                    newName = sName + GenderedString(" \"Sokole Oko\"", " \"Sokole Oko\"", nGender);
            }
            break;
        case CHAMPION_HASTE:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 25)
                    newName = sName+" the Hasty";
                else if (rand < 50)
                    newName = "The Rushing "+sName;
                else if (rand < 75)
                    newName = sName+" the Speedy";
                else
                    newName = "The Quick "+sName;
            }
            else
            {
                if (rand < 25)
                    newName = sName + " \"B�yskawica\"";
                else if (rand < 50)
                    newName = GenderedString("Gwa�towny ", "Gwa�towna ", nGender) + sName;
                else if (rand < 75)
                    newName = sName + GenderedString(" Szybki", " Szybka", nGender);
                else
                    newName = GenderedString("Po�pieszny ", "Po�pieszna ", nGender) + sName;
            }
            break;
        case CHAMPION_DMG_BOOST_MAGICAL:
        case CHAMPION_IMMUNITY_MAGICAL:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 50)
                    newName = sName+" the Magical";
                else
                    newName = sName+" of Magic";
            }
            else
            {
                if (rand < 50)
                    newName = sName + GenderedString(" Magiczny", " Magiczna", nGender);
                else
                    newName = sName+" od Magii";
            }
            break;
        case CHAMPION_DMG_BOOST_FIRE:
        case CHAMPION_IMMUNITY_FIRE_VULNERABILITY_COLD:
        case CHAMPION_IMMUNITY_COLD:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 50)
                    newName = sName+" the Flaming";
                else
                    newName = "The Burning "+sName;
            }
            else
            {
                if (rand < 50)
                    newName = sName + GenderedString(" Gorej�cy", " Gorej�ca", nGender);
                else
                    newName = GenderedString("Pal�cy ", "Pal�ca ", nGender) + sName;
            }
            break;
        case CHAMPION_DMG_BOOST_ELECTRICAL:
        case CHAMPION_IMMUNITY_ELECTRICAL:
        case CHAMPION_IMMUNITY_ELECTRICAL_VULNERABILITY_ACID:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 50)
                    newName = sName+" the Spark";
                else
                    newName = "The Shocking "+sName;
            }
            else
            {
                if (rand < 50)
                    newName = sName + " \"Iskra\"";
                else
                    newName = GenderedString("Pora�aj�cy ", "Pora�aj�ca ", nGender) + sName;
            }
            break;
        case CHAMPION_DMG_BOOST_COLD:
        case CHAMPION_IMMUNITY_FIRE:
        case CHAMPION_IMMUNITY_COLD_VULNERABILITY_FIRE:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 50)
                    newName = sName+" the Icicle";
                else
                    newName = "The Freezing "+sName;
            }
            else
            {
                if (rand < 50)
                    newName = sName + " \"Sopel\"";
                else
                    newName = GenderedString("Mro��cy ", "Mro��ca ", nGender) + sName;
            }
            break;
        case CHAMPION_DMG_BOOST_ACID:
        case CHAMPION_IMMUNITY_ACID:
        case CHAMPION_IMMUNITY_ACID_VULNERABILITY_ELECTRICAL:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 50)
                    newName = sName+" the Corrosive";
                else
                    newName = "The Acidic "+sName;
            }
            else
            {
                if (rand < 50)
                    newName = sName + GenderedString(" Korozyjny", " Korozyjna", nGender);
                else
                    newName = GenderedString("Kwa�ny ", "Kwa�na ", nGender) + sName;
            }
            break;
        case CHAMPION_DMG_BOOST_NEGATIVE:
        case CHAMPION_IMMUNITY_NEGATIVE:
        case CHAMPION_IMMUNITY_NEGATIVE_VULNERABILITY_DIVINE:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 50)
                    newName = sName+" the Fiend";
                else
                    newName = "The Dark "+sName;
            }
            else
            {
                if (rand < 50)
                    newName = sName + " \"Bies\"";
                else
                    newName = GenderedString("Mroczny ", "Mroczna ", nGender) + sName;
            }
            break;
        case CHAMPION_DMG_BOOST_DIVINE:
        case CHAMPION_IMMUNITY_DIVINE:
        case CHAMPION_IMMUNITY_DIVINE_VULNERABILITY_NEGATIVE:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 50)
                    newName = sName+" of the Planes";
                else
                    newName = "The Angelic "+sName;
            }
            else
            {
                if (rand < 50)
                    newName = sName + GenderedString(" Planarny", " Planarna", nGender);
                else
                    newName = GenderedString("Anielski ", "Anielska ", nGender) + sName;
            }
            break;
        case CHAMPION_DMG_BOOST_SONIC:
        case CHAMPION_IMMUNITY_SONIC:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 50)
                    newName = sName+" of Sound";
                else
                    newName = "The Singing "+sName;
            }
            else
            {
                if (rand < 50)
                    newName = sName + " od D�wi�ku";
                else
                    newName = GenderedString("�piewaj�cy ", "�piewaj�ca ", nGender) + sName;
            }
            break;
        case CHAMPION_FORTITUDE_BOOST:
        case CHAMPION_FORTITUDE_BOOST_REFLEX_PENALTY:
        case CHAMPION_FORTITUDE_BOOST_WILL_PENALTY:
        case CHAMPION_FORTITUDE_BOOST_OTHERS_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 50)
                    newName = sName+" the Rock";
                else
                    newName = "The Firm "+sName;
            }
            else
            {
                if (rand < 50)
                    newName = sName + " \"Ska�a\"";
                else
                    newName = GenderedString("Wytrwa�y ", "Wytrwa�a ", nGender) + sName;
            }
            break;
        case CHAMPION_REFLEX_BOOST:
        case CHAMPION_REFLEX_BOOST_FORTITUDE_PENALTY:
        case CHAMPION_REFLEX_BOOST_WILL_PENALTY:
        case CHAMPION_REFLEX_BOOST_OTHERS_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 50)
                    newName = sName+" the Nimble";
                else
                    newName = "The Swift "+sName;
            }
            else
            {
                if (rand < 50)
                    newName = sName + GenderedString(" Spr�ysty", " Spr�ysta", nGender);
                else
                    newName = GenderedString("Zwinny ", "Zwinna ", nGender) + sName;
            }
            break;
        case CHAMPION_WILL_BOOST:
        case CHAMPION_WILL_BOOST_FORTITUDE_PENALTY:
        case CHAMPION_WILL_BOOST_REFLEX_PENALTY:
        case CHAMPION_WILL_BOOST_OTHERS_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 50)
                    newName = sName+" the Unhesitating";
                else
                    newName = "The Iron-Willed "+sName;
            }
            else
            {
                if (rand < 50)
                    newName = sName + " \"�elazna Wola\"";
                else
                    newName = GenderedString("Nieugi�ty ", "Nieugi�ta ", nGender) + sName;
            }
            break;
        case CHAMPION_SAVES_BOOST:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 50)
                    newName = sName+" the Hero";
                else
                    newName = "The Blessed "+sName;
            }
            else
            {
                if (rand < 50)
                    newName = sName + " \"Bohater\"";
                else
                    newName = GenderedString("B�ogos�awiony ", "B�ogos�awiona ", nGender) + sName;
            }
            break;
        case CHAMPION_IMMUNITY_PIERCING:
        case CHAMPION_IMMUNITY_SLASHING:
        case CHAMPION_IMMUNITY_BLUDGEONING:
        case CHAMPION_IMMUNITY_PHYSICAL:
        case CHAMPION_REDUCTION:
        case CHAMPION_IMMUNITY_PHYSICAL_VULNERABILITY_SONIC:
        case CHAMPION_RESISTANCE_PHYSICAL_AC_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 25)
                    newName = sName+" the Unfazed";
                else if (rand < 50)
                    newName = sName+" the Thick-Skin";
                else if (rand < 75)
                    newName = "The Unwounded "+sName;
                else
                    newName = "The Immune "+sName;
            }
            else
            {
                if (rand < 25)
                    newName = sName + GenderedString(" Niewzruszony", " Niewzruszona", nGender);
                else if (rand < 50)
                    newName = sName + " \"Gruba Sk�ra\"";
                else if (rand < 75)
                    newName = sName + GenderedString("Pancerny ", "Pancerna ", nGender);
                else
                    newName = GenderedString("Niewra�liwy ", "Niewra�liwa ", nGender) + sName;
            }
            break;
        case CHAMPION_REGENERATION:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 25)
                    newName = sName+" the Growing";
                else if (rand < 50)
                    newName = sName+" the Regenerating";
                else if (rand < 75)
                    newName = "The Healthy "+sName;
                else
                    newName = "The Living "+sName;
            }
            else
            {
                if (rand < 25)
                    newName = sName + GenderedString(" Rosn�cy", " Rosn�ca", nGender);
                else if (rand < 50)
                    newName = sName + " od Regeneracji";
                else if (rand < 75)
                    newName = GenderedString("Zdrowy ", "Zdrowa ", nGender) + sName;
                else
                    newName = GenderedString("�yj�cy ", "�yj�ca ", nGender) + sName;
            }
            break;
        case CHAMPION_SPELL_RESISTANCE:
            if (lang == LANGUAGE_ENGLISH)
            {
                if (rand < 50)
                    newName = sName+" the Spell Breaker";
                else
                    newName = sName+" the Mundane";
            }
            else
            {
                if (rand < 50)
                    newName = sName + " \"�amacz Zakl��\"";
                else
                    newName = sName + GenderedString(" Przyziemny", " Przyziemna", nGender);
            }
            break;
    }

    return newName;
}

string AddRandomTitleToName(string sName, int nModifier, string sSeedName, int nGender=GENDER_MALE)
{
    string newName;
    int rand = RandomNext(50, sSeedName);
    int lang = GetServerLanguage();

    switch (rand)
    {
        case 0:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Annoyed";
            else
                newName = sName + GenderedString(" Zirytowany", " Zirytowana", nGender);
            break;
        case 1:
            if (lang == LANGUAGE_ENGLISH)
                newName = "Cruel "+sName;
            else
                newName = GenderedString("Okrutny ", "Okrutna ", nGender) + sName;
            break;
        case 2:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Smiling";
            else
                newName = sName + GenderedString(" U�miechni�ty", " U�miechni�ta", nGender);
            break;
        case 3:
            if (lang == LANGUAGE_ENGLISH)
                newName = "Mad "+sName;
            else
                newName = GenderedString("Szalony ", "Szalona ", nGender) + sName;
            break;
        case 4:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Weird";
            else
                newName = sName + GenderedString(" \"Dziwak\"", " \"Dziwaczka\"", nGender);
            break;
        case 5:
            if (lang == LANGUAGE_ENGLISH)
                newName = "Black "+sName;
            else
                newName = GenderedString("Czarny ", "Czarna ", nGender) + sName;
            break;
        case 6:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Faithless";
            else
                newName = sName + GenderedString(" Niewierny", " Niewierna", nGender);
            break;
        case 7:
            if (lang == LANGUAGE_ENGLISH)
                newName = "White "+sName;
            else
                newName = GenderedString("Bia�y ", "Bia�a ", nGender) + sName;
            break;
        case 8:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Fearless";
            else
                newName = sName + GenderedString(" Nieustraszony", " Nieustraszona", nGender);
            break;
        case 9:
            if (lang == LANGUAGE_ENGLISH)
                newName = "Crazy "+sName;
            else
                newName = GenderedString("Zwariowany ", "Zwariowana ", nGender) + sName;
            break;
        case 10:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Guardian";
            else
                newName = sName + GenderedString(" \"Stra�nik\"", " \"Stra�niczka\"", nGender);
            break;
        case 11:
            if (lang == LANGUAGE_ENGLISH)
                newName = "Sniffing "+sName;
            else
                newName = GenderedString("W�sz�cy ", "W�sz�ca ", nGender) + sName;
            break;
        case 12:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Bloodied";
            else
                newName = sName + GenderedString(" Zakrwawiony", " Zakrwawiona", nGender);
            break;
        case 13:
            if (lang == LANGUAGE_ENGLISH)
                newName = "Squalid "+sName;
            else
                newName = GenderedString("Plugawy ", "Plugawa ", nGender) + sName;
            break;
        case 14:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Great";
            else
                newName = sName + GenderedString(" Wielki", " Wielka", nGender);
            break;
        case 15:
            if (lang == LANGUAGE_ENGLISH)
                newName = "Little "+sName;
            else
                newName = GenderedString("Ma�y ", "Ma�a ", nGender) + sName;
            break;
        case 16:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Fang";
            else
                newName = sName + GenderedString(" \"Kie�\"", " \"Kie�\"", nGender);
            break;
        case 17:
            if (lang == LANGUAGE_ENGLISH)
                newName = "The Poisonous "+sName;
            else
                newName = GenderedString("Truj�cy ", "Truj�ca ", nGender) + sName;
            break;
        case 18:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Reaper";
            else
                newName = sName + GenderedString(" \"�niwiarz\"", " \"�niwiarz\"", nGender);
            break;
        case 19:
            if (lang == LANGUAGE_ENGLISH)
                newName = "Crimson "+sName;
            else
                newName = GenderedString("Karmazynowy ", "Karmazynowa ", nGender) + sName;
            break;
        case 20:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Avenger";
            else
                newName = sName + GenderedString(" \"M�ciciel\"", " \"M�cicielka\"", nGender);
            break;
        case 21:
            if (lang == LANGUAGE_ENGLISH)
                newName = "Sweet "+sName;
            else
                newName = GenderedString("S�odki ", "S�odka ", nGender) + sName;
            break;
        case 22:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Elder";
            else
                newName = sName + GenderedString(" Stary", " Stara", nGender);
            break;
        case 23:
            if (lang == LANGUAGE_ENGLISH)
                newName = "Sad "+sName;
            else
                newName = GenderedString("Smutny ", "Smutna ", nGender) + sName;
            break;
        case 24:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Beast";
            else
                newName = sName + GenderedString(" \"Bestia\"", " \"Bestia\"", nGender);
            break;
        case 25:
            if (lang == LANGUAGE_ENGLISH)
                newName = "The Wild "+sName;
            else
                newName = GenderedString("Zdzicza�y ", "Zdzicza�a ", nGender) + sName;
            break;
        case 26:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Prophet";
            else
                newName = sName + GenderedString(" \"Prorok\"", " \"Wyrocznia\"", nGender);
            break;
        case 27:
            if (lang == LANGUAGE_ENGLISH)
                newName = "Flegmatic "+sName;
            else
                newName = GenderedString("Flegmatyczny ", "Flegmatyczna ", nGender) + sName;
            break;
        case 28:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Brave";
            else
                newName = sName + GenderedString(" Odwa�ny", " Odwa�na", nGender);
            break;
        case 29:
            if (lang == LANGUAGE_ENGLISH)
                newName = "The Sick "+sName;
            else
                newName = GenderedString("Chory ", "Chora ", nGender) + sName;
            break;
        case 30:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Corpse Eater";
            else
                newName = sName + GenderedString(" \"Po�eracz Cia�\"", " \"Po�eraczka Cia�\"", nGender);
            break;
        case 31:
            if (lang == LANGUAGE_ENGLISH)
                newName = "The Hidden "+sName;
            else
                newName = GenderedString("Ukryty ", "Ukryta ", nGender) + sName;
            break;
        case 32:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Abandoned";
            else
                newName = sName + GenderedString(" Porzucony", " Porzucona", nGender);
            break;
        case 33:
            if (lang == LANGUAGE_ENGLISH)
                newName = "The Travelling "+sName;
            else
                newName = GenderedString("Podr�uj�cy ", "Podr�uj�ca ", nGender) + sName;
            break;
        case 34:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Fanatic";
            else
                newName = sName + GenderedString(" \"Fanatyk\"", " \"Fanatyczka\"", nGender);
            break;
        case 35:
            if (lang == LANGUAGE_ENGLISH)
                newName = "The Dreaming "+sName;
            else
                newName = GenderedString("�ni�cy ", "�ni�ca ", nGender) + sName;
            break;
        case 36:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Claw";
            else
                newName = sName + GenderedString(" \"Pazur\"", " \"Pazur\"", nGender);
            break;
        case 37:
            if (lang == LANGUAGE_ENGLISH)
                newName = "The Laughing "+sName;
            else
                newName = GenderedString("Roze�miany ", "Roze�miana ", nGender) + sName;
            break;
        case 38:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Younger";
            else
                newName = sName + GenderedString(" M�odszy", " M�odsza", nGender);
            break;
        case 39:
            if (lang == LANGUAGE_ENGLISH)
                newName = "The Crying "+sName;
            else
                newName = GenderedString("P�acz�cy ", "P�acz�ca ", nGender) + sName;
            break;
        case 40:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Hunter";
            else
                newName = sName + GenderedString(" \"�owca\"", " \"�owczyni\"", nGender);
            break;
        case 41:
            if (lang == LANGUAGE_ENGLISH)
                newName = "The Pale "+sName;
            else
                newName = GenderedString("Blady ", "Blada ", nGender) + sName;
            break;
        case 42:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Mystical";
            else
                newName = sName + GenderedString(" Mistyczny", " Mistyczna", nGender);
            break;
        case 43:
            if (lang == LANGUAGE_ENGLISH)
                newName = "Lazy "+sName;
            else
                newName = GenderedString("Leniwy ", "Leniwa ", nGender) + sName;
            break;
        case 44:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Blade";
            else
                newName = sName + GenderedString(" \"Ostrze\"", " \"Ostrze\"", nGender);
            break;
        case 45:
            if (lang == LANGUAGE_ENGLISH)
                newName = "The Wise "+sName;
            else
                newName = GenderedString("M�dry ", "M�dra ", nGender) + sName;
            break;
        case 46:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Coward";
            else
                newName = sName + GenderedString(" Tch�rzliwy", " Tch�rzliwa", nGender);
            break;
        case 47:
            if (lang == LANGUAGE_ENGLISH)
                newName = "The Stubborn "+sName;
            else
                newName = GenderedString("Uparty ", "Uparta ", nGender) + sName;
            break;
        case 48:
            if (lang == LANGUAGE_ENGLISH)
                newName = sName+" the Forgotten";
            else
                newName = sName + GenderedString(" Zapomniany", " Zapomniana", nGender);
            break;
        case 49:
            if (lang == LANGUAGE_ENGLISH)
                newName = "Nervous "+sName;
            else
                newName = GenderedString("Nerwowy ", "Nerwowa ", nGender) + sName;
            break;
    }

    return newName;
}

string GetModifierRumorString(string sName, int nModifier, int nGender=GENDER_MALE)
{
    string rumorString;
    int lang = GetServerLanguage();

    switch (nModifier)
    {
        case CHAMPION_AC_BOOST:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is very difficult to hit";
            }
            else
            {
                rumorString = GenderedString("jest trudny do trafienia", "jest trudna do trafienia", nGender);
            }
            break;
        case CHAMPION_AC_BOOST_ATTACK_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is very difficult to hit, but also does not hit very often";
            }
            else
            {
                rumorString = GenderedString("jest trudny do trafienia, ale sam r�wnie� ma problemy z trafianiem", "jest trudna do trafienia, ale sama r�wnie� na problemy z trafianiem", nGender);
            }
            break;
        case CHAMPION_ATK_BOOST:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "knows how to land a hit with almost every attack";
            }
            else
            {
                rumorString = "atakuje tak skutecznie, �e prawie nie spos�b tych atak�w unikn��";
            }
            break;
        case CHAMPION_ATTACK_BOOST_AC_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "knows how to land a hit with almost every attack, but is also easy to hit";
            }
            else
            {
                rumorString = GenderedString("atakuje tak skutecznie, �e prawie nie spos�b tych atak�w unikn��, ale sam te� jest �atwy do trafienia", "atakuje tak skutecznie, �e prawie nie spos�b tych atak�w unikn��, ale sama te� jest �atwa do trafienia", nGender);
            }
            break;
        case CHAMPION_HASTE:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is extremely fast";
            }
            else
            {
                rumorString = GenderedString("jest niesamowicie szybki", "jest niesamowicie szybka", nGender);
            }
            break;
        case CHAMPION_DMG_BOOST_MAGICAL:
        case CHAMPION_DMG_BOOST_FIRE:
        case CHAMPION_DMG_BOOST_ELECTRICAL:
        case CHAMPION_DMG_BOOST_COLD:
        case CHAMPION_DMG_BOOST_ACID:
        case CHAMPION_DMG_BOOST_NEGATIVE:
        case CHAMPION_DMG_BOOST_DIVINE:
        case CHAMPION_DMG_BOOST_SONIC:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "deals a lot of damage with their attacks";
            }
            else
            {
                rumorString = "zadaje swoimi atakami bardzo wysokie obra�enia";
            }
            break;
        case CHAMPION_FORTITUDE_BOOST:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "has great fortitude";
            }
            else
            {
                rumorString = GenderedString("jest bardzo wytrzyma�y", "jest bardzo wytrzyma�a", nGender);
            }
            break;
        case CHAMPION_FORTITUDE_BOOST_REFLEX_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "has great fortitude, but fairly bad reflexes";
            }
            else
            {
                rumorString = GenderedString("jest bardzo wytrzyma�y, ale prawie pozbawiony refleksu", "jest bardzo wytrzyma�a, ale prawie pozbawiona refleksu", nGender);
            }
            break;
        case CHAMPION_FORTITUDE_BOOST_WILL_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "has great fortitude, but their mind is weak";
            }
            else
            {
                rumorString = GenderedString("jest bardzo wytrzyma�y, ale s�aby na umy�le", "jest bardzo wytrzyma�a, ale s�aba na umy�le", nGender);
            }
            break;
        case CHAMPION_FORTITUDE_BOOST_OTHERS_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "has great fortitude, but underwhelming reflexes and strength of will";
            }
            else
            {
                rumorString = GenderedString("jest bardzo wytrzyma�y, ale ma kiepski refleks i r�wnie s�ab� wol�", "jest bardzo wytrzyma�a, ale ma kiepski refleks i r�wnie s�ab� wol�", nGender);
            }
            break;
        case CHAMPION_WILL_BOOST:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "has great strength of will";
            }
            else
            {
                rumorString = "cechuje si� wielk� si�� woli";
            }
            break;
        case CHAMPION_WILL_BOOST_FORTITUDE_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "has great strength of will, but their body is fragile";
            }
            else
            {
                rumorString = "cechuje si� wielk� si�� woli, ale w�t�ym cia�em";
            }
            break;
        case CHAMPION_WILL_BOOST_REFLEX_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "has great strength of will, but terrible reflexes";
            }
            else
            {
                rumorString = "cechuje si� wielk� si�� woli, ale fatalnym refleksem";
            }
            break;
        case CHAMPION_WILL_BOOST_OTHERS_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "has great strength of will, but their underwhelming fortitude and reflexes are their weakness";
            }
            else
            {
                rumorString = GenderedString("cechuje si� wielk� si�� woli, ale jego s�abo�ci� s� niska wytrzyma�o�� i s�aby refleks", "cechuje si� wielk� si�� woli, ale jej s�abo�ci� s� niska wytrzyma�o�� i s�aby refleks", nGender);
            }
            break;
        case CHAMPION_REFLEX_BOOST:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "has amazing reflexes";
            }
            else
            {
                rumorString = "ma fenomenalny refleks";
            }
            break;
        case CHAMPION_REFLEX_BOOST_FORTITUDE_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "has amazing reflexes, but their fortitude is lacking";
            }
            else
            {
                rumorString = "ma fenomenalny refleks, ale wytrzyma�o�� poni�ej przeci�tnej";
            }
            break;
        case CHAMPION_REFLEX_BOOST_WILL_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "has amazing reflexes, but weak resolve";
            }
            else
            {
                rumorString = "ma fenomenalny refleks, ale s�aby umys�";
            }
            break;
        case CHAMPION_REFLEX_BOOST_OTHERS_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "has amazing reflexes, but can't boast either good fortitude nor impressive willpower";
            }
            else
            {
                rumorString = "ma fenomenalny refleks, ale nie mo�e si� pochwali� ani wysok� wytrzyma�o�ci�, ani si�� woli";
            }
            break;
        case CHAMPION_SAVES_BOOST:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "boasts not only fortified mind, but also good reflexes and fortitude";
            }
            else
            {
                rumorString = "mo�e si� pochwali� nie tylko siln� wol�, ale r�wnie� dobrym refleksem i wytrzyma�o�ci�";
            }
            break;
        case CHAMPION_IMMUNITY_PIERCING:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to piercing damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia k�ute", "jest niewra�liwa na obra�enia k�ute", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_SLASHING:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to slashing damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia ci�te", "jest niewra�liwa na obra�enia ci�te", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_BLUDGEONING:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to bludgeoning damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia obuchowe", "jest niewra�liwa na obra�enia obuchowe", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_PHYSICAL:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to all kinds of physical damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na wszystkie rodzaje obra�e� fizyczne", "jest niewra�liwa na wszystkie rodzaje obra�e� fizyczne", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_PHYSICAL_VULNERABILITY_SONIC:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to all kinds of physical damage, but vulnerable to sonic damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na wszystkie rodzaje obra�e� fizyczne, ale podatny na obra�enia od d�wi�ku", "jest niewra�liwa na wszystkie rodzaje obra�e� fizyczne, ale podatna na obra�enia od d�wi�ku", nGender);
            }
            break;
        case CHAMPION_REDUCTION:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "has damage reduction that is difficult to get through with your attacks";
            }
            else
            {
                rumorString = "posiada redukcj� obra�e�, przez kt�r� trudno jest si� przebi� twoimi atakami";
            }
            break;
        case CHAMPION_IMMUNITY_MAGICAL:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to magical damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia od magii", "jest niewra�liwa na obra�enia od magii", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_FIRE:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to fire damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia od ognia", "jest niewra�liwa na obra�enia od ognia", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_FIRE_VULNERABILITY_COLD:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to fire damage, but vulnerable to cold damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia od ognia, ale podatny na obra�enia od zimna", "jest niewra�liwa na obra�enia od ognia, ale podatna na obra�enia od zimna", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_ELECTRICAL:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to electrical damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia od elektryczno�ci", "jest niewra�liwa na obra�enia od elektryczno�ci", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_ELECTRICAL_VULNERABILITY_ACID:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to electrical damage, but vulnerable to acid damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia od elektryczno�ci, ale podatny na obra�enia od kwasu", "jest niewra�liwa na obra�enia od elektryczno�ci, ale podatna na obra�enia od kwasu", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_COLD:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to cold damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia od zimna", "jest niewra�liwa na obra�enia od zimna", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_COLD_VULNERABILITY_FIRE:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to cold damage, but vulnerable to fire damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia od zimna, ale podatny na obra�enia od ognia", "jest niewra�liwa na obra�enia od zimna, ale podatna na obra�enia od ognia", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_ACID:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to acid damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia od kwasu", "jest niewra�liwa na obra�enia od kwasu", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_ACID_VULNERABILITY_ELECTRICAL:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to acid damage, but vulnerable to electrical damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia od kwasu, ale podatny na obra�enia od elektryczno�ci", "jest niewra�liwa na obra�enia od kwasu, ale podatna na obra�enia od elektryczno�ci", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_NEGATIVE:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to negative energy damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia od energii negatywnej", "jest niewra�liwa na obra�enia od energii negatywnej", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_NEGATIVE_VULNERABILITY_DIVINE:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to negative energy damage, but vulnerable to divine damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia od energii negatywnej, ale podatny na boskie obra�enia", "jest niewra�liwa na obra�enia od energii negatywnej, ale podatna na boskie obra�enia", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_DIVINE:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to divine damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na boskie obra�enia", "jest niewra�liwa na boskie obra�enia", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_DIVINE_VULNERABILITY_NEGATIVE:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to divine damage, but vulnerable to negative energy damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na boskie obra�enia, ale podatny na obra�enia od energii negatywnej", "jest niewra�liwa na boskie obra�enia, ale podatna na obra�enia od energii negatywnej", nGender);
            }
            break;
        case CHAMPION_IMMUNITY_SONIC:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is immune to sonic damage";
            }
            else
            {
                rumorString = GenderedString("jest niewra�liwy na obra�enia od d�wi�ku", "jest niewra�liwa na obra�enia od d�wi�ku", nGender);
            }
            break;
        case CHAMPION_REGENERATION:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "regenerates their wounds very quickly";
            }
            else
            {
                rumorString = "bardzo szybko regeneruje swoje zdrowie";
            }
            break;
        case CHAMPION_SPELL_RESISTANCE:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "has unusual resistance to spells cast at them";
            }
            else
            {
                rumorString = GenderedString("cechuje si� zadziwiaj�c� odporno�ci� na zakl�cia, kt�re spr�bujesz na niego rzuci�", "cechuje si� zadziwiaj�c� odporno�ci� na zakl�cia, kt�re spr�bujesz na ni� rzuci�", nGender);
            }
            break;
        case CHAMPION_RESISTANCE_PHYSICAL_AC_PENALTY:
            if (lang == LANGUAGE_ENGLISH)
            {
                rumorString = "is resistant to physical damage, but easy easy to hit";
            }
            else
            {
                rumorString = GenderedString("jest odporny na obra�enia fizyczne, ale �atwy do trafienia", "jest odporna na obra�enia fizyczne, ale �atwa do trafienia", nGender);
            }
            break;
    }

    return rumorString;
}

void ApplyModifierToChampion(object oChampion, int nModifier, int nLevel)
{
    effect buff;
    if (nModifier == CHAMPION_AC_BOOST)
    {
        buff = EffectACIncrease(5);
    }
    else if (nModifier == CHAMPION_ATK_BOOST)
    {
        buff = EffectAttackIncrease(5);
    }
    else if (nModifier == CHAMPION_HASTE)
    {
        buff = EffectHaste();
    }
    else if (nModifier == CHAMPION_DMG_BOOST_MAGICAL)
    {
        buff = EffectDamageIncrease(IPGetDamageBonusConstantFromNumber(nLevel*2), DAMAGE_TYPE_MAGICAL);
    }
    else if (nModifier == CHAMPION_DMG_BOOST_ACID)
    {
        buff = EffectDamageIncrease(IPGetDamageBonusConstantFromNumber(nLevel*2), DAMAGE_TYPE_ACID);
    }
    else if (nModifier == CHAMPION_DMG_BOOST_ELECTRICAL)
    {
        buff = EffectDamageIncrease(IPGetDamageBonusConstantFromNumber(nLevel*2), DAMAGE_TYPE_ELECTRICAL);
    }
    else if (nModifier == CHAMPION_DMG_BOOST_COLD)
    {
        buff = EffectDamageIncrease(IPGetDamageBonusConstantFromNumber(nLevel*2), DAMAGE_TYPE_COLD);
    }
    else if (nModifier == CHAMPION_DMG_BOOST_DIVINE)
    {
        buff = EffectDamageIncrease(IPGetDamageBonusConstantFromNumber(nLevel*2), DAMAGE_TYPE_DIVINE);
    }
    else if (nModifier == CHAMPION_DMG_BOOST_FIRE)
    {
        buff = EffectDamageIncrease(IPGetDamageBonusConstantFromNumber(nLevel*2), DAMAGE_TYPE_FIRE);
    }
    else if (nModifier == CHAMPION_DMG_BOOST_NEGATIVE)
    {
        buff = EffectDamageIncrease(IPGetDamageBonusConstantFromNumber(nLevel*2), DAMAGE_TYPE_NEGATIVE);
    }
    else if (nModifier == CHAMPION_DMG_BOOST_SONIC)
    {
        buff = EffectDamageIncrease(IPGetDamageBonusConstantFromNumber(nLevel*2), DAMAGE_TYPE_SONIC);
    }
    else if (nModifier == CHAMPION_FORTITUDE_BOOST)
    {
        buff = EffectSavingThrowIncrease(SAVING_THROW_FORT, 10);
    }
    else if (nModifier == CHAMPION_REFLEX_BOOST)
    {
        buff = EffectSavingThrowIncrease(SAVING_THROW_REFLEX, 10);
    }
    else if (CHAMPION_WILL_BOOST)
    {
        buff = EffectSavingThrowIncrease(SAVING_THROW_WILL, 10);
    }
    else if (nModifier == CHAMPION_IMMUNITY_PIERCING)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_PIERCING, 75);
    }
    else if (nModifier == CHAMPION_IMMUNITY_SLASHING)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_SLASHING, 75);
    }
    else if (nModifier == CHAMPION_IMMUNITY_BLUDGEONING)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_BLUDGEONING, 75);
    }
    else if (nModifier == CHAMPION_IMMUNITY_PHYSICAL)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_BLUDGEONING, 40);
        buff = EffectLinkEffects(EffectDamageImmunityIncrease(DAMAGE_TYPE_SLASHING, 40), buff);
        buff = EffectLinkEffects(EffectDamageImmunityIncrease(DAMAGE_TYPE_PIERCING, 40), buff);
    }
    else if (nModifier == CHAMPION_REDUCTION)
    {
        int amount = nLevel;
        int power = (nLevel) / 3;
        if (nLevel % 3 > 0)
            power++;

        buff = EffectDamageReduction(amount, IPGetDamagePowerConstantFromNumber(power));
    }
    else if (nModifier == CHAMPION_IMMUNITY_FIRE)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_FIRE, 75);
    }
    else if (nModifier == CHAMPION_IMMUNITY_SONIC)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_SONIC, 75);
    }
    else if (nModifier == CHAMPION_IMMUNITY_MAGICAL)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_MAGICAL, 75);
    }
    else if (nModifier == CHAMPION_IMMUNITY_ACID)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_ACID, 75);
    }
    else if (nModifier == CHAMPION_IMMUNITY_ELECTRICAL)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_ELECTRICAL, 75);
    }
    else if (nModifier == CHAMPION_IMMUNITY_COLD)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_COLD, 75);
    }
    else if (nModifier == CHAMPION_IMMUNITY_DIVINE)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_DIVINE, 75);
    }
    else if (nModifier == CHAMPION_IMMUNITY_NEGATIVE)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_NEGATIVE, 75);
    }
    else if (nModifier == CHAMPION_SAVES_BOOST)
    {
        buff = EffectSavingThrowIncrease(SAVING_THROW_ALL, 5);
    }
    else if (nModifier == CHAMPION_REGENERATION)
    {
        int regen = GetMaxHitPoints(oChampion) / 5;
        if (regen < 1)
            regen = 1;
        buff = EffectRegenerate(regen, 6.0f);
    }
    else if (nModifier == CHAMPION_SPELL_RESISTANCE)
    {
        int sr = 13 + nLevel;
        buff = EffectSpellResistanceIncrease(sr);
    }
    else if (nModifier == CHAMPION_FORTITUDE_BOOST_REFLEX_PENALTY)
    {
        buff = EffectSavingThrowIncrease(SAVING_THROW_FORT, 15);
        buff = EffectLinkEffects(EffectSavingThrowDecrease(SAVING_THROW_REFLEX, 5), buff);
    }
    else if (nModifier == CHAMPION_FORTITUDE_BOOST_WILL_PENALTY)
    {
        buff = EffectSavingThrowIncrease(SAVING_THROW_FORT, 15);
        buff = EffectLinkEffects(EffectSavingThrowDecrease(SAVING_THROW_WILL, 5), buff);
    }
    else if (nModifier == CHAMPION_FORTITUDE_BOOST_OTHERS_PENALTY)
    {
        buff = EffectSavingThrowIncrease(SAVING_THROW_FORT, 20);
        buff = EffectLinkEffects(EffectSavingThrowDecrease(SAVING_THROW_WILL, 5), buff);
        buff = EffectLinkEffects(EffectSavingThrowDecrease(SAVING_THROW_REFLEX, 5), buff);
    }
    else if (nModifier == CHAMPION_REFLEX_BOOST_FORTITUDE_PENALTY)
    {
        buff = EffectSavingThrowIncrease(SAVING_THROW_REFLEX, 15);
        buff = EffectLinkEffects(EffectSavingThrowDecrease(SAVING_THROW_FORT, 5), buff);
    }
    else if (nModifier == CHAMPION_REFLEX_BOOST_WILL_PENALTY)
    {
        buff = EffectSavingThrowIncrease(SAVING_THROW_REFLEX, 15);
        buff = EffectLinkEffects(EffectSavingThrowDecrease(SAVING_THROW_WILL, 5), buff);
    }
    else if (nModifier == CHAMPION_REFLEX_BOOST_OTHERS_PENALTY)
    {
        buff = EffectSavingThrowIncrease(SAVING_THROW_REFLEX, 20);
        buff = EffectLinkEffects(EffectSavingThrowDecrease(SAVING_THROW_WILL, 5), buff);
        buff = EffectLinkEffects(EffectSavingThrowDecrease(SAVING_THROW_FORT, 5), buff);
    }
    else if (nModifier == CHAMPION_WILL_BOOST_FORTITUDE_PENALTY)
    {
        buff = EffectSavingThrowIncrease(SAVING_THROW_WILL, 15);
        buff = EffectLinkEffects(EffectSavingThrowDecrease(SAVING_THROW_FORT, 5), buff);
    }
    else if (nModifier == CHAMPION_WILL_BOOST_REFLEX_PENALTY)
    {
        buff = EffectSavingThrowIncrease(SAVING_THROW_WILL, 15);
        buff = EffectLinkEffects(EffectSavingThrowDecrease(SAVING_THROW_REFLEX, 5), buff);
    }
    else if (nModifier == CHAMPION_WILL_BOOST_OTHERS_PENALTY)
    {
        buff = EffectSavingThrowIncrease(SAVING_THROW_WILL, 20);
        buff = EffectLinkEffects(EffectSavingThrowDecrease(SAVING_THROW_REFLEX, 5), buff);
        buff = EffectLinkEffects(EffectSavingThrowDecrease(SAVING_THROW_FORT, 5), buff);
    }
    else if (nModifier == CHAMPION_IMMUNITY_FIRE_VULNERABILITY_COLD)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_FIRE, 100);
        buff = EffectLinkEffects(EffectDamageImmunityDecrease(DAMAGE_TYPE_COLD, 50), buff);
    }
    else if (nModifier == CHAMPION_IMMUNITY_COLD_VULNERABILITY_FIRE)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_COLD, 100);
        buff = EffectLinkEffects(EffectDamageImmunityDecrease(DAMAGE_TYPE_FIRE, 50), buff);
    }
    else if (nModifier == CHAMPION_IMMUNITY_DIVINE_VULNERABILITY_NEGATIVE)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_DIVINE, 100);
        buff = EffectLinkEffects(EffectDamageImmunityDecrease(DAMAGE_TYPE_NEGATIVE, 50), buff);
    }
    else if (nModifier == CHAMPION_IMMUNITY_NEGATIVE_VULNERABILITY_DIVINE)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_NEGATIVE, 100);
        buff = EffectLinkEffects(EffectDamageImmunityDecrease(DAMAGE_TYPE_DIVINE, 50), buff);
    }
    else if (nModifier == CHAMPION_IMMUNITY_ACID_VULNERABILITY_ELECTRICAL)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_ACID, 100);
        buff = EffectLinkEffects(EffectDamageImmunityDecrease(DAMAGE_TYPE_ELECTRICAL, 50), buff);
    }
    else if (nModifier == CHAMPION_IMMUNITY_ELECTRICAL_VULNERABILITY_ACID)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_ELECTRICAL, 100);
        buff = EffectLinkEffects(EffectDamageImmunityDecrease(DAMAGE_TYPE_ACID, 50), buff);
    }
    else if (nModifier == CHAMPION_IMMUNITY_PHYSICAL_VULNERABILITY_SONIC)
    {
        buff = EffectDamageImmunityIncrease(DAMAGE_TYPE_PIERCING, 70);
        buff = EffectLinkEffects(EffectDamageImmunityIncrease(DAMAGE_TYPE_BLUDGEONING, 70), buff);
        buff = EffectLinkEffects(EffectDamageImmunityIncrease(DAMAGE_TYPE_SLASHING, 70), buff);
        buff = EffectLinkEffects(EffectDamageImmunityDecrease(DAMAGE_TYPE_SONIC, 75), buff);
    }
    else if (nModifier == CHAMPION_RESISTANCE_PHYSICAL_AC_PENALTY)
    {
        int amount = nLevel * 2 / 3;
        if (amount < 1)
            amount = 1;

        buff = EffectDamageResistance(DAMAGE_TYPE_PIERCING, amount);
        buff = EffectLinkEffects(EffectDamageResistance(DAMAGE_TYPE_BLUDGEONING, amount), buff);
        buff = EffectLinkEffects(EffectDamageResistance(DAMAGE_TYPE_SLASHING, amount), buff);
        buff = EffectLinkEffects(EffectACDecrease(5), buff);
    }
    else if (nModifier == CHAMPION_AC_BOOST_ATTACK_PENALTY)
    {
        buff = EffectACIncrease(10);
        buff = EffectLinkEffects(EffectAttackDecrease(5), buff);
    }
    else if (nModifier == CHAMPION_ATK_BOOST)
    {
        buff = EffectAttackIncrease(10);
        buff = EffectLinkEffects(EffectACDecrease(5), buff);
    }
    else
        LogWarning("Invalid champion modifier: %n (inc_modifiers, ApplyModifierToChampion)", nModifier);

    buff = SupernaturalEffect(buff);
    ApplyEffectToObject(DURATION_TYPE_PERMANENT, buff, oChampion);
}