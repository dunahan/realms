#include "inc_random"
#include "hnd_realms"

//////////////////
// ------------ //
// Realm script //
// ------------ //
//////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "rea_realm";

// Number of available overworld biomes
int overworldBiomeNumber = 1;

// Number of available plots (every PC level must be supported by at least one plot)
int plotsNumber = 1;

// Starting level of the first region in the realm
int startingLevel = 1;

// Number of levels player(s) should gain by traversing a single region
int levelsPerRegion = 1;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available overworld biome script names for nIndex in range [0, overworldBiomeNumber-1]
string OnGetOverworldBiomeScript(int nIndex)
{
    switch (nIndex)
    {
        case 0: return "bio_rural";
    }
    return "";
}

// Function that should return a random realm's name.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
string OnGetRandomRealmName(string sSeedName)
{
    switch (RandomNext(101, sSeedName))
    {
        case 0: return "Ancordia";
        case 1: return "Tamoria";
        case 2: return "Kalvenia";
        case 3: return "Pentragas";
        case 4: return "Queiland";
        case 5: return "Vosmon";
        case 6: return "Froiya";
        case 7: return "Thaubar";
        case 8: return "Ewhil";
        case 9: return "Eglain";
        case 10: return "Glaor Spar";
        case 11: return "Befristan";
        case 12: return "Pruacia";
        case 13: return "Glaylor";
        case 14: return "Ugrua";
        case 15: return "Ufristan";
        case 16: return "Traor Chyae";
        case 17: return "Graif When";
        case 18: return "Voscus";
        case 19: return "Kablium";
        case 20: return "Thiobar";
        case 21: return "Eflium";
        case 22: return "Druen Swein";
        case 23: return "Tabrua";
        case 24: return "Tesmen";
        case 25: return "Gecla";
        case 26: return "Fashiesia";
        case 27: return "Sheuya";
        case 28: return "Aplana";
        case 29: return "Ecreana";
        case 30: return "Wospainia";
        case 31: return "Hachaora";
        case 32: return "Aswela";
        case 33: return "Shaui Prua";
        case 34: return "Glea Blon";
        case 35: return "Xuthen";
        case 36: return "Whaburg";
        case 37: return "Skoussau";
        case 38: return "Oplar";
        case 39: return "Agrus";
        case 40: return "Fluyl Thyae";
        case 41: return "Prayla";
        case 42: return "Blueton";
        case 43: return "Oglary";
        case 44: return "Ublos";
        case 45: return "Yuglal";
        case 46: return "Ashein";
        case 47: return "Gleustein";
        case 48: return "Ubrad";
        case 49: return "Eclya";
        case 50: return "Shiusal";
        case 51: return "Beshabia";
        case 52: return "Fleitan";
        case 53: return "Jesnein";
        case 54: return "Grolia";
        case 55: return "Abrines";
        case 56: return "Nugliurus";
        case 57: return "Friasal";
        case 58: return "Flenia";
        case 59: return "Tespa";
        case 60: return "Troela";
        case 61: return "Aglya";
        case 62: return "Grusia";
        case 63: return "Etren";
        case 64: return "Aulora";
        case 65: return "Paltrasdin";
        case 66: return "Rannilsa";
        case 67: return "Nilsaran";
        case 68: return "Vul-Eala";
        case 69: return "Rannonna";
        case 70: return "Allosal";
        case 71: return "Gillan";
        case 72: return "Rizthor";
        case 73: return "Thorriz";
        case 74: return "Andralusia";
        case 75: return "Andrahal";
        case 76: return "Szaalzi";
        case 77: return "Hyliassa";
        case 78: return "Hep-Sandra";
        case 79: return "Grandilbala";
        case 80: return "Sirroklo";
        case 81: return "Firolora";
        case 82: return "Sapinur";
        case 83: return "Vamzora";
        case 84: return "Firtilla";
        case 85: return "Hirvamas";
        case 86: return "Zanspire";
        case 87: return "Langillan";
        case 88: return "Nutbola";
        case 89: return "Ransandra";
        case 90: return "Netolbos";
        case 91: return "Filfanta";
        case 92: return "Marholos";
        case 93: return "Tereuros";
        case 94: return "Morlusia";
        case 95: return "Myava";
        case 96: return "Tomuda";
        case 97: return "Galbor";
        case 98: return "Mystwald";
        case 99: return "Miteral";
        case 100: return "Arcedona";
    }
    return "";
}

// Function that should return available plot script name for nIndex in range [0, plotsNumber-1]
string OnGetRealmPlotScript(int nIndex)
{
    switch (nIndex)
    {
        case 0: return "plo_treasure";
    }
    return "";
}

// Function that should return the number of resource units required to craft a wand with the given spell.
// The result should typically depend on the spell's spell level, but one can base it on the particular spell, too, for balance purposes, for example.
int OnGetResourceUnitsRequiredToCraftWand(int nSpell, int nSpellLevel)
{
    return 5;
}

// Function that should return the number of resource units required to brew a potion with the given spell.
// The result should typically depend on the spell's spell level, but one can base it on the particular spell, too, for balance purposes, for example.
int OnGetResourceUnitsRequiredToBrewPotion(int nSpell, int nSpellLevel)
{
    return 5;
}

// Function that should return the number of resource units required to scribe a scroll with the given spell.
// The result should typically depend on the spell's spell level, but one can base it on the particular spell, too, for balance purposes, for example.
int OnGetResourceUnitsRequiredToScribeScroll(int nSpell, int nSpellLevel)
{
    return 5;
}

// Function that should return the number of resource unit an item with tag sItemTag is worth when crafting a wand.
// It should return 0 for all items other than standard crafting components.
int OnGetResourceUnitsOfItemForWandCrafting(string sItemTag)
{
    if (sItemTag == "it_crystal1")
        return 1;
    if (sItemTag == "it_crystal2")
        return 2;
    if (sItemTag == "it_crystal3")
        return 4;
    return 0;
}

// Function that should return the number of resource unit an item with tag sItemTag is worth when brewing a potion.
// It should return 0 for all items other than standard crafting components.
int OnGetResourceUnitsOfItemForPotionBrewing(string sItemTag)
{
    if (sItemTag == "it_herb1")
        return 1;
    if (sItemTag == "it_herb2")
        return 2;
    if (sItemTag == "it_herb3")
        return 4;
    return 0;
}

// Function that should return the number of resource unit an item with tag sItemTag is worth when scribing a scroll.
// It should return 0 for all items other than standard crafting components.
int OnGetResourceUnitsOfItemForScrollScribing(string sItemTag)
{
    if (sItemTag == "it_crystal1")
        return 1;
    if (sItemTag == "it_crystal2")
        return 2;
    if (sItemTag == "it_crystal3")
        return 4;
    return 0;
}