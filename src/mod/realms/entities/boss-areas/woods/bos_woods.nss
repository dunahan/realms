#include "inc_debug"
#include "inc_random"
#include "inc_language"
#include "hnd_bossareas"
#include "inc_biomes"
#include "inc_tiles"

////////////////////////
// ------------------ //
//  Boss area script  //
// ------------------ //
////////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "bos_woods";

// Number of facility area templates (each in 4 variants, one per area exit direction)
int areasNumber = 1;

// Minimum PC level this boss area supports
int minPlayerLevel = 1;

// Maximum PC level this boss area supports
int maxPlayerLevel = 40;

// Number of rumors regarding this boss area (can be 0)
int rumorsNumber = 1;


//////////////////////
// Script functions //
//////////////////////


// Function that should return ResRef of an available area to spawn for nIndex in range [0, areasNumber-1] and provided exits flag 
// (only single-exit areas are valid, i.e. flag values: 1, 2, 4 and 8).
// You can use oBiome to use different variants based on biome (only needed if the boss area is registered in a plot script  multiple biomes).
string OnGetBossAreaResRef(int nIndex, object oBiome, int nExitsFlag)
{
    string result = "woods_";

    switch (nIndex)
    {
        case 0: result += "01"; break;
        default: return "";
    }

    string exitsFlag = nExitsFlag < 10 ? "0" + IntToString(nExitsFlag) : IntToString(nExitsFlag);
    result += "_" + exitsFlag;
    return result;
}

// Function that is executed when the boss area is spawned. Use it to spawn creatures and objects in the boss area,
// create sub-areas and perform all other actions. You can use nBiomeStartingLevel and oBiome to base your special area's contents
// on factors such as expected PC level, townfolk's dominant race (to spawn appropriate NPCs), etc.
// You can also access other biome properties via functions executed on oBiome.
// Remember to call AddAreaToTile(oSpecialArea, newArea) from inc_tiles after creating any new sub-areas of oBossArea.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnInitializeBossArea(string sSeedName, object oBiome, object oBossArea, int nBiomeStartingLevel)
{
    LogInfo("Initializing forest boss area.");
    object wp = GetAreaWaypoint(oBossArea, "wp_testspequest");
    object obj = CreateObject(OBJECT_TYPE_PLACEABLE, "obj_testquestobj", GetLocation(wp));
    SetLocalInt(obj, "BOSS_AREA", TRUE);
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on the boss area instance.
// Boss area rumors will be added only to the final region's rumor pool (i.e. only final region town's NPCs will speak them).
string OnGetBossAreaRumor(object oBossArea, int nIndex, int nLanguage)
{
    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return "There is a boss forest nearby!";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return "Mamy bossowy las nieopodal!";
    }
    return "";
}

// Function that should return a random name for a boss area, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomBossAreaName(string sSeedName, int nLanguage)
{
    if (nLanguage == LANGUAGE_POLISH)
    {
        switch (RandomNext(1))
        {
            case 0: return "Bossowy Las";
        }
    }
    else
    {
        switch (RandomNext(1))
        {
            case 0: return "Boss Woods";
        }
    }
    return "";
}