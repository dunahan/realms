#include "inc_random"
#include "inc_language"
#include "hnd_plots"
#include "inc_plots"
#include "inc_realms"

/////////////////////
// --------------- //
//   Plot script   //
// --------------- //
/////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "";

// Number of boss area scripts that can generate in the plot
int bossAreasNumber = 1;

// Number of encounter types that can generate in the realm with this plot (in any biome generated, provided the plot has encounters for the biome's level; a plot can have 0 encounters)
int encountersNumber = 0;

// Number of rumors regarding this plot (can be 0)
int rumorsNumber = 1;

// Minimum PC level this plot supports
int minPlayerLevel = 1;

// Maximum PC level this plot supports
int maxPlayerLevel = 40;


//////////////////////
// Script functions //
//////////////////////

// Function that should return a a PlotJournalEntries struct for this plot, based on a LANGUAGE_* constant from inc_language and a plot object.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct PlotJournalEntries OnGetPlotJournalEntries(object oPlot, string sSeedName, int nLanguage)
{
    object realm = GetRealm();
    string realmName = GetRealmName(realm);
    struct PlotJournalEntries result;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Poszukiwanie Graala";
        result.journalPlotDescription = "Moja gildia wys�a�a mnie do odleg�ej krainy "+realmName+" w celu odnalezienia staro�ytnego artefaktu znanego jako Graal.";
        result.journalStart = "Dobrym pomys�em przed rozpocz�ciem poszukiwa� b�dzie zdobycie na tych obcych ziemiach troch� niezb�dnego do�wiadczenia i z�ota na sprz�t oraz zaopatrzenie. Mieszka�cy tej wioski z pewno�ci� maj� jakie� zlecenia dla kogo� z moimi umiej�tno�ciami.";
        result.journalFinishedRegionQuests = "Nie ma tu dla mnie wi�cej zada�. Czas uda� si� do innego regionu na tych ziemiach.";
        result.journalFailedRegionQuests = "Nie uda�o mi si� zrealizowa� wszystkich zlece�. Mam nadziej�, �e nie przeszkodzi mi to w mojej misji. Jedyne, co mi zostaje, to uda� si� do innego regionu na tych ziemiach.";
        result.journalReachedNextVillage = "Uda�o mi si� dotrze� do nast�pnej osady. Z pewno�ci� i tutaj mog� zarobi� troch� z�ota, kt�re pomo�e mi w poszukiwaniach Graala.";
        result.journalFinishedLastRegionQuests = "Wszystkie zadania wykonane. Gdzie� niedaleko ukryty jest Graal - czas go odnale�� i stawi� czo�a czemukolwiek, co b�dzie go strzec.";
        result.journalFailedLastRegionQuests = "Nie uda�o mi si� zrealizowa� wszystkich zada�. Nie pozostaje mi jednak nic innego, jak odnale�� Graala i stawi� czo�a czemukolwiek, co b�dzie go strzec.";
    }
    else
    {
        result.journalName = "The Search for the Grail";
        result.journalPlotDescription = "My guild has sent me to the remote realm of "+realmName+" for me to find an ancient artifact known as the Grail.";
        result.journalStart = "I should start by gaining some necessary experience and gold for equipment and provisions in this unknown land. The locals of this village surely have some tasks for somebody with my skills.";
        result.journalFinishedRegionQuests = "There are no more quests for me here. It is time to travel to another region in these lands.";
        result.journalFailedRegionQuests = "I was not able to complete all of the tasks. I hope it will not hinder my mission. All I can do now is leave this place and travel to another region in these lands.";
        result.journalReachedNextVillage = "I have reached the next settlement. I am sure I can earn some gold here as well. Then I will be better prepared to search for the Grail.";
        result.journalFinishedLastRegionQuests = "All that needed to be done is done. The Grail is hidden somewhere nearby - the time has come for me to find it and face whatever may guard it.";
        result.journalFailedLastRegionQuests = "I was not able to complete all of the tasks. However, I have little choice but to press on, find the Grail and face whatever may guard it.";
    }
    return result;
}

// Function that should return available boss area script for nIndex in range [0, bossAreasNumber-1]
string OnGetPlotBossAreaScript(int nIndex)
{
    switch (nIndex)
    {
        case 0: return "bos_woods";
    }
    return "";
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a plot instance;
// Plot rumors are added to the rumor pools of every biome in the generated map
string OnGetPlotRumor(object oPlot, int nIndex, int nLanguage)
{
    object realm = GetRealm();
    string realmName = GetRealmName(realm);

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return "You're here looking for the Grail, aren't you? I can't imagine any other reason to visit "+realmName+".";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return "Pewnie jeste� tu, �eby odnale�� Graala, prawda? "+realmName+" nie ma wiele wi�cej do zaoferowania przybyszom z tak daleka.";
    }
    return "";
}

// Function that should return available encounter script name for nIndex in range [0, encountersNumber-1]
string OnGetPlotEncounterScript(int nIndex)
{
    return "";
}