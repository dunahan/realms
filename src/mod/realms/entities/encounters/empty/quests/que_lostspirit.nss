#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_encounters"
#include "inc_towns"
#include "inc_tiles"
#include "inc_biomes"
#include "inc_common"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_lostspirit";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 1;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    //SCALING
    result.gold = RandomNext(101, sSeedName) + 400;
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Niespokojny duch";
        if (isMale)
            result.journalInitial = "Zmar�y ojciec mojego zleceniodawcy imieniem "+questgiverName+" n�ka jego i reszt� rodziny jako m�ciwy duch. Musz� zabra� wr�czony mi Kamie� Ducha w miejsce jego �mierci i z jego pomoc� przywo�a� ducha i odes�a� go w za�wiaty. "+questgiverName+" b�dzie czeka� w "+townName+" z sum� "+IntToString(questReward.gold)+" sztuk z�ota nagrody.";
        else
            result.journalInitial = "Zmar�y ojciec mojego zleceniodawczyni imieniem "+questgiverName+" n�ka j� i reszt� rodziny jako m�ciwy duch. Musz� zabra� wr�czony mi Kamie� Ducha w miejsce jego �mierci i z jego pomoc� przywo�a� ducha i odes�a� go w za�wiaty. "+questgiverName+" b�dzie czeka� w "+townName+" z sum� "+IntToString(questReward.gold)+" sztuk z�ota nagrody.";
        if (isMale)
            result.journalSuccess = "Allip zosta� odes�any i nie b�dzie ju� sprawia� problem�w. Czas powr�ci� do "+townName+" po "+IntToString(questReward.gold)+" sztuk z�ota, kt�re obieca� mi "+questgiverName+".";
        else
            result.journalSuccess = "Allip zosta� odes�any i nie b�dzie ju� sprawia� problem�w. Czas powr�ci� do "+townName+" po "+IntToString(questReward.gold)+" sztuk z�ota, kt�re obieca�a mi "+questgiverName+".";
        result.journalFailure = "Duch dalej b�dzie n�ka� swoje dziecko, zadanie nie zosta�o wykonane, a drugiej okazji nie b�dzie. "+questgiverName+" z "+townName+" musi si� z tym pogodzi�.";
        result.journalSuccessFinished = "Spotkania z duchami zmar�ych nigdy nie s� najmilszym prze�yciem, ale mam na os�od� sowit� zap�at� i mog� zamkn�� ten rozdzia�.";
        result.journalAbandoned = "Mo�e ten duch ma dobry pow�d, by by� niespokojny? Nie znam ca�ej historii i szczerze m�wi�c nie mam zamiaru si� w ni� miesza�.";
        result.npcIntroLine = "Masz mo�e jakie� do�wiadczenie z niespokojnymi duszami zmar�ych?";
        result.npcContentLine = "Nieszcz�cie spad�o na mnie i mych bliskich... M�j niedawno zmar�y ojciec nie odszed� na drug� stron�, lecz ci�gle powraca i nas nawiedza. Z jakiego� powodu nie mo�e zazna� spokoju.";
        result.npcRewardLine = "Mam przy sobie Kamie� Ducha zakupiony od w�drownego szamana. Podobno je�li kto� uda�by si� z tym kamieniem w miejsce �mierci ducha, m�g�by go przywo�a�. Zgodzisz si� to zrobi� i znale�� spos�b na przywr�cenie mu spokoju? Oferuj� nagrod� w wysoko�ci "+IntToString(questReward.gold)+" sztuk z�ota.";
        result.npcBeforeCompletionLine = "Czy m�j biedny ojciec zazna� ju� spokoju?";
        result.playerReportingSuccess = "Tak, jest ju� po drugiej stronie. Nic nie zak��ci jego odpoczynku.";
        result.playerReportingFailure = "Obawiam si�, �e nie. Musisz liczy� si� z tym, �e b�dzie kontynuowa� nocne wizyty.";
        result.npcReceivedSuccess = "Bogom niech dzi�ki b�d�. Mo�e teraz zmru�� oczy w nocy. Dzi�kuj�, oto twoja nagroda.";
        result.npcReceivedFailure = "Zgroza! Dlaczego to musia�o spotka� mnie...";
        result.npcAfterSuccess = "Teraz wiem, kogo zawo�a�, gdy b�d� mie� problemy z duchami.";
        result.npcAfterFailure = "Dlaczego on nie chce da� mi spokoju... dlaczego sam nie chce spokoju...";
        result.npcAfterRefusal = "Kogo mam prosi� o pomoc, je�li nie ciebie? Nie wyobra�am sobie, jakich katuszy doznaje dusza mojego ojca!";
    }
    else
    {
        result.journalName = "Restless spirit";
        result.journalInitial = "The dead father of my employer, "+questgiverName+", has been haunting their family as a vengeful spirit. I have been hired to use a Spirit Rune at the spot of his death to bring forth his spirit and send it to the afterlife. "+questgiverName+" will await me in "+townName+" with the reward of "+IntToString(questReward.gold)+" gold.";
        result.journalSuccess = "The allip has been sent away and will not trouble the family anymore. Time to return to "+townName+" to get my "+IntToString(questReward.gold)+" promised gold from "+questgiverName+".";
        result.journalFailure = "The spirit will continue to haunt his own child, the task is not done and there won't be a second chance. "+questgiverName+" of "+townName+" will have to come to terms with this situation.";
        result.journalSuccessFinished = "Encounters with spirits are never pleasant, but the good payment makes up for it and I can now leave this chapter behind me.";
        result.journalAbandoned = "Maybe that spirit has a good reason to haunt these people? I don't know the whole story and frankly, I don't intend to get into it.";
        result.npcIntroLine = "Do you have any experience of dealing with restless souls of the dead?";
        result.npcContentLine = "A tragedy has befallen me and my family... My recently deceased father's spirit has not passed to the afterlife, but comes back to haunt us. It can't find rest for some reason.";
        result.npcRewardLine = "I have a Spirit Rune bought from a travelling shaman. If someone were to bring this stone to the place where a spirit died, they could supposedly summon it. Please, will you agree to do this and find a way to let my father find rest? I'm offering a reward of "+IntToString(questReward.gold)+" gold pieces.";
        result.npcBeforeCompletionLine = "Has my poor father found rest yet?";
        result.playerReportingSuccess = "Yes, he's on the other side now. Nothing can disturb his rest anymore.";
        result.playerReportingFailure = "I'm afraid not. You need to realize that the hauntings may continue.";
        result.npcReceivedSuccess = "Thanks be to the gods. Maybe I can finally get some sleep this night. Thank you, here is your reward.";
        result.npcReceivedFailure = "Terrible! Why did it have to happen to me...";
        result.npcAfterSuccess = "Now I know who to call when I have any ghost-related troubles.";
        result.npcAfterFailure = "Why can't he leave me alone... why doesn't he want to rest himself...";
        result.npcAfterRefusal = "Who should I beg for help if not you? I can't imagine the torment my father's spirit is going through...";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return questgiverName+" is looking for someone to put their deceased father to rest. The poor fellow has ended as a vengeful spirit after his death.";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return isMale
            ? questgiverName+" szuka kogo�, kto spr�buje zapewni� spok�j jego zmar�emu ojcu. Biedak sta� si� m�ciwym duchem po swej �mierci."
            : questgiverName+" szuka kogo�, kto spr�buje zapewni� spok�j jej zmar�emu ojcu. Biedak sta� si� m�ciwym duchem po swej �mierci.";;
    }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{
    object item = CreateItemOnObject("it_spiritrune", oConversingPC);
    SetLocalObject(item, "Quest", oQuest);
    SetLocalObject(oQuest, "SpiritRune", item);
}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{
    object item = GetLocalObject(oQuest, "SpiritRune");
    DestroyObject(item);
}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{
    object item = GetLocalObject(oQuest, "SpiritRune");
    DestroyObject(item);
}

// Function that may perform custom logic when the quest is generated.
// This should be avoided when possible, because conceptually an encounter
// or a special area shouldn't change based on quest spawned, but it may sometimes
// be the best option (for example to give a creature an item the PC is supposed to bring back).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{
    if (RandomNext(5, sSeedName) == 0)
    {
        SetLocalInt(oQuest, "MURDERED", TRUE);
    }
}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{

}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{
    object spirit = GetLocalObject(oQuest, "Spirit");
    location deathLoc = GetLocalLocation(oRespawningPC, "LastDeathLocation");
    object encounter = GetQuestEncounter(oQuest);
    object encounterArea = GetEncounterArea(encounter);
    object deathArea = GetAreaFromLocation(deathLoc);

    if (!GetIsObjectValid(spirit) || deathArea != encounterArea || GetNumberOfPlayers(encounterArea) > 1)
        return;

    SetPlotFlag(spirit, FALSE);
    DestroyObject(spirit);
    UpdateQuestState(oQuest, QUEST_STATE_FAILED);
}