#include "inc_quests"
#include "inc_partyskills"
#include "inc_tokens"

int StartingConditional()
{
    //Return TRUE if Persuade check passes, but first set CUSTOM1001 to the questgiver's first name

    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    object questgiver = GetQuestGiver(quest);
    string name = GetLocalString(questgiver, "FirstName");
    SetCustomTokenEx(1001, name);

    return GetIsPartySkillSuccessful(GetPCSpeaker(), SKILL_PERSUADE, 11);
}
