#include "inc_quests"
#include "inc_common"
#include "inc_factions"
#include "hench_i0_ai"
#include "inc_convabort"

void main()
{
    //Depending on variables on spirit:
    //-make it leave and win the quest after successful persuasion
    //-make it leave and lose the quest (and give good points) if the spirit has revealed it's been murdered
    //-stop the attack counter (if it didn't complete yet) and begin fight if persuasion failed and the spirit attacks
    //-do nothing if PC said they'd return
    MarkConversationEnd();
    if (GetLocalInt(OBJECT_SELF, "QUEST_COMPLETE"))
    {
        object quest = GetLocalObject(OBJECT_SELF, "Quest");
        UpdateQuestState(quest, QUEST_STATE_COMPLETED);
        effect vfx = EffectVisualEffect(VFX_IMP_UNSUMMON);
        ApplyEffectAtLocation(DURATION_TYPE_INSTANT, vfx, GetLocation(OBJECT_SELF));
        DeleteLocalString(OBJECT_SELF, "Conversation");
        DestroyObject(OBJECT_SELF, 1.0f);
        return;
    }
    if (GetLocalInt(OBJECT_SELF, "READY_TO_ESCAPE"))
    {
        AdjustAligmentOfAllPCs(ALIGNMENT_GOOD, 3);
        object quest = GetLocalObject(OBJECT_SELF, "Quest");
        UpdateQuestState(quest, QUEST_STATE_FAILED);
        effect vfx = EffectVisualEffect(VFX_IMP_UNSUMMON);
        ApplyEffectAtLocation(DURATION_TYPE_INSTANT, vfx, GetLocation(OBJECT_SELF));
        DeleteLocalString(OBJECT_SELF, "Conversation");
        DestroyObject(OBJECT_SELF, 1.0f);
        return;
    }
    if (GetLocalInt(OBJECT_SELF, "READY_TO_ATTACK"))
    {
        SetLocalInt(OBJECT_SELF, "STARTED_ATTACKING", TRUE);
        SetFaction(OBJECT_SELF, FACTION_UNDEAD);
        HenchDetermineCombatRound();
        SetPlotFlag(OBJECT_SELF, FALSE);
        return;
    }
}
