#include "inc_quests"

int StartingConditional()
{
    //Return TRUE if the spirit has been murdered
    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    return GetLocalInt(quest, "MURDERED");
}
