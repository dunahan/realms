#include "inc_quests"
#include "inc_tokens"

int StartingConditional()
{
    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    object questgiver = GetQuestGiver(quest);
    string name = GetLocalString(questgiver, "FirstName");
    SetCustomTokenEx(1001, name);
    return TRUE;
}
