#include "inc_common"
#include "inc_quests"

void main()
{
    //Give good points for sparing the spirit, fail the quest and send the spirit away
    AdjustAligmentOfAllPCs(ALIGNMENT_GOOD, 3);
    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    UpdateQuestState(quest, QUEST_STATE_FAILED);
    effect vfx = EffectVisualEffect(VFX_IMP_UNSUMMON);
    ApplyEffectAtLocation(DURATION_TYPE_INSTANT, vfx, GetLocation(OBJECT_SELF));
    DeleteLocalString(OBJECT_SELF, "Conversation");
    DestroyObject(OBJECT_SELF, 1.0f);
}
