#include "inc_quests"

void main()
{
    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    UpdateQuestState(quest, QUEST_STATE_COMPLETED);
    effect vfx = EffectVisualEffect(VFX_IMP_UNSUMMON);
    ApplyEffectAtLocation(DURATION_TYPE_INSTANT, vfx, GetLocation(OBJECT_SELF));
    DestroyObject(OBJECT_SELF, 1.0f);
}