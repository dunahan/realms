#include "inc_quests"
#include "inc_encounters"
void main()
{
    object encounter = GetLocalObject(OBJECT_SELF, "Encounter");
    int killedCreaturesNum = GetLocalInt(encounter, "killedCreaturesNum");
    int toKillCreaturesNum = GetLocalInt(encounter, "toKillCreaturesNum");

    killedCreaturesNum = killedCreaturesNum + 1;
    SetLocalInt(encounter, "killedCreaturesNum", killedCreaturesNum);

    if(killedCreaturesNum == toKillCreaturesNum)
    {
        object quest = GetQuestOfEncounter(encounter);
        string questScript = GetQuestScript(quest);
        UpdateQuestState(quest, QUEST_STATE_COMPLETED);
    } 
}
