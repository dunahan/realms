#include "inc_quests"
#include "inc_encounters"
#include "inc_arrays"
void main()
{
    object encounter = GetLocalObject(OBJECT_SELF, "Encounter");
    object quest = GetQuestOfEncounter(encounter);
    string questScript = GetQuestScript(quest);
    if (questScript != "")
    {
        string encounterArray = "EncounterCreatures";
        int index = GetObjectArrayIndexByValue(encounterArray, OBJECT_SELF, 1, encounter);
        DeleteObjectArrayElement(encounterArray, index, encounter);
        if (GetObjectArraySize(encounterArray, encounter) == 0)
            UpdateQuestState(quest, QUEST_STATE_COMPLETED);
    }
}
