void main()
{
    object ranger = GetLocalObject(OBJECT_SELF, "Ranger");
    if (GetIsDead(ranger))
        return;

    ActionMoveToObject(ranger, TRUE);
}