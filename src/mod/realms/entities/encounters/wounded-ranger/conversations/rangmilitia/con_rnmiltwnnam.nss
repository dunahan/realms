#include "inc_encounters"
#include "inc_biomes"
#include "inc_towns"
#include "inc_tiles"
#include "inc_tokens"

int StartingConditional()
{
    //Set CUSTOM1001 token to the town's name and return TRUE
    object encounter = GetLocalObject(OBJECT_SELF, "Encounter");
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetTownName(town);
    SetCustomTokenEx(1001, townName);
    return TRUE;
}
