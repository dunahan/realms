#include "inc_arrays"
#include "inc_factions"
#include "inc_tiles"
#include "hench_i0_ai"
#include "inc_convabort"

void main()
{
    //Attack the party unless the militia has been marked as ready to leave,
    //in which case make him leave upon setting him to plot

    MarkConversationEnd();

    int i;
    if (GetLocalInt(OBJECT_SELF, "ReadyToLeave"))
    {
        //Leave
        object exitWp = GetNearestTileExitWaypoint(OBJECT_SELF);

        SetPlotFlag(OBJECT_SELF, TRUE);
        ActionMoveToObject(exitWp);
        ActionDoCommand(DestroyObject(OBJECT_SELF));
        DestroyObject(OBJECT_SELF, 30.0f);
        SetCommandable(FALSE);
        DeleteLocalString(OBJECT_SELF, "Conversation");

        for (i = 0; i < GetObjectArraySize("Militia", OBJECT_SELF); i++)
        {
            object militia = GetObjectArrayElement("Militia", i, OBJECT_SELF);

            SetPlotFlag(militia, TRUE);
            AssignCommand(militia, ActionMoveToObject(exitWp));
            AssignCommand(militia, ActionDoCommand(DestroyObject(OBJECT_SELF)));
            DestroyObject(militia, 30.0f);
            AssignCommand(militia, SetCommandable(FALSE));
        }
        return;
    }
    
    //Attack
    for (i = 0; i < GetObjectArraySize("Militia", OBJECT_SELF); i++)
    {
        object militia = GetObjectArrayElement("Militia", i, OBJECT_SELF);
        SetFaction(militia, FACTION_HOSTILE);
        AssignCommand(militia, HenchDetermineCombatRound());
    }
    SetFaction(OBJECT_SELF, FACTION_HOSTILE);
    HenchDetermineCombatRound();
}
