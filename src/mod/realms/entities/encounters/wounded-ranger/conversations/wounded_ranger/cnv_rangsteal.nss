#include "inc_common"

void main()
{
    object PC = GetPCSpeaker();

    //Give chaotic and evil points to party for stealing stuff from unconcious wounded ranger.
    AdjustAligmentOfAllPCs(ALIGNMENT_EVIL, 5);
    AdjustAligmentOfAllPCs(ALIGNMENT_CHAOTIC, 5);

    //Play the animation of searching, get the loot and kill the NPC.
    AssignCommand(PC, PlayAnimation(ANIMATION_LOOPING_GET_LOW, 1.0, 5.0));

    //Loot!
    int gold = GetLocalInt(OBJECT_SELF, "GOLD");
    string resref = GetLocalString(OBJECT_SELF, "LOOT");
    GiveGoldToCreature(PC, gold);
    CreateItemOnObject(resref, PC);

    //Mark the ranger as ready to die, guess he bled out or something
    SetLocalInt(OBJECT_SELF, "ReadyToDie", TRUE);

    //Fail the quest if it was about rescuing the ranger.
}
