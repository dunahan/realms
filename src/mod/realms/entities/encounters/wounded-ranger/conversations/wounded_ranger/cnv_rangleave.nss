#include "inc_convabort"

void main()
{
    //Mark ranger as ready to leave for his abort script
    MarkConversationStart("cnv_rangabort");
    SetLocalInt(OBJECT_SELF, "ReadyToLeave", TRUE);
}
