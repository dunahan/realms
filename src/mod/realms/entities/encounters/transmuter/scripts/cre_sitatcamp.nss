#include "inc_common"
#include "inc_factions"

void main()
{
    if (IsInConversation(OBJECT_SELF))
        return;

    object nearby = GetFirstObjectInShape(SHAPE_SPHERE, 10.0, GetLocation(OBJECT_SELF), TRUE, OBJECT_TYPE_CREATURE);
    while (GetIsObjectValid(nearby))
    {
        if (GetIsReactionTypeHostile(nearby))
            return;
        nearby = GetNextObjectInShape(SHAPE_SPHERE, 10.0, GetLocation(OBJECT_SELF), TRUE, OBJECT_TYPE_CREATURE);
    }
    
    object campfire = GetLocalObject(OBJECT_SELF, "Campfire");
    ActionDoCommand(SetFacingObject(campfire));
    PlayAnimation(ANIMATION_LOOPING_SIT_CROSS, 1.0f, 8.0f);
    SetLocalInt(OBJECT_SELF, "Sitting", TRUE);
    SetFaction(OBJECT_SELF, FACTION_TEMPORARY_NEUTRAL);
}