#include "inc_convabort"

int StartingConditional()
{
    //Return TRUE if the transmuter has already introduced himself
    MarkConversationStart("cnv_transabort");
    return GetLocalInt(OBJECT_SELF, "Hello");
}
