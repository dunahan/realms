#include "inc_tokens"

int StartingConditional()
{
    //Set token 1001 to transmuter's price and return TRUE
    int gold = GetLocalInt(OBJECT_SELF, "PRICE");
    SetCustomTokenEx(1001, IntToString(gold));
    return TRUE;
}
