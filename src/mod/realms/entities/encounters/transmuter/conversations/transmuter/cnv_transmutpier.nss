#include "inc_convoptions"
#include "inc_language"

void main()
{
    string optionsArray = "optionsArray";
    string valuesArray = "valuesArray";

    CreateStringArray(optionsArray);
    CreateStringArray(valuesArray);

    //Add piercing weapons
    AddStringArrayElement(optionsArray, GetLocalizedString("Rapier.", "Rapier."));
    AddStringArrayElement(valuesArray, "rapier");

    AddStringArrayElement(optionsArray, GetLocalizedString("Dagger.", "Sztylet."));
    AddStringArrayElement(valuesArray, "dagger");

    AddStringArrayElement(optionsArray, GetLocalizedString("Halberd.", "Halabarda."));
    AddStringArrayElement(valuesArray, "halberd");

    AddStringArrayElement(optionsArray, GetLocalizedString("Spear.", "W��cznia."));
    AddStringArrayElement(valuesArray, "spear");

    AddStringArrayElement(optionsArray, GetLocalizedString("Scythe.", "Kosa."));
    AddStringArrayElement(valuesArray, "scythe");

    AddStringArrayElement(optionsArray, GetLocalizedString("Trident.", "Tr�jz�b."));
    AddStringArrayElement(valuesArray, "trident");

    LoadConversationOptions(optionsArray, valuesArray);
}
