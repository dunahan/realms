#include "inc_convoptions"
#include "inc_language"

void main()
{
    string optionsArray = "optionsArray";
    string valuesArray = "valuesArray";

    CreateStringArray(optionsArray);
    CreateStringArray(valuesArray);

    //Add ranged weapons
    AddStringArrayElement(optionsArray, GetLocalizedString("Light crossbow.", "Lekka kusza."));
    AddStringArrayElement(valuesArray, "lxbow");

    AddStringArrayElement(optionsArray, GetLocalizedString("Heavy crossbow.", "Ci�ka kusza."));
    AddStringArrayElement(valuesArray, "hxbow");

    AddStringArrayElement(optionsArray, GetLocalizedString("Longbow.", "D�ugi �uk."));
    AddStringArrayElement(valuesArray, "lbow");

    AddStringArrayElement(optionsArray, GetLocalizedString("Shortbow.", "Kr�tki �uk."));
    AddStringArrayElement(valuesArray, "sbow");

    AddStringArrayElement(optionsArray, GetLocalizedString("Sling.", "Proca."));
    AddStringArrayElement(valuesArray, "sling");

    LoadConversationOptions(optionsArray, valuesArray);
}
