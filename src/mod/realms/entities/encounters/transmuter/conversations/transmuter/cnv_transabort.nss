#include "inc_convabort"
#include "inc_arrays"
#include "inc_encounters"
#include "inc_quests"
#include "hench_i0_ai"
#include "inc_factions"

void main()
{
    //Transmuter's abort script: spawn enemies trying to steal the package if appropriate
    //after acquiring it
    MarkConversationEnd();

    object package = GetLocalObject(OBJECT_SELF, "Package");
    if (!GetLocalInt(OBJECT_SELF, "AMBUSH") || GetLocalInt(OBJECT_SELF, "AfterAmbush") || GetItemPossessor(package) == OBJECT_SELF)
        return;
    SetLocalInt(OBJECT_SELF, "AfterAmbush", TRUE);

    //Spawn ambush
    int i;
    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    object encounter = GetQuestEncounter(quest);
    string array = "EnemyResRefs";
    int creatureNum = GetStringArraySize(array, quest);
    string shout = GetLocalString(quest, "ENEMYSHOUT");
    CreateObjectArray("Ambushers", 0, quest);
    for (i = 0; i < creatureNum; i++)
    {
        location loc = GetLocationWithinEncounterBounds(encounter, Random(2)*12.0, Random(2)*12.0);
        string resref = GetStringArrayElement(array, i, quest);
        object creature = CreateCreature(resref, loc);
        AddObjectArrayElement("Ambushers", creature, FALSE, quest);
        AssignCommand(creature, HenchDetermineCombatRound());
        if (i == 0)
            DelayCommand(5.0, AssignCommand(creature, SpeakString(shout)));
    }

    //Let transmuter join the fray
    SetFaction(OBJECT_SELF, FACTION_ALLY);
    ClearAllActions();
    HenchDetermineCombatRound();
}
