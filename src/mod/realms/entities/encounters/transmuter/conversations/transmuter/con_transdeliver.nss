#include "inc_quests"
#include "inc_tokens"

int StartingConditional()
{
    //Set token 1001 to the questgiver's full name and return TRUE if the PCs are on a delivery quest regarding this encounter
    //(and the package hasn't yet been handed out)
    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    object package = GetLocalObject(OBJECT_SELF, "Package");
    if (quest == OBJECT_INVALID || !GetIsQuestAccepted(quest) || GetItemPossessor(package) != OBJECT_SELF)
        return FALSE;

    object questgiver = GetQuestGiver(quest);
    SetCustomTokenEx(1001, GetName(questgiver));
    return TRUE;
}
