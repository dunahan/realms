#include "inc_chestcheck"

int StartingConditional()
{
    //Return FALSE if there are exactly two transmutation-compatible weapons put in the transmuter's chest and nothing else.
    object chest = GetLocalObject(OBJECT_SELF, "Chest");
    return !GetIsChestInventoryValid(chest);
}
