int StartingConditional()
{
    //Return TRUE if the weapons are too powerful for this transmuter to transmute
    object chest = GetLocalObject(OBJECT_SELF, "Chest");
    object item1 = GetFirstItemInInventory(chest);
    object item2 = GetNextItemInInventory(chest);
    int tier1 = StringToInt(GetStringLeft(GetStringRight(GetResRef(item1), 3), 2));
    int tier2 = StringToInt(GetStringLeft(GetStringRight(GetResRef(item2), 3), 2));
    int maxTier = GetLocalInt(OBJECT_SELF, "MAXTIER");
    if (tier1 > maxTier || tier2 > maxTier)
        return TRUE;
    return FALSE;
}
