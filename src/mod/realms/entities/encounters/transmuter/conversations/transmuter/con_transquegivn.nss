#include "inc_quests"
#include "inc_tokens"

int StartingConditional()
{
    //Set token 1001 to questgiver's name and return TRUE
    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    object questgiver = GetQuestGiver(quest);
    SetCustomTokenEx(1001, GetName(questgiver));

    return TRUE;
}
