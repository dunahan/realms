#include "inc_quests"
#include "inc_tokens"

int StartingConditional()
{
    //Set token 1001 to questgiver's name and return TRUE if no one attempted to persuade the NPC to hand out the package yet
    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    object questgiver = GetQuestGiver(quest);
    SetCustomTokenEx(1001, GetName(questgiver));

    return !GetLocalInt(OBJECT_SELF, "TriedPersuade");
}
