#include "inc_convoptions"
#include "inc_language"

void main()
{
    string optionsArray = "optionsArray";
    string valuesArray = "valuesArray";

    CreateStringArray(optionsArray);
    CreateStringArray(valuesArray);

    //Add slashing weapons
    AddStringArrayElement(optionsArray, GetLocalizedString("Greataxe.", "Top�r dwur�czny."));
    AddStringArrayElement(valuesArray, "greataxe");

    AddStringArrayElement(optionsArray, GetLocalizedString("Battleaxe.", "Top�r bojowy."));
    AddStringArrayElement(valuesArray, "battleaxe");

    AddStringArrayElement(optionsArray, GetLocalizedString("Handaxe.", "Toporek jednor�czny."));
    AddStringArrayElement(valuesArray, "handaxe");

    AddStringArrayElement(optionsArray, GetLocalizedString("Dwarven waraxe.", "Krasnoludzki top�r."));
    AddStringArrayElement(valuesArray, "dwarfaxe");

    AddStringArrayElement(optionsArray, GetLocalizedString("Bastard sword.", "Miecz p�torar�czny."));
    AddStringArrayElement(valuesArray, "bastard");

    AddStringArrayElement(optionsArray, GetLocalizedString("Greatsword.", "Miecz dwur�czny."));
    AddStringArrayElement(valuesArray, "greatsw");

    AddStringArrayElement(optionsArray, GetLocalizedString("Longsword.", "D�ugi miecz."));
    AddStringArrayElement(valuesArray, "longsw");

    AddStringArrayElement(optionsArray, GetLocalizedString("Short sword.", "Kr�tki miecz."));
    AddStringArrayElement(valuesArray, "shortsw");

    AddStringArrayElement(optionsArray, GetLocalizedString("Katana.", "Katana."));
    AddStringArrayElement(valuesArray, "katana");

    AddStringArrayElement(optionsArray, GetLocalizedString("Scimitar.", "Sejmitar."));
    AddStringArrayElement(valuesArray, "scimitar");

    AddStringArrayElement(optionsArray, GetLocalizedString("Two-bladed sword.", "Podw�jny miecz"));
    AddStringArrayElement(valuesArray, "twosword");

    AddStringArrayElement(optionsArray, GetLocalizedString("Double axe.", "Podw�jny top�r."));
    AddStringArrayElement(valuesArray, "doubleaxe");

    AddStringArrayElement(optionsArray, GetLocalizedString("Kama.", "Kama."));
    AddStringArrayElement(valuesArray, "kama");

    AddStringArrayElement(optionsArray, GetLocalizedString("Kukri.", "Kukri."));
    AddStringArrayElement(valuesArray, "kukri");

    AddStringArrayElement(optionsArray, GetLocalizedString("Sickle.", "Sierp."));
    AddStringArrayElement(valuesArray, "sickle");

    AddStringArrayElement(optionsArray, GetLocalizedString("Whip.", "Bicz."));
    AddStringArrayElement(valuesArray, "whip");

    LoadConversationOptions(optionsArray, valuesArray);
}
