#include "inc_common"

void main()
{
    //Mark the transmuter as having introduced himself
    SetLocalInt(OBJECT_SELF, "Hello", TRUE);
    ActionStopAnimation();
    ActionPlayAnimation(ANIMATION_FIREFORGET_BOW);
}
