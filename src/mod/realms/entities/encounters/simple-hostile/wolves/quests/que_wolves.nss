#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_encounters"
#include "inc_towns"
#include "inc_tiles"
#include "inc_biomes"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_wolves";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 1;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    //SCALING
    result.gold = RandomNext(101, sSeedName) + 420;
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Polowanie na wilki";
        if (isMale)
            result.journalInitial = "Czas na �owy! "+questgiverName+" z "+townName+" zleci� mi polowanie na wilki. Czuj�, �e m�j mieszek wzbogaci si� o obiecane "+IntToString(questReward.gold)+" sztuk z�ota.";
        else
            result.journalInitial = "Czas na �owy! "+questgiverName+" z "+townName+" zleci�a mi polowanie na wilki. Czuj�, �e m�j mieszek wzbogaci si� o obiecane "+IntToString(questReward.gold)+" sztuk z�ota.";
        result.journalSuccess = "My�liwskie zlecenie na wilki zako�czone sukcesem. Czas wpa�� do "+townName+" na kubek wina i odebra� nale�ne mi "+IntToString(questReward.gold)+" sztuk z�ota. "+questgiverName+" pewnie ju� na mnie czeka.";
        result.journalFailure = "Polowanie na wilki to trudniejsze zadanie, ni� mog�oby si� wydawa�. Trzeba by�o si� do tego lepiej przygotowa�. Trudno, "+questgiverName+" dowie si�, �e musi znale�� kogo� innego do tej roboty.";
        result.journalSuccessFinished = "�owiectwo, adrenalina i pe�ny mieszek - zlecenie na wilki zako�czy�o si� sukcesem.";
        result.journalAbandoned = "Nie mam czasu na uganianie si� za wilkami. W "+townName+" na pewno znajdzie si� inny mi�o�nik �owiectwa.";
        result.npcIntroLine = "Ta wataha robi si� coraz bardziej zuchwa�a. Mo�e chcia�by� urz�dzi� sobie ma�e �owy? Jestem w stanie dobrze zap�aci�.";
        result.npcContentLine = "�on� kowala pogryz�y wilki, gdy zbiera�a grzyby. Nie do wiary! Te bestie robi� si� coraz bardziej agresywne, wi�c kto� musi si� ich pozby�.";
        result.npcRewardLine = "Dla ciebie to b�dzie proste polowanie, a dla nas dost�p do skarb�w natury w okolicy. Zap�ac� niez�� sumk�, "+IntToString(questReward.gold)+" sztuk z�ota, by zn�w czu� si� bezpiecznie za miastem.";
        result.npcBeforeCompletionLine = "Czy ta okropna wataha nadal poluje w okolicach "+townName+"?";
        result.playerReportingSuccess = "Nie, to by�y udane �owy. Wilki ju� wam nie zagra�aj�.";
        if (isMale)
            result.playerReportingFailure = "Niestety, mia�e� racj�, te wilki s� wyj�tkowo niebezpieczne. Nie uda�o mi si� ich upolowa�.";
        else
            result.playerReportingFailure = "Niestety, mia�a� racj�, te wilki s� wyj�tkowo niebezpieczne. Nie uda�o mi si� ich upolowa�.";
        if (isMale)
            result.npcReceivedSuccess = "To wspaniale! W ko�cu udam si� na grzyby z �on�! Oto twoje z�oto.";
        else
            result.npcReceivedSuccess = "To wspaniale! W ko�cu udam si� na grzyby z m�em! Oto twoje z�oto.";
        if (isMale)
            result.npcReceivedFailure = "Co my teraz zrobimy? �udzi�em si�, �e twoje pojawienie si� b�dzie oznacza�o koniec k�opot�w. Bywaj zatem.";
        else
            result.npcReceivedFailure = "Co my teraz zrobimy? �udzi�am si�, �e twoje pojawienie si� b�dzie oznacza�o koniec k�opot�w. Bywaj zatem.";
        result.npcAfterSuccess = "Gratuluj�, to by�y zaprawd� udane �owy. Napiszemy o tobie w kronice osady!";
        result.npcAfterFailure = "Czas przesta� inwestowa� w p�rodki - trzeba wynaj�� prawdziwego specjalist�, gdy� niebezpiecze�stwo jest zbyt powa�ne.";
        result.npcAfterRefusal = "Nie dziwi� si� tobie, �e boisz si� tej watahy. Znajd� odwa�niejszego my�liwego.";
    }
    else
    {
        result.journalName = "Hunting wolves";
        result.journalInitial = "Time for some hunting. "+questgiverName+" from "+townName+" wants me to hunt down some wolves. I get the feeling this task will make me richer by the promised "+IntToString(questReward.gold)+" pieces of gold.";
        result.journalSuccess = "The hunt was successful. Time to visit "+townName+", grab a cup of wine and ask "+questgiverName+" for the promised "+IntToString(questReward.gold)+" gold pieces.";
        result.journalFailure = "Hunting wolves is more difficult than I thought. I should have prepared better. No matter, "+questgiverName+" will learn that they need to give this task to someone else.";
        result.journalSuccessFinished = "Hunting, adrenaline and gold - that task was a definite success.";
        result.journalAbandoned = "I have no time to waste on chasing after wolves. I'm sure there is some skilled huntsman in "+townName+" who can handle this type of job just fine.";
        result.npcIntroLine = "That pack is becoming bolder every day. What say you, would you like to go for a hunt? I can pay well.";
        result.npcContentLine = "The smith's wife has been bitten by wolves while collecting mushrooms. Unbelievable! These beasts have been unusually aggressive lately and I think it's time someone gets rid of them.";
        result.npcRewardLine = "An easy hunt for you that will let us enjoy the great outdoors and its treasures. I'll pay a good sum, "+IntToString(questReward.gold)+" gold pieces, if you are willing to do this.";
        result.npcBeforeCompletionLine = "Is that pack still lurking somewhere around "+townName+"?";
        result.playerReportingSuccess = "No, the hunt was successful. Wolves will not bother you anymore.";
        result.playerReportingFailure = "Unfortunately, you were right. These wolves are very dangerous. I've failed to hunt them down.";
        result.npcReceivedSuccess = "That's splendid! I can finally go collect some mushrooms with my spouse! Here is your promised gold.";
        result.npcReceivedFailure = "What should we do now? I hoped that your arrival means the end of troubles for our town. Well, goodbye to you.";
        result.npcAfterSuccess = "Congratulations, your hunt was successful, indeed. It shall be included in our settlement's chronicles someday!";
        result.npcAfterFailure = "Time to get serious and hire a true professional. The danger is too serious.";
        result.npcAfterRefusal = "I don't blame you for being afraid of this pack. Maybe I'll be able to find a more courageous adventurer.";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return "Have you heard about the smith's wife being attacked by wolves? "+questgiverName+" has gotten quite angry about it.";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return isMale ? 
            "Dotar�y twych uszu wie�ci o wilkach, kt�re pogryz�y �on� kowala? "+questgiverName+" podobno bardzo si� przej��." : 
            "Dotar�y twych uszu wie�ci o wilkach, kt�re pogryz�y �on� kowala? "+questgiverName+" podobno bardzo si� przej�a.";
    }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// This should be avoided when possible, because conceptually an encounter
// or a special area shouldn't change based on quest spawned, but it may sometimes
// be the best option (for example to give a creature an item the PC is supposed to bring back).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{

}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{

}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{

}