#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_encounters"
#include "inc_towns"
#include "inc_tiles"
#include "inc_biomes"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_badgers";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 1;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    //SCALING
    result.gold = RandomNext(101, sSeedName) + 400;
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Polowanie na borsuki";
        if (isMale)
            result.journalInitial = questgiverName+" z "+townName+" powiadomi� mnie o walce terytorialnej z borsukami. Za obron� okolicznych p�l dostan� "+IntToString(questReward.gold)+" sztuk z�ota.";
        else
            result.journalInitial = questgiverName+" z "+townName+" powiadomi�a mnie o walce terytorialnej z borsukami. Za obron� okolicznych p�l dostan� "+IntToString(questReward.gold)+" sztuk z�ota.";
        if (isMale)
            result.journalSuccess = "Borsuki przegra�y bitw� o okoliczn� �ywno��. Czas obwie�ci� sukces w "+townName+" i sk�pa� si� w chwale i z�ocie. "+questgiverName+" jest mi winien "+IntToString(questReward.gold)+" b�yszcz�cych monet.";
        else
            result.journalSuccess = "Borsuki przegra�y bitw� o okoliczn� �ywno��. Czas obwie�ci� sukces w "+townName+" i sk�pa� si� w chwale i z�ocie. "+questgiverName+" jest mi winna "+IntToString(questReward.gold)+" b�yszcz�cych monet.";
        if (isMale)
            result.journalFailure = "Przeciwnik okaza� si� zbyt pot�ny. Borsuki wygra�y bitw� o p�ody rolne... Pozostaje mi przekaza� z�e wie�ci zleceniodawcy. "+questgiverName+" z "+townName+" b�dzie zawiedziony.";
        else
            result.journalFailure = "Przeciwnik okaza� si� zbyt pot�ny. Borsuki wygra�y bitw� o p�ody rolne... Pozostaje mi przekaza� z�e wie�ci zleceniodawcy. "+questgiverName+" z "+townName+" b�dzie zawiedziona.";
        result.journalSuccessFinished = "Pola kukurydzy wok� "+townName+" s� ju� bezpieczne. �aden borsuk im nie zagra�a.";
        result.journalAbandoned = "Borsuki nie s� godnym mnie przeciwnikiem. W �yciu poszukuj� wi�kszych wyzwa�.";
        result.npcIntroLine = "Borsuki pogryz�y ca�e pole kukurydzy nad strumykiem. Nie b�dziemy mieli co je��. My�lisz, �e jeste� w stanie nam pom�c?";
        result.npcContentLine = "Borsuki nigdy nie by�y takie uci��liwe, kto� musia� je wyp�oszy� z ich teren�w. O tym czy starczy nam po�ywienia na najbli�szy sezon zdecyduje bitwa z tymi szkodnikami. Prze�yj� albo one, albo my.";
        result.npcRewardLine = "W tym momencie szkody wyrz�dzane przez borsuki s� tak wielkie, �e jestem w stanie zap�aci� nawet "+IntToString(questReward.gold)+" sztuk z�ota. Piszesz si�?";
        result.npcBeforeCompletionLine = "Jak zako�czy�a si� bitwa z borsukami?";
        result.playerReportingSuccess = "Wspania�ym zwyci�stwem.";
        result.playerReportingFailure = "Druzgoc�c� pora�k�. Borsuki mia�y niestety przewag� i nie uda�o si� ich wyprze�. Nadal gryz� okoliczne pola.";
        if (isMale)
            result.npcReceivedSuccess = "Wiedzia�em, �e ci si� uda! W pe�ni nale�y ci si� ten oto mieszek z�ota!";
        else
            result.npcReceivedSuccess = "Wiedzia�am, �e ci si� uda! W pe�ni nale�y ci si� ten oto mieszek z�ota!";
        result.npcReceivedFailure = "Biada nam wszystkim! Te szkodniki zjedz� ca�� nasz� kukurydz�...";
        result.npcAfterSuccess = "Bez gryz�cych pola borsuk�w wierz�, �e w tym roku jednak odb�dzie si� festyn kukurydzy.";
        result.npcAfterFailure = "Je�li borsuki nie przestan� gry�� naszych p�l, nie ma co liczy� na festyn kukurydzy w tym roku.";
        result.npcAfterRefusal = "Wszyscy zapami�taj�, �e nie chcesz pom�c wygra� bitwy z borsukami.";
    }
    else
    {
        result.journalName = "Hunting badgers";
        result.journalInitial = questgiverName+" from "+townName+" told me about the local farmers' territorial war with badgers. I will be rewarded with "+IntToString(questReward.gold)+" gold for successful defense of the local fields.";
        result.journalSuccess = "The badgers have lost the fight for food - and their lives. Time to share these news with "+townName+" and bathe in gold and glory. "+questgiverName+" owes me "+IntToString(questReward.gold)+" shiny coins.";
        result.journalFailure = "The enemy was too strong for me. The badgers have won their fight for food... I need to pass these news to my employer. "+questgiverName+" of "+townName+" is going to be disappointed.";
        result.journalSuccessFinished = "Corn fields around "+townName+" are safe now. No badger threat remains.";
        result.journalAbandoned = "Badgers are not an opponent worthy of my time. I look for greater challenges in life.";
        result.npcIntroLine = "Badgers have ravaged our corn fields over the stream. We'll have nothing to eat if nothing is done. Do you think you can help us?";
        result.npcContentLine = "These badgers were never this troublesome. Someone must have driven them off from their territories. A battle against these animals is what will decide whether we have enough food for the nearest season. It's either them or us.";
        result.npcRewardLine = "The damage is so great that I am willing to pay you as much as "+IntToString(questReward.gold)+" coins for you to get the job done. Are you in?";
        result.npcBeforeCompletionLine = "How was your battle with badgers?";
        result.playerReportingSuccess = "It has concluded in my victory.";
        result.playerReportingFailure = "It has concluded in my failure. The badgers had the upper hand. They are ravaging local fields as we speak.";
        result.npcReceivedSuccess = "I knew you could do this! Take this sack of gold, you've earned it.";
        result.npcReceivedFailure = "Woe to us all! These pests will eat all of our corn...";
        result.npcAfterSuccess = "I believe the Festival of Corn may be organized this year with these badgers gone.";
        result.npcAfterFailure = "If badgers don't stop ravaging our fields, we can't hope for the Festival of Corn to happen this year.";
        result.npcAfterRefusal = "People of "+townName+" will remember your indifference to our badger troubles.";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return "No one really wants to kill the badgers, but their ravaging our fields leaves us little choice. "+questgiverName+" is well aware of it, I think.";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return "Nikt z nas nie chce �mierci okolicznych borsuk�w, ale spustoszenie, jakie siej� na naszych polach uprawnych nie pozostawia nam wyboru. "+questgiverName+" chyba najlepiej zdaje sobie z tego spraw�.";
    }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// This should be avoided when possible, because conceptually an encounter
// or a special area shouldn't change based on quest spawned, but it may sometimes
// be the best option (for example to give a creature an item the PC is supposed to bring back).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{

}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{

}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{

}