#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_encounters"
#include "inc_towns"
#include "inc_tiles"
#include "inc_biomes"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_bandits";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 40;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    //SCALING
    result.gold = RandomNext(101, sSeedName) + 450;
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Polowanie na bandyt�w";
        result.journalInitial = "Wyrzutki, rozb�jnicy, z�odzieje. Zawsze znajd� si� ci, co licz� na �atwy zarobek czyim� kosztem. Mieszka�cy "+townName+" postanowili raz na zawsze zako�czy� to bezprawie - za egzekucj� panosz�cych si� w pobli�u bandyt�w otrzymam "+IntToString(questReward.gold)+" sztuk z�ota, kt�re wr�czy mi "+questgiverName+".";
        result.journalSuccess = "Wszyscy bandyci s� martwi. Czas powiadomi� mieszka�c�w "+townName+" o zako�czonej egzekucji, kto� musi pochowa� cia�a. "+questgiverName+" musi za� przekaza� mi obiecane "+IntToString(questReward.gold)+" sztuk z�ota.";
        result.journalFailure = "Nie nale�y lekcewa�y� zorganizowanej przestepczo�ci. Bandyci spu�cili mi �omot, kt�ry zapami�tam na d�ugo.";
        result.journalSuccessFinished = "Uda�o mi si� pomy�lnie zako�czy� misj� dobrze p�atnego wys�annika prawa - egzekucja bandyt�w odby�a si� bez niespodzianek.";
        result.journalAbandoned = "Nie chc� miesza� si� w stosunki bandyt�w i mieszka�c�w "+townName+", mam wystarczaj�co wiele innych k�opot�w.";
        result.npcIntroLine = "Wyrok zapad�. Mieszka�cy "+townName+" wydali os�d na okolicznych bandyt�w. Kar� b�dzie �mier�, mo�e ty za� wcielisz si� w kata?";
        result.npcContentLine = "Rozb�jnicy pocz�tkowo tylko kradli i zastraszali. Ostatnio do list przest�pstw dopisano kilka morderstw i podpale�, a na wszystko s� �wiadkowie i dowody. Pora wymierzy� sprawiedliwo��.";
        result.npcRewardLine = "Mieszka�cy postanowili wystawi� na bandyt�w list go�czy, nagrod� jest "+IntToString(questReward.gold)+" sztuk z�ota. Czy zechcesz wykona� to ponure zadanie?";
        result.npcBeforeCompletionLine = "Jak przebieg�a egzekucja?";
        result.playerReportingSuccess = "Bez wi�kszych problem�w. Wy�lij kogo�, by pogrzeba� cia�a.";
        result.playerReportingFailure = "Przeciw tym �otrom trzeba by�oby wys�a� wi�kszy oddzia� zbrojny. Ta grupa by�a zbyt liczna na moje si�y.";
        result.npcReceivedSuccess = "�wietnie, od razu sporz�dz� raport o wymierzonej karze. Oto twoja zap�ata.";
        if (isMale)
            result.npcReceivedFailure = "Spodziewa�em si�, �e trzeba b�dzie wezwa� dodatkowe wsparcie. C�, ciesz� si�, �e nic ci nie jest. Te dranie stanowi� niebezpieczn� zgraj�.";
        else
            result.npcReceivedFailure = "Spodziewa�am si�, �e trzeba b�dzie wezwa� dodatkowe wsparcie. C�, ciesz� si�, �e nic ci nie jest. Te dranie stanowi� niebezpieczn� zgraj�.";
        result.npcAfterSuccess = "Ci�ko uwierzy�, �e bandyci zostali pokonani. W ko�cu mo�na podr�owa� bez eskorty.";
        result.npcAfterFailure = "Zg�oszono kolejne morderstwo, tym razem ofiar� pad�a m�oda kobieta. Kto� musi wymierzy� w ko�cu tym �otrom sprawiedliwo��.";
        result.npcAfterRefusal = "Og�aszam wszem i wobec otwarty list go�czy na bandyt�w. Powtarzam, otwarty list go�czy. W�adze "+townName+" og�aszaj�, list go�czy na bandyt�w!";
    }
    else
    {
        result.journalName = "Hunting bandits";
        result.journalInitial = "Outcasts, raiders and thieves. There are always those eager to profit from someone else's misery. The people of "+townName+" have decided to end this once and for all - I am to receive "+IntToString(questReward.gold)+" coins if I serve justice to bandits roaming the region. "+questgiverName+" will be the one to hand me the reward.";
        result.journalSuccess = "All the bandits are dead. It is time to let inhabitants of "+townName+" know of the completed retribution, as someone must bury the bodies. On the other hand, "+questgiverName+" must hand me the promised "+IntToString(questReward.gold)+" gold.";
        result.journalFailure = "It was not wise to underestimate organized crime. The bandits gave me a beating I will remember for a long time.";
        result.journalSuccessFinished = "I've successfully completed my job as a law enforcer - a well-paid law enforcer. The bandits stood no chance against me.";
        result.journalAbandoned = "I don't feel like meddling in affairs of bandits and the people of "+townName+". I have enough worries on my own.";
        result.npcIntroLine = "The verdict's in. The citizens of "+townName+" have made the decision regarding local bandits. The punishment is death. Are you willing to become the executioner?";
        result.npcContentLine = "These raiders initially only stole and made threats. Recently they have been further accused of several murders and arson. We have witnesses and evidence for everything. It's time to bring them to justice.";
        result.npcRewardLine = "A warrant has been prepared for the bandits and the reward is "+IntToString(questReward.gold)+" pieces of gold. Will you undertake this grim task?";
        result.npcBeforeCompletionLine = "How was the execution?";
        result.playerReportingSuccess = "Without problems. Send someone over there to bury the bodies.";
        result.playerReportingFailure = "A larger strike force would be needed against those bastards. The group was too numerous for me to stand a chance.";
        result.npcReceivedSuccess = "Splendid, I shall prepare the report from the execution right away. Here is your payment.";
        result.npcReceivedFailure = "I was afraid it would come to this. Well, I'm glad that you're still alive. Those criminals are a dangerous bunch.";
        result.npcAfterSuccess = "Hard to believe the bandits have been dealt with. We can finally travel without escort.";
        result.npcAfterFailure = "Another murder has been reported, this time it is a young woman. Someone has to finally serve some justice to these thugs.";
        result.npcAfterRefusal = "I am hereby announcing an open warrant for bandits. People of "+townName+", an open warrant for bandits has been announced!";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return "I'm glad that "+questgiverName+" is ready to offer a reward for getting rid of the bandits on the outskirts. I hope someone takes this contract.";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return isMale ? 
            "Ciesz� si�, �e "+questgiverName+" jest got�w zap�aci� za pozbycie si� bandyt�w grasuj�cych po regionie. Mam nadziej�, �e kto� przyjmie zlecenie." :
            "Ciesz� si�, �e "+questgiverName+" jest gotowa zap�aci� za pozbycie si� bandyt�w grasuj�cych po regionie. Mam nadziej�, �e kto� przyjmie zlecenie.";
    }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// This should be avoided when possible, because conceptually an encounter
// or a special area shouldn't change based on quest spawned, but it may sometimes
// be the best option (for example to give a creature an item the PC is supposed to bring back).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{

}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{

}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{

}