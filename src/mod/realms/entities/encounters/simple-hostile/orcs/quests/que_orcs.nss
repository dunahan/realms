#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_encounters"
#include "inc_towns"
#include "inc_tiles"
#include "inc_biomes"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_orcs";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 40;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    //SCALING
    result.gold = RandomNext(101, sSeedName) + 450;
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Polowanie na ork�w";
        if (isMale)
            result.journalInitial = questgiverName+" z "+townName+" poprosi� mnie o zabicie terroryzuj�cych lokaln� ludno�� ork�w. Przewidywan� za to nagrod� jest "+IntToString(questReward.gold)+" sztuk z�ota.";
        else
            result.journalInitial = questgiverName+" z "+townName+" poprosi�a mnie o zabicie terroryzuj�cych lokaln� ludno�� ork�w. Przewidywan� za to nagrod� jest "+IntToString(questReward.gold)+" sztuk z�ota.";
        result.journalSuccess = "Orkowie zostali rozgromieni. Czas uda� si� z powrotem do "+townName+". "+questgiverName+" z pewno�ci� wyczekuje wie�ci o moim sukcesie, ja za� wyczekuj� mieszka z zawarto�ci� "+IntToString(questReward.gold)+" sztuk z�ota.";
        if (isMale)
            result.journalFailure = "Orkowie czmychn�li i prawdopodobnie wkr�tce wr�c� zem�ci� si� na mieszka�cach "+townName+". "+questgiverName+" b�dzie w�ciek�y.";
        else
            result.journalFailure = "Orkowie czmychn�li i prawdopodobnie wkr�tce wr�c� zem�ci� si� na mieszka�cach "+townName+". "+questgiverName+" b�dzie w�ciek�a.";
        result.journalSuccessFinished = "Orkowie nie stanowi� ju� zagro�enia dla "+townName+", a ja mam swoj� nagrod�.";
        result.journalAbandoned = "Potyczka z orkami nie przyniesie mi korzy�ci. "+questgiverName+" b�dzie musia� znale�� kogo� innego do tego zadania.";
        result.npcIntroLine = "Wiesz, jak wygl�da ork, prawda? Brzydkie, t�pe i okrutne stworzenie. Lepsze jest martwe, ni� �ywe, je�eli rozumiesz, co mam na my�li.";
        result.npcContentLine = "Nie ma wiele do opowiadania. W okolicy "+townName+" panosz� si� czasem zbrojne grupy ork�w. Jedna z nich sta�a si� ostatnio wyj�tkowo zuchwa�a i obawiam si� o �ycie moje i moich bliskich.";
        result.npcRewardLine = "Wyr�nij tych dzikus�w w pie�, a otrzymasz "+IntToString(questReward.gold)+" z�otych monet. Brzmi interesuj�co?";
        result.npcBeforeCompletionLine = "Mam nadziej�, �e tw�j powr�t oznacza, �e orkom uby�o �b�w.";
        result.playerReportingSuccess = "Mo�na tak powiedzie�. �adnemu nie dane by�o prze�y�.";
        result.playerReportingFailure = "Przykro mi, ale nie. Orkowie prze�yli. I b�d� pewnie w�ciekli.";
        if (isMale)
            result.npcReceivedSuccess = "Wiedzia�em, �e mog� na ciebie liczy�! Oto twoja nagroda, w pe�ni zas�u�ona.";
        else
            result.npcReceivedSuccess = "Wiedzia�am, �e mog� na ciebie liczy�! Oto twoja nagroda, w pe�ni zas�u�ona.";
        if (isMale)
            result.npcReceivedFailure = "Zatem sprawy przybra�y jeszcze gorszy obr�t, ni� mia�y do tej pory. �udzi�em si�, �e jeste� w�a�ciw� osoba na w�a�ciwym miejscu. Wi�cej tego b��du nie pope�ni�.";
        else
            result.npcReceivedFailure = "Zatem sprawy przybra�y jeszcze gorszy obr�t, ni� mia�y do tej pory. �udzi�am si�, �e jeste� w�a�ciw� osoba na w�a�ciwym miejscu. Wi�cej tego b��du nie pope�ni�.";
        result.npcAfterSuccess = "Mo�e tej nocy wreszcie usn� i nie b�d� mnie dr�czy� koszmary o �mierci moich bliskich z �ap tych potwor�w. Masz moj� wdzi�czno��.";
        result.npcAfterFailure = "Chyba nadszed� czas, by opu�ci� "+townName+". Nie jest tu ju� bezpiecznie.";
        result.npcAfterRefusal = "Mo�e to i lepiej. Tak naprawd� nie wygl�dasz na kogo�, kto m�g�by prze�y� spotkanie ze w�ciek�ymi orkami.";
    }
    else
    {
        result.journalName = "Hunting orcs";
        result.journalInitial = questgiverName+" from "+townName+" asked me to kill a group of orcs posing a threat to the village. The offered reward is "+IntToString(questReward.gold)+" pieces of gold.";
        result.journalSuccess = "The orcs are no more. Time to head back to "+townName+". I am sure "+questgiverName+" is eager to hear about the success, while I am eager to count "+IntToString(questReward.gold)+" gold pieces of my reward.";
        result.journalFailure = "The orcs have escaped and I expect them to exact revenge on "+townName+" soon. "+questgiverName+" is going to be furious when they hear this.";
        result.journalSuccessFinished = "These orcs no longer pose a threat to "+townName+" and I have my reward.";
        result.journalAbandoned = "Fighting orcs is not going to help fulfill my mission. "+questgiverName+" will need to find someone else to handle this.";
        result.npcIntroLine = "You do know what an orc looks like, right? Ugly, dumb and cruel creatures. Better dead than alive, if you catch my meaning.";
        result.npcContentLine = "There is not much to tell. Armed bands of orcs travel around the region occassionally. One of them has become increasingly bold over the last weeks and I fear for my life and for my family.";
        result.npcRewardLine = "Kill them all and you will be rewarded with "+IntToString(questReward.gold)+" gold coins. Does it sound interesting to you?";
        result.npcBeforeCompletionLine = "I hope your return means there are a few more orcish bodies with no heads in the world.";
        result.playerReportingSuccess = "You could say so. None have survived.";
        result.playerReportingFailure = "I am sorry, but the orcs have survived and they will probably be furious.";
        result.npcReceivedSuccess = "I knew I could count on you! Here is your reward, you deserve it.";
        result.npcReceivedFailure = "So the matters have gone from bad to worse. I hoped you were the right person for the job. I will not make that mistake again.";
        result.npcAfterSuccess = "Maybe this night I'll finally get some good sleep without having nightmares about my loved ones being murdered by these monsters. You have my gratitude.";
        result.npcAfterFailure = "It may be time for me to leave "+townName+". It's no longer safe here.";
        result.npcAfterRefusal = "Maybe it's for the better. On second thought you don't really look like the type to survive an encounter with raging orcs.";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return questgiverName+" is adamant about getting rid of the orcs lurking in the wilderness near the village.";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return questgiverName+" jest zdeterminowany, �eby znale�� spos�b na pozbycie si� ork�w czaj�cych si� w dziczy przy wiosce.";
    }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// This should be avoided when possible, because conceptually an encounter
// or a special area shouldn't change based on quest spawned, but it may sometimes
// be the best option (for example to give a creature an item the PC is supposed to bring back).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{

}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{

}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{

}