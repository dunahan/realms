#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_encounters"
#include "inc_towns"
#include "inc_tiles"
#include "inc_biomes"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_wererats";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 40;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    //SCALING
    result.gold = RandomNext(101, sSeedName) + 470;
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Polowanie na szczuro�aki";
        if (isMale)
            result.journalInitial = "Mieszka�cy "+townName+" op�akuj� przemienionych w szczuro�aki pobratymc�w. Nie ma dla nich odwrotu, dlatego "+questgiverName+" postanowi� skr�ci� m�ki potwor�w. Za zadanie otrzymam "+IntToString(questReward.gold)+" sztuk z�ota.";
        else
            result.journalInitial = "Mieszka�cy "+townName+" op�akuj� przemienionych w szczuro�aki pobratymc�w. Nie ma dla nich odwrotu, dlatego "+questgiverName+" postanowi� skr�ci� m�ki potwor�w. Za zadanie otrzymam "+IntToString(questReward.gold)+" sztuk z�ota.";
        result.journalSuccess = "Przemienieni w szczuro�aki mieszka�cy "+townName+" zgin�li z mojej r�ki. Powiadomi� zleceniodawc�, kt�rym jest "+questgiverName+", �e wszyscy zara�eni odeszli i odbior� "+IntToString(questReward.gold)+" sztuk z�ota zap�aty.";
        if (isMale)
            result.journalFailure = "Przypad�o�� szczuro�actwa czyni przeciwnika praktycznie niezwyci�onym. Pora�ka z tymi potworami wiele mnie nauczy�a. "+questgiverName+" z "+townName+" b�dzie zrozpaczony.";
        else
            result.journalFailure = "Przypad�o�� szczuro�actwa czyni przeciwnika praktycznie niezwyci�onym. Pora�ka z tymi potworami wiele mnie nauczy�a. "+questgiverName+" z "+townName+" b�dzie zrozpaczona.";
        result.journalSuccessFinished = "Plaga szczuro�ak�w zako�czona. Mieszka�cy "+townName+" mog� w spokoju op�akiwa� swoich zara�onych bliskich.";
        result.journalAbandoned = "Niech kto� inny w "+townName+" zajmie si� plag� szczuro�ak�w. Nie chc� si� czym� zarazi�.";
        result.npcIntroLine = "Dwie moje kuzynki zara�one, syn karczmarza, mleczareczka... Kto� musi skr�ci� ich marny los.";
        result.npcContentLine = "Coraz wi�cej mieszka�c�w "+townName+" zapada na szczuro�actwo. Tym biednym istotom ju� nie mo�na pom�c. Postanowili�my, �e �mier� b�dzie dla nich lepsza ni� �ycie w chorobie.";
        result.npcRewardLine = "Uda�o nam si� uzbiera� "+IntToString(questReward.gold)+" sztuk z�ota. Czy zgodzisz si� wykona� to zadanie?";
        result.npcBeforeCompletionLine = "Czy nasi bliscy �pi� ju� spokojnie?";
        result.playerReportingSuccess = "Wszystkie szczuro�aki nie �yj�.";
        result.playerReportingFailure = "Niestety, n�kaj�ca waszych bliskich przypad�o�� uczyni�a ich niezwyci�onymi. Nie uda�o mi si� skr�ci� ich m�k.";
        result.npcReceivedSuccess = "Dzi�kujemy. Dzi�kujemy ci z ca�go serca. Oto nasza zap�ata.";
        result.npcReceivedFailure = "I co my teraz zrobimy? Musimy im jako� pom�c. Mo�e sami spr�bujemy ukr�ci� ich m�ki.";
        result.npcAfterSuccess = "Wszystkim l�ej na sercu z wiedz�, �e nasi bliscy zaznali spokoju.";
        result.npcAfterFailure = "Nasi biedni bliscy op�tani likantropi�, c� za tragedia...";
        result.npcAfterRefusal = "Musz� znale�� kogo� innego do tego zadania. Nie mog� pozwoli� naszym pobratymcom cierpie�.";
    }
    else
    {
        result.journalName = "Hunting wererats";
        result.journalInitial = "Inhabitants of "+townName+" are mourning their relatives turned to wererats. There is no turning them back, so "+questgiverName+" decided to end their suffering. For this task I shall receive "+IntToString(questReward.gold)+" gold.";
        result.journalSuccess = "The people of "+townName+" turned into wererats have been slain by my hand. I will inform the employer, "+questgiverName+", that all the infected are dead and get my "+IntToString(questReward.gold)+" pieces of gold.";
        result.journalFailure = "The curse of lycantrophy has made the enemies almost invincible. This bitter defeat has taught me much. "+questgiverName+" of "+townName+" will despair over the news.";
        result.journalSuccessFinished = "The wererat plague is over. The inhabitants of "+townName+" can now mourn their deceased brethren in peace.";
        result.journalAbandoned = "Let someone else in "+townName+" deal with a wererat plague. I'd rather not get infected.";
        result.npcIntroLine = "My two cousins are infected, the innkeeper's son, even the milkmaid... Someone has to end their suffering.";
        result.npcContentLine = "More and more people of "+townName+" are affected with lycantrophy and turn into wererats. These poor creatures are beyond our help. We've made the difficult choice and decided that death is a better fate for them than this disease.";
        result.npcRewardLine = "We have collected "+IntToString(questReward.gold)+" gold coins. Is this enough to hire you for this task?";
        result.npcBeforeCompletionLine = "Are our relatives at peace yet?";
        result.playerReportingSuccess = "Yes. No wererats survived.";
        result.playerReportingFailure = "I'm afraid the curse has made your close ones invincible. I was unable to end their suffering.";
        result.npcReceivedSuccess = "Thank you. Thank you so very much. Here, this is our payment.";
        result.npcReceivedFailure = "What are we to do now? We must help them somehow. Maybe we can try to end their lives ourselves.";
        result.npcAfterSuccess = "Everyone feels a bit better knowing our loved ones are finally at peace.";
        result.npcAfterFailure = "Our poor families affected with lycantrophy, what a tragedy...";
        result.npcAfterRefusal = "I've got to find someone else to do this. We can't let our kin suffer, not like this.";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return questgiverName+" is really devastated with the news of more villagers turning into wererats.";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return isMale
            ? questgiverName+" jest naprawd� wstrz��ni�ty wie�ciami o wi�kszej liczbie mieszka�c�w zmieniaj�cych si� w szczuro�aki."
            : questgiverName+" jest naprawd� wstrz��ni�ta wie�ciami o wi�kszej liczbie mieszka�c�w zmieniaj�cych si� w szczuro�aki.";
    }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// This should be avoided when possible, because conceptually an encounter
// or a special area shouldn't change based on quest spawned, but it may sometimes
// be the best option (for example to give a creature an item the PC is supposed to bring back).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{

}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{

}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{

}