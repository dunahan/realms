#include "inc_random"
#include "inc_language"
#include "hnd_champions"
#include "inc_encounters"
#include "inc_scriptevents"
#include "inc_rngnames"
#include "x2_inc_itemprop"
#include "inc_modifiers"

///////////////////////
// ----------------- //
//  Champion script  //
// ----------------- //
///////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "cha_sspidbos";

// Minimum PC level this champion supports
int minPlayerLevel = 1;

// Maximum PC level this champion supports
int maxPlayerLevel = 40;

// Number of blueprints that may be used to spawn this script's champion
int blueprintsNumber = 1;

// Number of rumors regarding this champion (can be 0)
int rumorsNumber = 1;

// Number of modifiers (buffs/debuffs) this champion may have - can be 0, but if it's 1 or greater, the champion will always have some modifier applied
int modifiersNumber = 1000;

// ResRef of the trophy item that should drop from the champion upon their death and which can be traded to a bounty hunter NPC for gold
string trophyResRef = "it_spidergland";



//////////////////////
// Script functions //
//////////////////////

// Function that should return available champion ResRef for nIndex [0, blueprintsNumber-1]
string OnGetChampionResRef(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return "cre_sspiderboss";
    }
    return "";
}

// Function that should return the trophy item's name based on the champion name and a LANGUAGE_* constant from inc_language
string OnGetTrophyName(string sChampionName, int nLanguage)
{
    return GetLocalizedString("Spider gland", "Gruczo� paj�ka");
}

// Function that should return the trophy item's description based on the champion name and a LANGUAGE_* constant from inc_language
string OnGetTrophyDescription(string sChampionName, int nLanguage)
{
    return GetLocalizedString("Spiders use glands such as this one to create webs. This gland seems particularly nasty.", "Paj�ki u�ywaj� gruczo��w takich jak ten, by tworzy� swoje sieci. Ten gruczo� wygl�da szczeg�lnie paskudnie.");
}

// Function that should return the bounty one can get for the trophy from the champion based on level of the encounter's biome
// and possibly some random aspect.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
int OnGetChampionBounty(int nLevel, string sSeedName)
{
    return RandomNext(81) + 130;
}

// Function that runs after the champion creature has been created (before their OnSpawn event handler).
// This function can be used to alter the champion's encounter to accomodate for the champion,
// for example by storing spawned regular creatures on the encounter's object and then retrieving one of them and deleting it in this function
// to make room for the champion. It also lets you use a regular creature's blueprint as a champion and make all the necessary changes to the creature here
// or give the creature buffs based on level of the encounter's biome.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of champions, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function.
// If you absolutely need to use OnSpawn for this, use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnInitializeChampion(object oChampion, object oEncounter, string sSeedName)
{

}

int _ModifierToChampionConstant(int nModifierIndex)
{
    int result;

    //Extra AC
    if (nModifierIndex < 1*(100/2))
        result = CHAMPION_AC_BOOST;
    else if (nModifierIndex < 100)
        result = CHAMPION_AC_BOOST_ATTACK_PENALTY;

    //Extra attack
    else if (nModifierIndex < 100 + 1*(100/2))
        result = CHAMPION_ATK_BOOST;
    else if (nModifierIndex < 200)
        result = CHAMPION_ATTACK_BOOST_AC_PENALTY;

    //Haste
    else if (nModifierIndex < 300)
        result = CHAMPION_HASTE;

    //Extra damage
    else if (nModifierIndex < 300 + 1*(100/8))
        result = CHAMPION_DMG_BOOST_MAGICAL;
    else if (nModifierIndex < 300 + 2*(100/8))
        result = CHAMPION_DMG_BOOST_ACID;
    else if (nModifierIndex < 300 + 3*(100/8))
        result = CHAMPION_DMG_BOOST_ELECTRICAL;
    else if (nModifierIndex < 300 + 4*(100/8))
        result = CHAMPION_DMG_BOOST_COLD;
    else if (nModifierIndex < 300 + 5*(100/8))
        result = CHAMPION_DMG_BOOST_DIVINE;
    else if (nModifierIndex < 300 + 6*(100/8))
        result = CHAMPION_DMG_BOOST_FIRE;
    else if (nModifierIndex < 300 + 7*(100/8))
        result = CHAMPION_DMG_BOOST_NEGATIVE;
    else if (nModifierIndex < 400)
        result = CHAMPION_DMG_BOOST_SONIC;

    //Increased saves
    else if (nModifierIndex < 400 + 1*(100/13))
        result = CHAMPION_FORTITUDE_BOOST;
    else if (nModifierIndex < 400 + 2*(100/13))
        result = CHAMPION_REFLEX_BOOST;
    else if (nModifierIndex < 400 + 3*(100/13))
        result = CHAMPION_WILL_BOOST;
    else if (nModifierIndex < 400 + 4*(100/13))
        result = CHAMPION_WILL_BOOST_FORTITUDE_PENALTY;
    else if (nModifierIndex < 400 + 5*(100/13))
        result = CHAMPION_WILL_BOOST_REFLEX_PENALTY;
    else if (nModifierIndex < 400 + 6*(100/13))
        result = CHAMPION_WILL_BOOST_OTHERS_PENALTY;
    else if (nModifierIndex < 400 + 7*(100/13))
        result = CHAMPION_FORTITUDE_BOOST_WILL_PENALTY;
    else if (nModifierIndex < 400 + 8*(100/13))
        result = CHAMPION_FORTITUDE_BOOST_REFLEX_PENALTY;
    else if (nModifierIndex < 400 + 9*(100/13))
        result = CHAMPION_FORTITUDE_BOOST_OTHERS_PENALTY;
    else if (nModifierIndex < 400 + 10*(100/13))
        result = CHAMPION_REFLEX_BOOST_FORTITUDE_PENALTY;
    else if (nModifierIndex < 400 + 11*(100/13))
        result = CHAMPION_REFLEX_BOOST_WILL_PENALTY;
    else if (nModifierIndex < 400 + 12*(100/13))
        result = CHAMPION_REFLEX_BOOST_OTHERS_PENALTY;
    else if (nModifierIndex < 500)
        result = CHAMPION_SAVES_BOOST;

    //Physical immunity
    else if (nModifierIndex < 500 + 1*(100/6))
        result = CHAMPION_IMMUNITY_PIERCING;
    else if (nModifierIndex < 500 + 2*(100/6))
        result = CHAMPION_IMMUNITY_SLASHING;
    else if (nModifierIndex < 500 + 3*(100/6))
        result = CHAMPION_IMMUNITY_BLUDGEONING;
    else if (nModifierIndex < 500 + 4*(100/6))
        result = CHAMPION_IMMUNITY_PHYSICAL_VULNERABILITY_SONIC;
    else if (nModifierIndex < 500 + 5*(100/6))
        result = CHAMPION_RESISTANCE_PHYSICAL_AC_PENALTY;
    else if (nModifierIndex < 600)
        result = CHAMPION_IMMUNITY_PHYSICAL;

    //Non-physical immunity
    else if (nModifierIndex < 600 + 1*(100/15))
        result = CHAMPION_IMMUNITY_MAGICAL;
    else if (nModifierIndex < 600 + 2*(100/15))
        result = CHAMPION_IMMUNITY_ACID;
    else if (nModifierIndex < 600 + 3*(100/15))
        result = CHAMPION_IMMUNITY_ELECTRICAL;
    else if (nModifierIndex < 600 + 4*(100/15))
        result = CHAMPION_IMMUNITY_COLD;
    else if (nModifierIndex < 600 + 5*(100/15))
        result = CHAMPION_IMMUNITY_DIVINE;
    else if (nModifierIndex < 600 + 6*(100/15))
        result = CHAMPION_IMMUNITY_FIRE;
    else if (nModifierIndex < 600 + 7*(100/15))
        result = CHAMPION_IMMUNITY_NEGATIVE;
    else if (nModifierIndex < 600 + 8*(100/15))
        result = CHAMPION_IMMUNITY_SONIC;
    else if (nModifierIndex < 600 + 9*(100/15))
        result = CHAMPION_IMMUNITY_ACID_VULNERABILITY_ELECTRICAL;
    else if (nModifierIndex < 600 + 10*(100/15))
        result = CHAMPION_IMMUNITY_ELECTRICAL_VULNERABILITY_ACID;
    else if (nModifierIndex < 600 + 11*(100/15))
        result = CHAMPION_IMMUNITY_COLD_VULNERABILITY_FIRE;
    else if (nModifierIndex < 600 + 12*(100/15))
        result = CHAMPION_IMMUNITY_DIVINE_VULNERABILITY_NEGATIVE;
    else if (nModifierIndex < 600 + 13*(100/15))
        result = CHAMPION_IMMUNITY_FIRE_VULNERABILITY_COLD;
    else if (nModifierIndex < 600 + 14*(100/15))
        result = CHAMPION_IMMUNITY_NEGATIVE_VULNERABILITY_DIVINE;
    else if (nModifierIndex < 700)
        result = CHAMPION_IMMUNITY_PHYSICAL_VULNERABILITY_SONIC;

    //Damage reduction
    else if (nModifierIndex < 800)
        result = CHAMPION_REDUCTION;

    //Health regeneration
    else if (nModifierIndex < 900)
        result = CHAMPION_REGENERATION;

    //Spell resistance
    else if (nModifierIndex < 1000)
        result = CHAMPION_SPELL_RESISTANCE;
    
    return result;
}

// Function that should apply appropriate modifiers based on nIndex in range [0, modifiersNumber-1].
// Modifiers can be anything, be it effects such as AC increase, a weapon replacement, or even some extra enemy guards.
// You can base your modifier on the encounter's biome's level (nLevel), for example to give the champion more temporary hit points on higher levels.
// nIndex may be treated as a binary flag if you'd like your champions to be able to have multiple modifiers at once
// (for example, number 6 written in binary as 110 may represent presence of modifiers X and Y and absence of modifier Z),
// but this may make it difficult to make the champion balanced
void OnApplyChampionModifier(object oChampion, int nModifierIndex, int nLevel)
{
    int modifier = _ModifierToChampionConstant(nModifierIndex);
    ApplyModifierToChampion(oChampion, modifier, nLevel);
}

// Function that should return a champion's full name based on the modifier index provided, a LANGUAGE_* constant from inc_language and the randomness seed name.
// For example, if a modifier index corresponds to an AC increase effect and the champion is an orc,
// a good name idea may be "Gnash the Unhittable".
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetChampionName(int nModifierIndex, string sSeedName, int nLanguage)
{
    int modifier = _ModifierToChampionConstant(nModifierIndex);
    string name = RandomSpiderName(sSeedName);
    return name;
}

// Function that should return the description of a champion that will be spoken by a bounty hunter NPC (if there is a bounty on that champion).
// Information on the value of the bounty should be included in it. Information specific to the champion's modifier may also be included, but isn't necessary.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetChampionDescription(string sChampionName, int nBounty, int nModifierIndex, string sSeedName, int nLanguage)
{
    return GetLocalizedString(
        "A spider called "+sChampionName+" by the locals has been ambushing villagers traversing the region. I can offer you "+IntToString(nBounty)+" gold pieces if you bring me its web gland.", 
        "Paj�k zwany "+sChampionName+" przez tubylc�w atakuje z zaskoczenia podr�uj�cych po regionie. Mog� ci zaoferowa� "+IntToString(nBounty)+" sztuk z�ota, je�li zabijesz go i przyniesiesz mi jego gruczo� na dow�d czynu.");
}


// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on the champion creature.
// You may also base your rumor on the modifier index provided to make NPCs gossip about the champion's specific strengths and weaknesses.
string OnGetChampionRumor(string sChampionName, int nRumorIndex, int nModifierIndex, int nLanguage)
{
    int modifier = _ModifierToChampionConstant(nModifierIndex);
    string rumorPart = GetModifierRumorString(sChampionName, modifier, GENDER_MALE);
    switch (nRumorIndex)
    {
        default:
            return GetLocalizedString(sChampionName+" the spider has been the talk of the town lately. They say that it "+rumorPart+".", "Ten wielki paj�k "+sChampionName+" jest ostatnio g��wnym tematem bywalc�w w tawernie. M�wi�, �e "+rumorPart+".");
    }
    return "";
}