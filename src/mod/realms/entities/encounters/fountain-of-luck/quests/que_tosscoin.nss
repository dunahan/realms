#include "inc_arrays"
#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_encounters"
#include "inc_towns"
#include "inc_tiles"
#include "inc_biomes"
#include "inc_common"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_tosscoin";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 1;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    result.gold = 300 + RandomNext(201, sSeedName);
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "�yczenie pomy�lno�ci";
        if (isMale)
            result.journalInitial = questgiverName+" z "+townName+" przekaza� mi swoj� pami�tk� rodzinn�, star� monet�, kt�r� mam wrzuci� w jego imieniu do Fontanny Pomy�lno�ci. Otrzymam za to "+IntToString(questReward.gold)+" sztuk z�ota.";
        else
            result.journalInitial = questgiverName+" z "+townName+" przekaza�a mi swoj� pami�tk� rodzinn�, star� monet�, kt�r� mam wrzuci� w jej imieniu do Fontanny Pomy�lno�ci. Otrzymam za to "+IntToString(questReward.gold)+" sztuk z�ota.";
        if (isMale)
            result.journalSuccess = "Moneta spoczywa ju� na dnie fontanny. Pora wr�ci� do "+townName+", "+questgiverName+" powinien by� zadowolony i wr�czy� mi obiecane "+IntToString(questReward.gold)+" sztuk z�ota.";
        else
            result.journalSuccess = "Moneta spoczywa ju� na dnie fontanny. Pora wr�ci� do "+townName+", "+questgiverName+" powinna by� zadowolona i wr�czy� mi obiecane "+IntToString(questReward.gold)+" sztuk z�ota.";
        if (isMale)
            result.journalFailure = "Moneta przepad�a. "+questgiverName+" z "+townName+" z pewno�ci� b�dzie rozczarowany tymi wie�ciami.";
        else
            result.journalFailure = "Moneta przepad�a. "+questgiverName+" z "+townName+" z pewno�ci� b�dzie rozczarowany tymi wie�ciami.";
        result.journalSuccessFinished = "Je�li wierzy� w moc fontanny, "+questgiverName+" b�dzie w najbli�szym czasie wie�� bardzo pomy�lne �ycie. To nie by�o trudne zadanie, ale zap�ata by�a uczciwa.";
        if (isMale)
            result.journalAbandoned = "Wrzucanie monet do wody to nie jest zadanie dla mnie. "+questgiverName+" b�dzie musia� uda� si� do fontanny sam.";
        else
            result.journalAbandoned = "Wrzucanie monet do wody to nie jest zadanie dla mnie. "+questgiverName+" b�dzie musia�a uda� si� do fontanny sama.";
        result.npcIntroLine = "Znasz mo�e legendy o Fontannach Pomy�lno�ci? Wiesz co� o obietnicy szcz�cia dla tego, kto wrzuci do fontanny monet�?";
        if (isMale)
            result.npcContentLine = "Mam monet�, kt�r� chcia�bym wrzuci� do jednej takiej fontanny. To nie byle jaka sztuka z�ota, lecz cenna pami�tka rodzinna z wybit� na sobie twarz� mego dziadka. My�l�, �e w zamian za tak� monet� Fontanna obdarzy mnie szczeg�lnym szcz�ciem!";
        else
            result.npcContentLine = "Mam monet�, kt�r� chcia�abym wrzuci� do jednej takiej fontanny. To nie byle jaka sztuka z�ota, lecz cenna pami�tka rodzinna z wybit� na sobie twarz� mego dziadka. My�l�, �e w zamian za tak� monet� Fontanna obdarzy mnie szczeg�lnym szcz�ciem!";
        if (isMale)
            result.npcRewardLine = "Nie chc� jednak ryzykowa� kradzie�y, je�li mia�bym wyruszy� poza "+townName+" samemu. Czy zgodzisz si� wzi�� moj� monet� i wrzuci� j� dla mnie do fontanny w zamian za "+IntToString(questReward.gold)+" sztuk z�ota? Musisz jednak zachowa� ostro�no��, aby jej nie zgubi� w jakiej� potyczce.";
        else
            result.npcRewardLine = "Nie chc� jednak ryzykowa� kradzie�y, je�li mia�abym wyruszy� poza "+townName+" sama. Czy zgodzisz si� wzi�� moj� monet� i wrzuci� j� dla mnie do fontanny w zamian za "+IntToString(questReward.gold)+" sztuk z�ota? Musisz jednak zachowa� ostro�no��, aby jej nie zgubi� w jakiej� potyczce.";
        result.npcBeforeCompletionLine = "Oh, to ty. Jakie wie�ci przynosisz o mojej monecie?";
        result.playerReportingSuccess = "Zosta�a wrzucona do fontanny zgodnie z twoj� pro�b�.";
        result.playerReportingFailure = "Przykro mi, ale twoja moneta przepad�a.";
        if (isMale)
            result.npcReceivedSuccess = "Tak my�la�em! Czuj�, �e szcz�cie zacz�o mi dopisywa�! Oto twoja nagroda.";
        else
            result.npcReceivedSuccess = "Tak my�la�am! Czuj�, �e szcz�cie zacz�o mi dopisywa�! Oto twoja nagroda.";
        if (isMale)
            result.npcReceivedFailure = "Tego si� obawia�em, bo nie odczuwam �adnej r�nicy. Najwyra�niej nie jest mi dane zazna� u�miechu losu.";
        else
            result.npcReceivedFailure = "Tego si� obawia�am, bo nie odczuwam �adnej r�nicy. Najwyra�niej nie jest mi dane zazna� u�miechu losu.";
        result.npcAfterSuccess = "Czuj� to! Szcz�cie mi sprzyja! Czas p�j�� poszuka� zakopanych skarb�w!";
        result.npcAfterFailure = "Nie mam ani szcz�cia, ani mojej rodzinnej pami�tki. A mo�e nie mam jej dlatego, �e nie mam szcz�cia.";
        if (isMale)
            result.npcAfterRefusal = "Dlaczego tak dziwnie na mnie patrzysz? Czy to �le, �e chcia�bym, aby los w ko�cu si� do mnie u�miechn��?";
        else
            result.npcAfterRefusal = "Dlaczego tak dziwnie na mnie patrzysz? Czy to �le, �e chcia�abym, aby los w ko�cu si� do mnie u�miechn��?";
    }
    else
    {
        result.journalName = "Fortune wish";
        if (isMale)
            result.journalInitial = questgiverName+" of "+townName+" has given me his family heirloom, an old coin, which I am to toss into a Fountain of Fortune in his stead. For that I will receive "+IntToString(questReward.gold)+" gold.";
        else
            result.journalInitial = questgiverName+" of "+townName+" has given me her family heirloom, an old coin, which I am to toss into a Fountain of Fortune in his stead. For that I will receive "+IntToString(questReward.gold)+" gold.";
        result.journalSuccess = "The coin is resting in the fountain. It is time to go back to "+townName+". "+questgiverName+" should happily hand me my "+IntToString(questReward.gold)+" gold.";
        result.journalFailure = "The coin is gone. "+questgiverName+" of "+townName+" surely will not be happy about this.";
        result.journalSuccessFinished = "If I am to believe in the fountain's power, "+questgiverName+" will live a prosperous life in the near future. My task was not difficult, but the payment was more than fair.";
        if (isMale)
            result.journalAbandoned = "Tossing coins into fountains is not a task for someone like me. "+questgiverName+" will have to do it himself.";
        else
            result.journalAbandoned = "Tossing coins into fountains is not a task for someone like me. "+questgiverName+" will have to do it herself.";
        result.npcIntroLine = "Do you know the legends about Fountains of Fortune? About the promise of prosperity to those who toss coins into one of them?";
        result.npcContentLine = "I have a coin I would like to toss into one such fountain. Not any coin, mind you, but a family heirloom with my grandfather's face minted on it. I think that the Fountain would bless me with extraordinary fortune for such a coin!";
        result.npcRewardLine = "However, I do not want to risk getting robbed if I tried to leave "+townName+" myself. Will you agree to take my coin and toss it in my stead into the Fountain in exchange for "+IntToString(questReward.gold)+" gold? Just be careful not to lose it in a fight.";
        result.npcBeforeCompletionLine = "Oh, it is you. What news of my coin do you bring?";
        result.playerReportingSuccess = "It's been tossed into the Fountain, just like you requested.";
        result.playerReportingFailure = "I am sorry, but your coin has been lost.";
        result.npcReceivedSuccess = "I knew it! I can feel fortune smiling upon me already! Here is your reward.";
        result.npcReceivedFailure = "This is what I have been afraid of, because I cannot feel any difference. It seems I am not destined to be fortunate.";
        result.npcAfterSuccess = "I can feel it! Fortune is on my side! It is time for me to go search for buried treasures!";
        result.npcAfterFailure = "I am left with neither fortune, nor my heirloom. Or maybe I am left without my heirloom, because I am unfortunate?";
        result.npcAfterRefusal = "Why are you giving me this look? Is it wrong for me to wish for fortune to smile upon me at last?";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    int twojaStara = 654;
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return isMale
            ? questgiverName+" has told me he wanted to visit the Fountain of Fortune outside of town."
            : questgiverName+" has told me she wanted to visit the Fountain of Fortune outside of town.";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return isMale 
            ? questgiverName+" opowiada� mi, �e planuje wybra� si� do Fontanny Pomy�lno�ci za osad�."
            : questgiverName+" opowiada�a mi, �e planuje wybra� si� do Fontanny Pomy�lno�ci za osad�.";
    }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{
    object item = CreateItemOnObject("it_coin", oConversingPC);
    SetLocalObject(item, "Quest", oQuest);
    SetLocalObject(oQuest, "Coin", item);

    object encounter = GetQuestEncounter(oQuest);
    object fountain = GetLocalObject(encounter, "Fountain");
    SetEventScript(fountain, EVENT_SCRIPT_PLACEABLE_ON_USED, "obj_fountenemies");
}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{
    //Quest parameters
    int intimidationDc = 11;
    int persuadeDc = 11;
    int fountEnemies = RandomNext(3, sSeedName) == 0;

    if (!fountEnemies)
        return;

    //Hostile spawning logic
    SetLocalInt(oQuest, "INTIMIDATE_DC", intimidationDc);
    SetLocalInt(oQuest, "PERSUADE_DC", persuadeDc);

    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    int level = GetRegionStartingLevel(biome);
    
    string spawnerSeedName = ObjectToString(oQuest)+"_SpawnerSeed";
    SetRandomSeed(RandomNextAny(sSeedName), spawnerSeedName);
    SetLocalString(oQuest, "SpawnerSeed", spawnerSeedName);

    string group;
    switch (RandomNext(3, sSeedName))
    {
        case 0:
            group = "grp_goblins";
            break;
        case 1:
            group = "grp_orcs";
            break;
        case 2:
            group = "grp_bandits";
            break;
    }
    SetLocalString(oQuest, "CreatureGroup", group);

    object fountain = GetLocalObject(encounter, "Fountain");
    SetLocalObject(fountain, "Quest", oQuest);
}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{
    if (GetQuestState(oQuest) == QUEST_STATE_COMPLETED)
        return;

    object coin = GetLocalObject(oQuest, "Coin");
    object possessor = GetItemPossessor(coin);
    
    //Fail the quest if the person carrying the item is killed - or if no one was carrying the item at the time of death,
    //to prevent players from being smartasses and dropping the item to the ground before a fight
    if ((GetIsObjectValid(possessor) && oDead != possessor && GetPCMaster(possessor) != OBJECT_INVALID))
        return;

    UpdateQuestState(oQuest, QUEST_STATE_FAILED);
    DestroyObject(coin);
}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{

}