#include "inc_factions"
#include "inc_groups"

void main()
{
    object fountain = GetCreatureGroupSpawnerObject();
    object quest = GetLocalObject(fountain, "Quest");

    SetTag(OBJECT_SELF, "cre_founthost");
    SetFaction(OBJECT_SELF, FACTION_TEMPORARY_NEUTRAL);
    SetLocalObject(OBJECT_SELF, "Fountain", fountain);
    SetLocalObject(OBJECT_SELF, "Quest", quest);
}