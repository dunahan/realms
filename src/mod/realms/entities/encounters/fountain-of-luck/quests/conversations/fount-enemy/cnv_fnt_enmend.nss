#include "inc_convabort"
#include "inc_npcroutines"
#include "inc_scriptevents"

//Mark the Fountain of Fortune enemy conversation as finished/aborted, run away or start fighting
void main()
{
    MarkConversationEnd();
    
    object fount = GetLocalObject(OBJECT_SELF, "Fountain");
    SetEventScript(fount, EVENT_SCRIPT_PLACEABLE_ON_USED, "obj_convonuse");

    string tag = GetTag(OBJECT_SELF);

    if (GetLocalInt(OBJECT_SELF, "Escape"))
    {
        object PC = GetLastPCSpeaker();
        location moveAwayFrom = GetLocation(GetIsObjectValid(PC) ? PC : OBJECT_SELF);
        RunAway(OBJECT_SELF, moveAwayFrom, tag);
        SetLocalInt(fount, "EnemiesDealtWith", TRUE);
        return;
    }

    if (GetIsCreaturePreparingForAttack(OBJECT_SELF))
        LaunchAttackIfPrepared(OBJECT_SELF, FACTION_HOSTILE, tag);
    else
        PrepareCreaturesForAttack(OBJECT_SELF, FACTION_HOSTILE, tag, "", 0.0f);
}
