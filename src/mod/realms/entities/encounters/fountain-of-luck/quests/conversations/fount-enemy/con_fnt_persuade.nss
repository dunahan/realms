#include "inc_partyskills"
#include "inc_reputation"

//Return TRUE if party persuade check succeeds (and the PC has at least 10 gold)
int StartingConditional()
{
    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    int dc = GetLocalInt(quest, "PERSUADE_DC") + GetReputationDCModifier();
    object PC = GetPCSpeaker();

    return GetGold(PC) >= 10 && GetIsPartySkillSuccessful(PC, SKILL_PERSUADE, dc);
}
