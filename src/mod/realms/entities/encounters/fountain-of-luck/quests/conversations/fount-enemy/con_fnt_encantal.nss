// Return FALSE if the PCs have already talked to enemies in this quest,
// FALSE otherwise (also, mark the enemies as having been talked to)
int StartingConditional()
{
    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    if (GetLocalInt(quest, "EnemyTalkedTo"))
        return FALSE;

    SetLocalInt(quest, "EnemyTalkedTo", TRUE);
    return TRUE;
}
