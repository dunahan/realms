#include "inc_partyskills"
#include "inc_reputation"

//Return TRUE if party intimidate check succeeds
int StartingConditional()
{
    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    int dc = GetLocalInt(quest, "INTIMIDATE_DC") + GetReputationDCModifier();

    return GetIsPartySkillSuccessful(GetPCSpeaker(), SKILL_INTIMIDATE, dc);
}
