//Return TRUE if the lore check succeeded and the fountain is NOT safe to use
int StartingConditional()
{
    return GetLocalInt(OBJECT_SELF, "LoreSuccess") && GetLocalInt(OBJECT_SELF, "Unsafe");
}
