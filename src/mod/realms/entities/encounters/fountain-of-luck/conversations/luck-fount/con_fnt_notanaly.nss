//Returns TRUE if fountain has not been analyzed yet
int StartingConditional()
{
    return !GetLocalInt(OBJECT_SELF, "PostCheck") && !GetLocalInt(OBJECT_SELF, "Used");
}
