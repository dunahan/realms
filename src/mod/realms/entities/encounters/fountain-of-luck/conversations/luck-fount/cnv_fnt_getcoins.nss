#include "inc_common"

//Mark the fountain as used, change the speaker PC's alignment towards chaotic and give them coins from the fountain
void main()
{
    int coins = GetLocalInt(OBJECT_SELF, "Coins");
    object PC = GetPCSpeaker();

    GiveGoldToCreature(PC, coins);
    AdjustAligmentOfAllPCs(ALIGNMENT_CHAOTIC, 2);
    SetLocalInt(OBJECT_SELF, "Used", TRUE);
}
