#include "inc_common"

//Mark the fountain as used, take a coin from the PC and give the present party members debuffs
void Debuff(object oParty)
{
    effect debuff = EffectSavingThrowDecrease(SAVING_THROW_ALL, 1);
    debuff = SupernaturalEffect(debuff);
    ApplyEffectToObject(DURATION_TYPE_PERMANENT, debuff, oParty);
}

void main()
{
    object PC = GetPCSpeaker();

    TakeGoldFromCreature(1, PC, TRUE);
    SetLocalInt(OBJECT_SELF, "Used", TRUE);

    object party = GetFirstFactionMember(PC, FALSE);
    while (GetIsObjectValid(party))
    {
        Debuff(party);
        party = GetNextFactionMember(PC, FALSE);
    }
}
