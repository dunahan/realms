#include "inc_encounters"
#include "inc_quests"

//Toss the quest coin into the fountain
void main()
{
    object encounter = GetLocalObject(OBJECT_SELF, "Encounter");
    object quest = GetQuestOfEncounter(encounter);
    object coin = GetLocalObject(quest, "Coin");

    DestroyObject(coin);
    UpdateQuestState(quest, QUEST_STATE_COMPLETED);
}
