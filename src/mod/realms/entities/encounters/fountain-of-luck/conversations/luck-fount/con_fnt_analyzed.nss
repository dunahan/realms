//Returns TRUE if fountain has been analyzed
int StartingConditional()
{
    return GetLocalInt(OBJECT_SELF, "PostCheck") && !GetLocalInt(OBJECT_SELF, "Used");
}
