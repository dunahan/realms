#include "inc_common"

// Prices for various blessings in a temple

// Get a price for a given blessing and a biome index
int GetBlessingPrice(string sBlessing, int nBiomeIndex);

// Cast temple healing on the PC speaker and their party in the same area
void TempleHeal();

// Cast temple blessing on the PC speaker and their party in the same area
void TempleBless(string sBlessing);

int GetBlessingPrice(string sBlessing, int nBiomeIndex)
{
    switch (nBiomeIndex)
    {
        case 0:
            if (sBlessing == "Heal")
                return 200;
            else if (GetStringLeft(sBlessing, 1) == "G")
                return 400;
            else
                return 200;
            //SCALING
    }
    return 0;
}

void TempleHeal()
{
    object PC = GetPCSpeaker();
    ActionCastFakeSpellAtObject(SPELL_GREATER_RESTORATION, PC);
    object member = GetFirstFactionMember(PC, FALSE);
    while (GetIsObjectValid(member))
    {
        if (GetArea(member) == GetArea(OBJECT_SELF))
        {
            int heal = GetMaxHitPoints(member) - GetCurrentHitPoints(member);
            effect eHeal = EffectHeal(heal);
            if (heal < 1)
                heal = 1;
            ApplyEffectToObject(DURATION_TYPE_INSTANT, eHeal, member);
            ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_RESTORATION_GREATER), member);

            effect ef = GetFirstEffect(member);
            while (GetIsEffectValid(ef))
            {
                if (GetEffectSubType(ef) != SUBTYPE_SUPERNATURAL)
                {
                    int type = GetEffectType(ef);
                    if (type == EFFECT_TYPE_ABILITY_DECREASE ||
                            type == EFFECT_TYPE_AC_DECREASE ||
                            type == EFFECT_TYPE_ATTACK_DECREASE ||
                            type == EFFECT_TYPE_DAMAGE_DECREASE ||
                            type == EFFECT_TYPE_DAMAGE_IMMUNITY_DECREASE ||
                            type == EFFECT_TYPE_SAVING_THROW_DECREASE ||
                            type == EFFECT_TYPE_SPELL_RESISTANCE_DECREASE ||
                            type == EFFECT_TYPE_SKILL_DECREASE ||
                            type == EFFECT_TYPE_BLINDNESS ||
                            type == EFFECT_TYPE_DEAF ||
                            type == EFFECT_TYPE_CURSE ||
                            type == EFFECT_TYPE_DISEASE ||
                            type == EFFECT_TYPE_POISON ||
                            type == EFFECT_TYPE_PARALYZE ||
                            type == EFFECT_TYPE_NEGATIVELEVEL)
                    {
                        RemoveEffect(member, ef);
                    }
                }
                ef = GetNextEffect(member);
            }
        }

        member = GetNextFactionMember(PC, FALSE);
    }
}

void TempleBless(string sBlessing)
{
    string normalBlessing, greaterBlessing;
    if (GetStringLeft(sBlessing, 1) == "G")
    {
        normalBlessing = GetStringRight(sBlessing, GetStringLength(sBlessing)-1);
        greaterBlessing = sBlessing;
    }
    else
    {
        normalBlessing = sBlessing;
        greaterBlessing = "G" + sBlessing;
    }

    object PC = GetPCSpeaker();
    ActionCastFakeSpellAtObject(SPELL_BLESS, PC);
    object member = GetFirstFactionMember(PC, FALSE);
    while (GetIsObjectValid(member))
    {
        if (GetArea(member) == GetArea(OBJECT_SELF))
        {
            effect normalBlessEf = GetEffectByTag(member, "TEMPLE_BLESS_"+normalBlessing);
            effect greaterBlessEf = GetEffectByTag(member, "TEMPLE_BLESS_"+greaterBlessing);

            //Do nothing if the same or greater blessing of this type is already applied
            if ((GetIsEffectValid(normalBlessEf) && sBlessing == normalBlessing) || GetIsEffectValid(greaterBlessEf))
            {
            }
            //Else apply the blessing
            else
            {
                if (GetIsEffectValid(normalBlessEf))
                    RemoveEffect(member, normalBlessEf);

                effect newBless;
                if (sBlessing == "War")
                {
                    newBless = EffectAttackIncrease(1);
                }
                else if (sBlessing == "GWar")
                {
                    newBless = EffectAttackIncrease(2);
                }
                else if (sBlessing == "Protection")
                {
                    newBless = EffectACIncrease(1);
                }
                else if (sBlessing == "GProtection")
                {
                    newBless = EffectACIncrease(2);
                }
                else if (sBlessing == "Vitality")
                {
                    int extraHp = GetMaxHitPoints(member) / 4;
                    newBless = EffectTemporaryHitpoints(extraHp);
                }
                else if (sBlessing == "GVitality")
                {
                    int extraHp = GetMaxHitPoints(member) / 2;
                    newBless = EffectTemporaryHitpoints(extraHp);
                }
                else if (sBlessing == "Fortune")
                {
                    newBless = EffectSavingThrowIncrease(SAVING_THROW_ALL, 1);
                }
                else if (sBlessing == "GFortune")
                {
                    newBless = EffectSavingThrowIncrease(SAVING_THROW_ALL, 2);
                }
                else if (sBlessing == "Skill")
                {
                    newBless = EffectSkillIncrease(SKILL_ALL_SKILLS, 2);
                }
                else if (sBlessing == "GSkill")
                {
                    newBless = EffectSkillIncrease(SKILL_ALL_SKILLS, 4);
                }

                newBless = ExtraordinaryEffect(newBless);
                newBless = TagEffect(newBless, "TEMPLE_BLESS_"+sBlessing);
                ApplyEffectToObject(DURATION_TYPE_PERMANENT, newBless, member);
                ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_IMPROVE_ABILITY_SCORE), member);
            }
        }
        member = GetNextFactionMember(PC, FALSE);
    }
}
