#include "inc_tmpservice"
#include "inc_plots"
#include "inc_realms"
#include "inc_tokens"

int StartingConditional()
{
    string bless = GetScriptParam("Blessing");
    object realm = GetRealm();
    object plot = GetRealmPlot(realm);
    int biome = GetPlotCurrentBiomeIndex(plot);
    int price = GetBlessingPrice(bless, biome);

    SetCustomTokenEx(5001, IntToString(price));

    return TRUE;
}
