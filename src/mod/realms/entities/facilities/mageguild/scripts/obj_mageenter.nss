#include "inc_tiles"
#include "inc_horses"

void main()
{
    object user = GetClickingObject();  
    object tile = GetTile(OBJECT_SELF);
    object wp = GetTileWaypoint(tile, "wp_mageexit");
    AssignCommand(user, JumpToObjectRespectingHorses(wp));
}
