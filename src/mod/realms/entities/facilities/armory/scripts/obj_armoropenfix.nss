// Workaround for the issue with armory entrance door - the PC is sometimes teleported from the elevation after opening the door.

#include "inc_common"
#include "inc_tiles"

void main()
{
    object user = GetLastOpenedBy();
    object tile = GetTile(OBJECT_SELF);
    object wp = GetTileWaypoint(tile, "wp_armoryentrance");
    location destination = GetRotatedLocation(GetLocation(wp));
    AssignCommand(user, JumpToLocation(destination));
}
