#include "inc_arrays"
#include "hnd_biomes"
#include "inc_random"
#include "inc_language"
#include "inc_loot"

//////////////////
// ------------ //
// Biome script //
// ------------ //
//////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string sScriptName = "bio_rural";

// Values of RGB that determine the biome's fog color
int fogColorRed = 104;
int fogColorGreen = 126;
int fogColorBlue = 145;
int fogNightColorRed = 104;
int fogNightColorGreen = 126;
int fogNightColorBlue = 145;

// Percentage chance of a particular weather happening in the region of this biome (must sum up to 100)
int weatherChanceClear = 80;
int weatherChanceRain = 20;
int weatherChanceSnow = 0;

// Skyboxes for particular weather
int skyboxClear = SKYBOX_GRASS_CLEAR;
int skyboxRain = SKYBOX_GRASS_STORM;
int skyboxSnow = SKYBOX_NONE;

// Color representing the biome on the realm map (RGB values from 0 to 255)
int mapRed = 0;
int mapGreen = 65;
int mapBlue = 65;

// String identifier to determine which regular areas should spawn in this biome
// For areasId set to "plains", the module will search for areas with names "plains_X_Y", where X is an individual area template identifier
// and Y is a number denoting what exits are available in the area, padded with zero to two digits.
// Example: "plains_01_03" is an area template "01" of region with areasId "plains" with exits to the north and east
// ("03" can be written as 0011 in binary, with bits corresponding to directions WSEN, respectively)
string areasId = "rural";

// Number of regular area templates (each in 11 variants, because all possible exits combinations are required except dead ends)
int regularAreasNumber = 1;

// Number of basic (typically simple hostiles to fight against) encounter types that can generate in the biome - an encounter has 50% chance of being basic
int basicEncountersNumber = 10;

// Number of advanced encounter types that can generate in the biome - an encounter has 50% chance of being advanced
int advancedEncountersNumber = 3;

// Chance that an encounter will be advanced as opposed to basic expressed as percentage [0-100]
int advancedEncounterChance = 50;

// Number of special area types that can generate in the biome
int specialAreasNumber = 2;

// Chance of spawning a rest encounter when resting in wilderness in the biome
float restEncounterChance = 0.5f;

// Number of town types that can generate in the biome
int townsNumber = 2;

// Number of tracks that the biome may have as a day theme
int dayTracksNumber = 3;

// Number of tracks that the biome may have as a night theme
int nightTracksNumber = 3;

// Number of tracks that the biome may have as a battle theme
int battleTracksNumber = 4;

// Number of rumors regarding this biome (can be 0)
int rumorsNumber = 1;


//////////////////////
// Script functions //
//////////////////////

// Function that should return ResRef of an available regular area to spawn for nIndex in range [0, regularAreasNumber-1] and provided exits flag (i.e. 15, 14, 13, etc.)
string OnGetBiomeRegularAreaResRef(int nIndex, int nExitsFlag)
{
    string result = areasId + "_";
    string template;

    switch (nIndex)
    {
        case 0:
            template = "01";
            break;
        case 1:
            template = "02";
            break;
        case 2:
            template = "03";
            break;
        case 3:
            template = "04";
            break;
        case 4:
            template = "05";
            break;
        case 5:
            template = "06";
            break;
        case 6:
            template = "07";
            break;
        case 7:
            template = "08";
            break;
        case 8:
            template = "09";
            break;
        case 9:
            template = "10";
            break;
        case 10:
            template = "11";
            break;
        case 11:
            template = "12";
            break;
        case 12:
            template = "13";
            break;
        case 13:
            template = "14";
            break;
        case 14:
            template = "15";
            break;
        case 15:
            template = "16";
            break;
        case 16:
            template = "17";
            break;
        case 17:
            template = "18";
            break;
        case 18:
            template = "19";
            break;
        case 19:
            template = "20";
            break;
        default:
            return "";
    }

    string exitsFlag = nExitsFlag < 10 ? "0" + IntToString(nExitsFlag) : IntToString(nExitsFlag);
    result += template + "_" + exitsFlag;
    return result;
}

// Function that should return available basic encounter script name for nIndex in range [0, basicEncountersNumber-1]
string OnGetBiomeBasicEncounterScript(int nIndex)
{
    switch (nIndex)
    {
        case 0: return "enc_empty";
        case 1: return "enc_goblins";
        case 2: return "enc_rats";
        case 3: return "enc_orcs";
        case 4: return "enc_wolves";
        case 5: return "enc_spiders";
        case 6: return "enc_bandits";
        case 7: return "enc_badgers";
        case 8: return "enc_fbeetles";
        case 9: return "enc_wererats";
    }
    return "";
}

// Function that should return available advanced encounter script name for nIndex in range [0, advancedEncountersNumber-1]
string OnGetBiomeAdvancedEncounterScript(int nIndex)
{
    switch (nIndex)
    {
        case 0: return "enc_luckfount";
        case 1: return "enc_transmuter";
        case 2: return "enc_ranger";
    }
    return "";
}

// Function that should return available special area script name for nIndex in range [0, specialAreasNumber-1]
string OnGetBiomeSpecialAreaScript(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return "spe_arena";
        case 1:
            return "spe_bandtoll";
    }
    return "";
}

// Function that should return available town script name for nIndex in range [0, townsNumber-1]
string OnGetBiomeTownScript(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return "twn_humvil"; //Human village
        case 1:
            return "twn_halvil"; //Halfling village
    }
    return "";
}

// Function that should return available day track constant for nIndex in range [0, dayTracksNumber-1]
int OnGetBiomeDayTrack(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return TRACK_RURALDAY2;
        case 1:
            return 83;
        case 2:
            return 133;
    }
    return -1;
}

// Function that should return available night track constant for nIndex in range [0, nightTracksNumber-1]
int OnGetBiomeNightTrack(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return TRACK_RURALNIGHT;
        case 1:
            return 137;
        case 2:
            return 82;
    }
    return -1;
}

// Function that should return available battle track constant for nIndex in range [0, battleTracksNumber-1]
int OnGetBiomeBattleTrack(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return TRACK_BATTLE_RURAL1;
        case 1:
            return TRACK_HOTU_BATTLE_SMALL;
        case 2:
            return TRACK_BATTLE_CITY3;
        case 3:
            return 91;
    }
    return -1;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1]
string OnGetBiomeRumor(int nIndex, int nLanguage)
{
    if (nLanguage == LANGUAGE_ENGLISH)
        switch (nIndex)
        {
            case 0:
                return "It's a good thing we live on plains and not in some dark forest.";
        }
    if (nLanguage == LANGUAGE_POLISH)
        switch (nIndex)
        {
            case 0:
                return "Dobrze, �e nie mieszkamy w jakim� ciemnym lesie, tylko na polankach.";
        }
    return "";
}

// Function that should return a random name for a region of this biome, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomBiomeRegionName(string sSeedName, int nLanguage)
{
    switch (RandomNext(5, sSeedName))
    {
        case 0:
            return "Wielkopolska";
        case 1:
            return "Ma�opolska";
        case 2:
            return "Mazowsze";
        case 3:
            return "Kujawy";
        case 4:
            return "Pomorze";
    }
    return "";
}

// Function that should return a resref of an item for player level equal to nLevel, item's stack size and/or gold.
// nLootQuality is a LOOT_QUALITY_* constant and represents the relative power/worth of that item for a PC at level nLevel and gold.
// As a rule of thumb, player at nLevel should find such an item with LOOT_QUALITY_GOOD useful, but not "great", while LOOT_QUALITY_AVERAGE could be useful occassionally.
// You can return gold without items by leaving the itemResRef struct field empty or itemStackSize set to 0.
// This function will be called multiple times (with varying loot quality) when a treasure chest is generated to prepare items and gold for it.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct TreasureChestLoot OnGetBiomeLoot(string sSeedName, int nLevel, int nLootQuality)
{
    return GetLoot(sSeedName, nLevel, nLootQuality);
}

// This function should prepare a rest encounter suitable for the biome's level,
// defined as a string array on OBJECT_INVALID named "RestEncounter" containing ResRefs of creatures to spawn in that encounter.
// You can use Random() in this function and not RandomNext() because it does not affect the map generation procedure,
// i.e. it's called only after the map has already been generated.
void OnPrepareRestEncounter(int nLevel)
{
    string array = "RestEncounter";
    CreateStringArray(array);
    int rand = Random(9);

    if (rand == 0)
    {
        int i;
        int num = 3 + Random(3);
        for (i = 0; i < num; i++)
        {
            int rand = Random(3);
            string resref = rand == 0 ? "cre_goblinb" : "cre_goblina";
            AddStringArrayElement(array, resref);
        }
    }
    else if (rand == 1)
    {
        int i;
        int num = 4 + Random(3);
        for (i = 0; i < num; i++)
        {
            int rand = Random(3);
            string resref = "cre_rat";
            AddStringArrayElement(array, resref);
        }
    }
    else if (rand == 2)
    {
        int i;
        int num = 2 + Random(2);
        for (i = 0; i < num; i++)
        {
            int rand = Random(3);
            string resref = rand == 0 ? "cre_orcyoungb" : "cre_orcyounga";
            AddStringArrayElement(array, resref);
        }
    }
    else if (rand == 3)
    {
        int i;
        int num = 1 + Random(2);
        for (i = 0; i < num; i++)
        {
            string resref = "cre_wolf";
            AddStringArrayElement(array, resref);
        }
    }
    else if (rand == 4)
    {
        int i;
        int num = 3 + Random(3);
        for (i = 0; i < num; i++)
        {
            string resref = "cre_smallspider";
            AddStringArrayElement(array, resref);
        }
    }
    else if (rand == 5)
    {
        int i;
        int num = 2 + Random(3);
        for (i = 0; i < num; i++)
        {
            int rand = Random(3);
            string resref = rand == 0 ? "cre_banditb" : "cre_bandita";
            AddStringArrayElement(array, resref);
        }
    }
    else if (rand == 6)
    {
        int i;
        int num = 3 + Random(3);
        for (i = 0; i < num; i++)
        {
            string resref = "cre_badger";
            AddStringArrayElement(array, resref);
        }
    }
    else if (rand == 7)
    {
        int i;
        int num = 3 + Random(3);
        for (i = 0; i < num; i++)
        {
            string resref = "cre_firebeetle";
            AddStringArrayElement(array, resref);
        }
    }
    else if (rand == 8)
    {
        int i;
        int num = 1 + Random(2);
        for (i = 0; i < num; i++)
        {
            string resref = "cre_wererat";
            AddStringArrayElement(array, resref);
        }
    }
}

// Function that should create in lResourceLocation and return a placeable object that yields some overworld resource when clicked,
// based on the biome's level and whether the resource is found underground/in a cave or not.
// Ensure the placeable will yield an appropriate number of resources for the biome's level.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
object OnSpawnResourcePlaceable(string sSeedName, location lResourceLocation, int nLevel, int nIsUndergroundResource)
{
    //Select resource to spawn
    string resourceType;
    if (nIsUndergroundResource)
        resourceType = "metal";
    else switch (RandomNext(3, sSeedName))
        {
            case 0:
                resourceType = "wood";
                break;
            case 1:
                resourceType = "crystal";
                break;
            case 2:
                resourceType = "herb";
                break;
        }

    //Select resource rarity
    string resourceRarity;
    int rarityNumber = RandomNext(10, sSeedName);
    if (rarityNumber < 6)
        resourceRarity = "1";
    else if (rarityNumber < 9)
        resourceRarity = "2";
    else
        resourceRarity = "3";

    //Select number of units to spawn
    int resNum = 1;
    int maxModification = 0;
    switch (nLevel)
    {
        case 1:
            resNum = 1;
            break;
        case 2:
            resNum = 2;
            maxModification = 1;
            break;
        case 3:
            resNum = 3;
            maxModification = 1;
            break;
        case 4:
            resNum = 5;
            maxModification = 1;
            break;
        case 5:
            resNum = 7;
            maxModification = 2;
            break;
        case 6:
            resNum = 8;
            maxModification = 2;
            break;
        case 7:
            resNum = 12;
            maxModification = 3;
            break;
        case 8:
            resNum = 15;
            maxModification = 3;
            break;
        case 9:
            resNum = 18;
            maxModification = 3;
            break;
        case 10:
            resNum = 22;
            maxModification = 4;
            break;
        case 11:
            resNum = 26;
            maxModification = 4;
            break;
        case 12:
            resNum = 30;
            maxModification = 5;
            break;
        case 13:
            resNum = 35;
            maxModification = 5;
            break;
        case 14:
            resNum = 40;
            maxModification = 5;
            break;
        case 15:
            resNum = 45;
            maxModification = 5;
            break;
        case 16:
            resNum = 50;
            maxModification = 5;
            break;
        case 17:
            resNum = 60;
            maxModification = 5;
            break;
    }
    if (resourceType == "metal")
    {
        resNum *= 2;
        maxModification *= 2;
    }
    float randomModifier = IntToFloat(RandomNext(201, sSeedName) - 100) / 100;
    int modification = FloatToInt(randomModifier * maxModification);
    resNum += modification;

    //Spawn resource
    object resource = CreateObject(OBJECT_TYPE_PLACEABLE, "obj_"+resourceType+resourceRarity, lResourceLocation);
    SetLocalInt(resource, "ResNum", resNum);
    return resource;
}