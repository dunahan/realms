#include "inc_factions"
#include "hench_i0_ai"
#include "inc_partyskills"

void main()
{
    //Make the bandits go hostile if someone tries to browse their wagon and a party hide check is failed
    int dc = GetLocalInt(GetArea(OBJECT_SELF), "WAGONHIDE_DC");
    object PC = GetLastOpenedBy();
    if (GetIsPartySkillSuccessful(PC, SKILL_HIDE, dc))
        return;

    int i = 1;
    string tag = "cre_bandtoll";
    object bandit = GetNearestObjectByTag(tag, OBJECT_SELF, i++);
    while (GetIsObjectValid(bandit))
    {
        if (!GetLocalInt(bandit, "STARTED_ATTACKING"))
        {
            SetLocalInt(bandit, "STARTED_ATTACKING", TRUE);
            SetFaction(bandit, FACTION_BANDITS);
            AssignCommand(bandit, HenchDetermineCombatRound());
        }
        bandit = GetNearestObjectByTag(tag, OBJECT_SELF, i++);
    }
}
