#include "inc_factions"
#include "hench_i0_ai"

void main()
{
    //Entering the bandit camp area from one of the triggers
    if (!GetIsPC(GetEnteringObject()) || GetLocalInt(GetArea(OBJECT_SELF), "CAN_PASS"))
        return;

    object area = GetArea(OBJECT_SELF);
    object enterTrigger = GetLocalObject(area, "EnterTrigger");
    if (enterTrigger != OBJECT_INVALID && enterTrigger != OBJECT_SELF)
    {
        int i = 1;
        string tag = "cre_bandtoll";
        object bandit = GetNearestObjectByTag(tag, OBJECT_SELF, i++);
        while (GetIsObjectValid(bandit))
        {
            if (!GetLocalInt(OBJECT_SELF, "STARTED_ATTACKING"))
            {
                SetLocalInt(bandit, "STARTED_ATTACKING", TRUE);
                SetFaction(bandit, FACTION_BANDITS);
                AssignCommand(bandit, HenchDetermineCombatRound());
            }
            bandit = GetNearestObjectByTag(tag, OBJECT_SELF, i++);
        }
        return;
    }

    SetLocalObject(area, "EnterTrigger", OBJECT_SELF);
}
