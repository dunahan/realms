#include "inc_quests"

//Complete the breaking blockade quest if all the bandits die
void main()
{
    object area = GetArea(OBJECT_SELF);
    object bandit = GetFirstObjectInArea(area);
    while (GetIsObjectValid(bandit))
    {
        if (bandit != OBJECT_SELF && GetTag(bandit) == "cre_bandtoll" && !GetIsDead(bandit))
            return;
        bandit = GetNextObjectInArea(area);
    }

    object quest = GetLocalObject(area, "QUEST");
    UpdateQuestState(quest, QUEST_STATE_COMPLETED);
}