#include "inc_common"
#include "inc_language"

// Warn an approaching PC
void main()
{
    object PC = GetLastPerceived();
    object area = GetArea(OBJECT_SELF);
    if (!GetIsPC(PC) || GetLocalInt(area, "CAN_PASS") || GetLocalInt(area, "WARNED"))
        return;

    SetLocalInt(area, "WARNED", TRUE);
    SetFacingObject(PC);
    string msg = GetLocalizedString(
        "Greetings, traveller. Be sure to to pay us a small fee before passing through, are we clear?",
        "Pozdrowienia. Nie zapomnij ui�ci� skromnej op�aty za przej�cie przed wyruszeniem w dalsz� drog�, jasne?");
    SpeakString(msg);
}