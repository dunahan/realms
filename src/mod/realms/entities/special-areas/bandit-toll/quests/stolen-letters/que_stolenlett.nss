#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_tiles"
#include "inc_biomes"
#include "inc_towns"
#include "inc_scriptevents"
#include "inc_common"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_stolenlett";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 40;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object area = GetQuestSpecialArea(quest);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object area = GetQuestSpecialArea(quest);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    result.gold = 450 + RandomNext(201, sSeedName);
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object area = GetQuestSpecialArea(oQuest);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Skradzione listy mi�osne";
        if (isMale)
            result.journalInitial = questgiverName+" z "+townName+" pragnie odzyska� listy mi�osne po zmar�ej �onie, kt�re zosta�y skradzione przez bandyt�w i trzymane w ich obozie. Listy te s� dla zleceniodawcy na tyle wa�ne, �e oferuje "+IntToString(questReward.gold)+" sztuk z�ota za wykonanie zadania.";
        else
            result.journalInitial = questgiverName+" z "+townName+" pragnie odzyska� listy mi�osne po zmar�ym m�u, kt�re zosta�y skradzione przez bandyt�w i trzymane w ich obozie. Listy te s� dla zleceniodawczyni na tyle wa�ne, �e oferuje "+IntToString(questReward.gold)+" sztuk z�ota za wykonanie zadania.";
        result.journalSuccess = "Listy odzyskane. Wystarczy, �e dostarcz� je do "+townName+", a "+questgiverName+" zap�aci mi "+IntToString(questReward.gold)+" sztuk z�ota.";
        if (isMale)
            result.journalFailure = "Listy zosta�y nieszcz�liwie zniszczone podczas jednej ze stoczonych walk. "+questgiverName+" z "+townName+" nie przeczyta ju� nigdy wi�cej ciep�ych s��w od swojej ukochanej.";
        else
            result.journalFailure = "Listy zosta�y nieszcz�liwie zniszczone podczas jednej ze stoczonych walk. "+questgiverName+" z "+townName+" nie przeczyta ju� nigdy wi�cej ciep�ych s��w od swojego ukochanego.";
        if (isMale)
            result.journalSuccessFinished = "Listy dostarczone zosta�y z powrotem do ich adresata. Nie zast�pi� mu tej, kt�ra je napisa�a, ale mo�e u�mierz� troch� b�l.";
        else
            result.journalSuccessFinished = "Listy dostarczone zosta�y z powrotem do ich adresatki. Nie zast�pi� jej tego, kt�ry je napisa�, ale mo�e u�mierz� troch� b�l.";
        result.journalAbandoned = "Odzyskiwanie list�w mi�osnych z r�k bandyt�w? My�l�, �e moje umiej�tno�ci mog� zosta� wykorzystane w lepszy spos�b.";
        if (isMale)
            result.npcIntroLine = "Te listy by�y wszystkim, co mi po niej zosta�o, a teraz przepad�y...";
        else
            result.npcIntroLine = "Te listy by�y wszystkim, co mi po nim zosta�o, a teraz przepad�y...";
        if (isMale)
            result.npcContentLine = "Rok temu tragicznie zmar�a moja �ona. Jedyna pami�tka, kt�ra mi po niej zosta�a, to listy, kt�re pisa�a do mnie przed laty. Zawsze mia�em je ze sob�, wi�c straci�em je, gdy napadli mnie niedawno bandyci i zmusili do oddania wszystkiego, co mia�em.";
        else
            result.npcContentLine = "Rok temu tragicznie zmar� m�j m��. Jedyna pami�tka, kt�ra mi po nim zosta�a, to listy, kt�re pisa� do mnie przed laty. Zawsze mia�am je ze sob�, wi�c straci�am je, gdy napadli mnie niedawno bandyci i zmusili do oddania wszystkiego, co mia�am.";
        result.npcRewardLine = "Prosz�, odzyskaj moje listy. Bandyci najpewniej trzymaj� je w z reszt� �up�w w swoim obozie, w kt�rym si� ufortyfikowali i ��daj� zap�aty od podr�nych za przej�cie. Mog� odda� ci reszt� moich oszcz�dno�ci, "+IntToString(questReward.gold)+" sztuk z�ota.";
        result.npcBeforeCompletionLine = "Przynosisz jakie� wie�ci? A mo�e moje listy?";
        result.playerReportingSuccess = "Mam je ze sob�. Oto one.";
        result.playerReportingFailure = "Przykro mi, ale listy zosta�y zniszczone doszcz�tnie w czasie walki.";
        result.npcReceivedSuccess = "Dzi�kuj�! Nie wiesz, ile to dla mnie znaczy. Prosz�, we� z�oto. Nie jest mi potrzebne.";
        result.npcReceivedFailure = "Nie, tylko nie to... w takim razie nic mi ju� nie zosta�o...";
        result.npcAfterSuccess = "Dzi�kuj� ci raz jeszcze. Zawdzi�czam ci t� reszt� szcz�cia, kt�ra mi zosta�a.";
        if (isMale)
            result.npcAfterFailure = "Dlaczego? Najpierw moja ukochana, a teraz ostatnia pami�tka po niej. Co uczyni�em, �eby na to zas�u�y�?";
        else
            result.npcAfterFailure = "Dlaczego? Najpierw m�j ukochany, a teraz ostatnia pami�tka po nim. Co uczyni�am, �eby na to zas�u�y�?";
        result.npcAfterRefusal = "Ostatnie promyki s�o�ca w moim �yciu gasn�, a ja nie mog� nic zrobi�...";
    }
    else
    {
        result.journalName = "Stolen love letters";
        if (isMale)
            result.journalInitial = questgiverName+" of "+townName+" wishes to retrieve love letters written to him by his late wife. They have been stolen by bandits and can be found among their other loot in their camp. The letters are important enough for my employer to offer me "+IntToString(questReward.gold)+" gold pieces for this task.";
        else
            result.journalInitial = questgiverName+" of "+townName+" wishes to retrieve love letters written to her by her late husband. They have been stolen by bandits and can be found among their other loot in their camp. The letters are important enough for my employer to offer me "+IntToString(questReward.gold)+" gold pieces for this task.";
        result.journalSuccess = "I have the letters. All I need to do now is to deliver them to "+townName+", where "+questgiverName+" will pay me "+IntToString(questReward.gold)+" gold pieces.";
        if (isMale)
            result.journalFailure = "The letters have been destroyed in a battle. "+questgiverName+" from "+townName+" will never be able to read his love's words again.";
        else
            result.journalFailure = "The letters have been destroyed in a battle. "+questgiverName+" from "+townName+" will never be able to read her love's words again.";
        if (isMale)
            result.journalSuccessFinished = "The letters are in their recipient's possession once more. They won't bring his late wife back to life, but maybe they can ease his pain.";
        else
            result.journalSuccessFinished = "The letters are in their recipient's possession once more. They won't bring her late husband back to life, but maybe they can ease her pain.";
        result.journalAbandoned = "Retrieving love letters from bandits? I think my skills can be utilized better.";
        if (isMale)
            result.npcIntroLine = "These letters were everything that remained of her and now they are gone...";
        else
            result.npcIntroLine = "These letters were everything that remained of him and now they are gone...";
        if (isMale)
            result.npcContentLine = "My wife died tragically a year ago. The only memento I had of her were the letters she wrote to me years ago. I carried them with me at all times, so I lost them when bandits attacked me and forced to hand over all my belongings.";
        else
            result.npcContentLine = "My husband died tragically a year ago. The only memento I had of him were the letters he wrote to me years ago. I carried them with me at all times, so I lost them when bandits attacked me and forced to hand over all my belongings.";
        result.npcRewardLine = "Please, get my letters back. The bandits probably keep them with the rest of their loot in a wagon in their camp, where they have fortified themselves and demand payment from travellers. I'll gladly give you the rest of my savings in return, "+IntToString(questReward.gold)+" pieces of gold.";
        result.npcBeforeCompletionLine = "Do you bring any news? Or letters?";
        result.playerReportingSuccess = "I've retrieved them, here they are.";
        result.playerReportingFailure = "I am sorry, but the letters have been destroyed permanently during battle.";
        result.npcReceivedSuccess = "Thank you so much! You do not know how much this means to me. Please, take this gold. I do not need it.";
        result.npcReceivedFailure = "No, not this... this means I have nothing left anymore...";
        result.npcAfterSuccess = "Thank you once again. I owe you that last shard of happiness I still have.";
        if (isMale)
            result.npcAfterFailure = "Why? First my love, then the only reminder I had of her. What have I done to deserve this?";
        else
            result.npcAfterFailure = "Why? First my love, then the only reminder I had of him. What have I done to deserve this?";
        result.npcAfterRefusal = "The last rays of sunshine in my life are fading and there is nothing I can do to hold onto them...";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    object tile = GetTile(questGiver);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_ENGLISH)
        switch (nIndex)
        {
            case 0:
                return isMale 
                    ? questgiverName+" is devastated after the death of his wife. Not only that, I've heard that love letters she once wrote to him have been stolen. I really hope he can get through this."
                    : questgiverName+" is devastated after the death of her husband. Not only that, I've heard that love letters he once wrote to her have been stolen. I really hope she can get through this.";
        }
    if (nLanguage == LANGUAGE_POLISH)
        switch (nIndex)
        {
            case 0:
                return isMale 
                    ? questgiverName+" jest zrozpaczony po �mierci �ony, a teraz na domiar z�ego skradziono mu listy mi�osne, kt�re kiedy� do niego pisa�a. Mam nadziej�, �e to wytrzyma."
                    : questgiverName+" jest zrozpaczona po �mierci m�a, a teraz na domiar z�ego skradziono jej listy mi�osne, kt�re kiedy� do niej pisa�. Mam nadziej�, �e to wytrzyma.";
        }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed.
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{
    object area = GetQuestSpecialArea(oQuest);
    SetLocalObject(area, "QUEST", oQuest);

    object wagon = GetNearestObjectByTag("obj_banditwagon", GetFirstObjectInArea(area));
    object letters = CreateItemOnObject("it_loveletters", wagon);
    
    SetLocalObject(letters, "Quest", oQuest);
    SetLocalObject(oQuest, "Letters", letters);
}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{
    object letters = GetLocalObject(oQuest, "Letters");
    object possessor = GetItemPossessor(letters);
    
    //Fail the quest if the person carrying the item is killed - or if no one was carrying the item at the time of death,
    //to prevent players from being smartasses and dropping the item to the ground before a fight
    if (!GetLocalInt(letters, "PickedUp") || (GetIsObjectValid(possessor) && oDead != possessor && GetPCMaster(possessor) != OBJECT_INVALID))
        return;

    UpdateQuestState(oQuest, QUEST_STATE_FAILED);
    DestroyObject(letters);
}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{
    
}