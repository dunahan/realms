// Tag-based script template.
// This is intended to be a starting point for writing an item's tag-based script.
// Copy this to a script whose name is the tag of the item in question.
// Edit the event handlers (scroll down to find them) as desired.

#include "x2_inc_switches"
#include "inc_quests"

// -----------------------------------------------------------------------------
// This first part is standard and generic.
// There should be no need to edit it; just skip down to the next part.
// -----------------------------------------------------------------------------


// The individual event handlers.

void OnAcquire(object oEventItem, object oAcquiredBy, object oTakenFrom, int nStackSize);
void OnActivate(object oEventItem, object oActTarget, location lActTarget, object oActivator);
void OnEquip(object oEventItem, object oEquippedBy);
void OnHit(object oEventItem, object oHitTarget, object oCaster);
int  OnSpellCast(object oEventItem, int nSpell, object oCaster);
void OnUnacquire(object oEventItem, object oLostBy);
void OnUnequip(object oEventItem, object oUnequippedBy);


// The main function.
void main()
{
    int nEvent = GetUserDefinedItemEventNumber();

    // Spells might continue to their spell scripts. All other events are
    // completely handled by this script.
    if ( nEvent != X2_ITEM_EVENT_SPELLCAST_AT )
        SetExecutedScriptReturnValue();

    // Determine which event triggered this script's execution.
    switch ( nEvent )
    {
        // Item was acquired.
        case X2_ITEM_EVENT_ACQUIRE:
                OnAcquire(GetModuleItemAcquired(), GetModuleItemAcquiredBy(),
                          GetModuleItemAcquiredFrom(), GetModuleItemAcquiredStackSize());
                break;

        // Item was activated ("activate item" or "unique power").
        case X2_ITEM_EVENT_ACTIVATE:
                OnActivate(GetItemActivated(), GetItemActivatedTarget(),
                           GetItemActivatedTargetLocation(), GetItemActivator());
                break;

        // Item was equipped by a PC.
        case X2_ITEM_EVENT_EQUIP:
                OnEquip(GetPCItemLastEquipped(), GetPCItemLastEquippedBy());
                break;

        // Item is a weapon that just hit a target, or it is the armor of someone
        // who was just hit.
        case X2_ITEM_EVENT_ONHITCAST:
                OnHit(GetSpellCastItem(), GetSpellTargetObject(), OBJECT_SELF);
                break;

        // A PC (or certain NPCs) cast a spell at the item.
        case X2_ITEM_EVENT_SPELLCAST_AT:
                if ( OnSpellCast(GetSpellTargetObject(), GetSpellId(), OBJECT_SELF) )
                    SetExecutedScriptReturnValue();
                break;

        // Item was unacquired.
        case X2_ITEM_EVENT_UNACQUIRE:
                OnUnacquire(GetModuleItemLost(), GetModuleItemLostBy());
                break;

        // Item was unequipped by a PC.
        case X2_ITEM_EVENT_UNEQUIP:
                OnUnequip(GetPCItemLastUnequipped(), GetPCItemLastUnequippedBy());
                break;
    }
}


// -----------------------------------------------------------------------------
// Event handlers
// -----------------------------------------------------------------------------
// This second part is where you add your desired functionality. Each event
// has its own function with relavant information passed as parameters.
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
// oEventItem was acquired (by a PC or an NPC).
// Run by the module.
void OnAcquire(object oEventItem, object oAcquiredBy, object oTakenFrom, int nStackSize)
{
    if (!GetIsPC(oAcquiredBy))
        return;

    //Mark letters as picked up at least once
    SetLocalInt(oEventItem, "PickedUp", TRUE);

    object quest = GetLocalObject(oEventItem, "Quest");
    UpdateQuestState(quest, QUEST_STATE_COMPLETED);
}


// -----------------------------------------------------------------------------
// oEventItem was activated ("activate item" or "unique power").
// Run by the module.
void OnActivate(object oEventItem, object oActTarget, location lActTarget, object oActivator)
{
    // Default: do nothing.
}


// -----------------------------------------------------------------------------
// oEventItem was equipped by a PC.
// Run by the module.
void OnEquip(object oEventItem, object oEquippedBy)
{
    // Default: do nothing.
}


// -----------------------------------------------------------------------------
// oEventItem is a weapon that just hit a target, or it is the armor of someone who
// was just hit by someone else's weapon.
// Run by the caster.
void OnHit(object oEventItem, object oHitTarget, object oCaster)
{
    // Default: do nothing.
}


// -----------------------------------------------------------------------------
// Someone cast a spell at oEventItem.
// This usually only fires if a PC cast the spell, but it also fires for
// DM-possessed NPCs and NPCs in an area with the "X2_L_WILD_MAGIC" local integer set.
//
// Return TRUE to prevent the spell script from firing.
// Return FALSE to proceed normally.
//
// This fires after the UMD check, module spellhook, item creation, and
// sequencer handlers decide they do not want to handle/interrupt this spell.
// This fires before the check to see if this is a spell that normally can
// target items (and before the spell script itself runs).
//
// Run by the caster.
int OnSpellCast(object oEventItem, int nSpell, object oCaster)
{
    // Default: just proceed normally.
    return FALSE;
}


// -----------------------------------------------------------------------------
// oEventItem was unacquired/lost (by a PC or NPC).
// Run by the module.
void OnUnacquire(object oEventItem, object oLostBy)
{
    if (!GetIsPC(oLostBy))
        return;
    object quest = GetLocalObject(oEventItem, "Quest");
    UpdateQuestState(quest, QUEST_STATE_LINGERING);
}


// -----------------------------------------------------------------------------
// oEventItem was unequipped by a PC.
// Run by the module.
void OnUnequip(object oEventItem, object oUnequippedBy)
{
    // Default: do nothing.
}
