#include "inc_factions"
#include "inc_quests"

void main()
{
    //Make bandits leave and complete the quest
    if (GetLocalInt(OBJECT_SELF, "STARTED_LEAVING"))
        return;

    object specialArea = GetArea(OBJECT_SELF);
    object quest = GetLocalObject(specialArea, "QUEST");

    object PC = GetPCSpeaker();
    int i = 1;
    string tag = "cre_bandtoll";
    object bandit = GetNearestObjectByTag(tag, PC, i++);
    while (GetIsObjectValid(bandit))
    {
        SetLocalInt(bandit, "STARTED_LEAVING", TRUE);
        SetFaction(bandit, FACTION_TEMPORARY_NEUTRAL);
        SetPlotFlag(bandit, TRUE);
        AssignCommand(bandit, ActionMoveAwayFromObject(PC, TRUE));
        DestroyObject(bandit, 3.0f);
        bandit = GetNearestObjectByTag(tag, PC, i++);
    }

    UpdateQuestState(quest, QUEST_STATE_COMPLETED);
}
