#include "inc_factions"
#include "hench_i0_ai"

void main()
{
    //Attack the PCs if applicable
    if (!GetLocalInt(OBJECT_SELF, "READY_TO_ATTACK"))
        return;

    object PC = GetPCSpeaker();
    int i = 1;
    string tag = "cre_bandtoll";
    object bandit = OBJECT_SELF;
    while (GetIsObjectValid(bandit))
    {
        if (!GetLocalInt(bandit, "STARTED_ATTACKING"))
        {
            SetLocalInt(bandit, "STARTED_ATTACKING", TRUE);
            SetFaction(bandit, FACTION_BANDITS);
            AssignCommand(bandit, HenchDetermineCombatRound());
        }
        bandit = GetNearestObjectByTag(tag, OBJECT_SELF, i++);
    }
}
