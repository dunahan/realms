#include "inc_quests"
#include "inc_specialare"
#include "inc_encounters"

int StartingConditional()
{
    //Return TRUE if on a quest to break bandit blockade
    object specialArea = GetArea(OBJECT_SELF);
    object quest = GetLocalObject(specialArea, "QUEST");
    string questScript = GetQuestScript(quest);
    if (questScript == "que_breakblock" && GetQuestState(quest) == QUEST_STATE_LINGERING)
        return TRUE;
    return FALSE;
}
