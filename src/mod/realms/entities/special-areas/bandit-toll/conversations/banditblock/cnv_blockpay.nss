void main()
{
    //Pay the bandit toll and denote that the party can pass through
    object area = GetArea(OBJECT_SELF);
    int toll = GetLocalInt(area, "TOLL");
    TakeGoldFromCreature(toll, GetPCSpeaker(), TRUE);
    SetLocalInt(area, "CAN_PASS", TRUE);
}
