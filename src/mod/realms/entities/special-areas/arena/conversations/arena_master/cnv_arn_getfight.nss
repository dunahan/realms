#include "inc_convoptions"

//Store the selected participant
void main()
{
    string selectedValue = GetSelectedOptionValue();
    object participant = StringToObject(selectedValue);
    SetLocalObject(OBJECT_SELF, "Participant", participant);
}
