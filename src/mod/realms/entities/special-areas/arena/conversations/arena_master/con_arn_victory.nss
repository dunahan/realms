#include "inc_convtokens"

//Set token 1001 to the reward value and return TRUE if an arena challenge has been completed
int StartingConditional()
{
    SetCreatureConversationTokens();
    object encounter = GetLocalObject(OBJECT_SELF, "Encounter");
    if (GetLocalInt(encounter, "Completed"))
        return TRUE;
    return FALSE;
}
