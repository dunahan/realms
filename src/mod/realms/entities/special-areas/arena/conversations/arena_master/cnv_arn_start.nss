#include "inc_companions"
#include "inc_tiles"

//Teleport the participant to the arena, mark the fight as started and start the fight after a moment.
void main()
{
    object encounter = GetLocalObject(OBJECT_SELF, "Encounter");
    SetLocalInt(encounter, "Started", TRUE);

    //Teleport participants
    object PC = GetPCSpeaker();
    object wp = GetTileWaypoint(encounter, "wp_eastgate");
    object area = GetArea(OBJECT_SELF);
    object party = GetFirstFactionMember(PC);
    while (GetIsObjectValid(party))
    {
        if (GetArea(party) == area)
            AssignCommand(party, JumpToObject(wp));
        party = GetNextFactionMember(PC);
    }
}
