//Return TRUE if an arena challenge was lost
int StartingConditional()
{
    object encounter = GetLocalObject(OBJECT_SELF, "Encounter");
    if (GetLocalInt(encounter, "Lost"))
        return TRUE;
    return FALSE;
}
