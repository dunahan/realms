//Return TRUE if an arena fight is already in progress
int StartingConditional()
{
    object encounter = GetLocalObject(OBJECT_SELF, "Encounter");
    if (GetLocalInt(encounter, "Started"))
        return TRUE;
    return FALSE;
}
