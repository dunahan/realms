//Return TRUE if the reward has already been handed out
int StartingConditional()
{
    object encounter = GetLocalObject(OBJECT_SELF, "Encounter");
    if (GetLocalInt(encounter, "PostReward"))
        return TRUE;
    return FALSE;
}
