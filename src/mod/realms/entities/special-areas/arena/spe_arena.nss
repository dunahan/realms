#include "inc_debug"
#include "inc_random"
#include "inc_language"
#include "hnd_specialare"
#include "inc_biomes"
#include "inc_tiles"
#include "inc_areas"
#include "inc_convtokens"
#include "inc_scriptevents"
#include "inc_towns"
#include "inc_arenawaves"

/////////////////////////
// ------------------- //
// Special area script //
// ------------------- //
/////////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "spe_arena";

// Number of facility area templates (each in 4, 11 or 15 variants based on deadendAreas and passThroughAreas values)
int areasNumber = 1;

// Boolean value determining whether this special area may appear as a deadend area (an area with only a single entrance/exit).
// Either this or passThroughAreas needs to be TRUE (or both).
// If this is TRUE, area templates need to include variants with single exits (i.e. _01, _02, _04, _08).
int deadendAreas = TRUE;

// Boolean value determining whether this special area may appear as a pass-through area (an area with with multiple entrances/exits).
// Either this or deadendAreas needs to be TRUE (or both).
// If this is TRUE, area templates need to include variants with multiple exits (i.e. _03, _05, _06, _07, _09, _10, _11, _12, _13, _14, _15).
int passThroughAreas = TRUE;

// Minimum PC level this special area supports
int minPlayerLevel = 1;

// Maximum PC level this special area supports
int maxPlayerLevel = 40;

// Number of rumors regarding this special area (can be 0)
int rumorsNumber = 1;

// Number of available regular (non-specialty) quests for this encounter
int regularQuestsNumber = 1;

// Number of available specialty quests for this encounter (can be 0)
int specialtyQuestsNumber = 0;

// Set this to TRUE if you want quests for this special area only to generate if no preferred ones are available
int lowQuestGenerationPriority = FALSE;



//////////////////////
// Script functions //
//////////////////////


// Function that should return ResRef of an available area to spawn for nIndex in range [0, areasNumber-1] and provided exits flag (i.e. 15, 14, 13, etc.).
// You can use oBiome to use different variants based on biome (only needed if the special area is registered in multiple biomes).
// Only those exit flags that are determined by passThroughAreas and deadendAreas values need to be considered.
string OnGetSpecialAreaResRef(int nIndex, object oBiome, int nExitsFlag)
{
    string result = "arena_";

    switch (nIndex)
    {
        case 0: result += "rural"; break;
        default: return "";
    }

    string exitsFlag = nExitsFlag < 10 ? "0" + IntToString(nExitsFlag) : IntToString(nExitsFlag);
    result += "_" + exitsFlag;
    return result;
}

// Function that is executed when the special area is spawned. Use it to spawn creatures and objects in the special area,
// create sub-areas and perform all other actions. You can use nBiomeStartingLevel and oBiome to base your special area's contents
// on factors such as expected PC level, townfolk's dominant race (to spawn appropriate NPCs), etc.
// You can also access other biome properties via functions executed on oBiome.
// Remember to call AddAreaToTile(oSpecialArea, newArea) from inc_tiles after creating any new sub-areas of oSpecialArea
// and note that any areas created this way will not have rest encounters turned on by default, but oSpecialArea will (see inc_resting for details).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnInitializeSpecialArea(string sSeedName, object oBiome, object oSpecialArea, int nBiomeStartingLevel)
{
    //Set encounter variables
    int reward = nBiomeStartingLevel * (400 + RandomNext(200, sSeedName));
    SetLocalInt(oSpecialArea, "REWARD", reward);
    SetLocalInt(oSpecialArea, "BATTLES", 4);
    int i;
    for (i = 1; i <= GetLocalInt(oSpecialArea, "BATTLES"); i++)
        AddArenaWave(oSpecialArea, sSeedName, nBiomeStartingLevel, i);

    //Create areas
    string name = GetRegionName(oBiome) + " - Arena";
    object interior = CopyAreaWithEventScripts("arena_interior", "", name);
    object arena = CopyAreaWithEventScripts("arena", "", name);
    AddEventScript(SUBEVENT_AREA_ON_EXIT, "are_arenaquit", arena);
    AddAreaToTile(oSpecialArea, interior);
    AddAreaToTile(oSpecialArea, arena);

    //Create arena master NPC
    object town = GetTownOfBiome(oBiome);
    string townScript = GetTownScript(town);
    LogInfo("TownScript: " + townScript);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, sSeedName));
    object wp = GetTileWaypoint(oSpecialArea, "wp_arenamaster");
    object arenaMaster = CreateCreature(resref, GetLocation(wp));
    int language = GetServerLanguage();
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(arenaMaster), GetGender(arenaMaster), language, sSeedName);
    string lastName = GetRandomCommonerLastName(townScript, GetRacialType(arenaMaster), GetGender(arenaMaster), language, sSeedName);
    SetName(arenaMaster, firstName + " " + lastName);

    //Arena master variables
    SetLocalString(arenaMaster, "Clothes", "nw_cloth011");
    SetLocalString(arenaMaster, "Conversation", "arena_master");
    ClearCreatureEventScriptFromVariable(arenaMaster, SUBEVENT_CREATURE_ON_CONVERSATION);
    AddEventScript(SUBEVENT_CREATURE_ON_CONVERSATION, "cre_varconvo", arenaMaster);
    SetLocalObject(arenaMaster, "Encounter", oSpecialArea);

    //Arena master conversation tokens
    StoreCreatureConversationToken(arenaMaster, 1001, IntToString(GetLocalInt(oSpecialArea, "REWARD")));
    StoreCreatureConversationToken(arenaMaster, 1002, IntToString(GetLocalInt(oSpecialArea, "BATTLES")));
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on the special area instance
string OnGetSpecialAreaRumor(object oSpecialArea, int nIndex, int nLanguage)
{
    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return "Have you been to the arena nearby? I hear you can earn some good coin if you're strong enough to survive there.";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return "Mo�e chcesz odwiedzi� poblisk� aren�? Podobno mo�na zarobi� okr�g�� sumk�, o ile jeste� wystarczaj�co silny, by przetrwa�.";
    }
    return "";
}

// Function that should return a random name for a special area, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomSpecialAreaName(string sSeedName, int nLanguage)
{
    if (nLanguage == LANGUAGE_POLISH)
    {
        switch (RandomNext(1))
        {
            case 0: return "Arena";
        }
    }
    else
    {
        switch (RandomNext(1))
        {
            case 0: return "Arena";
        }
    }
    return "";
}

// Function that should return available regular quest script names for nIndex in range [0, regularQuestsNumber-1]
string OnGetSpecialAreaRegularQuestScript(int nIndex)
{
    switch (nIndex)
    {
        case 0: return "que_arenachamp";
    }
    return "";
}

// Function that should return available specialty quest script names for nIndex in range [0, specialtyQuestsNumber-1];
// A specialty quest is one that may either not be doable by the player(s) because it requires a certain skill or attributes
// (for example, a quest to pick pocket something from an NPC), or is inherently evil (for example murder) 
// and/or bad for reputation, so that good-aligned PCs or anyone caring about their reputation would refuse it;
// This function never runs if specialtyQuestsNumber is 0, so it does not matter what it returns in that case (it still needs to exist and compile)
string OnGetSpecialAreaSpecialtyQuestScript(int nIndex)
{
    return "";
}