#include "inc_debug"
#include "inc_specialare"
#include "inc_tiles"

// Retreat from the arena, ending the challenge.
// If not all enemies have been defeated, the challenger loses.
void main()
{
    object specialArea = GetTile(OBJECT_SELF);
    object PC = GetEnteringObject();
    if (!GetIsPC(PC))
        return;

    object wp = GetTileWaypoint(specialArea, "wp_arenagate");
    AssignCommand(PC, JumpToObject(wp));
}
