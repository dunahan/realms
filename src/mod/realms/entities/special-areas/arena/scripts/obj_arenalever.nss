#include "inc_debug"
#include "inc_tiles"
#include "inc_arrays"
#include "inc_scriptevents"
#include "inc_language"
#include "inc_common"

// Spawn next wave of enemies in the arena upon use
void main()
{
    //Check if previous wave has been defeated
    object area = GetArea(OBJECT_SELF);
    object user = GetLastUsedBy();
    object obj = GetFirstObjectInArea(area);
    while (GetIsObjectValid(obj))
    {
        if (GetLocalInt(obj, "ArenaMonster"))
        {
            string msg = GetLocalizedString("Not all enemies have been defeated!", "Nie wszyscy wrogowie zostali pokonani!");
            FloatingTextStringOnCreature(msg, user);
            return;
        }
        obj = GetNextObjectInArea(area);
    }

    object specialArea = GetTile(OBJECT_SELF);
    int wave = GetLocalInt(specialArea, "CurrentWave") + 1;

    if (wave == GetLocalInt(specialArea, "BATTLES") + 1)
    {
        string msg = GetLocalizedString("All the waves have been defeated!", "Wszystkie fale wrog�w zosta�y pokonane!");
        FloatingTextStringOnCreature(msg, user);
        return;
    }

    SetLocalInt(specialArea, "CurrentWave", wave);
    string waveArray = "WAVE_"+IntToString(wave);

    int i;
    for (i = 0; i < GetStringArraySize(waveArray, specialArea); i++)
    {
        string resref = GetStringArrayElement(waveArray, i, specialArea);
        object wp = GetTileWaypoint(specialArea, Random(2) ? "wp_northgate" : "wp_southgate");
        object creature = CreateCreature(resref, GetLocation(wp));
        SetLocalInt(creature, "ArenaMonster", TRUE);
        AssignCommand(creature, StartCombat());
    }

    PlaySound("as_cv_crank1");
}
