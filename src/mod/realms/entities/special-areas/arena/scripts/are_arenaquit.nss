#include "inc_tiles"
#include "inc_quests"

void main()
{
    object specialArea = GetTile(OBJECT_SELF);

    object obj = GetFirstObjectInArea(OBJECT_SELF);
    int noEnemies = TRUE;
    while (GetIsObjectValid(obj))
    {
        if (GetLocalInt(obj, "ArenaMonster"))
        {
            noEnemies = FALSE;
            DestroyObject(obj);
        }
        obj = GetNextObjectInArea(OBJECT_SELF);
    }

    if (noEnemies == FALSE || GetLocalInt(specialArea, "BATTLES") != GetLocalInt(specialArea, "CurrentWave"))
    {
        SetLocalInt(specialArea, "Lost", TRUE);
        
        //If there is a quest to bring the figurine, fail it
        object quest = GetQuestOfSpecialArea(specialArea);
        if (GetQuestScript(quest) == "que_arenachamp")
            UpdateQuestState(quest, QUEST_STATE_FAILED);
    }
    else
        SetLocalInt(specialArea, "Completed", TRUE);
}