#include "inc_arrays"
#include "inc_random"

// Wave spawning for the arena

string AddArenaWave(object oArenaSpecialArea, string sSeedName, int nLevel, int nFightNumber)
{
    string array = "WAVE_"+IntToString(nFightNumber);
    CreateStringArray(array, 0, oArenaSpecialArea);
    
    if (nLevel == 1)
    {
        switch (RandomNext(8, sSeedName))
        {
            case 0:
                //Lvl1: Goblins
                AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_goblinb" : "cre_goblina", FALSE, oArenaSpecialArea);
                AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_goblinb" : "cre_goblina", FALSE, oArenaSpecialArea);
                AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_goblinb" : "cre_goblina", FALSE, oArenaSpecialArea);
                AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_goblinb" : "cre_goblina", FALSE, oArenaSpecialArea);
                if (nFightNumber > 1)
                {
                    AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_goblinb" : "cre_goblina", FALSE, oArenaSpecialArea);
                }
                if (nFightNumber > 2)
                {
                    AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_goblinb" : "cre_goblina", FALSE, oArenaSpecialArea);
                }
                break;
            case 1:
                //Lvl1: Small spider
                AddStringArrayElement(array, "cre_smallspider", FALSE, oArenaSpecialArea);
                AddStringArrayElement(array, "cre_smallspider", FALSE, oArenaSpecialArea);
                AddStringArrayElement(array, "cre_smallspider", FALSE, oArenaSpecialArea);
                AddStringArrayElement(array, "cre_smallspider", FALSE, oArenaSpecialArea);
                if (nFightNumber > 1)
                {
                    AddStringArrayElement(array, "cre_smallspider", FALSE, oArenaSpecialArea);
                }
                if (nFightNumber > 2)
                {
                    AddStringArrayElement(array, "cre_smallspider", FALSE, oArenaSpecialArea);
                }
                break;
            case 2:
                //Lvl1: Rats and wererats
                if (nFightNumber == 1)
                {
                    AddStringArrayElement(array, "cre_rat", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_rat", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_rat", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_rat", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_rat", FALSE, oArenaSpecialArea);
                }
                else if (nFightNumber == 2)
                {
                    AddStringArrayElement(array, "cre_direrat", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_rat", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_rat", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_direrat", FALSE, oArenaSpecialArea);
                }
                else if (nFightNumber == 3)
                {
                    AddStringArrayElement(array, "cre_wererat", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_wererat", FALSE, oArenaSpecialArea);
                }
                else if (nFightNumber == 4)
                {
                    AddStringArrayElement(array, "cre_wererat", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_wererat", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_rat", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_rat", FALSE, oArenaSpecialArea);
                }
                break;
            case 3:
                //Lvl1: Orcs
                AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_orcyoungb" : "cre_orcyounga", FALSE, oArenaSpecialArea);
                AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_orcyoungb" : "cre_orcyounga", FALSE, oArenaSpecialArea);
                AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_orcyoungb" : "cre_orcyounga", FALSE, oArenaSpecialArea);
                if (nFightNumber > 1)
                    AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_orcyoungb" : "cre_orcyounga", FALSE, oArenaSpecialArea);
                if (nFightNumber > 2)
                    AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_orcyoungb" : "cre_orcyounga", FALSE, oArenaSpecialArea);
                break;
            case 4:
                //Lvl1: Fire beetle
                AddStringArrayElement(array, "cre_firebeetle", FALSE, oArenaSpecialArea);
                AddStringArrayElement(array, "cre_firebeetle", FALSE, oArenaSpecialArea);
                AddStringArrayElement(array, "cre_firebeetle", FALSE, oArenaSpecialArea);
                AddStringArrayElement(array, "cre_firebeetle", FALSE, oArenaSpecialArea);
                if (nFightNumber > 1)
                {
                    AddStringArrayElement(array, "cre_firebeetle", FALSE, oArenaSpecialArea);
                }
                if (nFightNumber > 2)
                {
                    AddStringArrayElement(array, "cre_firebeetle", FALSE, oArenaSpecialArea);
                }
                break;
            case 5:
                //Lvl1: Badger, dire badger or wolf (depending on wave)
                if (nFightNumber == 1)
                {
                    AddStringArrayElement(array, "cre_badger", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_badger", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_badger", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_badger", FALSE, oArenaSpecialArea);
                }
                else if (nFightNumber == 2)
                {
                    AddStringArrayElement(array, "cre_direbadger", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_direbadger", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_badger", FALSE, oArenaSpecialArea);
                }
                else if (nFightNumber == 3)
                {
                    AddStringArrayElement(array, "cre_direbadger", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_direbadger", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_direbadger", FALSE, oArenaSpecialArea);
                }
                else
                {
                    AddStringArrayElement(array, "cre_direbadger", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_direbadger", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_direbadger", FALSE, oArenaSpecialArea);
                    AddStringArrayElement(array, "cre_badger", FALSE, oArenaSpecialArea);
                }
                break;
            case 6:
                //Lvl1: Bandits
                AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_banditb" : "cre_bandita", FALSE, oArenaSpecialArea);
                AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_banditb" : "cre_bandita", FALSE, oArenaSpecialArea);
                AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_banditb" : "cre_bandita", FALSE, oArenaSpecialArea);
                if (nFightNumber > 2)
                    AddStringArrayElement(array, RandomNext(3, sSeedName) == 0 ? "cre_banditb" : "cre_bandita", FALSE, oArenaSpecialArea);
                break;
            case 7:
                //Lvl1: Wolves
                AddStringArrayElement(array, "cre_wolf", FALSE, oArenaSpecialArea);
                AddStringArrayElement(array, "cre_wolf", FALSE, oArenaSpecialArea);
                if (nFightNumber > 2)
                    AddStringArrayElement(array, "cre_wolf", FALSE, oArenaSpecialArea);
                break;
        }
    }

    return array;
}