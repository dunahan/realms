#include "inc_random"
#include "inc_language"
#include "inc_rngnames"
#include "hnd_towns"

//////////////////
// ------------ //
// Town script  //
// ------------ //
//////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "twn_humvil";

// String identifier to determine which town areas should spawn in this biome
// For areasId set to "village", the module will search for areas with names "village_X_Y", where X is an individual area template identifier
// and Y is a number denoting what exits are available in the area, padded with zero to two digits.
// Example: "village_01_03" is an area template "01" of region with areasId "village" with exits to the north and east
// ("03" can be written as 0011 in binary, with bits corresponding to directions WSEN, respectively)
string areasId = "humvil";

// Number of town area templates (each in 15 variants, because all possible exits combinations are required)
int areasNumber = 5;

// Number of commoner blueprints that can spawn in this town
int commonersNumber = 2;

// Number of bounty hunter blueprints that can spawn in this town
int bountyHuntersNumber = commonersNumber;

// Number of stable master blueprints that can spawn in this town
int stableMastersNumber = commonersNumber;

// Number of available tavern area templates for town
int tavernsNumber = 5;

// Number of innkeeper blueprints that can spawn in this town's tavern
int innkeepersNumber = commonersNumber;

// Number of available store entities for this town
int storesNumber = 1;

// Number of available facility entities for this town
int facilitiesNumber = 4;

// Number of tracks that the town may have as a day theme
int dayTracksNumber = 3;

// Number of tracks that the town may have as a night theme
int nightTracksNumber = 3;

// Number of tracks that the town may have as a battle theme
int battleTracksNumber = 1;

// Number of tracks that the town's tavern may have as a day theme
int tavernDayTracksNumber = 9;

// Number of tracks that the town's tavern may have as a night theme
int tavernNightTracksNumber = 9;

// Number of tracks that the town's tavern may have as a battle theme
int tavernBattleTracksNumber = 1;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available commoner ResRef for nIndex [0, commonersNumber-1].
// If you want to use RandomNext() in the OnSpawn script of a commoner, use ObjectToString(OBJECT_SELF) as the RandomNext()'s seed.
// Random() should be avoided altogether.
string OnGetTownCommonerResRef(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return "npc_hu_m";
        case 1:
            return "npc_hu_f";
    }
    return "";
}

// Function that should return available bounty hunter NPC ResRef for nIndex [0, bountyHuntersNumber-1].
// If you want to use RandomNext() in the OnSpawn script of a bounty hunter, use ObjectToString(OBJECT_SELF) as the RandomNext()'s seed.
// Random() should be avoided altogether.
string OnGetTownBountyHunterResRef(int nIndex)
{
    return OnGetTownCommonerResRef(nIndex);
}

// Function that should return available stable master NPC ResRef for nIndex [0, stableMastersNumber-1].
// If you want to use RandomNext() in the OnSpawn script of a stable master, use ObjectToString(OBJECT_SELF) as the RandomNext()'s seed.
// Random() should be avoided altogether.
string OnGetTownStableMasterResRef(int nIndex)
{
    return OnGetTownCommonerResRef(nIndex);
}

// Function that should return ResRef of an available town area to spawn for nIndex in range [0, areasNumber-1] and provided exits flag (i.e. 15, 14, 13, etc.)
string OnGetTownAreaResRef(int nIndex, int nExitsFlag)
{
    string result = areasId + "_";
    string template;

    switch (nIndex)
    {
        case 0:
            template = "01";
            break;
        case 1:
            template = "02";
            break;
        case 2:
            template = "03";
            break;
        case 3:
            template = "04";
            break;
        case 4:
            template = "05";
            break;
        default:
            return "";
    }

    string exitsFlag = nExitsFlag < 10 ? "0" + IntToString(nExitsFlag) : IntToString(nExitsFlag);
    result += template + "_" + exitsFlag;
    return result;
}

// Function that should return available store script name for nIndex in range [0, storesNumber-1]
string OnGetTownStoreScript(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return "sto_humvil";
    }
    return "";
}

// Function that should return available facility script name for nIndex in range [0, facilitiesNumber-1]
// Ensure there is at least one facility on the list with PC level range suitable for every PC level for which this town may spawn,
// i.e. avoid a situation in which all facilities listed are for levels 30-40, while the town is spawned in a biome for level 5.
string OnGetTownFacilityScript(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return "fac_thiefguild";
        case 1:
            return "fac_temple";
        case 2:
            return "fac_mageguild";
        case 3:
            return "fac_armory";
    }
    return "";
}

// Function that should return ResRef of an available tavern area to spawn for nIndex in range [0, tavernsNumber-1]
string OnGetTavernAreaResRef(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return "inn04";
        case 1:
            return "inn05";
        case 2:
            return "inn06";
        case 3:
            return "inn09";
        case 4:
            return "inn10";
    }
    return "";
}

// Function that should return available day track constant for nIndex in range [0, dayTracksNumber-1]
int OnGetTownDayTrack(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return TRACK_RURALDAY1;
        case 1:
            return 122;
        case 2:
            return 85;
    }
    return -1;
}

// Function that should return available night track constant for nIndex in range [0, nightTracksNumber-1]
int OnGetTownNightTrack(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return TRACK_RURALNIGHT;
        case 1: 
            return 86;
        case 2: 
            return 95;
    }
    return -1;
}

// Function that should return available battle track constant for nIndex in range [0, battleTracksNumber-1]
int OnGetTownBattleTrack(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return TRACK_BATTLE_CITY1;
    }
    return -1;
}

// Function that should return available tavern day track constant for nIndex in range [0, tavernDayTracksNumber-1]
int OnGetTavernDayTrack(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return TRACK_TAVERN1;
        case 1:
            return TRACK_TAVERN2;
        case 2:
            return TRACK_TAVERN3;
        case 3:
            return TRACK_TAVERN4;
        case 4:
            return 111;
        case 5:
            return 112;
        case 6:
            return 115;
        case 7:
            return 124;
        case 8:
            return 132;
    }
    return -1;
}

// Function that should return available tavern night track constant for nIndex in range [0, tavernNightTracksNumber-1]
int OnGetTavernNightTrack(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return TRACK_TAVERN1;
        case 1:
            return TRACK_TAVERN2;
        case 2:
            return TRACK_TAVERN3;
        case 3:
            return TRACK_TAVERN4;
        case 4:
            return 111;
        case 5:
            return 112;
        case 6:
            return 115;
        case 7:
            return 124;
        case 8:
            return 132;
    }
    return -1;
}

// Function that should return available tavern battle track constant for nIndex in range [0, tavernBattleTracksNumber-1]
int OnGetTavernBattleTrack(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return TRACK_BATTLE_CITY2;
    }
    return -1;
}

// Function that should return a random name for this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomTownName(string sSeedName, int nLanguage)
{
    return RandomHumanTownName(sSeedName);
}

// Function that should return a random name for a tavern of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomTavernName(string sSeedName, int nLanguage)
{
    return RandomTavernName(sSeedName);
}

// Function that should return available innkeeper ResRef for nIndex [0, innkeepersNumber-1].
// If you want to use RandomNext() in the OnSpawn script of an innkeeper, use ObjectToString(OBJECT_SELF) as the RandomNext()'s seed.
// Random() should be avoided altogether.
string OnGetInnkeeperResRef(int nIndex)
{
    return OnGetTownCommonerResRef(nIndex);
}

// Function that should create a tavern's store object at oDestinationWaypoint and return it.
// You can generate some inventory programatically and randomly, or you can simply create a store of a given ResRef.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
object OnSpawnTavernInventory(string sSeedName, object oDestinationWaypoint)
{
    string resref;
    switch (RandomNext(1, sSeedName))
    {
        case 0:
            resref = "nw_storebar01";
            break;
    }
    return CreateObject(OBJECT_TYPE_STORE, resref, GetLocation(oDestinationWaypoint));
}

// Function that should create a town's stable master's store object at oDestinationWaypoint and return it.
// You can use nBiomeStartingLevel to spawn merchant's items appropriate to the level the PCs will typically have.
// You can generate some inventory programatically and randomly, or you can simply create a store of a given ResRef.
// Note that the stable master's store should only include horse tokens that will spawn a horse upon purchase and destroy themselves
// (items tagged "it_horsetoken" with resref "it_{horse}" corresponding to a horse blueprint "cre_{horse}").
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
object OnSpawnStableInventory(string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint)
{
    int num = RandomNext(4, sSeedName) + 1;
    int i;
    object store = CreateObject(OBJECT_TYPE_STORE, "sto_empty", GetLocation(oDestinationWaypoint));
    for (i = 0; i < num; i++)
    {
        string resref;
        switch (RandomNext(4, sSeedName))
        {
            case 0:
                resref = "it_horse1";
                break;
            case 1:
                resref = "it_horse2";
                break;
            case 2:
                resref = "it_horse3";
                break;
            case 3:
                resref = "it_horse4";
                break;
        }
        CreateItemOnObject(resref, store);
    }
    return store;
}

// Function that should return a random first name for a commoner NPC based on their race, gender and possibly server language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomCommonerFirstName(int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    return nGender == GENDER_FEMALE ? RandomFemaleHumanName(sSeedName) : RandomMaleHumanName(sSeedName);
}

// Function that should return a random last name for a commoner NPC based on their race, gender and possibly server language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomCommonerLastName(int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    return RandomHumanSurname(sSeedName);
}

// Function that should return a random first name for an innkeeper NPC based on their race, gender and possibly server language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomInnkeeperFirstName(int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    return nGender == GENDER_FEMALE ? RandomFemaleHumanName(sSeedName) : RandomMaleHumanName(sSeedName);
}

// Function that should return a random last name for an innkeeper NPC based on their race, gender and possibly server language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomInnkeeperLastName(int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    return RandomHumanSurname(sSeedName);
}

// Function that should return a random first name for a bounty hunter NPC based on their race, gender and possibly server language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomBountyHunterFirstName(int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    return nGender == GENDER_FEMALE ? RandomFemaleHumanName(sSeedName) : RandomMaleHumanName(sSeedName);
}

// Function that should return a random last name for a bounty hunter NPC based on their race, gender and possibly server language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomBountyHunterLastName(int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    return RandomHumanSurname(sSeedName);
}

// Function that should return a random first name for a stable master NPC based on their race, gender and possibly server language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomStableMasterFirstName(int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    return nGender == GENDER_FEMALE ? RandomFemaleHumanName(sSeedName) : RandomMaleHumanName(sSeedName);
}

// Function that should return a random last name for a stable master NPC based on their race, gender and possibly server language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomStableMasterLastName(int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    return RandomHumanSurname(sSeedName);
}
