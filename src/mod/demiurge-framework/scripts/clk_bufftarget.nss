#include "inc_debug"
#include "inc_epicspells"

void main()
{
    object PC = GetLastPlayerToSelectTarget();
    object caster = GetLocalObject(PC, "SpellCasterCompanion");
    object target = GetTargetingModeSelectedObject();
    int spell = GetLocalInt(caster, "Selected_Spell");

    SetLocalInt(caster, "Deekin_Spell_Cast", spell);
    SetLocalObject(caster, "Henchman_Spell_Target", target);
    ExecuteScript("x2_d1_unsetgroup", caster);
    ExecuteScript("x0_d1_henchspell", caster);
}
