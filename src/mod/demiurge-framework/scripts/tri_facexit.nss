#include "inc_facilities"
#include "inc_tiles"
#include "inc_horses"

//Script for exiting from a facility to the main town area

void main()
{
    object area = GetArea(OBJECT_SELF);
    object town = GetTownOfFacility(area);
    object PC = GetEnteringObject();
    
    object wp = GetTileWaypoint(town, "wp_facsignpost");
    AssignCommand(PC, JumpToObjectRespectingHorses(wp));
}
