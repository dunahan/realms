#include "inc_debug"
#include "inc_colors"
#include "inc_arrays"
#include "inc_language"
#include "inc_databases"
// This library contains functions for reading and writing to the Realms database

string sDatabaseName = "REALMS";

const int MAP_IMAGE_SIZE_SMALL = 1;
const int MAP_IMAGE_SIZE_MEDIUM = 2;
const int MAP_IMAGE_SIZE_LARGE = 3;

struct PlayerSettings
{
    int mapImageSize;
    int mapOffsetX;
    int mapOffsetY;
};

struct CompanionInformation
{
    string id;
    string firstName;
    string lastName;
    string description;
    int class;
    int race;
    int alignmentGood;
    int alignmentLawful;
    int finished;
    string author;
};

// Should be run on module startup - initializes the save file's db
void InitializeSavDb();

// Should be run on module startup - initializes the external db if it doesn't exist
void InitializeDb();

// Add player's info to the Realms database if it's not yet there, returns TRUE if the player was absent in the database, FALSE otherwise
int RegisterPlayer(object oPC);

// Retrieves the player's settings from the Realms database
struct PlayerSettings GetPlayerSettings(object oPC);

// Updates the player's settings in the Realms database
void SetPlayerSettings(object oPC, struct PlayerSettings settings);

// Deletes unfinished companions from the database
void DeleteUnfinishedCompanions();

// Function to be called at the beginning of a companion creation process; returns the new companion's ID
string StartNewCompanionCreationAndReturnID(object oPC, string sFirstName, string sLastName, string sDescription);

// Adds a copy of oPC as a companion instance for the given companion ID and level
void AddCompanionSnapshot(string nCompanionId, int nLevel, object oPC);

// Marks a companion as finished
void MarkCompanionAsFinished(string sCompanionId);

// Returns the number of instances of the companion with the given ID
int GetNumberOfCompanionInstances(string nCompanionId);

// Creates a companion instance of the given ID and level at lLocation
object CreateCompanionInstance(string nCompanionId, int nLevel, location lLocation);

// Returns the companion information struct for a companion with the given ID
struct CompanionInformation GetCompanionInformation(string nCompanionId);

// Returns a display string of a companion with the given ID, i.e. "John Smith - NG Human Rogue"
string GetCompanionDisplayString(string sCompanionId, int nNameOnly=FALSE);

// Prepare arrays to be used for conversation options with companions (from inc_convoptions)
void PrepareCompanionArrays(string optionsArray, string valuesArray, int nCustomOnly=TRUE);

// Exports a finished companion with sId from the realms db to a new file - sFileName must end with ".sqlite3"
void ExportCompanion(string sFileName, string sId);

// Imports a companion from a database file to the realms db and returns value based on result:
// 0 - success
// 1 - error: companion with the given ID already exists in the realms db
// 2 - error: database file not found or doesn't contain correct companion data
// sCompanionDb should be only the name of the database, without the ".sqlite3" extension
int ImportCompanion(string sCompanionDb);

// Delete a companion with the given ID from the realms database
void DeleteCompanion(string sId);

void InitializeSavDb()
{
    string sql =
        "CREATE TABLE CompanionVariables ( "+
        "   CompanionId TEXT NOT NULL, "+
        "   VarName TEXT NOT NULL, "+
        "   Int INT, "+
        "   String TEXT, "+
        "   Float REAL, "+
        "   Object TEXT, "+ //we actually store ObjectToString's result here, because we want the reference, so that it replicates Set/GetLocalObject behavior
        "   PRIMARY KEY (CompanionId, VarName) "+
        ");"
        ;
    sqlquery query = SqlPrepareQueryObject(GetModule(), sql);
    SqlStep(query);
    if (SqlGetError(query) != "")
        LogWarning("CompanionVariables table creation error (inc_realmsdb, InitializeSavDb)");
}

void InitializeDb()
{
    //check if db exists
    string checkDbExistsSql = "SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='Versions';";
    sqlquery query = SqlPrepareQueryCampaign(sDatabaseName, checkDbExistsSql);
    SqlStep(query);

    if (SqlGetError(query) != "")
        LogWarning("Table count error (inc_realmsdb, InitializeDb)");

    int exists = SqlGetInt(query, 0);

    if (exists)
    {
        string checkDbVersionSql = "SELECT MAX(Version) FROM Versions;";
        query = SqlPrepareQueryCampaign(sDatabaseName, checkDbVersionSql);

        int version;
        if (SqlStep(query) == TRUE)
        {
            version = SqlGetInt(query, 0);
        }
        if (SqlGetError(query) != "")
            LogWarning("Database version check error (inc_realmsdb, InitializeDb)");
        if (version > 0)
            return;
    }

    //If we got so far, it means the db does not exist, so we create it
    string sql = "CREATE TABLE Versions (Version INT NOT NULL);";
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlStep(query);
    if (SqlGetError(query) != "")
        LogWarning("Versions table creation error (inc_realmsdb, InitializeDb)");

    sql = "INSERT INTO Versions (Version) VALUES (1);";
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlStep(query);
    if (SqlGetError(query) != "")
        LogWarning("Versions table update error (inc_realmsdb, InitializeDb)");

    sql =
        "CREATE TABLE Players ( "+
        "   PlayerName TEXT PRIMARY KEY NOT NULL, "+
        "   MapImageSize INT NOT NULL, "+
        "   ImageOffsetX INT NOT NULL, "+
        "   ImageOffsetY INT NOT NULL "+
        ");"
        ;
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlStep(query);
    if (SqlGetError(query) != "")
        LogWarning("Players table creation error (inc_realmsdb, InitializeDb)");

    sql =
        "CREATE TABLE Companions ( "+
        "   Id TEXT PRIMARY KEY NOT NULL, "+ //UUID
        "   FirstName TEXT NOT NULL, "+
        "   LastName TEXT NOT NULL, "+
        "   Description TEXT NOT NULL, "+
        "   Class INT NOT NULL, "+
        "   Race INT NOT NULL, "+
        "   AlignmentGood INT NOT NULL, "+
        "   AlignmentLawful INT NOT NULL, "+
        "   Finished INT NOT NULL, "+
        "   Author TEXT NOT NULL "+
        ");"
        ;
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlStep(query);
    if (SqlGetError(query) != "")
        LogWarning("Companions table creation error (inc_realmsdb, InitializeDb)");

    sql =
        "CREATE TABLE CompanionInstances ( "+
        "   CompanionId TEXT NOT NULL, "+ //UUID
        "   Level INT NOT NULL, "+
        "   Object BLOB NOT NULL, "+
        "   FOREIGN KEY(CompanionId) REFERENCES Companions(Id), "+
        "   PRIMARY KEY (CompanionId, Level) "+
        ");"
        ;
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlStep(query);
    if (SqlGetError(query) != "")
        LogWarning("CompanionInstances table creation error (inc_realmsdb, InitializeDb)");
}

int RegisterPlayer(object oPC)
{
    string playerName = GetPCPlayerName(oPC);

    string sql = "SELECT COUNT(*) FROM Players WHERE PlayerName = @playerName;";
    sqlquery query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@playerName", playerName);
    SqlStep(query);

    if (SqlGetError(query) != "")
        LogWarning("Player count error (inc_realmsdb, RegisterPlayer, oPC)");

    int exists = SqlGetInt(query, 0);

    if (exists)
        return FALSE;

    sql = "INSERT INTO Players (PlayerName, MapImageSize, ImageOffsetX, ImageOffsetY) VALUES (@playerName, 3, 0, 0);";
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@playerName", playerName);
    SqlStep(query);

    if (SqlGetError(query) != "")
        LogWarning("Player registration error (inc_realmsdb, RegisterPlayer, oPC)");
    return TRUE;
}

struct PlayerSettings GetPlayerSettings(object oPC)
{
    struct PlayerSettings result;
    string playerName = GetPCPlayerName(oPC);

    string sql = "SELECT MapImageSize, ImageOffsetX, ImageOffsetY FROM Players WHERE PlayerName = @playerName;";
    sqlquery query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@playerName", playerName);
    SqlStep(query);

    if (SqlGetError(query) != "")
        LogWarning("Player settings retrieval error (inc_realmsdb, GetPlayerSettings, oPC)");

    result.mapImageSize = SqlGetInt(query, 0);
    result.mapOffsetX = SqlGetInt(query, 1);
    result.mapOffsetY = SqlGetInt(query, 2);

    return result;
}

void SetPlayerSettings(object oPC, struct PlayerSettings settings)
{
    string playerName = GetPCPlayerName(oPC);
    string sql =
        "UPDATE Players SET "+
        "MapImageSize = @mapImageSize, "+
        "ImageOffsetX = @imageOffsetX, "+
        "ImageOffsetY = @imageOffsetY "+
        "WHERE PlayerName = @playerName; "
        ;
    sqlquery query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@playerName", playerName);
    SqlBindInt(query, "@mapImageSize", settings.mapImageSize);
    SqlBindInt(query, "@imageOffsetX", settings.mapOffsetX);
    SqlBindInt(query, "@imageOffsetY", settings.mapOffsetY);
    SqlStep(query);

    if (SqlGetError(query) != "")
        LogWarning("Player settings update error (inc_realmsdb, SetPlayerSettings, settings)");
}

void DeleteUnfinishedCompanions()
{
    string sql;
    sqlquery query;

    sql =
        "DELETE FROM CompanionInstances "+
        "WHERE CompanionId IN (SELECT Id FROM Companions WHERE Finished = 0);"
        ;
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlStep(query);

    if (SqlGetError(query) != "")
        LogWarning("Unfinished companions deletion error in step 1 (inc_realmsdb, DeleteUnfinishedCompanions)");

    sql =
        "DELETE FROM Companions "+
        "WHERE Finished = 0;"
        ;
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlStep(query);

    if (SqlGetError(query) != "")
        LogWarning("Unfinished companions deletion error in step 2 (inc_realmsdb, DeleteUnfinishedCompanions)");
}

string StartNewCompanionCreationAndReturnID(object oPC, string sFirstName, string sLastName, string sDescription)
{
    string sql;
    sqlquery query;

    string id = GetRandomUUID();
    int class = GetClassByPosition(1, oPC);
    int race = GetRacialType(oPC);
    int alignmentGood = GetAlignmentGoodEvil(oPC);
    int alignmentLawful = GetAlignmentLawChaos(oPC);
    int finished = FALSE;
    string author = GetPCPlayerName(oPC);

    sql =
        "INSERT INTO Companions (Id, FirstName, LastName, Description, Class, Race, AlignmentGood, AlignmentLawful, Finished, Author) "+
        "VALUES "+
        "(@id, @firstName, @lastName, @description, @class, @race, @alignmentGood, @alignmentLawful, @finished, @author); "
        ;
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@id", id);
    SqlBindString(query, "@firstName", sFirstName);
    SqlBindString(query, "@lastName", sLastName);
    SqlBindString(query, "@description", sDescription);
    SqlBindInt(query, "@class", class);
    SqlBindInt(query, "@race", race);
    SqlBindInt(query, "@alignmentGood", alignmentGood);
    SqlBindInt(query, "@alignmentLawful", alignmentLawful);
    SqlBindInt(query, "@finished", finished);
    SqlBindString(query, "@author", author);
    SqlStep(query);

    if (SqlGetError(query) != "")
        LogWarning("Companion creation error (inc_realmsdb, StartNewCompanionCreationAndReturnID, oPC)");

    return id;
}

void AddCompanionSnapshot(string nCompanionId, int nLevel, object oPC)
{
    string sql;
    sqlquery query;

    sql = "SELECT COUNT(*) FROM Companions WHERE Id = @id AND Finished = 0;";
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@id", nCompanionId);
    SqlStep(query);
    if (SqlGetInt(query, 0) == 0)
    {
        LogWarning("Companion does not exist or is marked as finished (inc_realmsdb, AddCompanionSnapshot, nCompanionId)");
        return;
    }

    sql = "SELECT COUNT(*) FROM CompanionInstances WHERE CompanionId = @id AND Level = @level;";
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@id", nCompanionId);
    SqlBindInt(query, "@level", nLevel);
    SqlStep(query);
    if (SqlGetInt(query, 0) == 1)
    {
        LogWarning("Companion instance for that level (%n) and ID ("+ nCompanionId +") already exists (inc_realmsdb, AddCompanionSnapshot, nLevel)", nLevel);
        return;
    }

    sql =
        "INSERT INTO CompanionInstances (CompanionId, Level, Object) "+
        "VALUES (@id, @level, @object); "
        ;
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@id", nCompanionId);
    SqlBindInt(query, "@level", nLevel);
    SqlBindObject(query, "@object", oPC);
    SqlStep(query);
    if (SqlGetError(query) != "")
        LogWarning("Companion instance addition error (inc_realmsdb, AddCompanionSnapshot, oPC)");
}

void MarkCompanionAsFinished(string sCompanionId)
{
    string sql;
    sqlquery query;

    sql = "UPDATE Companions SET Finished = 1 WHERE Id = @id;";
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@id", sCompanionId);
    SqlStep(query);
    if (SqlGetError(query) != "")
        LogWarning("Marking companion as finished failed (inc_realmsdb, MarkCompanionAsFinished)");
}

int GetNumberOfCompanionInstances(string nCompanionId)
{
    string sql;
    sqlquery query;

    sql = "SELECT COUNT(*) FROM CompanionInstances WHERE CompanionId = @id;";

    query = SqlPrepareQueryObject(GetModule(), sql);
    SqlBindString(query, "@id", nCompanionId);
    if (SqlStep(query) == FALSE || SqlGetError(query) != "")
    {
        query = SqlPrepareQueryCampaign(sDatabaseName, sql);
        SqlBindString(query, "@id", nCompanionId);
        SqlStep(query);
    }

    int numOfInstances = SqlGetInt(query, 0);
    if (SqlGetError(query) != "")
        LogWarning("Companion instance count error for ID: "+ nCompanionId +" (inc_realmsdb, GetNumberOfCompanionInstances, nCompanionId)");
    return numOfInstances;
}

object CreateCompanionInstance(string nCompanionId, int nLevel, location lLocation)
{
    string sql;
    sqlquery query;

    sql = "SELECT Object FROM CompanionInstances WHERE CompanionId = @id AND Level = @level;";

    query = SqlPrepareQueryObject(GetModule(), sql);
    SqlBindString(query, "@id", nCompanionId);
    SqlBindInt(query, "@level", nLevel);
    if (SqlStep(query) == FALSE || SqlGetError(query) != "")
    {
        query = SqlPrepareQueryCampaign(sDatabaseName, sql);
        SqlBindString(query, "@id", nCompanionId);
        SqlBindInt(query, "@level", nLevel);
        SqlStep(query);
    }

    if (SqlGetError(query) != "")
        LogWarning("Companion instance creation error for ID: "+nCompanionId+" and level %n (inc_realmsdb, GetNumberOfCompanionInstances, nCompanionId)", nLevel);
    object companion = SqlGetObject(query, 0, lLocation);
    return companion;
}

struct CompanionInformation GetCompanionInformation(string nCompanionId)
{
    string sql;
    sqlquery query;

    sql =
        "SELECT Id, FirstName, LastName, Description, Class, Race, AlignmentGood, AlignmentLawful, Finished, Author "+
        "FROM Companions WHERE Id = @id AND Finished = 1; "
        ;

    query = SqlPrepareQueryObject(GetModule(), sql);
    SqlBindString(query, "@id", nCompanionId);
    if (SqlStep(query) == FALSE || SqlGetError(query) != "")
    {
        query = SqlPrepareQueryCampaign(sDatabaseName, sql);
        SqlBindString(query, "@id", nCompanionId);
        SqlStep(query);
    }

    if (SqlGetError(query) != "")
        LogWarning("Companion information retrieval error for ID: "+nCompanionId+" (inc_realmsdb, GetNumberOfCompanionInstances, nCompanionId)");

    struct CompanionInformation result;
    result.id = SqlGetString(query, 0);
    result.firstName = SqlGetString(query, 1);
    result.lastName = SqlGetString(query, 2);
    result.description = SqlGetString(query, 3);
    result.class = SqlGetInt(query, 4);
    result.race = SqlGetInt(query, 5);
    result.alignmentGood = SqlGetInt(query, 6);
    result.alignmentLawful = SqlGetInt(query, 7);
    result.finished = SqlGetInt(query, 8);
    result.author = SqlGetString(query, 9);
    return result;
}

string GetCompanionDisplayString(string sCompanionId, int nNameOnly=FALSE)
{
    struct CompanionInformation info = GetCompanionInformation(sCompanionId);

    string alignmentString = GetAlignmentString(info.alignmentGood, info.alignmentLawful);
    string raceString = GetRaceString(info.race);
    string classString = GetClassString(info.class);
    string fullName = info.lastName == "" ? info.firstName : info.firstName+" "+info.lastName;
    if (nNameOnly)
        return fullName;
    string result = fullName+" - "+alignmentString+" "+raceString+" "+classString; //i.e. "John Smith - NG Human Rogue"
    return result;
}

void PrepareCompanionArrays(string optionsArray, string valuesArray, int nCustomOnly=TRUE)
{
    CreateStringArray(optionsArray);
    CreateStringArray(valuesArray);

    string sql;
    sqlquery query;

    sql =
        "SELECT Id, FirstName, LastName, Class, Race, AlignmentGood, AlignmentLawful "+
        "FROM Companions WHERE Finished = 1; "
        ;
    int i;
    for (i = 0; i < 2; i++)
    {
        if (nCustomOnly && i > 0)
            break;

        query = i == 0
                ? SqlPrepareQueryCampaign(sDatabaseName, sql)
                : SqlPrepareQueryObject(GetModule(), sql);

        while (SqlStep(query))
        {
            string id = SqlGetString(query, 0);
            string firstName = SqlGetString(query, 1);
            string lastName = SqlGetString(query, 2);
            int class = SqlGetInt(query, 3);
            int race = SqlGetInt(query, 4);
            int alignmentGood = SqlGetInt(query, 5);
            int alignmentLawful = SqlGetInt(query, 6);

            AddStringArrayElement(valuesArray, id);
            string alignmentString = GetAlignmentString(alignmentGood, alignmentLawful);
            string raceString = GetRaceString(race);
            string classString = GetClassString(class);
            string fullName = lastName == "" ? firstName : firstName+" "+lastName;
            string option = fullName+" - "+alignmentString+" "+raceString+" "+classString; //i.e. "John Smith - NG Human Rogue"
            option = "[" + option + "]";
            option = ColorStringWithStoredColor(option, "10");
            AddStringArrayElement(optionsArray, option);
        }
        if (SqlGetError(query) != "")
            LogWarning("Companion array preparation error (inc_realmsdb, PrepareCompanionArrays)");
    }
}

void ExportCompanion(string sFileName, string sId)
{
    if (GetStringLeft(sFileName, 9) != "rlm_char_" || GetStringRight(sFileName, 8) != ".sqlite3")
    {
        LogWarning("Unexpected filename: " + sFileName + " (inc_realmsdb, ExportCompanion, sFileName)");
        return;
    }
    string exportDb = GetStringLeft(sFileName, GetStringLength(sFileName)-8);

    struct CompanionInformation info = GetCompanionInformation(sId);
    if (info.id == "")
    {
        LogWarning("Non-existent companion to export (inc_realmsdb, ExportCompanion, sId)");
        return;
    }

    DestroyCampaignDatabase(exportDb);

    //Create the new db
    string sql = "CREATE TABLE ValidityCheck (CompanionDb INT NOT NULL);";
    sqlquery query = SqlPrepareQueryCampaign(exportDb, sql);
    SqlStep(query);
    if (SqlGetError(query) != "")
    {
        LogWarning("ValidityCheck table creation error (inc_realmsdb, ExportCompanion)");
        return;
    }
    sql = "INSERT INTO ValidityCheck (CompanionDb) VALUES (1);";
    query = SqlPrepareQueryCampaign(exportDb, sql);
    SqlStep(query);
    if (SqlGetError(query) != "")
    {
        LogWarning("ValidityCheck insert error (inc_realmsdb, ExportCompanion)");
        return;
    }
    sql =
        "CREATE TABLE Companions ( "+
        "   Id TEXT PRIMARY KEY NOT NULL, "+ //UUID
        "   FirstName TEXT NOT NULL, "+
        "   LastName TEXT NOT NULL, "+
        "   Description TEXT NOT NULL, "+
        "   Class INT NOT NULL, "+
        "   Race INT NOT NULL, "+
        "   AlignmentGood INT NOT NULL, "+
        "   AlignmentLawful INT NOT NULL, "+
        "   Finished INT NOT NULL, "+
        "   Author TEXT NOT NULL "+
        ");"
        ;
    query = SqlPrepareQueryCampaign(exportDb, sql);
    SqlStep(query);
    if (SqlGetError(query) != "")
    {
        LogWarning("Companions table creation error (inc_realmsdb, ExportCompanion)");
        return;
    }

    sql =
        "CREATE TABLE CompanionInstances ( "+
        "   CompanionId TEXT NOT NULL, "+ //UUID
        "   Level INT NOT NULL, "+
        "   Object BLOB NOT NULL, "+
        "   FOREIGN KEY(CompanionId) REFERENCES Companions(Id), "+
        "   PRIMARY KEY (CompanionId, Level) "+
        ");"
        ;
    query = SqlPrepareQueryCampaign(exportDb, sql);
    SqlStep(query);
    if (SqlGetError(query) != "")
    {
        LogWarning("CompanionInstances table creation error (inc_realmsdb, ExportCompanion)");
        return;
    }

    //Insert the core companion data
    sql =
        "INSERT INTO Companions (Id, FirstName, LastName, Description, Class, Race, AlignmentGood, AlignmentLawful, Finished, Author) "+
        "VALUES "+
        "(@id, @firstName, @lastName, @description, @class, @race, @alignmentGood, @alignmentLawful, @finished, @author); "
        ;
    query = SqlPrepareQueryCampaign(exportDb, sql);
    SqlBindString(query, "@id", info.id);
    SqlBindString(query, "@firstName", info.firstName);
    SqlBindString(query, "@lastName", info.lastName);
    SqlBindString(query, "@description", info.description);
    SqlBindInt(query, "@class", info.class);
    SqlBindInt(query, "@race", info.race);
    SqlBindInt(query, "@alignmentGood", info.alignmentGood);
    SqlBindInt(query, "@alignmentLawful", info.alignmentLawful);
    SqlBindInt(query, "@finished", info.finished);
    SqlBindString(query, "@author", info.author);
    SqlStep(query);
    if (SqlGetError(query) != "")
    {
        LogWarning("Companion creation error (inc_realmsdb, ExportCompanion)");
        return;
    }

    //Migrate the instances
    sql = "SELECT Object, Level FROM CompanionInstances WHERE CompanionId = @id;";
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@id", info.id);
    location loc = GetStartingLocation();
    while (SqlStep(query))
    {
        object companion = SqlGetObject(query, 0, loc);
        int level = SqlGetInt(query, 1);

        sql =
            "INSERT INTO CompanionInstances (CompanionId, Level, Object) "+
            "VALUES (@id, @level, @object); "
            ;
        sqlquery newQuery = SqlPrepareQueryCampaign(exportDb, sql);
        SqlBindString(newQuery, "@id", info.id);
        SqlBindInt(newQuery, "@level", level);
        SqlBindObject(newQuery, "@object", companion);
        SqlStep(newQuery);
        if (SqlGetError(newQuery) != "")
            LogWarning("Companion instance addition error (inc_realmsdb, ExportCompanion)");
        DestroyObject(companion);
    }
    if (SqlGetError(query) != "")
        LogWarning("Companion instance export error (inc_realmsdb, ExportCompanion)");
}

int ImportCompanion(string sCompanionDb)
{
    //Delete unfinished companions just in case there are leftovers from a failed import attempt
    DeleteUnfinishedCompanions();

    //Verify (very crudely) the companion's database file
    if (!GetDatabaseExists(sCompanionDb))
        return 2;
    int firstIteration = TRUE;
    string sql = "SELECT CompanionDb FROM ValidityCheck;";
    sqlquery query = SqlPrepareQueryCampaign(sCompanionDb, sql);
    while (SqlStep(query))
    {
        if (!firstIteration || SqlGetInt(query, 0) == FALSE)
            return 2;
        firstIteration = FALSE;
    }
    if (SqlGetError(query) != "")
        return 2;

    //Get companion information
    sql =
        "SELECT Id, FirstName, LastName, Description, Class, Race, AlignmentGood, AlignmentLawful, Finished, Author "+
        "FROM Companions WHERE Finished = 1; "
        ;
    query = SqlPrepareQueryCampaign(sCompanionDb, sql);
    SqlStep(query);
    if (SqlGetError(query) != "")
    {
        LogWarning("Companion information retrieval error for (inc_realmsdb, ImportCompanion)");
        return 2;
    }
    struct CompanionInformation companion;
    companion.id = SqlGetString(query, 0);
    companion.firstName = SqlGetString(query, 1);
    companion.lastName = SqlGetString(query, 2);
    companion.description = SqlGetString(query, 3);
    companion.class = SqlGetInt(query, 4);
    companion.race = SqlGetInt(query, 5);
    companion.alignmentGood = SqlGetInt(query, 6);
    companion.alignmentLawful = SqlGetInt(query, 7);
    companion.finished = SqlGetInt(query, 8);
    companion.author = SqlGetString(query, 9);

    //Ensure this companion does not exist in the realms db
    struct CompanionInformation info = GetCompanionInformation(companion.id);
    if (info.id != "")
        return 1;

    //Insert the core companion data
    sql =
        "INSERT INTO Companions (Id, FirstName, LastName, Description, Class, Race, AlignmentGood, AlignmentLawful, Finished, Author) "+
        "VALUES "+
        "(@id, @firstName, @lastName, @description, @class, @race, @alignmentGood, @alignmentLawful, 0, @author); "
        ;
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@id", companion.id);
    SqlBindString(query, "@firstName", companion.firstName);
    SqlBindString(query, "@lastName", companion.lastName);
    SqlBindString(query, "@description", companion.description);
    SqlBindInt(query, "@class", companion.class);
    SqlBindInt(query, "@race", companion.race);
    SqlBindInt(query, "@alignmentGood", companion.alignmentGood);
    SqlBindInt(query, "@alignmentLawful", companion.alignmentLawful);
    SqlBindString(query, "@author", companion.author);
    SqlStep(query);
    if (SqlGetError(query) != "")
    {
        LogWarning("Companion creation error (inc_realmsdb, ImportCompanion)");
        return 3;
    }

    //Migrate the instances
    sql = "SELECT Object, Level FROM CompanionInstances WHERE CompanionId = @id;";
    query = SqlPrepareQueryCampaign(sCompanionDb, sql);
    SqlBindString(query, "@id", companion.id);
    location loc = GetStartingLocation();
    while (SqlStep(query))
    {
        object companionInstance = SqlGetObject(query, 0, loc);
        int level = SqlGetInt(query, 1);

        sql =
            "INSERT INTO CompanionInstances (CompanionId, Level, Object) "+
            "VALUES (@id, @level, @object); "
            ;
        sqlquery newQuery = SqlPrepareQueryCampaign(sDatabaseName, sql);
        SqlBindString(newQuery, "@id", companion.id);
        SqlBindInt(newQuery, "@level", level);
        SqlBindObject(newQuery, "@object", companionInstance);
        SqlStep(newQuery);
        if (SqlGetError(newQuery) != "")
        {
            LogWarning("Companion instance addition error (inc_realmsdb, ImportCompanion)");
            return 3;
        }
        DestroyObject(companionInstance);
    }
    if (SqlGetError(query) != "")
    {
        LogWarning("Companion instance import error (inc_realmsdb, ImportCompanion)");
        return 3;
    }

    //Mark the companion as finished
    sql =
        "UPDATE Companions SET Finished = 1 WHERE Id = @id;";
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@id", companion.id);
    SqlStep(query);
    if (SqlGetError(query) != "")
    {
        LogWarning("Companion finalization error (inc_realmsdb, ImportCompanion)");
        return 3;
    }

    return 0;
}

void DeleteCompanion(string sId)
{
    //Mark the companion as unfinished, just in case something goes wrong later
    string sql = "UPDATE Companions SET Finished = 0 WHERE Id = @id;";
    sqlquery query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@id", sId);
    SqlStep(query);
    if (SqlGetError(query) != "")
    {
        LogWarning("Marking companion as unfinished error (inc_realmsdb, DeleteCompanion)");
        return;
    }

    //Delete companion instances
    sql = "DELETE FROM CompanionInstances WHERE CompanionId = @id;";
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@id", sId);
    SqlStep(query);
    if (SqlGetError(query) != "")
    {
        LogWarning("Companion instances deletion error (inc_realmsdb, DeleteCompanion)");
        return;
    }

    //Delete the companion
    sql = "DELETE FROM Companions WHERE Id = @id;";
    query = SqlPrepareQueryCampaign(sDatabaseName, sql);
    SqlBindString(query, "@id", sId);
    SqlStep(query);
    if (SqlGetError(query) != "")
    {
        LogWarning("Companion deletion error (inc_realmsdb, DeleteCompanion)");
        return;
    }
}
