#include "inc_debug"
#include "inc_hndvars"
#include "inc_realms"
#include "inc_ruleset"
#include "inc_encounters"
#include "inc_specialare"

//Library for using various quest implementations dynamically

struct QuestDialogLines
{
    string journalInitial; //"I need to do X"
    string journalSuccess; //"I did X, I must report"
    string journalFailure; //"I failed X, Y won't be happy about it"
    string journalSuccessFinished; //"Y was happy to hear of my success"
    string journalAbandoned; //"I decided to drop the task. Y was not happy."
    string npcIntroLine; //"I'm in need!"
    string npcContentLine; //"I need you to X."
    string npcRewardLine; //"I will give you X gold and item Z"
    string npcBeforeCompletionLine; //"Have you done X?"
    string playerReportingSuccess; //"I did X."
    string playerReportingFailure; //"I failed X."
    string npcReceivedSuccess; //"Thanks for doing X."
    string npcReceivedFailure; //"You messed up."
    string npcAfterSuccess; //"Thanks once again!"
    string npcAfterFailure; //"Get out of my sight."
    string npcAfterRefusal; //"I will find someone else."
    string journalName; //"X"
};

struct QuestReward
{
    int gold;
    string itemResRef;
    int itemStackSize;
};

const int QUEST_STATE_LINGERING = 1;
const int QUEST_STATE_COMPLETED = 2;
const int QUEST_STATE_FAILED = 3;
const int QUEST_STATE_AFTER_COMPLETED = 4;
const int QUEST_STATE_REFUSED = 5;
const int QUEST_STATE_AFTER_FAILED = 6;
const int QUEST_STATE_NONE = 7;

// Getter for the quest script's questgiversNumber field
int GetNumberOfQuestgivers(string sQuestScript);

// Getter for the quest script's rumorsNumber field
int GetQuestRumorsNumber(string sQuestScript);

// Getter for the quest script's minPlayerLevel field
int GetQuestMinPlayerLevel(string sQuestScript);

// Getter for the quest script's maxPlayerLevel field
int GetQuestMaxPlayerLevel(string sQuestScript);

// Returns the quest's questgiver creature ResRef corresponding to index nIndex in range [0, questgiversNumber-1]
string GetQuestgiverResRef(string sQuestScript, int nIndex, object oTown);

// Returns a random first name for a questgiver based on a quest script, a questgiver, a seed name and a LANGUAGE_* constant from inc_language
string GetRandomQuestgiverFirstName(string sQuestScript, object oQuestGiver, string sSeedName, int nLanguage);

// Returns a random last name for a questgiver based on a quest script, a questgiver, a seed name and a LANGUAGE_* constant from inc_language
string GetRandomQuestgiverLastName(string sQuestScript, object oQuestGiver, string sSeedName, int nLanguage);

// Returns a QuestReward struct (containing data on the quest's rewards) based on a quest script and a seed name
struct QuestReward GetQuestReward(string sQuestScript, string sSeedName);

// Returns a QuestDialogLines struct (containing all strings to be display in the PC's journal during the quest, as well as in the conversation with the questgiver)
// based on a quest script, a quest reward struct, a quest object, a seed name and a LANGUAGE_* constant from inc_language
struct QuestDialogLines GetQuestDialogLines(string sQuestScript, struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage);

// Returns the quest's rumor corresponding to index nIndex in range [0, rumorsNumber-1]
string GetQuestRumor(object oQuest, int nIndex, int nLanguage);

// Execute custom logic that should take place when a quest is accepted by a PC
void CustomQuestTakingLogic(object oQuest, object oConversingPC);

// Execute custom logic that should take place when a quest is completed and returned by a PC
void CustomQuestCompletionLogic(object oQuest, object oConversingPC);

// Execute custom logic that should take place when a quest is failed and returned by a PC
void CustomQuestFailureLogic(object oQuest, object oConversingPC);

// Execute custom logic that should take place when a quest is generated
void CustomQuestInitializationLogic(string sSeedName, object oQuest);

// Execute custom logic that should take place when a player or their henchman dies when the quest is in progress or completed (but not yet returned)
void CustomQuestDeathLogic(object oQuest, object oDead);

// Execute custom logic that should take place when a player respawns when the quest is in progress or completed (but not yet returned)
void CustomQuestRespawnLogic(object oQuest, object oRespawningPC);

//Instance functions

// Quest object constructor for encounters - creates a quest object
object CreateQuestInstanceFromEncounter(string sQuestScript, object oEncounter, int nUniqueQuestId, object oQuestGiver);

// Quest object constructor for special areas - creates a quest object
object CreateQuestInstanceFromSpecialArea(string sQuestScript, object oSpecialAreas, int nUniqueQuestId, object oQuestGiver);

// Activates a quest, so that it can be accepted from the questgiver (questgivers with inactive quests will instead say normal rumors when talked to)
void ActivateQuest(object oQuest);

// Returns TRUE if the quest has been activated and FALSE otherwise
int GetIsQuestActive(object oQuest);

// Returns the encounter object for which the given quest was created,
// returns OBJECT_INVALID if the quest was generated for a special area
object GetQuestEncounter(object oQuest);

// Returns the special area object for which the given quest was created,
// returns OBJECT_INVALID if the quest was generated for an encounter
object GetQuestSpecialArea(object oQuest);

// Returns a quest's script name
string GetQuestScript(object oQuest);

// Updates all the PCs' journal entries related to the given quest to a given QUEST_STATE_* constant
void UpdateQuestState(object oQuest, int questState);

// Returns a quest's QUEST_STATE_* constant
int GetQuestState(object oQuest);

// Returns a quest's unique ID
int GetQuestID(object oQuest);

// Returns a quest's questgiver creature object
object GetQuestGiver(object oQuest);

// Returns a quest associated with a questgiver
object GetQuestFromQuestgiver(object oQuestGiver);

// Set a quest's journal and dialog lines
void SetQuestDialogLines(object oQuest, struct QuestDialogLines questLines);

// Set a quest's reward
void SetQuestReward(object oQuest, struct QuestReward questReward);

// Get a quest's journal and dialog lines
struct QuestDialogLines GetQuestInstanceDialogLines(object oQuest);

// Get a quest's reward
struct QuestReward GetQuestInstanceReward(object oQuest);

// Returns TRUE if the quest has been accepted by PCs and FALSE otherwise
int GetIsQuestAccepted(object oQuest);

// Sets the quest's accepted status to TRUE
void SetQuestAccepted(object oQuest);

// Marks the quest as tracked by the given PC
void SetTrackedQuest(object oPC, object oQuest);

// Returns the quest being tracked by the given PC;
//returns OBJECT_INVALID if the PC isn't tracking any quest
object GetTrackedQuest(object oPC);

// Returns the name of the quest with a questgiver, for example:
// John Smith - "Forest Trouble"
string GetQuestNameWithQuestgiver(object oQuest);

// Clear quest tracking for all PCs who are currently tracking oQuest
void UntrackQuest(object oQuest);


//Script functions
int GetNumberOfQuestgivers(string sQuestScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 0);
    ExecuteScript(sQuestScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Invalid number of questgivers returned in quest " + sQuestScript);
    return result;
}

int GetQuestRumorsNumber(string sQuestScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 6);
    ExecuteScript(sQuestScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0)
        LogWarning("Negative number of rumors returned in quest " + sQuestScript);
    return result;
}

string GetQuestgiverResRef(string sQuestScript, int nIndex, object oTown)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 1);
    SetLocalInt(mod, funcArg1, nIndex);
    SetLocalObject(mod, funcArg2, oTown);
    ExecuteScript(sQuestScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid questgiver creature ResRef returned in quest " + sQuestScript + ", nIndex = %n", nIndex);
    return result;
}

string GetRandomQuestgiverFirstName(string sQuestScript, object oQuestGiver, string sSeedName, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 2);
    SetLocalObject(mod, funcArg1, oQuestGiver);
    SetLocalString(mod, funcArg2, sSeedName);
    SetLocalInt(mod, funcArg3, nLanguage);
    ExecuteScript(sQuestScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid questgiver first name returned in quest " + sQuestScript + ", sSeedName = " + sSeedName + ", nLanguage = %n", nLanguage);
    return result;
}

string GetRandomQuestgiverLastName(string sQuestScript, object oQuestGiver, string sSeedName, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 3);
    SetLocalObject(mod, funcArg1, oQuestGiver);
    SetLocalString(mod, funcArg2, sSeedName);
    SetLocalInt(mod, funcArg3, nLanguage);
    ExecuteScript(sQuestScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid questgiver last name returned in quest " + sQuestScript + ", sSeedName = " + sSeedName + ", nLanguage = %n", nLanguage);
    return result;
}

struct QuestReward GetQuestReward(string sQuestScript, string sSeedName)
{
    object mod = GetModule();
    object realm = GetRealm();
    struct Ruleset rules = GetRuleset(realm);
    SetLocalInt(mod, funcHandler, 4);
    SetLocalString(mod, funcArg1, sSeedName);
    ExecuteScript(sQuestScript);

    struct QuestReward result;
    result.gold = FloatToInt(GetLocalInt(mod, funcResult) * rules.questGold);
    result.itemResRef = GetLocalString(mod, funcResult2);
    result.itemStackSize = GetLocalInt(mod, funcResult3);
    return result;
}

struct QuestDialogLines GetQuestDialogLines(string sQuestScript, struct QuestReward questReward, object oQuestGiver, string sSeedName, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 5);
    SetLocalInt(mod, funcArg1, questReward.gold);
    SetLocalString(mod, funcArg2, questReward.itemResRef);
    SetLocalInt(mod, funcArg3, questReward.itemStackSize);
    SetLocalObject(mod, funcArg4, oQuestGiver);
    SetLocalString(mod, funcArg5, sSeedName);
    SetLocalInt(mod, funcArg6, nLanguage);
    ExecuteScript(sQuestScript);

    struct QuestDialogLines result;
    result.journalInitial = GetLocalString(mod, funcResult);
    result.journalSuccess = GetLocalString(mod, funcResult2);
    result.journalFailure = GetLocalString(mod, funcResult3);
    result.journalSuccessFinished = GetLocalString(mod, funcResult4);
    result.journalAbandoned = GetLocalString(mod, funcResult16);
    result.npcIntroLine = GetLocalString(mod, funcResult5);
    result.npcContentLine = GetLocalString(mod, funcResult6);
    result.npcRewardLine = GetLocalString(mod, funcResult7);
    result.npcBeforeCompletionLine = GetLocalString(mod, funcResult8);
    result.playerReportingSuccess = GetLocalString(mod, funcResult9);
    result.playerReportingFailure = GetLocalString(mod, funcResult10);
    result.npcReceivedSuccess = GetLocalString(mod, funcResult11);
    result.npcReceivedFailure = GetLocalString(mod, funcResult12);
    result.npcAfterSuccess = GetLocalString(mod, funcResult13);
    result.npcAfterFailure = GetLocalString(mod, funcResult14);
    result.npcAfterRefusal = GetLocalString(mod, funcResult17);
    result.journalName = GetLocalString(mod, funcResult15);
    return result;
}

string GetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 7);
    SetLocalObject(mod, funcArg1, oQuest);
    SetLocalInt(mod, funcArg2, nIndex);
    SetLocalInt(mod, funcArg3, nLanguage);
    string sQuestScript = GetQuestScript(oQuest);
    ExecuteScript(sQuestScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid rumor returned in quest " + sQuestScript + ", nIndex = %n, nLanguage = %n", nIndex, nLanguage);
    return result;
}

void CustomQuestTakingLogic(object oQuest, object oConversingPC)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 9);
    SetLocalObject(mod, funcArg1, oQuest);
    SetLocalObject(mod, funcArg2, oConversingPC);
    string sQuestScript = GetQuestScript(oQuest);
    ExecuteScript(sQuestScript);
}

void CustomQuestCompletionLogic(object oQuest, object oConversingPC)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 10);
    SetLocalObject(mod, funcArg1, oQuest);
    SetLocalObject(mod, funcArg2, oConversingPC);
    string sQuestScript = GetQuestScript(oQuest);
    ExecuteScript(sQuestScript);
}

void CustomQuestFailureLogic(object oQuest, object oConversingPC)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 11);
    SetLocalObject(mod, funcArg1, oQuest);
    SetLocalObject(mod, funcArg2, oConversingPC);
    string sQuestScript = GetQuestScript(oQuest);
    ExecuteScript(sQuestScript);
}

void CustomQuestInitializationLogic(string sSeedName, object oQuest)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 12);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalObject(mod, funcArg2, oQuest);
    string sQuestScript = GetQuestScript(oQuest);
    ExecuteScript(sQuestScript);
}

void CustomQuestDeathLogic(object oQuest, object oDead)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 15);
    SetLocalObject(mod, funcArg1, oQuest);
    SetLocalObject(mod, funcArg2, oDead);
    string sQuestScript = GetQuestScript(oQuest);
    ExecuteScript(sQuestScript);
}

void CustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 16);
    SetLocalObject(mod, funcArg1, oQuest);
    SetLocalObject(mod, funcArg2, oRespawningPC);
    string sQuestScript = GetQuestScript(oQuest);
    ExecuteScript(sQuestScript);
}

int GetQuestMinPlayerLevel(string sQuestScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 13);
    ExecuteScript(sQuestScript);
    return GetLocalInt(mod, funcResult);
}

int GetQuestMaxPlayerLevel(string sQuestScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 14);
    ExecuteScript(sQuestScript);
    return GetLocalInt(mod, funcResult);
}

//Instance functions

//Constructor
object CreateQuestInstanceFromEncounter(string sQuestScript, object oEncounter, int nUniqueQuestId, object oQuestGiver)
{
    object realm = GetEncounterRealm(oEncounter);
    object questObject = CreateObject(OBJECT_TYPE_PLACEABLE, "obj_invisible", GetStartingLocation());

    AddObjectArrayElement(REALM_QUESTS_ARRAY, questObject, FALSE, realm);
    SetLocalString(questObject, "QUEST_SCRIPT", sQuestScript);
    SetQuestOfEncounter(oEncounter, questObject);
    SetLocalInt(questObject, "QUEST_ACCEPTED", FALSE);
    SetLocalInt(questObject, "QUEST_STATE", QUEST_STATE_NONE);
    SetLocalInt(questObject, "QUEST_ID", nUniqueQuestId);
    SetLocalObject(questObject, "QUEST_ENCOUNTER", oEncounter);
    SetLocalObject(questObject, "QUEST_QUESTGIVER", oQuestGiver);
    SetLocalObject(oQuestGiver, "QUEST", questObject);
    return questObject;
}

object CreateQuestInstanceFromSpecialArea(string sQuestScript, object oSpecialArea, int nUniqueQuestId, object oQuestGiver)
{
    object realm = GetRealm();
    object questObject = CreateObject(OBJECT_TYPE_PLACEABLE, "obj_invisible", GetStartingLocation());

    AddObjectArrayElement(REALM_QUESTS_ARRAY, questObject, FALSE, realm);
    SetLocalString(questObject, "QUEST_SCRIPT", sQuestScript);
    SetQuestOfSpecialArea(oSpecialArea, questObject);
    SetLocalInt(questObject, "QUEST_ACCEPTED", FALSE);
    SetLocalInt(questObject, "QUEST_STATE", QUEST_STATE_NONE);
    SetLocalInt(questObject, "QUEST_ID", nUniqueQuestId);
    SetLocalObject(questObject, "QUEST_SPEAREA", oSpecialArea);
    SetLocalObject(questObject, "QUEST_QUESTGIVER", oQuestGiver);
    SetLocalObject(oQuestGiver, "QUEST", questObject);
    return questObject;
}

//Instance functions
int GetIsQuestActive(object oQuest)
{
    return GetLocalInt(oQuest, "QUEST_ACTIVE");
}

object GetQuestEncounter(object oQuest)
{
    return GetLocalObject(oQuest, "QUEST_ENCOUNTER");
}

object GetQuestSpecialArea(object oQuest)
{
    return GetLocalObject(oQuest, "QUEST_SPEAREA");
}

string GetQuestScript(object oQuest)
{
    return GetLocalString(oQuest, "QUEST_SCRIPT");
}

int GetQuestState(object oQuest)
{
    return GetLocalInt(oQuest, "QUEST_STATE");
}

int GetQuestID(object oQuest)
{
    return GetLocalInt(oQuest, "QUEST_ID");
}

void _UpdateQuestMarker(object oQuest)
{
    object questgiver = GetQuestGiver(oQuest);
    int questState = GetQuestState(oQuest);
    int isQuestActive = GetIsQuestActive(oQuest);
    effect questgiverEffect = GetFirstEffect(questgiver);
    while (GetIsEffectValid(questgiverEffect))
    {
        if (GetEffectTag(questgiverEffect) == "QuestMark")
            RemoveEffect(questgiver, questgiverEffect);
        questgiverEffect = GetNextEffect(questgiver);
    }
    if (!isQuestActive)
        return;
    int vfxId = 0;
    switch (questState)
    {
        case QUEST_STATE_NONE: vfxId = 673; break; //gold exclamation
        case QUEST_STATE_LINGERING: vfxId = 680; break; //silver question mark
        case QUEST_STATE_COMPLETED: vfxId = 674; break; //gold question mark
        case QUEST_STATE_FAILED: vfxId = 684; break; //red question mark
    }
    if (questState == QUEST_STATE_COMPLETED && !GetIsQuestAccepted(oQuest))
        vfxId = 673;
    if (vfxId != 0)
    {
        effect questMarker = TagEffect(EffectVisualEffect(vfxId), "QuestMark");
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, questMarker, questgiver);
    }
}

void ActivateQuest(object oQuest)
{
    SetLocalInt(oQuest, "QUEST_ACTIVE", TRUE);
    _UpdateQuestMarker(oQuest);
}

void UpdateQuestState(object oQuest, int questState)
{
    int previousState = GetQuestState(oQuest);
    SetLocalInt(oQuest, "QUEST_STATE", questState);
    int id = GetQuestID(oQuest);
    string plotId = "quest_" + IntToString(id);

    if (questState != QUEST_STATE_REFUSED && (questState == QUEST_STATE_NONE || GetIsQuestAccepted(oQuest) == FALSE))
    {
        RemoveJournalQuestEntry(plotId, GetFirstPC(), TRUE, TRUE);
        return;
    }

    _UpdateQuestMarker(oQuest);
    if (questState == QUEST_STATE_AFTER_FAILED)
        questState = QUEST_STATE_FAILED; //The same journal entry is used AFTER_FAILED as FAILED
    if (previousState != QUEST_STATE_NONE || questState != QUEST_STATE_REFUSED)
        AddJournalQuestEntry(plotId, questState, GetFirstPC(), TRUE, TRUE, TRUE);
}

object GetQuestGiver(object oQuest)
{
    return GetLocalObject(oQuest, "QUEST_QUESTGIVER");
}

void SetQuestDialogLines(object oQuest, struct QuestDialogLines questLines)
{
    SetLocalString(oQuest, "QUEST_LINE_journalInitial", questLines.journalInitial);
    SetLocalString(oQuest, "QUEST_LINE_journalSuccess", questLines.journalSuccess);
    SetLocalString(oQuest, "QUEST_LINE_journalFailure", questLines.journalFailure);
    SetLocalString(oQuest, "QUEST_LINE_journalSuccessFinished", questLines.journalSuccessFinished);
    SetLocalString(oQuest, "QUEST_LINE_journalAbandoned", questLines.journalAbandoned);
    SetLocalString(oQuest, "QUEST_LINE_npcIntroLine", questLines.npcIntroLine);
    SetLocalString(oQuest, "QUEST_LINE_npcContentLine", questLines.npcContentLine);
    SetLocalString(oQuest, "QUEST_LINE_npcRewardLine", questLines.npcRewardLine);
    SetLocalString(oQuest, "QUEST_LINE_npcBeforeCompletionLine", questLines.npcBeforeCompletionLine);
    SetLocalString(oQuest, "QUEST_LINE_playerReportingSuccess", questLines.playerReportingSuccess);
    SetLocalString(oQuest, "QUEST_LINE_playerReportingFailure", questLines.playerReportingFailure);
    SetLocalString(oQuest, "QUEST_LINE_npcReceivedSuccess", questLines.npcReceivedSuccess);
    SetLocalString(oQuest, "QUEST_LINE_npcReceivedFailure", questLines.npcReceivedFailure);
    SetLocalString(oQuest, "QUEST_LINE_npcAfterSuccess", questLines.npcAfterSuccess);
    SetLocalString(oQuest, "QUEST_LINE_npcAfterFailure", questLines.npcAfterFailure);
    SetLocalString(oQuest, "QUEST_LINE_npcAfterRefusal", questLines.npcAfterRefusal);
    SetLocalString(oQuest, "QUEST_LINE_journalName", questLines.journalName);
}

void SetQuestReward(object oQuest, struct QuestReward questReward)
{
    SetLocalInt(oQuest, "QUEST_REWARD_gold", questReward.gold);
    SetLocalString(oQuest, "QUEST_REWARD_itemResRef", questReward.itemResRef);
    SetLocalInt(oQuest, "QUEST_REWARD_itemStackSize", questReward.itemStackSize);
}

struct QuestDialogLines GetQuestInstanceDialogLines(object oQuest)
{
    struct QuestDialogLines result;
    result.journalInitial = GetLocalString(oQuest, "QUEST_LINE_journalInitial");
    result.journalSuccess = GetLocalString(oQuest, "QUEST_LINE_journalSuccess");
    result.journalFailure = GetLocalString(oQuest, "QUEST_LINE_journalFailure");
    result.journalSuccessFinished = GetLocalString(oQuest, "QUEST_LINE_journalSuccessFinished");
    result.journalAbandoned = GetLocalString(oQuest, "QUEST_LINE_journalAbandoned");
    result.npcIntroLine = GetLocalString(oQuest, "QUEST_LINE_npcIntroLine");
    result.npcContentLine = GetLocalString(oQuest, "QUEST_LINE_npcContentLine");
    result.npcRewardLine = GetLocalString(oQuest, "QUEST_LINE_npcRewardLine");
    result.npcBeforeCompletionLine = GetLocalString(oQuest, "QUEST_LINE_npcBeforeCompletionLine");
    result.playerReportingSuccess = GetLocalString(oQuest, "QUEST_LINE_playerReportingSuccess");
    result.playerReportingFailure = GetLocalString(oQuest, "QUEST_LINE_playerReportingFailure");
    result.npcReceivedSuccess = GetLocalString(oQuest, "QUEST_LINE_npcReceivedSuccess");
    result.npcReceivedFailure = GetLocalString(oQuest, "QUEST_LINE_npcReceivedFailure");
    result.npcAfterSuccess = GetLocalString(oQuest, "QUEST_LINE_npcAfterSuccess");
    result.npcAfterFailure = GetLocalString(oQuest, "QUEST_LINE_npcAfterFailure");
    result.npcAfterRefusal = GetLocalString(oQuest, "QUEST_LINE_npcAfterRefusal");
    result.journalName = GetLocalString(oQuest, "QUEST_LINE_journalName");
    return result;
}

struct QuestReward GetQuestInstanceReward(object oQuest)
{
    struct QuestReward result;
    result.gold = GetLocalInt(oQuest, "QUEST_REWARD_gold");
    result.itemResRef = GetLocalString(oQuest, "QUEST_REWARD_itemResRef");
    result.itemStackSize = GetLocalInt(oQuest, "QUEST_REWARD_itemStackSize");
    return result;
}

int GetIsQuestAccepted(object oQuest)
{
    return GetLocalInt(oQuest, "QUEST_ACCEPTED");
}

void SetQuestAccepted(object oQuest)
{
    SetLocalInt(oQuest, "QUEST_ACCEPTED", TRUE);
}

void SetTrackedQuest(object oPC, object oQuest)
{
    SetLocalObject(oPC, "QUEST_TRACKED", oQuest);
}

object GetTrackedQuest(object oPC)
{
    return GetLocalObject(oPC, "QUEST_TRACKED");
}

string GetQuestNameWithQuestgiver(object oQuest)
{
    object questgiver = GetQuestGiver(oQuest);
    string questgiverName = GetName(questgiver);
    struct QuestDialogLines dialogLines = GetQuestInstanceDialogLines(oQuest);
    string questName = dialogLines.journalName;

    return questgiverName+" - \""+questName+"\"";
}

void UntrackQuest(object oQuest)
{
    object PC = GetFirstPC();
    while (GetIsObjectValid(PC))
    {
        object tracked = GetTrackedQuest(PC);
        if (tracked == oQuest)
            SetTrackedQuest(PC, OBJECT_INVALID);
        PC = GetNextPC();
    }
}

object GetQuestFromQuestgiver(object oQuestGiver)
{
    return GetLocalObject(oQuestGiver, "QUEST");
}