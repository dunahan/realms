#include "inc_debug"
#include "inc_hndvars"
#include "inc_arrays"
#include "inc_tiles"
#include "inc_towns"
#include "inc_plots"

//Library for using various boss area implementations dynamically

// Getter for the boss area script's areasNumber field
int GetNumberOfBossAreas(string sBossAreaScript);

// Getter for the boss area script's minPlayerLevel field
int GetBossAreaMinPlayerLevel(string sBossAreaScript);

// Getter for the boss area script's maxPlayerLevel field
int GetBossAreaMaxPlayerLevel(string sBossAreaScript);

// Getter for the boss area script's rumorsNumber field
int GetBossAreaRumorsNumber(string sBossAreaScript);

// Returns the boss area's area ResRef corresponding to index nIndex in range [0, areasNumber-1] and area exits given by nExitsFlag (1, 2, 4 and 8 are valid values)
// and based on the biome the boss area is in (so that a snow biome will spawn a different [snowy] area than a plains biome, for example)
string GetBossAreaResRef(string sBossAreaScript, int nIndex, object oBiome, int nExitsFlag);

// Initialize the boss area (whose main area is oBossArea)
void InitializeBossArea(string sSeedName, object oBiome, object oBossArea, int nBiomeStartingLevel);

// Returns the boss area's rumor corresponding to index nIndex in range [0, rumorsNumber-1]
string GetBossAreaRumor(object oBossArea, int nIndex, int nLanguage);

// Returns a random boss area name for a given boss area script, based on a LANGUAGE_* constant from inc_language
string GetRandomBossAreaName(string sBossAreaScript, string sSeedName, int nLanguage);

//Instance functions

// Boss area initialization (InitializeBossArea in inc_bossareas needs to be called, too, to perform custom boss area actions)
void InitializeBossAreaInstance(string sBossAreaScript, object oBossArea, object oBiome, object oPlot);

// Returns the given boss area's script name
string GetBossAreaScript(object oBossArea);

// Returns the boss area's biome
object GetBiomeOfBossArea(object oBossArea);


//Script functions
int GetNumberOfBossAreas(string sBossAreaScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 0);
    ExecuteScript(sBossAreaScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Boss area " + sBossAreaScript + " returned invalid areasNumber value: %n", result);
    return result;
}

int GetBossAreaMinPlayerLevel(string sBossAreaScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 1);
    ExecuteScript(sBossAreaScript);
    return GetLocalInt(mod, funcResult);
}

int GetBossAreaMaxPlayerLevel(string sBossAreaScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 2);
    ExecuteScript(sBossAreaScript);
    return GetLocalInt(mod, funcResult);
}

int GetBossAreaRumorsNumber(string sBossAreaScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 3);
    ExecuteScript(sBossAreaScript);
    return GetLocalInt(mod, funcResult);
}

string GetBossAreaResRef(string sBossAreaScript, int nIndex, object oBiome, int nExitsFlag)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 4);
    SetLocalInt(mod, funcArg1, nIndex);
    SetLocalObject(mod, funcArg2, oBiome);
    SetLocalInt(mod, funcArg3, nExitsFlag);
    ExecuteScript(sBossAreaScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid area resref returned in boss area " + sBossAreaScript + ", nIndex = %n", nIndex);
    return result;
}

void InitializeBossArea(string sSeedName, object oBiome, object oBossArea, int nBiomeStartingLevel)
{
    object mod = GetModule();
    string bossAreaScript = GetBossAreaScript(oBossArea);
    SetLocalInt(mod, funcHandler, 5);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalObject(mod, funcArg2, oBiome);
    SetLocalObject(mod, funcArg3, oBossArea);
    SetLocalInt(mod, funcArg4, nBiomeStartingLevel);
    ExecuteScript(bossAreaScript);
}

string GetBossAreaRumor(object oBossArea, int nIndex, int nLanguage)
{
    object mod = GetModule();
    string bossAreaScript = GetBossAreaScript(oBossArea);
    SetLocalInt(mod, funcHandler, 6);
    SetLocalObject(mod, funcArg1, oBossArea);
    SetLocalInt(mod, funcArg2, nIndex);
    SetLocalInt(mod, funcArg3, nLanguage);
    ExecuteScript(bossAreaScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid rumor returned in boss area " + bossAreaScript + ", nIndex = %n, nLanguage = %n", nIndex, nLanguage);
    return result;
}

string GetRandomBossAreaName(string sBossAreaScript, string sSeedName, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 7);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalInt(mod, funcArg2, nLanguage);
    ExecuteScript(sBossAreaScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid boss area name returned in " + sBossAreaScript + ", sSeedName = " + sSeedName + ", nLanguage = %n", nLanguage);
    return result;
}

//Initializator
void InitializeBossAreaInstance(string sBossAreaScript, object oBossArea, object oBiome, object oPlot)
{
    SetLocalObject(oBossArea, "BOSSAREA_BIOME", oBiome);
    SetBossAreaOfPlot(oPlot, oBossArea);
    SetLocalString(oBossArea, "BOSSAREA_SCRIPT", sBossAreaScript); 
}

//Instance functions
string GetBossAreaScript(object oBossArea)
{
    return GetLocalString(oBossArea, "BOSSAREA_SCRIPT");
}

object GetBiomeOfBossArea(object oBossArea)
{
    return GetLocalObject(oBossArea, "BOSSAREA_BIOME");
}