#include "inc_debug"
#include "inc_hndvars"
#include "inc_arrays"
#include "inc_tiles"
#include "inc_towns"

//Library for using various special area implementations dynamically

// Getter for the special area script's areasNumber field
int GetNumberOfSpecialAreas(string sSpecialAreaScript);

// Getter for the special area script's minPlayerLevel field
int GetSpecialAreaMinPlayerLevel(string sSpecialAreaScript);

// Getter for the special area script's maxPlayerLevel field
int GetSpecialAreaMaxPlayerLevel(string sSpecialAreaScript);

// Getter for the special area script's rumorsNumber field
int GetSpecialAreaRumorsNumber(string sSpecialAreaScript);

// Getter for the special area script's deadendAreas field
int GetSpecialAreaDeadendAreas(string sSpecialAreaScript);

// Getter for the special area script's passThroughAreas field
int GetSpecialAreaPassthroughAreas(string sSpecialAreaScript);

// Getter for the special area script's regularQuestsNumber field
int GetNumberOfSpecialAreaRegularQuests(string sSpecialAreaScript);

// Getter for the special area script's specialtyQuestsNumber field
int GetNumberOfSpecialAreaSpecialtyQuests(string sSpecialAreaScript);

// Returns the special area's area ResRef corresponding to index nIndex in range [0, areasNumber-1] and area exits given by nExitsFlag and based on the biome the special area is in
// (so that a snow biome will spawn a different [snowy] area than a plains biome, for example)
string GetSpecialAreaResRef(string sSpecialAreaScript, int nIndex, object oBiome, int nExitsFlag);

// Initialize the special area (whose main area is oSpecialArea)
void InitializeSpecialArea(string sSeedName, object oBiome, object oSpecialArea, int nBiomeStartingLevel);

// Returns the special area's rumor corresponding to index nIndex in range [0, rumorsNumber-1]
string GetSpecialAreaRumor(object oSpecialArea, int nIndex, int nLanguage);

// Returns a random special area name for a given special area script, based on a LANGUAGE_* constant from inc_language
string GetRandomSpecialAreaName(string sSpecialAreaScript, string sSeedName, int nLanguage);

// Returns TRUE if the given special area script has a low quest generation priority,
// in which case it will not generate quests unless only encounters of low quest generation priority are available
int GetSpecialAreaHasLowQuestGenerationPriority(string sSpecialAreaScript);

// Returns the special area's regular quest script name corresponding to index nIndex in range [0, regularQuestsNumber-1]
string GetSpecialAreaRegularQuestScript(string sSpecialAreaScript, int nIndex);

// Returns the special area's specialty quest script name corresponding to index nIndex in range [0, specialtyQuestsNumber-1]
string GetSpecialAreaSpecialtyQuestScript(string sSpecialAreaScript, int nIndex);

//Instance functions

// Special area initialization (InitializeSpecialArea in inc_specialare needs to be called, too, to perform custom special area actions)
void InitializeSpecialAreaInstance(string sSpecialAreaScript, int nLowQuestGenerationPriority, int nEnRouteToTown, object oSpecialArea, object oBiome);

// Returns the given special area's script name
string GetSpecialAreaScript(object oSpecialArea);

// Returns the special area's biome
object GetBiomeOfSpecialArea(object oSpecialArea);

// Returns an object representation of a quest that was generated for this special area (OBJECT_INVALID if no quest was generated)
object GetQuestOfSpecialArea(object oSpecialArea);

// Sets a generated quest represented as an object as this special area's quest
void SetQuestOfSpecialArea(object oSpecialArea, object oQuest);



//Script functions
int GetNumberOfSpecialAreas(string sSpecialAreaScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 0);
    ExecuteScript(sSpecialAreaScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Special area " + sSpecialAreaScript + " returned invalid areasNumber value: %n", result);
    return result;
}

int GetSpecialAreaMinPlayerLevel(string sSpecialAreaScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 1);
    ExecuteScript(sSpecialAreaScript);
    return GetLocalInt(mod, funcResult);
}

int GetSpecialAreaMaxPlayerLevel(string sSpecialAreaScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 2);
    ExecuteScript(sSpecialAreaScript);
    return GetLocalInt(mod, funcResult);
}

int GetSpecialAreaRumorsNumber(string sSpecialAreaScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 3);
    ExecuteScript(sSpecialAreaScript);
    return GetLocalInt(mod, funcResult);
}

int GetSpecialAreaDeadendAreas(string sSpecialAreaScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 8);
    ExecuteScript(sSpecialAreaScript);
    return GetLocalInt(mod, funcResult);
}

int GetSpecialAreaPassthroughAreas(string sSpecialAreaScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 9);
    ExecuteScript(sSpecialAreaScript);
    return GetLocalInt(mod, funcResult);
}

string GetSpecialAreaResRef(string sSpecialAreaScript, int nIndex, object oBiome, int nExitsFlag)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 4);
    SetLocalInt(mod, funcArg1, nIndex);
    SetLocalObject(mod, funcArg2, oBiome);
    SetLocalInt(mod, funcArg3, nExitsFlag);
    ExecuteScript(sSpecialAreaScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid area resref returned in special area " + sSpecialAreaScript + ", nIndex = %n", nIndex);
    return result;
}

void InitializeSpecialArea(string sSeedName, object oBiome, object oSpecialArea, int nBiomeStartingLevel)
{
    object mod = GetModule();
    string specialAreaScript = GetSpecialAreaScript(oSpecialArea);
    SetLocalInt(mod, funcHandler, 5);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalObject(mod, funcArg2, oBiome);
    SetLocalObject(mod, funcArg3, oSpecialArea);
    SetLocalInt(mod, funcArg4, nBiomeStartingLevel);
    ExecuteScript(specialAreaScript);
}

string GetSpecialAreaRumor(object oSpecialArea, int nIndex, int nLanguage)
{
    object mod = GetModule();
    string specialAreaScript = GetSpecialAreaScript(oSpecialArea);
    SetLocalInt(mod, funcHandler, 6);
    SetLocalObject(mod, funcArg1, oSpecialArea);
    SetLocalInt(mod, funcArg2, nIndex);
    SetLocalInt(mod, funcArg3, nLanguage);
    ExecuteScript(specialAreaScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid rumor returned in special area " + specialAreaScript + ", nIndex = %n, nLanguage = %n", nIndex, nLanguage);
    return result;
}

string GetRandomSpecialAreaName(string sSpecialAreaScript, string sSeedName, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 7);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalInt(mod, funcArg2, nLanguage);
    ExecuteScript(sSpecialAreaScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid special area name returned in " + sSpecialAreaScript + ", sSeedName = " + sSeedName + ", nLanguage = %n", nLanguage);
    return result;
}

int GetNumberOfSpecialAreaRegularQuests(string sSpecialAreaScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 13);
    ExecuteScript(sSpecialAreaScript);
    return GetLocalInt(mod, funcResult);
}

int GetNumberOfSpecialAreaSpecialtyQuests(string sSpecialAreaScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 14);
    ExecuteScript(sSpecialAreaScript);
    return GetLocalInt(mod, funcResult);
}

int GetSpecialAreaHasLowQuestGenerationPriority(string sSpecialAreaScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 10);
    ExecuteScript(sSpecialAreaScript);
    return GetLocalInt(mod, funcResult);
}

string GetSpecialAreaRegularQuestScript(string sSpecialAreaScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 11);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sSpecialAreaScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid regular quest script name returned in special area " + sSpecialAreaScript + ", nIndex = %n", nIndex);
    return result;
}

string GetSpecialAreaSpecialtyQuestScript(string sSpecialAreaScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 12);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sSpecialAreaScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid specialty quest script name returned in special area " + sSpecialAreaScript + ", nIndex = %n", nIndex);
    return result;
}

//Instance functions

//Initializator
void InitializeSpecialAreaInstance(string sSpecialAreaScript, int nLowQuestGenerationPriority, int nEnRouteToTown, object oSpecialArea, object oBiome)
{
    SetLocalObject(oSpecialArea, "SPEAREA_BIOME", oBiome);
    SetLocalString(oSpecialArea, "SPEAREA_SCRIPT", sSpecialAreaScript);

    string encountersForQuestsArray;
    if (!nLowQuestGenerationPriority && !nEnRouteToTown)
        encountersForQuestsArray = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_1;
    else if (nLowQuestGenerationPriority && !nEnRouteToTown)
        encountersForQuestsArray = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_2;
    else if (!nLowQuestGenerationPriority && nEnRouteToTown)
        encountersForQuestsArray = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_3;
    else
        encountersForQuestsArray = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_4;
    AddObjectArrayElement(encountersForQuestsArray, oSpecialArea, FALSE, oBiome);   
}

//Instance functions
string GetSpecialAreaScript(object oSpecialArea)
{
    return GetLocalString(oSpecialArea, "SPEAREA_SCRIPT");
}

object GetBiomeOfSpecialArea(object oSpecialArea)
{
    return GetLocalObject(oSpecialArea, "SPEAREA_BIOME");
}

object GetQuestOfSpecialArea(object oSpecialArea)
{
    return GetLocalObject(oSpecialArea, "SPEAREA_QUEST");
}

void SetQuestOfSpecialArea(object oSpecialArea, object oQuest)
{
    SetLocalObject(oSpecialArea, "SPEAREA_QUEST", oQuest);
}