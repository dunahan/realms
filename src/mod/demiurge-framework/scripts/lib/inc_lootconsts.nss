const int LOOT_QUALITY_USELESS = 1;
const int LOOT_QUALITY_BAD = 2;
const int LOOT_QUALITY_AVERAGE = 3;
const int LOOT_QUALITY_GOOD = 4;
const int LOOT_QUALITY_GREAT = 5;
const int LOOT_QUALITY_EXTRAORDINARY = 6;

struct TreasureChestLoot
{
    int gold;
    string itemResRef;
    int itemStackSize;
};