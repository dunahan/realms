#include "inc_common"
#include "inc_resting"
#include "x0_i0_spawncond"
#include "inc_random"
#include "inc_str_rtile"
#include "inc_str_rregion"
#include "inc_names"
#include "inc_colors"
#include "inc_realms"
#include "inc_debug"
#include "inc_biomes"
#include "inc_language"
#include "inc_arrays"
#include "inc_towns"
#include "inc_tiles"
#include "inc_encounters"
#include "inc_quests"
#include "inc_dftokens"
#include "inc_rngnames"
#include "inc_scriptevents"
#include "inc_areas"
#include "inc_stores"
#include "inc_facilities"
#include "inc_specialare"
#include "inc_plots"
#include "inc_bossareas"
#include "inc_champions"
#include "inc_traps"

// ---------------
// -- Constants --
// ---------------

const int AXIS_X = 0;
const int AXIS_Y = 1;
const int AXIS_NONE = -1;

// ---------------
// --  Structs  --
// ---------------

struct RegionCutData
{
    int axis;
    int coordinate;
};

// ------------------
// --  Parameters  --
// ------------------

// Set to FALSE to make it possible for very big regions to form
int nBalancedRegions = TRUE;

// Set to FALSE to allow regions' leading nodes to spawn next to each other
int nSpaceAroundRegions = TRUE;

// Sets the minimum number of areas that a region should be allowed to have;
// Values too small may result in silly-small regions, while values too large in some regions being very big (due to absorbing smaller regions)
int nMinRegionSize = 10;

// Sets the maximum number of areas that a region should be allowed to have;
// Values too small in relation to nMinRegionSize may cause map generation errors, recommended value is 3*nMinRegionSize or higher
int nMaxRegionSize = 30;

// Sets the average number of region tiles per one generated special tile (more special tiles may appear if there are many dead ends in the region's map)
int nSpecialTileRarity = 3;

// *UNUSED AT THE MOMENT* Sets the average number of region tiles per one generated settlement tile; A minimum of one settlement is generated for every region
int nSettlementRarity = 15;

// Sets the maximum distance (in tiles) a settlement may spawn from the region's entrance (to ensure one does not have to traverse the whole region before arriving in the settlement)
int nMaxSettlementDistance = 3;

// Determines whether town tiles are allowed to generate on tiles bordering the predecessing region
int nNoTownsOnBorders = TRUE;

// -------------------------
// -- Function prototypes --
// -------------------------

// Generates a new realm map
// sDescriptor - identifier of the new realm
// nHeight - vertical length of the map in areas
// nWidth - horizontal length of the map in areas
// nRNGSeed - seed to use for randomness, 0 selects it at random
// nStep - number of steps indexed from 1, the function needs to be called 79 times (a TMI workaround), make sure each call is delayed by 0.0f
// oRealm - object to store the generated map structures on
void GenerateRealmMap(int nWidth, int nHeight, int nStep, object oRealm);

// Generates path for a realm map generated via GenerateRealmMap
// sDescriptor - identifier of the realm
// nStep - number of steps indexed from 1, the function needs to be called 20 times
// oRealm - object to store the generated map structures on
void GeneratePaths(int nStep, object oRealm);

// Determines which tiles in regions have special purpose (towns, dungeons, etc) and what it is
// sDescriptor - identifier of the new region
// nStep - number of steps indexed from 1, the function needs to be called 20 times
// oRealm - object to store the generated map structures on
void GenerateSpecialTiles(int nStep, object oRealm);

// Finds paths between tiles within regions in the generated realm.
// sDescriptor - identifier of the realm
// nStep - number of steps indexed from 1, the function needs to be called 1351 times - yes, seriously
// oRealm - object to store the generated map structures on
void FindPaths(int nStep, object oRealm);

// Finds paths between regions in the generated realm.
// nStep - number of steps indexed from 1, the function needs to be called 400 times
// oRealm - object to store the generated map structures on
void FindPathsBetweenRegions(int nStep, object oRealm);

// Creates a plot and boss area
void SpawnPlot(object oRealm);

// Deletes all template areas in the module (to save on memory, loading times and save file sizes).
// Template areas are those that are not the starting area, the companion creator area, pseudo-limbo, or part of a tile.
void DeleteTemplateAreas();

// -----------------------------
// -- Function implementation --
// -----------------------------

//Functions for GenerateRealmMap

void _ManageSemaphor(int nStep, int nLastStep)
{
    //First step: reset minor step counter
    if (nStep == 1)
        SetLocalInt(OBJECT_SELF, "PROC_NEXTMINORSTEP", 1);
    //Last step: let the next major step of the procedure run
    if (nStep == nLastStep)
    {
        AddToLocalInt(OBJECT_SELF, "PROC_NEXTMAJORSTEP", 1);
    }
    AddToLocalInt(OBJECT_SELF, "PROC_NEXTMINORSTEP", 1);

    //Also, calculate map's "checksum" - ugly place to do this, I know, shush already, alright?
    object mod = GetModule();
    int currChecksum = GetLocalInt(mod, "MAP_CHECKSUM");
    if (currChecksum == 0)
        currChecksum = 1;
    int currSeed = GetLocalInt(mod, "_custom_seed_default");
    currChecksum *= currSeed;
    SetLocalInt(mod, "MAP_CHECKSUM", currChecksum);
}

void _CreateRealmGrid(int nWidth, int nHeight, object oRealm)
{
    //Create an array of tile arrays - each tile array is one row of tiles in the world
    CreateStringArray("Grid", nHeight, oRealm);
    int i, j;
    for (i = 0; i < nHeight; i++)
    {
        string innerArrayName = "Grid_MapRow"+IntToString(i);
        SetStringArrayElement("Grid", nHeight, innerArrayName, oRealm);

        CreateRealmTileArray(innerArrayName, nWidth, oRealm);
        for (j = 0; j < nWidth; j++)
        {
            struct RealmTile tile;
            tile.x = j;
            tile.y = i;
            tile.index = ConvertTileCoordinatesToIndex(j, i, nWidth);
            tile.region = -1; //this is so that unset region can be differentiated from a set one
            tile.distances = "tileDistances_" + IntToString(tile.index);

            //block specific passageways if the tile is on the map edge
            if (j == 0)
                tile.blocked = PATH_WEST;
            if (j == nWidth-1)
                tile.blocked = tile.blocked | PATH_EAST;
            if (i == 0)
                tile.blocked = tile.blocked | PATH_NORTH;
            if (i == nHeight-1)
                tile.blocked = tile.blocked | PATH_SOUTH;

            SetRealmTileArrayElement(innerArrayName, j, tile, oRealm);
        }
    }
}

struct RealmTile _GetTileOnGrid(int x, int y, object oRealm)
{
    string innerArrayName = "Grid_MapRow"+IntToString(y);

    int yMax = GetStringArraySize("Grid", oRealm);
    int xMax = GetRealmTileArraySize(innerArrayName, oRealm);
    AssertIntLower(yMax, y, "inc_world, _GetTileOnGrid, y");
    AssertIntLower(xMax, x, "inc_world, _GetTileOnGrid, x");

    return GetRealmTileArrayElement(innerArrayName, x, oRealm);
}

int _GetTileFieldOnGrid(int x, int y, string sField, object oRealm)
{
    string innerArrayName = "Grid_MapRow"+IntToString(y);

    int yMax = GetStringArraySize("Grid", oRealm);
    int xMax = GetRealmTileArraySize(innerArrayName, oRealm);
    AssertIntLower(yMax, y, "inc_world, _GetTileFieldOnGrid, y");
    AssertIntLower(xMax, x, "inc_world, _GetTileFieldOnGrid, x");

    return GetRealmTileArrayElementField(innerArrayName, x, sField, oRealm);
}

string _GetStringTileFieldOnGrid(int x, int y, string sField, object oRealm)
{
    string innerArrayName = "Grid_MapRow"+IntToString(y);

    int yMax = GetStringArraySize("Grid", oRealm);
    int xMax = GetRealmTileArraySize(innerArrayName, oRealm);
    AssertIntLower(yMax, y, "inc_world, _GetTileFieldOnGrid, y");
    AssertIntLower(xMax, x, "inc_world, _GetTileFieldOnGrid, x");

    return GetRealmTileArrayStringElementField(innerArrayName, x, sField, oRealm);
}

string _GetTileDistancesFieldOnGrid(int x, int y, object oRealm)
{
    string innerArrayName = "Grid_MapRow"+IntToString(y);

    int yMax = GetStringArraySize("Grid", oRealm);
    int xMax = GetRealmTileArraySize(innerArrayName, oRealm);
    AssertIntLower(yMax, y, "inc_world, _GetTileFieldOnGrid, y");
    AssertIntLower(xMax, x, "inc_world, _GetTileFieldOnGrid, x");

    return GetRealmTileArrayElementDistancesField(innerArrayName, x, oRealm);
}

void _SetTileOnGrid(struct RealmTile tile, object oRealm)
{
    string innerArrayName = "Grid_MapRow"+IntToString(tile.y);

    int yMax = GetStringArraySize("Grid", oRealm);
    int xMax = GetRealmTileArraySize(innerArrayName, oRealm);
    AssertIntLower(yMax, tile.y, "inc_world, _SetTileOnGrid, tile.y");
    AssertIntLower(xMax, tile.x, "inc_world, _SetTileOnGrid, tile.x");

    SetRealmTileArrayElement(innerArrayName, tile.x, tile, oRealm);
}

void _SetTileFieldOnGrid(int x, int y, string sField, int nValue, object oRealm)
{
    string innerArrayName = "Grid_MapRow"+IntToString(y);

    int yMax = GetStringArraySize("Grid", oRealm);
    int xMax = GetRealmTileArraySize(innerArrayName, oRealm);
    AssertIntLower(yMax, y, "inc_world, _SetTileFieldOnGrid, y");
    AssertIntLower(xMax, x, "inc_world, _SetTileFieldOnGrid, x");

    SetRealmTileArrayElementField(innerArrayName, x, sField, nValue, oRealm);
}

void _SetStringTileFieldOnGrid(int x, int y, string sField, string sValue, object oRealm)
{
    string innerArrayName = "Grid_MapRow"+IntToString(y);

    int yMax = GetStringArraySize("Grid", oRealm);
    int xMax = GetRealmTileArraySize(innerArrayName, oRealm);
    AssertIntLower(yMax, y, "inc_world, _SetTileFieldOnGrid, y");
    AssertIntLower(xMax, x, "inc_world, _SetTileFieldOnGrid, x");

    SetRealmTileArrayStringElementField(innerArrayName, x, sField, sValue, oRealm);
}

int _GetDistanceBetweenTiles(struct RealmTile tile1, struct RealmTile tile2)
{
    //Manhattan distance
    int xDiff = abs(tile1.x - tile2.x);
    int yDiff = abs(tile1.y - tile2.y);
    return xDiff + yDiff;
}

int _GetNumberOfRegions(int nWidth, int nHeight)
{
    //One region for every 20 areas
    int areasNum = nWidth*nHeight;
    int result = areasNum < 20 ? 1 : areasNum/20;

    //Randomly change the final result by adding more regions:
    //the maximum is 1 plus 1 for every 50 areas
    int maxExtraRegions = 2 + areasNum/50;
    result += RandomNext(maxExtraRegions);

    return result;
}

//This function returns the region ID of the tile array element closest to the tile provided
int _GetClosestRegion(string sArray, struct RealmTile tile, object oRealm)
{
    if (tile.region != -1)
        return tile.region;
    int i;
    int arraySize = GetRealmTileArraySize(sArray, oRealm);
    int minDistance = 999999;
    int region = -1; //placeholder value
    for (i = 0; i < arraySize; i++)
    {
        struct RealmTile arrayTile; // = GetRealmTileArrayElement(sArray, i, oRealm);
        arrayTile.x = GetRealmTileArrayElementField(sArray, i, "x", oRealm);
        arrayTile.y = GetRealmTileArrayElementField(sArray, i, "y", oRealm);
        arrayTile.region = GetRealmTileArrayElementField(sArray, i, "region", oRealm);
        int distance = _GetDistanceBetweenTiles(tile, arrayTile);
        if (distance < minDistance) //new smallest distance
        {
            minDistance = distance;
            region = arrayTile.region;
        }
    }

    AssertIntNotEqual(-1, region, "inc_world, _GetClosestRegion, region");
    return region;
}

//This function is for random selection of a tile without repetitions.
void _CreateTemporaryTileIndexArraysForSelection(string sArrayNW, string sArrayNE, string sArraySE, string sArraySW, int nWidth, int nHeight, object oRealm)
{
    //Four arrays are used instead of one to distribute regions more evenly,
    //every array corresponds to roughly a quarter of the map and leading nodes will be generated in turns in each of them
    CreateIntArray(sArrayNW, 0, oRealm);
    CreateIntArray(sArrayNE, 0, oRealm);
    CreateIntArray(sArraySE, 0, oRealm);
    CreateIntArray(sArraySW, 0, oRealm);

    int i, j;
    for (i = 1; i < nHeight-1; i++) //Add every tile on the grid except map borders
        for (j = 1; j < nWidth-1; j++)
        {
            string array;
            if (i < nHeight/2) //southern half
            {
                if (j < nWidth/2) //south-western quarter
                    array = sArraySW;
                else //south-eastern quarter
                    array = sArraySE;
            }
            else //northern half
            {
                if (j < nWidth/2) //north-western quarter
                    array = sArrayNW;
                else //north-eastern quarter
                    array = sArrayNE;
            }

            int index = ConvertTileCoordinatesToIndex(j, i, nWidth);
            AddIntArrayElement(array, index, FALSE, oRealm);
        }
}

//This function returns the region's neighboring region that is the smallest
int _GetSmallestNeighboringRegionIndex(struct Region reg, string sRegionArray, object oRealm)
{
    int regionsNum = GetStringArraySize(reg.neighbors, oRealm);
    int i;
    int smallestRegionIndex = ARRAY_INDEX_INVALID;
    int smallestSize = 9999;
    for (i = 0; i < regionsNum; i++)
    {
        string tilesOnBorderArray = GetStringArrayElement(reg.neighbors, i, oRealm);
        int borderingTilesNum = GetIntArraySize(tilesOnBorderArray, oRealm);

        //Only regions neighboring the region considered are to be accounted for
        if (borderingTilesNum == 0)
            continue;

        struct Region neighbor = GetRegionArrayElement(sRegionArray, i, oRealm);
        int regionSize = GetIntArraySize(neighbor.tiles, oRealm);
        if (regionSize < smallestSize)
        {
            smallestRegionIndex = i;
            smallestSize = regionSize;
        }
    }

    AssertIntNotEqual(ARRAY_INDEX_INVALID, smallestRegionIndex, "inc_world, _GetSmallestNeighboringRegionIndex, smallestRegionIndex");
    return smallestRegionIndex;
}

//This function generates region's base properties, given an array of regions and a slot of the region to modify
void _GenerateRegion(string sArray, int nArrayIndex, int regionsNum, string sSeedName, object oRealm)
{
    int arraySize = GetRegionArraySize(sArray, oRealm);
    string arrayIndexString = IntToString(nArrayIndex);
    string tilesArrayName = sArray + "_tiles" + arrayIndexString;
    string neighborsArrayName = sArray + "_neighbors" + arrayIndexString;
    AssertIntLower(arraySize, nArrayIndex, "inc_world, _GenerateRegion, nArrayIndex");

    //to save space and time, this is an array of tile indexes and not tiles themselves, as calculated by ConvertTileCoordinatesToIndex
    CreateIntArray(tilesArrayName, 0, oRealm);

    //create a 2D array of tiles neighboring other regions
    CreateStringArray(neighborsArrayName, regionsNum, oRealm);
    int i;
    for (i = 0; i < regionsNum; i++)
    {
        string intArrayName = neighborsArrayName + "_TilesOnBorder" + IntToString(i);
        SetStringArrayElement(neighborsArrayName, i, intArrayName, oRealm);
        CreateIntArray(intArrayName, 0, oRealm);
    }

    //number of available biomes
    string realmScript = GetRealmScript(oRealm);
    int biomesNum = GetNumberOfOverworldBiomes(realmScript);

    struct Region reg;
    reg.id = nArrayIndex;
    reg.type = GetOverworldBiomeScript(realmScript, RandomNext(biomesNum));
    reg.name = GetRandomBiomeRegionName(reg.type, sSeedName, GetServerLanguage());
    reg.tiles = tilesArrayName;
    reg.neighbors = neighborsArrayName;

    SetRegionArrayElement(sArray, nArrayIndex, reg, oRealm);
}

//This function searches for tiles that border with other regions and adds them to the region's list of borders, as well as adds the neighbor to the list of neighboring regions
void _FindBorders(struct Region reg, int nWidth, int nHeight, object oRealm)
{
    string arrayNeighbors = reg.neighbors;
    int tilesNum = GetIntArraySize(reg.tiles, oRealm);

    int i;
    for (i = 0; i < tilesNum; i++)
    {
        struct Coordinates coords = ConvertTileIndexToCoordinates(GetIntArrayElement(reg.tiles, i, oRealm), nWidth);
        struct RealmTile tile = _GetTileOnGrid(coords.x, coords.y, oRealm);
        struct RealmTile neighbor;

        if (coords.y > 0) //north
        {
            neighbor = _GetTileOnGrid(coords.x, coords.y-1, oRealm);
            if (neighbor.region != tile.region)
            {
                string arrayOfBorderTiles = GetStringArrayElement(arrayNeighbors, neighbor.region, oRealm); //get the descriptor of the array of tile IDs neighboring the region
                AddIntArrayElement(arrayOfBorderTiles, tile.index, FALSE, oRealm); //add the neighboring tile's index to the array of tile IDs neighboring the region
            }
        }
        if (coords.y < nHeight-1) //south
        {
            neighbor = _GetTileOnGrid(coords.x, coords.y+1, oRealm);
            if (neighbor.region != tile.region)
            {
                string arrayOfBorderTiles = GetStringArrayElement(arrayNeighbors, neighbor.region, oRealm); //get the descriptor of the array of tile IDs neighboring the region
                AddIntArrayElement(arrayOfBorderTiles, tile.index, FALSE, oRealm);
            }
        }
        if (coords.x > 0) //west
        {
            neighbor = _GetTileOnGrid(coords.x-1, coords.y, oRealm);
            if (neighbor.region != tile.region)
            {
                string arrayOfBorderTiles = GetStringArrayElement(arrayNeighbors, neighbor.region, oRealm); //get the descriptor of the array of tile IDs neighboring the region
                AddIntArrayElement(arrayOfBorderTiles, tile.index, FALSE, oRealm);
            }
        }
        if (coords.x < nWidth-1) //east
        {
            neighbor = _GetTileOnGrid(coords.x+1, coords.y, oRealm);
            if (neighbor.region != tile.region)
            {
                string arrayOfBorderTiles = GetStringArrayElement(arrayNeighbors, neighbor.region, oRealm); //get the descriptor of the array of tile IDs neighboring the region
                AddIntArrayElement(arrayOfBorderTiles, tile.index, FALSE, oRealm);
            }
        }
    }
}

//This functions iterates through the lists of the region's tiles that border other regions and updates their blocked paths to make it impossible to generate paths across regions
void _SetBlockedPaths(struct Region reg, int nWidth, int nHeight, object oRealm)
{
    string arrayNeighbors = reg.neighbors;
    int neighborsNum = GetStringArraySize(reg.neighbors, oRealm);
    int i, j;
    for (i = 0; i < neighborsNum; i++) //for every region on the list of neighboring regions
    {
        string borderingTilesArray = GetStringArrayElement(arrayNeighbors, i, oRealm);
        int borderingTilesNum = GetIntArraySize(borderingTilesArray, oRealm);

        for (j = 0; j < borderingTilesNum; j++) //for every tile bordering that neighbor
        {
            int tileIsBorder = FALSE;
            struct Coordinates coords = ConvertTileIndexToCoordinates(GetIntArrayElement(borderingTilesArray, j, oRealm), nWidth);
            struct RealmTile tile = _GetTileOnGrid(coords.x, coords.y, oRealm);
            struct RealmTile neighbor;

            if (coords.y > 0) //north
            {
                neighbor = _GetTileOnGrid(coords.x, coords.y-1, oRealm);
                if (neighbor.region != tile.region)
                {
                    tileIsBorder = TRUE;
                    tile.blocked = tile.blocked | PATH_NORTH;
                }
            }
            if (coords.y < nHeight-1) //south
            {
                neighbor = _GetTileOnGrid(coords.x, coords.y+1, oRealm);
                if (neighbor.region != tile.region)
                {
                    tileIsBorder = TRUE;
                    tile.blocked = tile.blocked | PATH_SOUTH;
                }
            }
            if (coords.x > 0) //west
            {
                neighbor = _GetTileOnGrid(coords.x-1, coords.y, oRealm);
                if (neighbor.region != tile.region)
                {
                    tileIsBorder = TRUE;
                    tile.blocked = tile.blocked | PATH_WEST;
                }
            }
            if (coords.x < nWidth-1) //east
            {
                neighbor = _GetTileOnGrid(coords.x+1, coords.y, oRealm);
                if (neighbor.region != tile.region)
                {
                    tileIsBorder = TRUE;
                    tile.blocked = tile.blocked | PATH_EAST;
                }
            }

            //update the tile due to modification of the blocked field
            if (tileIsBorder)
                _SetTileOnGrid(tile, oRealm);
        }
    }
}

//This function is used to make one region absorb another, changing relevant structs accordingly
void _AbsorbRegion(struct Region oldReg, struct Region newReg, int regionsNum, int nWidth, int nHeight, object oRealm)
{
    LogInfo("Region " + IntToString(oldReg.id) + " absorbed by region " + IntToString(newReg.id));

    //Move tiles from oldReg.tiles to newReg.tiles
    int regSize = GetIntArraySize(oldReg.tiles, oRealm);
    int i;
    for (i = 0; i < regSize; i++)
    {
        //Add tiles from the old region to the new one
        int tileIndex = GetIntArrayElement(oldReg.tiles, i, oRealm);
        AddIntArrayElement(newReg.tiles, tileIndex, FALSE, oRealm);
        struct Coordinates coords = ConvertTileIndexToCoordinates(tileIndex, nWidth);

        //Update moved tiles
        struct RealmTile tile = _GetTileOnGrid(coords.x, coords.y, oRealm); //TODO: replace with single field change functions
        tile.region = newReg.id;
        tile.biome = newReg.type;
        _SetTileOnGrid(tile, oRealm);
    }

    //Remove tiles from the list of the old region's tiles and neighbors
    CreateIntArray(oldReg.tiles, 0, oRealm);
    for (i = 0; i < regionsNum; i++)
    {
        if (i == oldReg.id)
            continue;

        int j;
        string bordersArray;
        string newBordersArray;
        int borderTilesNum;

        //Absorbed region's tiles that bordered region i now should be added to the list of the absorbing region's tiles bordering region i
        bordersArray = GetStringArrayElement(oldReg.neighbors, i, oRealm);
        if (i != newReg.id) //we don't want the new region to "think" it's neighboring itself
        {
            newBordersArray = GetStringArrayElement(newReg.neighbors, i, oRealm);
            borderTilesNum = GetIntArraySize(bordersArray, oRealm);
            for (j = 0; j < borderTilesNum; j++)
            {
                int tileIndex = GetIntArrayElement(bordersArray, j, oRealm);
                AddIntArrayElement(newBordersArray, tileIndex, FALSE, oRealm);
            }
        }
        CreateIntArray(bordersArray, 0, oRealm);

        //For every other region move tiles from the list of tiles bordering the old region to the list of borders of the new region (unless they are there already)
        struct Region potentialNeighbor = GetRegionArrayElement("Regions", i, oRealm);
        bordersArray = GetStringArrayElement(potentialNeighbor.neighbors, oldReg.id, oRealm);
        if (i != newReg.id) //we don't want the new region to "think" it's neighboring itself
        {
            newBordersArray = GetStringArrayElement(potentialNeighbor.neighbors, newReg.id, oRealm);
            borderTilesNum = GetIntArraySize(bordersArray, oRealm);
            for (j = 0; j < borderTilesNum; j++)
            {
                int tileIndex = GetIntArrayElement(bordersArray, j, oRealm);
                if (GetIntArrayIndexByValue(newBordersArray, tileIndex, 1, oRealm) == ARRAY_INDEX_INVALID)
                    AddIntArrayElement(newBordersArray, tileIndex, FALSE, oRealm);
            }
        }
        CreateIntArray(bordersArray, 0, oRealm);
    }
}

//Returns TRUE if cutting an oversized region reg according to cutData would result in at least one of the regions being disjointed
int _GetCutCreatesDisjointRegion(struct Region reg, struct RegionCutData cutData, int nRegStartingCoord, int nRegMaxCoord, int nWidth, object oRealm)
{
    int arraySize = nRegMaxCoord - nRegStartingCoord + 1;
    LogInfo("Min coord: " + IntToString(nRegStartingCoord) + ", max coord: " + IntToString(nRegMaxCoord) + ", array size: " + IntToString(arraySize));
    string coordinatesCheckedReg1 = "coordinatesCheckedReg1";
    string coordinatesCheckedReg2 = "coordinatesCheckedReg2";
    CreateIntArray(coordinatesCheckedReg1, arraySize, oRealm);
    CreateIntArray(coordinatesCheckedReg2, arraySize, oRealm);

    string regTileArray = reg.tiles;
    int regSize = GetIntArraySize(regTileArray, oRealm);
    int i;
    for (i = 0; i < regSize; i++)
    {
        int tileIndex = GetIntArrayElement(regTileArray, i, oRealm);
        struct Coordinates coords = ConvertTileIndexToCoordinates(tileIndex, nWidth);
        int coordinate = cutData.axis == AXIS_Y ? coords.y : coords.x;
        int coordinateAlongCut = cutData.axis == AXIS_Y ? coords.x : coords.y;

        if (coordinate <= cutData.coordinate)
            SetIntArrayElement(coordinatesCheckedReg1, coordinateAlongCut - nRegStartingCoord, 1, oRealm);
        else
            SetIntArrayElement(coordinatesCheckedReg2, coordinateAlongCut - nRegStartingCoord, 1, oRealm);
    }

    int j;
    int regionsConnected = TRUE;
    for (j = 0; j < 2; j++)
    {
        string axisAreaPresencee = "";
        string array = j ? coordinatesCheckedReg2 : coordinatesCheckedReg1;
        int foundFirstCoordOfRegion = FALSE;
        int foundLastCoordOfRegion = FALSE;
        int regionDisconnected = FALSE;
        for (i = 0; i < arraySize; i++)
        {
            int areaPresent = GetIntArrayElement(array, i, oRealm);
            axisAreaPresencee += IntToString(areaPresent);
            if (areaPresent)
            {
                if (foundLastCoordOfRegion) //if we stumble upon a coordinate with an area after determining the region should already end earlier, the region is disconnected
                {
                    regionDisconnected = TRUE;
                    break;
                }
                foundFirstCoordOfRegion = TRUE; //upon finding a coordinate with an area, it's certain that we have found the first coordinate value of the region
            }
            else if (foundFirstCoordOfRegion) //if we did not find an area after already finding a coordinate with an area, this means the region apparently ended
                foundLastCoordOfRegion = TRUE;
        }
        LogInfo("Region's axis area presence: " + axisAreaPresencee);
        if (regionDisconnected)
        {
            regionsConnected = FALSE;
            LogInfo("The newly created region " + IntToString(j+1) + " is disconnected");
        }
    }
    ClearIntArray(coordinatesCheckedReg1, oRealm);
    ClearIntArray(coordinatesCheckedReg2, oRealm);

    return !regionsConnected;
}

//This function is used to calculate and return the optimal region cut info for oversized regions;
//returns AXIS_NONE in the "axis" field if cutting the region would violate some region's integrity or minimal size
struct RegionCutData _GetOptimalRegionCut(struct Region reg, int nWidth, object oRealm)
{
    string regTileArray = reg.tiles;
    int regSize = GetIntArraySize(regTileArray, oRealm);
    string areasToTheLeftArray = "areasToTheLeft"; //every p-th array element will hold the number of region areas with x coordinate below or equal (starting region X + p)
    string areasAboveArray = "areasAbove"; //every p-th array element will hold the number of region areas with y coordinate below or equal (starting region Y + p)
    CreateIntArray(areasToTheLeftArray, regSize, oRealm);
    CreateIntArray(areasAboveArray, regSize, oRealm);

    int regStartingX = 999;
    int regMaxX = -1;
    int regStartingY = 999;
    int regMaxY = -1;
    int j;

    for (j = 0; j < regSize; j++) //Determining starting and maximum array coordinates
    {
        int tileIndex = GetIntArrayElement(regTileArray, j, oRealm);
        struct Coordinates coords = ConvertTileIndexToCoordinates(tileIndex, nWidth);

        if (coords.x < regStartingX)
            regStartingX = coords.x;
        if (coords.y < regStartingY)
            regStartingY = coords.y;
        if (coords.x > regMaxX)
            regMaxX = coords.x;
        if (coords.y > regMaxY)
            regMaxY = coords.y;
    }
    LogInfo("Starting X: " + IntToString(regStartingX));
    LogInfo("Max X: " + IntToString(regMaxX));
    LogInfo("Starting Y: " + IntToString(regStartingY));
    LogInfo("Max Y: " + IntToString(regMaxY));

    for (j = 0; j < regSize; j++) //Incrementing appropriate array positions
    {
        int tileIndex = GetIntArrayElement(regTileArray, j, oRealm);
        struct Coordinates coords = ConvertTileIndexToCoordinates(tileIndex, nWidth);

        int currentVal = GetIntArrayElement(areasToTheLeftArray, coords.x - regStartingX, oRealm);
        SetIntArrayElement(areasToTheLeftArray, coords.x - regStartingX, currentVal+1, oRealm);
        currentVal = GetIntArrayElement(areasAboveArray, coords.y - regStartingY, oRealm);
        SetIntArrayElement(areasAboveArray, coords.y - regStartingY, currentVal+1, oRealm);
    }

    int valuesOfX = regMaxX - regStartingX;
    int optimumXCut = -1;
    int optimumXCutNumberOfAreas = -1; //number of areas to the left of the optimum X coordinate
    int perfectXCut = FALSE;
    int xCutSelected = TRUE;
    for (j = 1; j < valuesOfX; j++) //searching for the best X coordinate value for a cut along the Y axis
    {
        int currentVal = GetIntArrayElement(areasToTheLeftArray, j, oRealm);
        int previousVal = GetIntArrayElement(areasToTheLeftArray, j-1, oRealm);
        int newVal = currentVal + previousVal;

        int offset = abs(newVal - regSize/2);

        if (offset == 0 || offset == 1) //if we actually found a perfect cut (splitting the region exactly in half), let's not bother anymore
        {
            optimumXCut = regStartingX + j;
            optimumXCutNumberOfAreas = newVal;
            perfectXCut = TRUE;
            break;
        }
        if (offset < abs(optimumXCutNumberOfAreas - regSize/2))
        {
            optimumXCut = regStartingX + j;
            optimumXCutNumberOfAreas = newVal;
        }
        SetIntArrayElement(areasToTheLeftArray, j, newVal, oRealm);
    }

    int valuesOfY = regMaxY - regStartingY;
    int optimumYCut = -1;
    int optimumYCutNumberOfAreas = -1; //number of areas to the left of the optimum Y coordinate
    if (!perfectXCut)
        for (j = 1; j < valuesOfY; j++) //searching for the best Y coordinate value for a cut along the X axis
        {
            int currentVal = GetIntArrayElement(areasAboveArray, j, oRealm);
            int previousVal = GetIntArrayElement(areasAboveArray, j-1, oRealm);
            int newVal = currentVal + previousVal;

            int offset = abs(newVal - regSize/2);

            if (offset == 0 || offset == 1) //if we actually found a perfect cut (splitting the region exactly in half), let's not bother anymore
            {
                optimumYCut = regStartingY + j;
                optimumYCutNumberOfAreas = newVal;
                xCutSelected = FALSE;
                break;
            }

            if (xCutSelected && offset < abs(optimumXCutNumberOfAreas - regSize/2)) //compare with the optimal X cut until we find something better
                xCutSelected = FALSE;

            if (!xCutSelected && offset < abs(optimumYCutNumberOfAreas - regSize/2)) //once we've surpassed the optimal X cut, compare with current Y cut
            {
                optimumYCut = regStartingY + j;
                optimumYCutNumberOfAreas = newVal;
            }
            SetIntArrayElement(areasAboveArray, j, newVal, oRealm);
        }

    ClearIntArray(areasToTheLeftArray, oRealm);
    ClearIntArray(areasAboveArray, oRealm);

    struct RegionCutData cutData;
    cutData.axis = xCutSelected ? AXIS_X : AXIS_Y;
    cutData.coordinate = xCutSelected ? optimumXCut : optimumYCut;

    string axis = xCutSelected ? "X" : "Y";
    int firstSize = xCutSelected ? optimumXCutNumberOfAreas : optimumYCutNumberOfAreas;

    LogInfo("Best cut: " + axis + "-axis value, coordinate: " + IntToString(cutData.coordinate));
    LogInfo("First product region's size: " + IntToString(firstSize));
    LogInfo("Second product region's size: " + IntToString(regSize-firstSize));

    //Discard the cut if it would result in a region below the minimum size
    if (regSize-firstSize < nMinRegionSize || firstSize < nMinRegionSize)
    {
        LogInfo("Cut discarded - one of the regions is too small");
        cutData.axis = AXIS_NONE;
    }

    //Discard the cut if it would result in a disjointed region
    int startingCoord = xCutSelected ? regStartingY : regStartingX;
    int maxCoord = xCutSelected ? regMaxY : regMaxX;
    if (_GetCutCreatesDisjointRegion(reg, cutData, startingCoord, maxCoord, nWidth, oRealm))
    {
        LogInfo("Cut discarded - at least one of the regions is disjointed");
        cutData.axis = AXIS_NONE;
    }

    return cutData;
}

//Performs the region split according to cutData, adds a new region to the list and moves all the relevant info
void _SplitRegion(struct Region reg, struct RegionCutData cutData, int regionsNum, int nWidth, int nHeight, object oRealm)
{
    //Add new int array to the neighbors list of every region
    int i;
    string regionsArray = "Regions";
    int regArraySize = GetRegionArraySize(regionsArray, oRealm);
    for (i = 0; i < regArraySize; i++)
    {
        string neighborsArrayName = GetRegionArrayElementStringField(regionsArray, i, "neighbors", oRealm);
        string intArrayName = neighborsArrayName + "_TilesOnBorder" + IntToString(regionsNum);
        AddStringArrayElement(neighborsArrayName, intArrayName, FALSE, oRealm);

        CreateIntArray(intArrayName, 0, oRealm);
    }

    //Create a new region
    struct Region newReg;
    AddRegionArrayElement(regionsArray, newReg, FALSE, oRealm);
    _GenerateRegion(regionsArray, regionsNum, regionsNum+1, "default", oRealm);
    newReg = GetRegionArrayElement(regionsArray, regionsNum, oRealm);

    //Move all areas with the appropriate coordinate > cutData's coordinate from the original region to the new one and add bordering tiles to each other's appropriate neighbors' list along the way
    string originalRegTiles = reg.tiles;
    string newRegTiles = newReg.tiles;
    int originalRegSize = GetIntArraySize(originalRegTiles, oRealm);
    string regTilesNeighboringWithNew = GetStringArrayElement(reg.neighbors, newReg.id, oRealm);
    string newRegTilesNeighboringWithOld = GetStringArrayElement(newReg.neighbors, reg.id, oRealm);
    string tempArray = "tempArray"; //temporary array to store remaining tile indexes in - it's more efficient to do that and recreate the list of tiles based on it than delete multiple tiles
    CreateIntArray(tempArray, 0, oRealm);
    for (i = 0; i < originalRegSize; i++)
    {
        int tileIndex = GetIntArrayElement(originalRegTiles, i, oRealm);
        struct Coordinates coords = ConvertTileIndexToCoordinates(tileIndex, nWidth);

        int coordinate = cutData.axis == AXIS_X ? coords.x : coords.y;
        if (coordinate <= cutData.coordinate)
        {
            AddIntArrayElement(tempArray, tileIndex, FALSE, oRealm);
            if (coordinate == cutData.coordinate) //the area may potentially be neighboring the new region
            {
                int neighbor;
                if (cutData.axis == AXIS_X)
                    neighbor = _GetTileFieldOnGrid(coords.x + 1, coords.y, "region", oRealm);
                else
                    neighbor = _GetTileFieldOnGrid(coords.x, coords.y + 1, "region", oRealm);
                if (neighbor == newReg.id || neighbor == reg.id) //we check for reg.id, too, because the neighboring tile may not have yet been assigned the new region and if it's the old region, we know for certain it will be moved because of its coordinates
                    AddIntArrayElement(regTilesNeighboringWithNew, tileIndex, FALSE, oRealm);
            }
        }
        else
        {
            AddIntArrayElement(newRegTiles, tileIndex, FALSE, oRealm);
            _SetTileFieldOnGrid(coords.x, coords.y, "region", newReg.id, oRealm);
            _SetStringTileFieldOnGrid(coords.x, coords.y, "biome", newReg.type, oRealm);
            if (coordinate == cutData.coordinate + 1) //the area may potentially be neighboring the old region
            {
                int neighbor;
                if (cutData.axis == AXIS_X)
                    neighbor = _GetTileFieldOnGrid(coords.x - 1, coords.y, "region", oRealm);
                else
                    neighbor = _GetTileFieldOnGrid(coords.x, coords.y - 1, "region", oRealm);
                if (neighbor == reg.id)
                    AddIntArrayElement(newRegTilesNeighboringWithOld, tileIndex, FALSE, oRealm);
            }
        }
    }
    CopyIntArray(tempArray, originalRegTiles, oRealm, oRealm);
    ClearIntArray(tempArray, oRealm);

    //Iterate over the original region's neighbors
    string otherNeighbors; //list of arrays of neighbors of a neighboring region
    for (i = 0; i < regArraySize; i++)
    {
        string otherNeighbors = GetRegionArrayElementStringField(regionsArray, i, "neighbors", oRealm);
        string tilesNeighboringWithReg = GetStringArrayElement(otherNeighbors, reg.id, oRealm);
        string tilesNeighboringWithNewReg = GetStringArrayElement(otherNeighbors, newReg.id, oRealm);
        int j;
        int tilesNeighboringWithRegSize = GetIntArraySize(tilesNeighboringWithReg, oRealm);
        string tempArray = "tempArray"; //temporary array again
        CreateIntArray(tempArray, 0, oRealm);
        for (j = 0; j < tilesNeighboringWithRegSize; j++) //Iterate over the given region's list of tiles neighboring the original region
        {
            int tileIndex = GetIntArrayElement(tilesNeighboringWithReg, j, oRealm);
            struct Coordinates coords = ConvertTileIndexToCoordinates(tileIndex, nWidth);

            int coordinate = cutData.axis == AXIS_X ? coords.x : coords.y;
            if (coordinate < cutData.coordinate) //don't move the tile (logically, because tempArray is used) if coordinate is lower (not lower or equal!) to cutData's coordinate
            {
                AddIntArrayElement(tempArray, tileIndex, FALSE, oRealm);
            }
            else if (coordinate > cutData.coordinate + 1) //move the tile if coordinate is higher by at least two than cutData's coordinate
            {
                AddIntArrayElement(tilesNeighboringWithNewReg, tileIndex, FALSE, oRealm);
            }
            else //if coordinate is close to the border (equal cutData's coordinate or higher by 1), check the tile's neighbors individually
            {
                int neighboringOld = FALSE;
                int neighboringNew = FALSE;

                int west = -1;
                if (coords.x > 0)
                    west = _GetTileFieldOnGrid(coords.x - 1, coords.y, "region", oRealm);
                int east = -1;
                if (coords.x < nWidth - 1)
                    east = _GetTileFieldOnGrid(coords.x + 1, coords.y, "region", oRealm);
                int north = -1;
                if (coords.y > 0)
                    north = _GetTileFieldOnGrid(coords.x, coords.y - 1, "region", oRealm);
                int south = -1;
                if (coords.y < nHeight - 1)
                    south = _GetTileFieldOnGrid(coords.x, coords.y + 1, "region", oRealm);

                if (north == reg.id || east == reg.id || south == reg.id || west == reg.id)
                    neighboringOld = TRUE;
                if (north == newReg.id || east == newReg.id || south == newReg.id || west == newReg.id)
                    neighboringNew = TRUE;

                if (neighboringNew)
                    AddIntArrayElement(tilesNeighboringWithNewReg, tileIndex, FALSE, oRealm);
                if (neighboringOld)
                    AddIntArrayElement(tempArray, tileIndex, FALSE, oRealm);
            }
        }
        CopyIntArray(tempArray, tilesNeighboringWithReg, oRealm, oRealm);
        ClearIntArray(tempArray, oRealm);
    }

    //Move all areas with the appropriate coordinate > cutData's coordinate from the original region's neighbors lists to the new one's
    string originalRegNeighborLists = reg.neighbors;
    string newRegNeighborLists = newReg.neighbors;
    int originalRegNeighborListsSize = GetStringArraySize(originalRegNeighborLists, oRealm);
    for (i = 0; i < originalRegNeighborListsSize; i++)
    {
        string regBorderTiles = GetStringArrayElement(originalRegNeighborLists, i, oRealm);
        string newRegBorderTiles = GetStringArrayElement(newRegNeighborLists, i, oRealm);
        int regBorderTilesSize = GetIntArraySize(regBorderTiles, oRealm);
        tempArray = "tempArray"; //temporary array yet again
        CreateIntArray(tempArray, 0, oRealm);
        int j;
        for (j = 0; j < regBorderTilesSize; j++)
        {
            int tileIndex = GetIntArrayElement(regBorderTiles, j, oRealm);
            struct Coordinates coords = ConvertTileIndexToCoordinates(tileIndex, nWidth);

            int coordinate = cutData.axis == AXIS_X ? coords.x : coords.y;
            if (coordinate <= cutData.coordinate)
                AddIntArrayElement(tempArray, tileIndex, FALSE, oRealm);
            else
                AddIntArrayElement(newRegBorderTiles, tileIndex, FALSE, oRealm);
        }
        CopyIntArray(tempArray, regBorderTiles, oRealm, oRealm);
        ClearIntArray(tempArray, oRealm);
    }
}

void SetRealmParameters(int nWidth, int nPlayers, string sRNGSeed, object oRealm)
{
    //New RNG
    int seed;
    if (sRNGSeed == "0")
    {
        seed = SetRandomSeed();
        sRNGSeed = IntToString(seed);
    }
    else
    {
        seed = SetRandomSeed(GetSeedFromString(sRNGSeed));
    }
    LogInfo("Started realm generation with random seed: " + IntToString(seed) + ", seed string: " + sRNGSeed);
    SetRealmSeed(oRealm, sRNGSeed);

    //Realm's name
    string realmScript = GetRealmScript(oRealm);
    string randomRealmName = GetRandomRealmName(realmScript, "default");
    SetRealmName(oRealm, randomRealmName);

    //Realm's size
    int size;
    switch (nWidth)
    {
        case 4:
            size = REALM_SIZE_TINY;
            break;
        case 8:
            size = REALM_SIZE_SMALL;
            break;
        case 12:
            size = REALM_SIZE_MEDIUM;
            break;
        case 16:
            size = REALM_SIZE_LARGE;
            break;
        default:
            LogWarning("Unexpected realm dimensions");
            break;
    }
    SetRealmSize(oRealm, size);
}

void GenerateRealmMap(int nWidth, int nHeight, int nStep, object oRealm)
{
    if (nStep != 1 && nStep != GetLocalInt(OBJECT_SELF, "PROC_NEXTMINORSTEP"))
    {
        DelayCommand(0.0f, GenerateRealmMap(nWidth, nHeight, nStep, oRealm));
        return;
    }
    _ManageSemaphor(nStep, 151);

    //common data and retrieving data stored in earlier steps
    int i, j;
    string tempArray = "Temp";
    string tempArrayNW = tempArray+"NW"; //arrays to store
    string tempArrayNE = tempArray+"NE";
    string tempArraySE = tempArray+"SE";
    string tempArraySW = tempArray+"SW";
    string leadingArray = "LeadingNodes";
    string regionsArray = "Regions";
    object oModule = GetModule();
    int regionsNum = GetLocalInt(oModule, "TEMP_REGNUM");
    int leadingTileNum = GetLocalInt(oModule, "TEMP_LEADNUM");

    if (nStep == 1) //the function needs to be broken into multiple steps to omit TMI
    {
        regionsNum = _GetNumberOfRegions(nWidth, nHeight);
        leadingTileNum = nWidth * nHeight / 8;
        _CreateRealmGrid(nWidth, nHeight, oRealm);

        LogInfo("Done generating grid...");

        //Create a list of leading nodes
        _CreateTemporaryTileIndexArraysForSelection(tempArrayNW, tempArrayNE, tempArraySE, tempArraySW, nWidth, nHeight, oRealm);
        int size = nWidth*nHeight;
        CreateRealmTileArray(leadingArray, leadingTileNum, oRealm);
        for (i = 0; i < leadingTileNum; i++) //for each spot in the leading node array, randomly pick a tile, remove it from further selection and give it a region ID
        {
            //choose the array to select a leading node from - this is done to distribute the nodes evenly throughout the map
            string selectedQuarter;
            switch (i % 4)
            {
                case 0:
                    selectedQuarter = tempArrayNW;
                    break;
                case 1:
                    selectedQuarter = tempArrayNE;
                    break;
                case 2:
                    selectedQuarter = tempArraySE;
                    break;
                case 3:
                    selectedQuarter = tempArraySW;
                    break;
            }

            int tempArrayIndex = RandomNext(GetIntArraySize(selectedQuarter, oRealm));
            int randIndex = GetIntArrayElement(selectedQuarter, tempArrayIndex, oRealm);
            struct Coordinates coords = ConvertTileIndexToCoordinates(randIndex, nWidth);
            struct RealmTile tile = _GetTileOnGrid(coords.x, coords.y, oRealm);
            DeleteIntArrayElement(selectedQuarter, tempArrayIndex, oRealm);

            if (i < regionsNum)
                tile.region = i;
            SetRealmTileArrayElement(leadingArray, i, tile, oRealm);
        }

        ClearIntArray(tempArrayNW, oRealm);
        ClearIntArray(tempArrayNE, oRealm);
        ClearIntArray(tempArraySE, oRealm);
        ClearIntArray(tempArraySW, oRealm);

        LogInfo("Done creating list of leading nodes...");

        //storing the results to be used in later steps
        object oModule = GetModule();
        SetLocalInt(oModule, "TEMP_REGNUM", regionsNum);
        SetLocalInt(oModule, "TEMP_LEADNUM", leadingTileNum);
    }

    else if (nStep == 2)
    {
        //Create the regions array
        CreateRegionArray(regionsArray, regionsNum, oRealm);
        for (i = 0; i < regionsNum; i++)
            _GenerateRegion(regionsArray, i, regionsNum, "default", oRealm);

        //First N leading nodes become first nodes of regions, other nodes will copy the closest region's ID in a random order (no need to reshuffle, because they already were added to the array in random order)
        CreateRealmTileArray(tempArray, regionsNum, oRealm);
        for (i = 0; i < regionsNum; i++)
        {
            struct RealmTile tile = GetRealmTileArrayElement(leadingArray, i, oRealm);
            SetRealmTileArrayElement(tempArray, i, tile, oRealm);
        }
        for (i = regionsNum; i < leadingTileNum; i++)
        {
            struct RealmTile tile = GetRealmTileArrayElement(leadingArray, i, oRealm);
            int region = _GetClosestRegion(tempArray, tile, oRealm);
            struct Region reg = GetRegionArrayElement(regionsArray, region, oRealm);
            tile.region = region;
            tile.biome = reg.type;
            SetRealmTileArrayElement(leadingArray, i, tile, oRealm);

            //Important algorithm modification: extra nodes forming a path between an original region node and the newly created region node should be added as this region's leading nodes
            //Otherwise, in some particular maps a region could be split in two by another region, breaking path generation
            int x = tile.x;
            int y = tile.y;
            struct RealmTile originalRegTile = GetRealmTileArrayElement(leadingArray, region, oRealm);
            AssertIntEqual(tile.region, originalRegTile.region, "(inc_world, GenerateRealmMap, originalRegTile.region)");
            while (x != originalRegTile.x || y != originalRegTile.y)
            {
                if (x != originalRegTile.x)
                {
                    if (x > originalRegTile.x)
                        x--;
                    else
                        x++;

                    if (x != originalRegTile.x || y != originalRegTile.y)
                    {
                        struct RealmTile pathTile = _GetTileOnGrid(x, y, oRealm);
                        pathTile.region = tile.region;
                        pathTile.biome = tile.biome;
                        AddRealmTileArrayElement(leadingArray, pathTile, FALSE, oRealm);
                    }
                }
                if (y != originalRegTile.y)
                {
                    if (y > originalRegTile.y)
                        y--;
                    else
                        y++;

                    if (x != originalRegTile.x || y != originalRegTile.y)
                    {
                        struct RealmTile pathTile = _GetTileOnGrid(x, y, oRealm);
                        pathTile.region = tile.region;
                        pathTile.biome = tile.biome;
                        AddRealmTileArrayElement(leadingArray, pathTile, FALSE, oRealm);
                    }
                }
            }

            if (!nBalancedRegions) //Testing showed it is generally better to leave nBalancedRegions as TRUE
                AddRealmTileArrayElement(tempArray, tile, FALSE, oRealm);
        }
        ClearRealmTileArray(tempArray, oRealm);

        LogInfo("Done assigning regions to leading nodes...");
    }
    else if (nStep <= 18)
    {
        //Iterate through the whole realm map and assign the closest region to each tile
        i = nStep-3; //it should go from 0 to 15 (at most)
        if (i < nHeight)
            for (j = 0; j < nWidth; j++)
            {
                struct RealmTile tile = _GetTileOnGrid(j, i, oRealm);
                //tile.region = _GetTileFieldOnGrid(j, i, "region", oRealm);
                //tile.index = _GetTileFieldOnGrid(j, i, "index", oRealm);
                //tile.x = _GetTileFieldOnGrid(j, i, "x", oRealm);
                //tile.y = _GetTileFieldOnGrid(j, i, "y", oRealm);
                int region = _GetClosestRegion(leadingArray, tile, oRealm);
                struct Region reg = GetRegionArrayElement(regionsArray, region, oRealm); //THIS is weird - it should be replacable with the two lines above
                //reg.type = GetRegionArrayElementIntField(regionsArray, region, "type", oRealm);
                //reg.tiles = GetRegionArrayElementStringField(regionsArray, region, "tiles", oRealm);
                tile.region = region;
                tile.biome = reg.type;
                //_SetTileOnGrid(tile, oRealm);
                _SetTileFieldOnGrid(j, i, "region", tile.region, oRealm);
                _SetStringTileFieldOnGrid(j, i, "biome", tile.biome, oRealm);
                AddIntArrayElement(reg.tiles, tile.index, FALSE, oRealm); //add the tile's index to the list of region's areas
            }
    }
    else if (nStep <= 38)
    {
        //Find the borders of the regions
        i = nStep-19; //i should go from 0 to 20 (potentially)
        if (i < regionsNum)
        {
            struct Region reg = GetRegionArrayElement(regionsArray, i, oRealm);
            _FindBorders(reg, nWidth, nHeight, oRealm);
        }
    }
    else if (nStep <= 58)
    {
        //Detect regions that are too small in size and make them a part of the smallest neighboring region
        i = nStep-39; //i should go from 0 to 20 (potentially)
        if (i < regionsNum)
        {
            struct Region reg = GetRegionArrayElement(regionsArray, i, oRealm);

            int size = GetIntArraySize(reg.tiles, oRealm);
            if (size >= nMinRegionSize)
                return;

            int smallestNeighborIndex = _GetSmallestNeighboringRegionIndex(reg, regionsArray, oRealm);

            if (smallestNeighborIndex == ARRAY_INDEX_INVALID)
                return;

            struct Region newReg = GetRegionArrayElement(regionsArray, smallestNeighborIndex, oRealm);
            _AbsorbRegion(reg, newReg, regionsNum, nWidth, nHeight, oRealm);
        }
    }
    else if (nStep <= 104)
    {
        //Splitting too big regions in roughly half
        i = nStep-59; //i should go from 0 to 46 (potentially, just to be safe... really, it won't hurt)
        if (i < regionsNum)
        {
            struct Region reg = GetRegionArrayElement(regionsArray, i, oRealm);
            string regTileArray = reg.tiles;
            while (TRUE) //we may need to split a region multiple times, until it's small enough
            {
                int regSize = GetIntArraySize(regTileArray, oRealm);
                LogInfo("Region: " + IntToString(reg.id) + ", size: " + IntToString(regSize));
                if (regSize <= nMaxRegionSize)
                    return;

                LogInfo("Oversized region with ID: " + IntToString(reg.id));
                struct RegionCutData cutData = _GetOptimalRegionCut(reg, nWidth, oRealm);

                if (cutData.axis == AXIS_NONE)
                    break;

                _SplitRegion(reg, cutData, regionsNum, nWidth, nHeight, oRealm);
                regionsNum++;
                SetLocalInt(oModule, "TEMP_REGNUM", regionsNum);
            }
        }
    }
    else if (nStep <= 150)
    {
        //Set blocked paths to contain paths generated later in the region
        i = nStep-105; //i should go from 0 to 46
        if (i < regionsNum)
        {
            struct Region reg = GetRegionArrayElement(regionsArray, i, oRealm);
            _SetBlockedPaths(reg, nWidth, nHeight, oRealm);
        }
    }
    else
    {
        LogInfo("Done determining neighbors...");

        DeleteLocalInt(oModule, "TEMP_REGNUM");
        DeleteLocalInt(oModule, "TEMP_LEADNUM");

        ClearRealmTileArray(tempArray, oRealm);
        ClearRealmTileArray(leadingArray, oRealm);
        LogInfo("Finished core realm generation");
    }
}

// ---------------------------------------------------------------------

//Functions for path generation

//Converts a directions flags integer to an array of directions
void _DirectionsToArray(int nDirections, string sName, int nUnlistedOnly, object oRealm)
{
    CreateIntArray(sName, 0, oRealm);

    if (nUnlistedOnly) //if this is true, we add to the array only those directions that do NOT have flags set
    {
        if (!(nDirections & PATH_NORTH))
            AddIntArrayElement(sName, PATH_NORTH, FALSE, oRealm);
        if (!(nDirections & PATH_EAST))
            AddIntArrayElement(sName, PATH_EAST, FALSE, oRealm);
        if (!(nDirections & PATH_SOUTH))
            AddIntArrayElement(sName, PATH_SOUTH, FALSE, oRealm);
        if (!(nDirections & PATH_WEST))
            AddIntArrayElement(sName, PATH_WEST, FALSE, oRealm);
        return;
    }

    if (nDirections & PATH_NORTH)
        AddIntArrayElement(sName, PATH_NORTH, FALSE, oRealm);
    if (nDirections & PATH_EAST)
        AddIntArrayElement(sName, PATH_EAST, FALSE, oRealm);
    if (nDirections & PATH_SOUTH)
        AddIntArrayElement(sName, PATH_SOUTH, FALSE, oRealm);
    if (nDirections & PATH_WEST)
        AddIntArrayElement(sName, PATH_WEST, FALSE, oRealm);
}

//Given a tile, this function returns an array of directions that can be selected from it
void _GenerateAllowedDirectionsArrayForTile(struct RealmTile tile, string dirArray, int nWidth, int nHeight, object oRealm)
{
    struct RealmTile neighbor;
    int unallowedDirections = tile.blocked;
    CreateIntArray(dirArray, 0, oRealm);

    if ((tile.blocked & PATH_NORTH) == 0)
    {
        AssertIntGreater(0, tile.y, "(inc_world, _GenerateAllowedDirectionsArrayForTile, tile.y)");
        //neighbor = _GetTileOnGrid(tile.x, tile.y-1, oRealm);
        neighbor.region = _GetTileFieldOnGrid(tile.x, tile.y-1, "region", oRealm);
        neighbor.set = _GetTileFieldOnGrid(tile.x, tile.y-1, "set", oRealm);
        AssertIntEqual(tile.region, neighbor.region, "(inc_world, _GenerateAllowedDirectionsArrayForTile, northNeighbor.region ["+IntToString(tile.x)+","+IntToString(tile.y)+"])");
        if (neighbor.set == TILE_CURRENT_PATH)
            unallowedDirections = unallowedDirections | PATH_NORTH;
    }
    if ((tile.blocked & PATH_EAST) == 0)
    {
        AssertIntLower(nWidth-1, tile.x, "(inc_world, _GenerateAllowedDirectionsArrayForTile, tile.x)");
        //neighbor = _GetTileOnGrid(tile.x+1, tile.y, oRealm);
        neighbor.region = _GetTileFieldOnGrid(tile.x+1, tile.y, "region", oRealm);
        neighbor.set = _GetTileFieldOnGrid(tile.x+1, tile.y, "set", oRealm);
        AssertIntEqual(tile.region, neighbor.region, "(inc_world, _GenerateAllowedDirectionsArrayForTile, eastNeighbor.region ["+IntToString(tile.x)+","+IntToString(tile.y)+"])");
        if (neighbor.set == TILE_CURRENT_PATH)
            unallowedDirections = unallowedDirections | PATH_EAST;
    }
    if ((tile.blocked & PATH_SOUTH) == 0)
    {
        AssertIntLower(nHeight-1, tile.y, "(inc_world, _GenerateAllowedDirectionsArrayForTile, tile.y)");
        //neighbor = _GetTileOnGrid(tile.x, tile.y+1, oRealm);
        neighbor.region = _GetTileFieldOnGrid(tile.x, tile.y+1, "region", oRealm);
        neighbor.set = _GetTileFieldOnGrid(tile.x, tile.y+1, "set", oRealm);
        AssertIntEqual(tile.region, neighbor.region, "(inc_world, _GenerateAllowedDirectionsArrayForTile, southNeighbor.region ["+IntToString(tile.x)+","+IntToString(tile.y)+"])");
        if (neighbor.set == TILE_CURRENT_PATH)
            unallowedDirections = unallowedDirections | PATH_SOUTH;
    }
    if ((tile.blocked & PATH_WEST) == 0)
    {
        AssertIntGreater(0, tile.x, "(inc_world, _GenerateAllowedDirectionsArrayForTile, tile.x)");
        //neighbor = _GetTileOnGrid(tile.x-1, tile.y, oRealm);
        neighbor.region = _GetTileFieldOnGrid(tile.x-1, tile.y, "region", oRealm);
        neighbor.set = _GetTileFieldOnGrid(tile.x-1, tile.y, "set", oRealm);
        AssertIntEqual(tile.region, neighbor.region, "(inc_world, _GenerateAllowedDirectionsArrayForTile, westNeighbor.region ["+IntToString(tile.x)+","+IntToString(tile.y)+"])");
        if (neighbor.set == TILE_CURRENT_PATH)
            unallowedDirections = unallowedDirections | PATH_WEST;
    }

    _DirectionsToArray(unallowedDirections, dirArray, TRUE, oRealm);
}

int _GetOppositeDirection(int nDirection)
{
    switch (nDirection)
    {
        case PATH_NORTH:
            return PATH_SOUTH;
        case PATH_SOUTH:
            return PATH_NORTH;
        case PATH_EAST:
            return PATH_WEST;
        case PATH_WEST:
            return PATH_EAST;
    }
    LogWarning(IntToString(nDirection) + " is an invalid direction constant (inc_world, _GetOppositeDirection, nDirection)");
    return 0;
}

void _GenerateSingleRegionPath(string unusedTiles, int nFirstPathInRegion, int nWidth, int nHeight, object oRealm)
{
    //Prepare necessary variables and arrays
    string dirArray = "dirArray";
    string pathTilesArray = "pathArray";
    struct RealmTile tile;
    struct Coordinates coords;
    int index;
    CreateIntArray(pathTilesArray, 0, oRealm); //Initialize a temporary array containing tiles in this path

    //Randomly select an area
    int unusedNum = GetIntArraySize(unusedTiles, oRealm);
    int indexPosition = RandomNext(unusedNum);
    index = GetIntArrayElement(unusedTiles, indexPosition, oRealm);
    coords = ConvertTileIndexToCoordinates(index, nWidth);
    //tile = _GetTileOnGrid(coords.x, coords.y, oRealm);
    tile.region = _GetTileFieldOnGrid(coords.x, coords.y, "region", oRealm);
    tile.blocked = _GetTileFieldOnGrid(coords.x, coords.y, "blocked", oRealm);
    tile.x = coords.x;
    tile.y = coords.y;
    tile.paths = _GetTileFieldOnGrid(coords.x, coords.y, "paths", oRealm);
    tile.index = _GetTileFieldOnGrid(coords.x, coords.y, "index", oRealm);
    DeleteIntArrayElement(unusedTiles, indexPosition, oRealm);

    //Add the new area to the path, marking it as being part of a current path in the process
    AddIntArrayElement(pathTilesArray, tile.index, FALSE, oRealm);
    tile.set = TILE_CURRENT_PATH;
    //_SetTileOnGrid(tile, oRealm);
    _SetTileFieldOnGrid(tile.x, tile.y, "set", tile.set, oRealm);

    while (TRUE)
    {
        //Get an array of directions to randomly select a direction from
        coords.x = tile.x;
        coords.y = tile.y;
        _GenerateAllowedDirectionsArrayForTile(tile, dirArray, nWidth, nHeight, oRealm);

        //First stop condition (no possible directions to follow)
        if (GetIntArraySize(dirArray, oRealm) == 0)
        {
            //End of path if it's the first path in the region
            if (nFirstPathInRegion)
            {
                int i;
                int pathSize = GetIntArraySize(pathTilesArray, oRealm);
                //We're done with this path, so every area in it should be marked as previous path
                for (i = 0; i < pathSize; i++)
                {
                    index = GetIntArrayElement(pathTilesArray, i, oRealm);
                    coords = ConvertTileIndexToCoordinates(index, nWidth);
                    //tile = _GetTileOnGrid(coords.x, coords.y, oRealm);
                    tile.region = _GetTileFieldOnGrid(coords.x, coords.y, "region", oRealm);
                    tile.blocked = _GetTileFieldOnGrid(coords.x, coords.y, "blocked", oRealm);
                    tile.x = coords.x;
                    tile.y = coords.y;
                    tile.paths = _GetTileFieldOnGrid(coords.x, coords.y, "paths", oRealm);
                    tile.index = _GetTileFieldOnGrid(coords.x, coords.y, "index", oRealm);
                    tile.set = TILE_PREV_PATH;
                    //_SetTileOnGrid(tile, oRealm);
                    _SetTileFieldOnGrid(tile.x, tile.y, "set", tile.set, oRealm);
                }

                ClearIntArray(dirArray, oRealm);
                ClearIntArray(pathTilesArray, oRealm);
                return;
            }

            //Otherwise iterate over current path in search of a tile to find another path from
            int i;
            int pathSize = GetIntArraySize(pathTilesArray, oRealm);
            for (i = 0; i < pathSize; i++)
            {
                index = GetIntArrayElement(pathTilesArray, i, oRealm);
                coords = ConvertTileIndexToCoordinates(index, nWidth);
                //tile = _GetTileOnGrid(coords.x, coords.y, oRealm);
                tile.region = _GetTileFieldOnGrid(coords.x, coords.y, "region", oRealm);
                tile.blocked = _GetTileFieldOnGrid(coords.x, coords.y, "blocked", oRealm);
                tile.x = coords.x;
                tile.y = coords.y;
                tile.paths = _GetTileFieldOnGrid(coords.x, coords.y, "paths", oRealm);
                tile.index = _GetTileFieldOnGrid(coords.x, coords.y, "index", oRealm);
                tile.set = _GetTileFieldOnGrid(coords.x, coords.y, "set", oRealm);
                _GenerateAllowedDirectionsArrayForTile(tile, dirArray, nWidth, nHeight, oRealm);

                if (GetIntArraySize(dirArray, oRealm) > 0)
                    break;

                if (i == pathSize-1)
                    LogWarning("Expected to find a tile with a non-empty array of allowed directions (inc_world, _GenerateSingleRegionPath, dirArray)");
            }
        }

        //Do the random selection of the direction (aka next tile in the path)
        int dirsNum = GetIntArraySize(dirArray, oRealm);
        int direction = GetIntArrayElement(dirArray, RandomNext(dirsNum), oRealm);

        //Add the pathway selected to the tile
        tile.paths = tile.paths | direction;
        //_SetTileOnGrid(tile, oRealm);
        _SetTileFieldOnGrid(tile.x, tile.y, "paths", tile.paths, oRealm);

        //Get the new tile and select opposite direction to be added to the new tile (connection to the previous one on the path)
        switch (direction)
        {
            case PATH_NORTH:
                //tile = _GetTileOnGrid(tile.x, tile.y-1, oRealm);
                tile.x = tile.x;
                tile.y = tile.y-1;
                tile.set = _GetTileFieldOnGrid(tile.x, tile.y, "set", oRealm);
                tile.region = _GetTileFieldOnGrid(tile.x, tile.y, "region", oRealm);
                tile.blocked = _GetTileFieldOnGrid(tile.x, tile.y, "blocked", oRealm);
                tile.paths = _GetTileFieldOnGrid(tile.x, tile.y, "paths", oRealm);
                tile.index = _GetTileFieldOnGrid(tile.x, tile.y, "index", oRealm);
                break;
            case PATH_EAST:
                //tile = _GetTileOnGrid(tile.x+1, tile.y, oRealm);
                tile.x = tile.x+1;
                tile.y = tile.y;
                tile.set = _GetTileFieldOnGrid(tile.x, tile.y, "set", oRealm);
                tile.region = _GetTileFieldOnGrid(tile.x, tile.y, "region", oRealm);
                tile.blocked = _GetTileFieldOnGrid(tile.x, tile.y, "blocked", oRealm);
                tile.paths = _GetTileFieldOnGrid(tile.x, tile.y, "paths", oRealm);
                tile.index = _GetTileFieldOnGrid(tile.x, tile.y, "index", oRealm);
                break;
            case PATH_SOUTH:
                //tile = _GetTileOnGrid(tile.x, tile.y+1, oRealm);
                tile.x = tile.x;
                tile.y = tile.y+1;
                tile.set = _GetTileFieldOnGrid(tile.x, tile.y, "set", oRealm);
                tile.region = _GetTileFieldOnGrid(tile.x, tile.y, "region", oRealm);
                tile.blocked = _GetTileFieldOnGrid(tile.x, tile.y, "blocked", oRealm);
                tile.paths = _GetTileFieldOnGrid(tile.x, tile.y, "paths", oRealm);
                tile.index = _GetTileFieldOnGrid(tile.x, tile.y, "index", oRealm);
                break;
            case PATH_WEST:
                //tile = _GetTileOnGrid(tile.x-1, tile.y, oRealm);
                tile.x = tile.x-1;
                tile.y = tile.y;
                tile.set = _GetTileFieldOnGrid(tile.x, tile.y, "set", oRealm);
                tile.region = _GetTileFieldOnGrid(tile.x, tile.y, "region", oRealm);
                tile.blocked = _GetTileFieldOnGrid(tile.x, tile.y, "blocked", oRealm);
                tile.paths = _GetTileFieldOnGrid(tile.x, tile.y, "paths", oRealm);
                tile.index = _GetTileFieldOnGrid(tile.x, tile.y, "index", oRealm);
                break;
        }
        direction = _GetOppositeDirection(direction);

        //Add the new area to the path, marking it as being part of a current path in the process and adding the pathway back to the previous tile
        AddIntArrayElement(pathTilesArray, tile.index, FALSE, oRealm);
        tile.paths = tile.paths | direction;
        if (tile.set != TILE_PREV_PATH)
            tile.set = TILE_CURRENT_PATH;
        //_SetTileOnGrid(tile, oRealm);
        _SetTileFieldOnGrid(tile.x, tile.y, "paths", tile.paths, oRealm);
        _SetTileFieldOnGrid(tile.x, tile.y, "set", tile.set, oRealm);

        //Second stop condition (joined another path)
        if (tile.set == TILE_PREV_PATH)
        {
            int i;
            int pathSize = GetIntArraySize(pathTilesArray, oRealm);
            //We're done with this path, so every area in it should be marked as previous path
            for (i = 0; i < pathSize; i++)
            {
                index = GetIntArrayElement(pathTilesArray, i, oRealm);
                coords = ConvertTileIndexToCoordinates(index, nWidth);
                //tile = _GetTileOnGrid(coords.x, coords.y, oRealm);
                tile.set = TILE_PREV_PATH;
                //_SetTileOnGrid(tile, oRealm);
                _SetTileFieldOnGrid(coords.x, coords.y, "set", tile.set, oRealm);
            }

            ClearIntArray(dirArray, oRealm);
            ClearIntArray(pathTilesArray, oRealm);
            return;
        }

        //Remove the area newly added to the path from the list of unused areas available for selection as a start of a new path
        index = GetIntArrayIndexByValue(unusedTiles, tile.index, 1, oRealm);
        AssertIntNotEqual(ARRAY_INDEX_INVALID, index, "(inc_world, _GenerateSingleRegionPath, index)");
        DeleteIntArrayElement(unusedTiles, index, oRealm);
    }
}

int _GetIsNumberPowerOfTwo(int nNumber) //useful to check if an area has only a single exit
{
    int x = nNumber;
    return (x != 0) && ((x & (x - 1)) == 0);
}

void _GenerateSingleRegionExtraPaths(struct Region reg, int nWidth, int nHeight, object oRealm)
{
    LogInfo("Generating extra paths for region %n", reg.id);
    string tilesArray = reg.tiles;
    int numOfTiles = GetIntArraySize(tilesArray, oRealm);
    LogInfo("%n tiles in the region", numOfTiles);

    int i;
    for (i = 0; i < numOfTiles/4; i++)
    {
        int randomIndex = RandomNext(numOfTiles);
        int tileIndex = GetIntArrayElement(tilesArray, randomIndex, oRealm);
        struct Coordinates coords = ConvertTileIndexToCoordinates(tileIndex, nWidth);
        struct RealmTile tile = _GetTileOnGrid(coords.x, coords.y, oRealm);
        LogInfo("Finding extra connections for (%n,%n), paths: %n", coords.x, coords.y, tile.paths);

        //if the paths var is a power of 2, then it means the area has only a single exit - we don't want to ruin single-exit areas
        if (_GetIsNumberPowerOfTwo(tile.paths))
        {
            LogInfo("Tile is a single-exit area, no extra connections added");
            continue;
        }

        if (((tile.paths & PATH_NORTH) == FALSE) && coords.y > 0)
        {
            LogInfo("Checking out northern neighbor");
            int neighborRegion = _GetTileFieldOnGrid(coords.x, coords.y-1, "region", oRealm);
            if (neighborRegion == reg.id)
            {
                int neighborPaths = _GetTileFieldOnGrid(coords.x, coords.y-1, "paths", oRealm);
                if (!_GetIsNumberPowerOfTwo(neighborPaths))
                {
                    _SetTileFieldOnGrid(coords.x, coords.y-1, "paths", neighborPaths | PATH_SOUTH, oRealm);
                    _SetTileFieldOnGrid(coords.x, coords.y, "paths", tile.paths | PATH_NORTH, oRealm);
                    LogInfo("Connecting tiles (%n,%n) and (%n,%n)", coords.x, coords.y, coords.x, coords.y-1);
                    continue;
                }
            }
        }
        if (((tile.paths & PATH_SOUTH) == FALSE) && coords.y < nHeight-1)
        {
            LogInfo("Checking out southern neighbor");
            int neighborRegion = _GetTileFieldOnGrid(coords.x, coords.y+1, "region", oRealm);
            if (neighborRegion == reg.id)
            {
                int neighborPaths = _GetTileFieldOnGrid(coords.x, coords.y+1, "paths", oRealm);
                if (!_GetIsNumberPowerOfTwo(neighborPaths))
                {
                    _SetTileFieldOnGrid(coords.x, coords.y+1, "paths", neighborPaths | PATH_NORTH, oRealm);
                    _SetTileFieldOnGrid(coords.x, coords.y, "paths", tile.paths | PATH_SOUTH, oRealm);
                    LogInfo("Connecting tiles (%n,%n) and (%n,%n)", coords.x, coords.y, coords.x, coords.y+1);
                    continue;
                }
            }
        }
        if (((tile.paths & PATH_WEST) == FALSE) && coords.x > 0)
        {
            LogInfo("Checking out western neighbor");
            int neighborRegion = _GetTileFieldOnGrid(coords.x-1, coords.y, "region", oRealm);
            if (neighborRegion == reg.id)
            {
                int neighborPaths = _GetTileFieldOnGrid(coords.x-1, coords.y, "paths", oRealm);
                if (!_GetIsNumberPowerOfTwo(neighborPaths))
                {
                    _SetTileFieldOnGrid(coords.x-1, coords.y, "paths", neighborPaths | PATH_EAST, oRealm);
                    _SetTileFieldOnGrid(coords.x, coords.y, "paths", tile.paths | PATH_WEST, oRealm);
                    LogInfo("Connecting tiles (%n,%n) and (%n,%n)", coords.x, coords.y, coords.x-1, coords.y);
                    continue;
                }
            }
        }
        if (((tile.paths & PATH_EAST) == FALSE) && coords.x < nWidth-1)
        {
            LogInfo("Checking out eastern neighbor");
            int neighborRegion = _GetTileFieldOnGrid(coords.x+1, coords.y, "region", oRealm);
            if (neighborRegion == reg.id)
            {
                int neighborPaths = _GetTileFieldOnGrid(coords.x+1, coords.y, "paths", oRealm);
                if (!_GetIsNumberPowerOfTwo(neighborPaths))
                {
                    _SetTileFieldOnGrid(coords.x+1, coords.y, "paths", neighborPaths | PATH_WEST, oRealm);
                    _SetTileFieldOnGrid(coords.x, coords.y, "paths", tile.paths | PATH_EAST, oRealm);
                    LogInfo("Connecting tiles (%n,%n) and (%n,%n)", coords.x, coords.y, coords.x+1, coords.y);
                    continue;
                }
            }
        }
    }
}

void _GenerateRegionPaths(struct Region reg, int nWidth, int nHeight, object oRealm)
{
    //Create a list of references (indexes) to areas in the region to represent areas yet unused in path generation
    string unusedTiles = "unusedTilesArray";
    int tilesNum = GetIntArraySize(reg.tiles, oRealm);
    LogInfo("Number of tiles in region " + IntToString(reg.id) + ": " + IntToString(tilesNum));
    SetLocalInt(oRealm, "TEMP_TILESINREG", GetLocalInt(oRealm, "TEMP_TILESINREG") + tilesNum);
    if (tilesNum == 0)
        return;
    CreateIntArray(unusedTiles, tilesNum, oRealm);
    int i;
    for (i = 0; i < tilesNum; i++)
    {
        int tileIndex = GetIntArrayElement(reg.tiles, i, oRealm);
        SetIntArrayElement(unusedTiles, i, tileIndex, oRealm);
    }

    _GenerateSingleRegionPath(unusedTiles, TRUE, nWidth, nHeight, oRealm);
    while (GetIntArraySize(unusedTiles, oRealm) > 0)
        _GenerateSingleRegionPath(unusedTiles, FALSE, nWidth, nHeight, oRealm);

    _GenerateSingleRegionExtraPaths(reg, nWidth, nHeight, oRealm);
    LogInfo("Finished path generation in region");
}

void _GenerateRegionsConnection(struct Region reg1, struct Region reg2, int nWidth, int nHeight, object oRealm)
{
    string borderTiles = GetStringArrayElement(reg1.neighbors, reg2.id, oRealm);
    int borderTilesNum = GetIntArraySize(borderTiles, oRealm);

    if (borderTilesNum == 0)
        return;

    //Select a tile from reg1 bordering with reg2
    int arrayIndex = RandomNext(borderTilesNum);
    int selectedTileIndex = GetIntArrayElement(borderTiles, arrayIndex, oRealm);
    struct Coordinates coords = ConvertTileIndexToCoordinates(selectedTileIndex, nWidth);
    struct RealmTile tile = _GetTileOnGrid(coords.x, coords.y, oRealm);

    //Find the neighboring reg2 tile
    struct RealmTile neighbor;
    int direction = 0;
    do
    {
        if (coords.y > 0)
        {
            neighbor = _GetTileOnGrid(coords.x, coords.y-1, oRealm);
            if (reg2.id == neighbor.region)
            {
                direction = PATH_NORTH;
                break;
            }
        }
        if (coords.x < nWidth-1)
        {
            neighbor = _GetTileOnGrid(coords.x+1, coords.y, oRealm);
            if (reg2.id == neighbor.region)
            {
                direction = PATH_EAST;
                break;
            }
        }
        if (coords.y < nHeight-1)
        {
            neighbor = _GetTileOnGrid(coords.x, coords.y+1, oRealm);
            if (reg2.id == neighbor.region)
            {
                direction = PATH_SOUTH;
                break;
            }
        }
        if (coords.x > 0)
        {
            neighbor = _GetTileOnGrid(coords.x-1, coords.y, oRealm);
            if (reg2.id == neighbor.region)
            {
                direction = PATH_WEST;
                break;
            }
        }
    }
    while (FALSE);
    AssertIntNotEqual(0, direction, "(inc_world, _GenerateRegionsConnection, direction)");

    //Delete everything on the lists of neighboring tiles (both ways) and add only that single tile
    CreateIntArray(borderTiles, 0, oRealm);
    AddIntArrayElement(borderTiles, selectedTileIndex, FALSE, oRealm);
    string neighborBorderTiles = GetStringArrayElement(reg2.neighbors, reg1.id, oRealm);
    CreateIntArray(neighborBorderTiles, 0, oRealm);
    AddIntArrayElement(neighborBorderTiles, neighbor.index, FALSE, oRealm);

    //Add the path
    tile.paths = tile.paths | direction;
    direction = _GetOppositeDirection(direction);
    neighbor.paths = neighbor.paths | direction;
    _SetTileOnGrid(tile, oRealm);
    _SetTileOnGrid(neighbor, oRealm);
}

void _SetBossAreaTile(object oRealm, int nLastRegionIndex)
{
    string row = "Grid_MapRow0";
    string regionsArray = "Regions";
    string singleExitTilesArray = "SingleExitTiles";
    struct Region selectedRegion = GetRegionArrayElement(regionsArray, nLastRegionIndex, oRealm);
    int width = GetRealmTileArraySize(row, oRealm);
    int startingIndex = GetLocalInt(oRealm, "StartingIndex");
    LogInfo("Selecting boss area for last region (region index: %n)", nLastRegionIndex);

    int tilesNum = GetIntArraySize(selectedRegion.tiles, oRealm);
    CreateIntArray(singleExitTilesArray);
    int i;
    struct Coordinates coords;
    for (i = 0; i < tilesNum; i++)
    {
        int index = GetIntArrayElement(selectedRegion.tiles, i, oRealm);
        coords = ConvertTileIndexToCoordinates(index, width);
        int paths = _GetTileFieldOnGrid(coords.x, coords.y, "paths", oRealm);
        LogInfo("Considering boss area candidate tile index %n, paths: %n", index, paths);
        if (_GetIsNumberPowerOfTwo(paths) && index != startingIndex)
        {
            LogInfo("Adding boss area candidate tile index %n", index);
            AddIntArrayElement(singleExitTilesArray, index);
        }
    }
    int singleExitsNum = GetIntArraySize(singleExitTilesArray);
    if (singleExitsNum == 0)
        LogFatal("No single exit areas in the final region remaining to generate a boss area (inc_generation, _SetBossAreaTile)");
    int bossAreaIndex = GetIntArrayElement(singleExitTilesArray, RandomNext(singleExitsNum));
    LogInfo("Selected boss area tile index: %n", bossAreaIndex);
    SetLocalInt(oRealm, "BossAreaIndex", bossAreaIndex);
    coords = ConvertTileIndexToCoordinates(bossAreaIndex, width);
    _SetTileFieldOnGrid(coords.x, coords.y, "type", TILE_TYPE_BOSS, oRealm);
    ClearIntArray(singleExitTilesArray);
}

void _GenerateInterRegionConnections(object oRealm)
{
    //Declare names of arrays
    string regionsArray = "Regions";
    string dfsStackArray = "DfsStack";
    string neighborIdsArray = "NeighborIds";
    string visitedRegionsArray = "VisitedRegions";
    string nonEmptyRegionsArray = "NonEmptyRegs";

    string row = "Grid_MapRow0";
    int height = GetStringArraySize("Grid", oRealm);
    int width = GetRealmTileArraySize(row, oRealm);
    int regionOrder = 1;

    //Create a stack of region indexes for DFS and the array for visited regions
    CreateIntArray(dfsStackArray);
    CreateIntArray(visitedRegionsArray, 45);

    //Randomly select a non-empty starting region and push to stack
    int regNum = GetRegionArraySize(regionsArray, oRealm);
    int i;
    CreateIntArray(nonEmptyRegionsArray);
    for (i = 0; i < regNum; i++)
    {
        string regTilesArray = GetRegionArrayElementStringField(regionsArray, i, "tiles", oRealm);
        if (GetIntArraySize(regTilesArray, oRealm) > 0)
            AddIntArrayElement(nonEmptyRegionsArray, i);
    }
    regNum = GetIntArraySize(nonEmptyRegionsArray);
    LogInfo("Number of non-empty regions: %n", regNum);
    int selectedRegionIndex = GetIntArrayElement(nonEmptyRegionsArray, RandomNext(regNum));
    AddIntArrayElement(dfsStackArray, selectedRegionIndex);
    LogInfo("Added %n to dfsStack", selectedRegionIndex);
    LogInfo("Starting in region %n", selectedRegionIndex);

    while (TRUE)
    {
        LogInfo("Selected region %n", selectedRegionIndex);
        struct Region selectedRegion = GetRegionArrayElement(regionsArray, selectedRegionIndex, oRealm);

        //Choose starting realm tile while we're at it
        if (regionOrder == 1)
        {
            int tilesNum = GetIntArraySize(selectedRegion.tiles, oRealm);
            int entranceIndex = GetIntArrayElement(selectedRegion.tiles, RandomNext(tilesNum), oRealm);
            SetLocalInt(oRealm, "StartingIndex", entranceIndex);
        }

        if (GetRegionArrayElementIntField(regionsArray, selectedRegionIndex, "order", oRealm) == 0)
        {
            LogInfo("Setting order of region %n to %n", selectedRegionIndex, regionOrder);
            //like the starting tile, choose the boss tile, too
            if (regionOrder == regNum)
            {
                _SetBossAreaTile(oRealm, selectedRegionIndex);
            }
            SetRegionArrayElementIntField(regionsArray, selectedRegionIndex, "order", regionOrder++, oRealm);
        }

        //Mark as visited
        SetIntArrayElement(visitedRegionsArray, selectedRegionIndex, TRUE);

        //Select a random unvisited neighbor
        string neighborsArray = GetRegionArrayElementStringField(regionsArray, selectedRegionIndex, "neighbors", oRealm);
        CreateIntArray(neighborIdsArray);
        int neighborIdsSize = 0;
        int neighborId;
        for (neighborId = 0; neighborId < GetStringArraySize(neighborsArray, oRealm); neighborId++)
        {
            if (GetIntArrayElement(visitedRegionsArray, neighborId) == TRUE)
                continue;
            string neighboringTilesArray = GetStringArrayElement(neighborsArray, neighborId, oRealm);
            if (GetIntArraySize(neighboringTilesArray, oRealm) == 0)
                continue;
            AddIntArrayElement(neighborIdsArray, neighborId);
            LogInfo("%n is an unvisited neighbor of %n", neighborId, selectedRegionIndex);
            neighborIdsSize++;
        }

        if (neighborIdsSize == 0)
        {
            LogInfo("%n has no unvisited neighbors", selectedRegionIndex);
            //No available neighbors - we must go back and resume searching there
            int dfsSize = GetIntArraySize(dfsStackArray);
            //DFS is complete, wrap everything up and finish
            if (dfsSize == 1)
            {
                ClearIntArray(dfsStackArray);
                ClearIntArray(visitedRegionsArray);
                ClearIntArray(neighborIdsArray);
                ClearIntArray(nonEmptyRegionsArray);
                LogInfo("DFS stack empty, inter-region connections generation finished");
                return;
            }
            int removedIndex = GetIntArrayElement(dfsStackArray, dfsSize-1);
            LogInfo("Removed %n from dfsStack", removedIndex);
            DeleteIntArrayElement(dfsStackArray, dfsSize-1);
            selectedRegionIndex = GetIntArrayElement(dfsStackArray, dfsSize-2);
        }
        else
        {
            int neighborsArrayIndex = RandomNext(neighborIdsSize);
            int selectedNeighborIndex = GetIntArrayElement(neighborIdsArray, neighborsArrayIndex);
            LogInfo("Generating connection between regions %n and %n", selectedRegionIndex, selectedNeighborIndex);
            struct Region selectedNeighbor = GetRegionArrayElement(regionsArray, selectedNeighborIndex, oRealm);

            //Generate connection
            _GenerateRegionsConnection(selectedRegion, selectedNeighbor, width, height, oRealm);

            //Select the neighbor as the next region and push it to stack
            selectedRegionIndex = selectedNeighborIndex;
            AddIntArrayElement(dfsStackArray, selectedRegionIndex);
            LogInfo("Added %n to dfsStack", selectedRegionIndex);
        }
    }
}

void GeneratePaths(int nStep, object oRealm)
{
    if (nStep != 1 && nStep != GetLocalInt(OBJECT_SELF, "PROC_NEXTMINORSTEP"))
    {
        DelayCommand(0.0f, GeneratePaths(nStep, oRealm));
        return;
    }
    _ManageSemaphor(nStep, 46);

    //Get width of the map
    string regionsArray = "Regions";
    string row = "Grid_MapRow0";
    int height = GetStringArraySize("Grid", oRealm);
    int width = GetRealmTileArraySize(row, oRealm);

    int regNum = GetRegionArraySize(regionsArray, oRealm);
    int i = nStep-1; //it should go from 0 to 45

    if (i == 0)
        SetLocalInt(oRealm, "TEMP_TILESINREG", 0);

    if (i < regNum)
        _GenerateRegionPaths(GetRegionArrayElement(regionsArray, i, oRealm), width, height, oRealm);

    if (i == regNum)
    {
        //Generation of region connections
        /*int j;
        for (j = 0; j < regNum; j++)
        {
            int k;
            for (k = j+1; k < regNum; k++)
            {
                struct Region reg1 = GetRegionArrayElement(regionsArray, j, oRealm);
                struct Region reg2 = GetRegionArrayElement(regionsArray, k, oRealm);
                _GenerateRegionsConnection(reg1, reg2, width, height, oRealm);
            }
        }*/

        LogInfo("Number of regions in array: %n", regNum);
        _GenerateInterRegionConnections(oRealm);

        AssertIntEqual(width*height, GetLocalInt(oRealm, "TEMP_TILESINREG"), "(inc_world, GeneratePaths, width*height)");
        DeleteLocalInt(oRealm, "TEMP_TILESINREG");
        LogInfo("Finished generating paths for all regions");
    }
}

// ---------------------------------------------------------------------

//Functions for assigning towns and other special purposes to tiles

int _GetNumberOfTileExits(int nDirections)
{
    int numOfDirs = 0;
    if (nDirections & PATH_NORTH)
        numOfDirs++;
    if (nDirections & PATH_EAST)
        numOfDirs++;
    if (nDirections & PATH_SOUTH)
        numOfDirs++;
    if (nDirections & PATH_WEST)
        numOfDirs++;

    return numOfDirs;
}

//Returns a minimum number of special tiles to have in the region based on its size (minimum, because there will be at least as many special tiles as dead ends in the region)
int _DetermineMinimumNumberOfSpecialTiles(int nRegionSize)
{
    int specialTilesNum = nRegionSize / nSpecialTileRarity;
    if (specialTilesNum > 0)
        return specialTilesNum;
    return 1;
}

int _DetermineNumberOfSettlements(int nRegionSize)
{
    return 1;

    /*int settlementTilesNum = nRegionSize / nSettlementRarity;
    if (settlementTilesNum > 0)
        return settlementTilesNum;
    return 1;*/
}

//Helper function for deleting a neighboring tile in one array and adding it to another (if it existed in sourceArray)
void _MoveTileIndexToForbidden(string sSourceArray, string sDestinationArray, int nTileIndex, object oRealm)
{
    int tilePosition = GetIntArrayIndexByValue(sSourceArray, nTileIndex, 1, oRealm);
    if (tilePosition != ARRAY_INDEX_INVALID)
    {
        DeleteIntArrayElement(sSourceArray, tilePosition, oRealm);
        AddIntArrayElement(sDestinationArray, nTileIndex, FALSE, oRealm);
    }
}

//Move all neighbors of a given tile from sSourceArray to sDestinationArray
void _MoveNeighborsToForbidden(string sSourceArray, string sDestinationArray, int nTileIndex, int nWidth, object oRealm)
{
    struct Coordinates coords = ConvertTileIndexToCoordinates(nTileIndex, nWidth);
    int paths = _GetTileFieldOnGrid(coords.x, coords.y, "paths", oRealm);
    if (paths & PATH_NORTH)
    {
        int neighborIndex = ConvertTileCoordinatesToIndex(coords.x, coords.y-1, nWidth);
        _MoveTileIndexToForbidden(sSourceArray, sDestinationArray, neighborIndex, oRealm);
    }
    if (paths & PATH_EAST)
    {
        int neighborIndex = ConvertTileCoordinatesToIndex(coords.x+1, coords.y, nWidth);
        _MoveTileIndexToForbidden(sSourceArray, sDestinationArray, neighborIndex, oRealm);
    }
    if (paths & PATH_SOUTH)
    {
        int neighborIndex = ConvertTileCoordinatesToIndex(coords.x, coords.y+1, nWidth);
        _MoveTileIndexToForbidden(sSourceArray, sDestinationArray, neighborIndex, oRealm);
    }
    if (paths & PATH_WEST)
    {
        int neighborIndex = ConvertTileCoordinatesToIndex(coords.x-1, coords.y, nWidth);
        _MoveTileIndexToForbidden(sSourceArray, sDestinationArray, neighborIndex, oRealm);
    }
}

int _GetShortestPathNextTileIndex(int nSourceIndex, int nDestinationIndex, object oRealm)
{
    if (nSourceIndex == nDestinationIndex)
        return nSourceIndex;

    int height = GetStringArraySize("Grid", oRealm);
    int width = GetRealmTileArraySize("Grid_MapRow0", oRealm);
    struct Coordinates sourceCoords = ConvertTileIndexToCoordinates(nSourceIndex, width);
    int targetIndex = nDestinationIndex;
    int sourcePaths = _GetTileFieldOnGrid(sourceCoords.x, sourceCoords.y, "paths", oRealm);

    int i;
    int shortestDistance = 99999;
    int finalX, finalY;
    for (i = 0; i < 4; i++)
    {
        int x = sourceCoords.x;
        int y = sourceCoords.y;
        int pathDirection;
        switch (i)
        {
            case 0:
                pathDirection = PATH_NORTH;
                y--;
                break;
            case 1:
                pathDirection = PATH_EAST;
                x++;
                break;
            case 2:
                pathDirection = PATH_SOUTH;
                y++;
                break;
            case 3:
                pathDirection = PATH_WEST;
                x--;
                break;
        }

        if ((sourcePaths & pathDirection) == FALSE)
            continue;

        if (x < 0 || x >= width || y < 0 || y >= height)
            continue;

        string distancesArray = _GetStringTileFieldOnGrid(x, y, "distances", oRealm);
        int distance = GetIntArrayElement(distancesArray, targetIndex, oRealm);
        if (distance < shortestDistance)
        {
            shortestDistance = distance;
            finalX = x;
            finalY = y;
        }
    }
    return ConvertTileCoordinatesToIndex(finalX, finalY, width);
}

void _GenerateSpecialTilesInRegion(struct Region reg, int nWidth, object oRealm)
{
    //Get arrays and size of region's tile array
    string tileArray = reg.tiles;
    string regionsArray = "Regions";
    string specialTilesArray = "specialTiles";
    string otherTilesArray = "otherTiles";
    string forbiddenTilesArray = "forbiddenTiles";
    string potentialTownTilesArray = "potentialTownTiles";
    CreateIntArray(specialTilesArray, 0, oRealm);
    CreateIntArray(otherTilesArray, 0, oRealm);
    CreateIntArray(forbiddenTilesArray, 0, oRealm);
    CreateIntArray(potentialTownTilesArray, 0, oRealm);
    int arraySize = GetIntArraySize(tileArray, oRealm);

    //Get the distances array for the region's entrance (first) area in order to check proximity of other areas to the entrance (we can choose a random area if it's the first region)
    int i;
    int entranceIndex;
    string entranceDistances;
    int realmStartingTileIndex = GetLocalInt(oRealm, "StartingIndex");
    int realmBossTileIndex = GetLocalInt(oRealm, "BossAreaIndex");
    LogInfo("Realm starting tile: %n, realm boss tile: %n", realmStartingTileIndex, realmBossTileIndex);
    int shortestDistanceToStartingTile;
    if (reg.order == 1)
    {
        entranceIndex = realmStartingTileIndex;
        struct Coordinates coords = ConvertTileIndexToCoordinates(entranceIndex, nWidth);
        entranceDistances = _GetTileDistancesFieldOnGrid(coords.x, coords.y, oRealm);
    }
    else
    {
        shortestDistanceToStartingTile = 256;
        for (i = 0; i < arraySize; i++)
        {
            int index = GetIntArrayElement(tileArray, i, oRealm);
            struct Coordinates coords = ConvertTileIndexToCoordinates(index, nWidth);
            string distances = _GetTileDistancesFieldOnGrid(coords.x, coords.y, oRealm);
            int distanceToStartingTile = GetIntArrayElement(distances, realmStartingTileIndex, oRealm);
            if (realmStartingTileIndex != index)
                AssertIntNotEqual(0, distanceToStartingTile, "Distance to starting tile not found");
            if (distanceToStartingTile < shortestDistanceToStartingTile)
            {
                shortestDistanceToStartingTile = distanceToStartingTile;
                entranceDistances = distances;
                entranceIndex = index;
            }
        }
    }
    struct Coordinates entranceCoords = ConvertTileIndexToCoordinates(entranceIndex, nWidth);
    struct Coordinates startCoords = ConvertTileIndexToCoordinates(realmStartingTileIndex, nWidth);
    LogInfo("Region entrance at (%n,%n), distance to realm starting tile at (%n,%n) is: " + IntToString(shortestDistanceToStartingTile), entranceCoords.x, entranceCoords.y, startCoords.x, startCoords.y);

    //Divide tiles into two categories (arrays), based on whether they have a single exit or not (and take note of areas close to the region's entrance to choose town area from)
    for (i = 0; i < arraySize; i++)
    {
        int index = GetIntArrayElement(tileArray, i, oRealm);
        struct Coordinates coords = ConvertTileIndexToCoordinates(index, nWidth);
        int paths = _GetTileFieldOnGrid(coords.x, coords.y, "paths", oRealm);
        int pathsNum = _GetNumberOfTileExits(paths);

        int distanceToEntrance = GetIntArrayElement(entranceDistances, index, oRealm);
        if (index != realmBossTileIndex && distanceToEntrance < nMaxSettlementDistance && (!nNoTownsOnBorders || distanceToEntrance > 0))
        {
            AddIntArrayElement(potentialTownTilesArray, index, FALSE, oRealm);
            LogInfo("Added tile %n to potential town tiles", index);
        }

        if (pathsNum == 1 && index != realmBossTileIndex)
        {
            AddIntArrayElement(specialTilesArray, index, FALSE, oRealm);
        }
        else if (index != realmBossTileIndex)
            AddIntArrayElement(otherTilesArray, index, FALSE, oRealm);
    }

    //Calculate the number of extra special tiles (the ones generated in other tiles than dead ends)
    int extraSpecialTilesNum = _DetermineMinimumNumberOfSpecialTiles(arraySize);
    extraSpecialTilesNum += 1; //settlement
    int deadEndsNum = GetIntArraySize(specialTilesArray, oRealm);
    extraSpecialTilesNum -= deadEndsNum;
    if (extraSpecialTilesNum < 0)
        extraSpecialTilesNum = 0;

    //Add neighbors of dead ends to the forbidden array and remove them from otherTilesArray
    for (i = 0; i < deadEndsNum; i++)
    {
        int tileIndex = GetIntArrayElement(specialTilesArray, i, oRealm);
        _MoveNeighborsToForbidden(otherTilesArray, forbiddenTilesArray, tileIndex, nWidth, oRealm);
    }

    //Choose a tile sufficiently close to the region's entrance to be the town area
    int potentialTownTilesArraySize = GetIntArraySize(potentialTownTilesArray, oRealm);
    int townTileIndex = GetIntArrayElement(potentialTownTilesArray, RandomNext(potentialTownTilesArraySize), oRealm);
    LogInfo("Region's town tile: %n", townTileIndex);
    _MoveNeighborsToForbidden(otherTilesArray, forbiddenTilesArray, townTileIndex, nWidth, oRealm);
    struct Coordinates coords = ConvertTileIndexToCoordinates(townTileIndex, nWidth);
    int tileType = reg.order == 1 ? TILE_TYPE_START : TILE_TYPE_SETTLEMENT;
    _SetTileFieldOnGrid(coords.x, coords.y, "type", tileType, oRealm);
    LogInfo("Region: %n, Order: %n, Town: (%n,%n)", reg.id, reg.order, coords.x, coords.y);

    //Take note of tiles en route to town
    int sourceTile = entranceIndex;
    int destTile = townTileIndex;
    do
    {
        struct Coordinates sourceCoords = ConvertTileIndexToCoordinates(sourceTile, nWidth);
        _SetTileFieldOnGrid(sourceCoords.x, sourceCoords.y, "roadtotown", TRUE, oRealm);
        sourceTile = _GetShortestPathNextTileIndex(sourceTile, destTile, oRealm);
    }
    while (sourceTile != destTile);


    //Randomly choose extra tiles to become special tiles
    int usingForbidden = FALSE;
    for (i = 0; i < extraSpecialTilesNum; i++)
    {
        int otherTilesArraySize = GetIntArraySize(otherTilesArray, oRealm);

        //If size zero is reached, it means we need to start using the forbiddenArray, so substitute it
        if (otherTilesArraySize == 0)
        {
            LogInfo("Usable tiles array empty, starting to use forbidden array");
            ClearIntArray(otherTilesArray, oRealm);
            otherTilesArray = forbiddenTilesArray;
            otherTilesArraySize = GetIntArraySize(otherTilesArray, oRealm);
            usingForbidden = TRUE;
        }
        int arrayIndex = RandomNext(otherTilesArraySize);
        int tileIndex = GetIntArrayElement(otherTilesArray, arrayIndex, oRealm);
        AddIntArrayElement(specialTilesArray, tileIndex, FALSE, oRealm);
        DeleteIntArrayElement(otherTilesArray, arrayIndex, oRealm);

        //Add neighbors to forbidden list and remove them from this one
        if (usingForbidden == FALSE)
            _MoveNeighborsToForbidden(otherTilesArray, forbiddenTilesArray, tileIndex, nWidth, oRealm);
    }

    //Mark the rest of selected tiles as special
    int specialTilesArraySize = GetIntArraySize(specialTilesArray, oRealm);
    for (i = 0; i < specialTilesArraySize; i++)
    {
        int tileIndex = GetIntArrayElement(specialTilesArray, i, oRealm);
        if (tileIndex == townTileIndex)
            continue;
        struct Coordinates coords = ConvertTileIndexToCoordinates(tileIndex, nWidth);
        _SetTileFieldOnGrid(coords.x, coords.y, "type", TILE_TYPE_SPECIAL, oRealm);
    }

    ClearIntArray(otherTilesArray, oRealm);
    ClearIntArray(forbiddenTilesArray, oRealm);
    ClearIntArray(potentialTownTilesArray, oRealm);
    ClearIntArray(specialTilesArray, oRealm);
}

void GenerateSpecialTiles(int nStep, object oRealm)
{
    if (nStep != 1 && nStep != GetLocalInt(OBJECT_SELF, "PROC_NEXTMINORSTEP"))
    {
        DelayCommand(0.0f, GenerateSpecialTiles(nStep, oRealm));
        return;
    }
    _ManageSemaphor(nStep, 46);

    //Get width of the map
    string regionsArray = "Regions";
    string row = "Grid_MapRow0";
    int width = GetRealmTileArraySize(row, oRealm);

    int regNum = GetRegionArraySize(regionsArray, oRealm);
    int i = nStep-1; //it should go from 0 to 45
    if (i == 0)
        SetLocalInt(oRealm, "TEMP_STARTREG", TRUE);
    int startingRegion = GetLocalInt(oRealm, "TEMP_STARTREG");

    if (i < regNum)
    {
        struct Region reg = GetRegionArrayElement(regionsArray, i, oRealm);
        int regSize = GetIntArraySize(reg.tiles, oRealm);
        if (regSize > 0)
        {
            _GenerateSpecialTilesInRegion(reg, width, oRealm);
            SetLocalInt(oRealm, "TEMP_STARTREG", FALSE);
        }
    }

    if (i == regNum)
    {
        DeleteLocalInt(oRealm, "TEMP_STARTREG");
        LogInfo("Finished generating special tiles");
    }
}

// ---------------------------------------------------------------------

//Functions for path finding

//Returns the first encountered unanalyzed direction from a direction flags integer (flag value 0 means unanalyzed); returns -1 if all directions are analyzed
int _GetFirstNotAnalyzedDirection(int directionFlags)
{
    int i;
    for (i = 0; i < 4; i++)
    {
        int direction;
        switch (i)
        {
            case 0:
                direction = PATH_NORTH;
                break;
            case 1:
                direction = PATH_EAST;
                break;
            case 2:
                direction = PATH_SOUTH;
                break;
            case 3:
                direction = PATH_WEST;
                break;
        }

        if (!(directionFlags & direction))
            return direction;
    }

    return -1;
}

int _GetDistanceToTile(int tileIndex, int targetTileIndex, int nWidth, object oRealm)
{
    struct Coordinates coords = ConvertTileIndexToCoordinates(tileIndex, nWidth);
    string tileDistances = _GetTileDistancesFieldOnGrid(coords.x, coords.y, oRealm);
    int distance = GetIntArrayElement(tileDistances, targetTileIndex, oRealm);
    return distance;
}

void _SetDistanceToTile(int tileIndex, int targetTileIndex, int nDistance, int nWidth, object oRealm)
{
    struct Coordinates coords = ConvertTileIndexToCoordinates(tileIndex, nWidth);
    string tileDistances = _GetTileDistancesFieldOnGrid(coords.x, coords.y, oRealm);
    SetIntArrayElement(tileDistances, targetTileIndex, nDistance, oRealm);
}

int _GetPathExists(int tileIndex, int nDirection, int nWidth, object oRealm)
{
    struct Coordinates coords = ConvertTileIndexToCoordinates(tileIndex, nWidth);
    int tilePaths = _GetTileFieldOnGrid(coords.x, coords.y, "paths", oRealm);
    if ((tilePaths & nDirection) > 0)
        return TRUE;
    return FALSE;
}

//This function does NOT make sure that a neighbor in that direction exists
int _GetNeighborTileIndex(int tileIndex, int nDirection, int nWidth)
{
    switch (nDirection)
    {
        case PATH_NORTH:
            return tileIndex - nWidth;
        case PATH_EAST:
            return tileIndex + 1;
        case PATH_SOUTH:
            return tileIndex + nWidth;
        case PATH_WEST:
            return tileIndex - 1;
    }
    return -1;
}

void _FindPathsToTileInRegion(struct Region reg, int tileIndex, int nWidth, int nHeight, object oRealm)
{
    LogInfo("Finding paths to tile: " + IntToString(tileIndex));

    int storedDistance;
    int selectedTileIndex;

    //Create template list of directions for all tiles in the region - we create an array for all the tiles, to make accessing values for particular tile indexes faster
    int i;
    int numberOfTiles = nWidth * nHeight;
    string regDirectionsArray = "regDirectionsArray";
    CreateIntArray(regDirectionsArray, numberOfTiles, oRealm);

    //Create tile stack for DFS
    string tileStackArray = "tileStackArray";
    CreateIntArray(tileStackArray, 0, oRealm);

    //Start with the target tile
    selectedTileIndex = tileIndex;

    //Store the target tile's region to ensure we don't leave it when searching for paths
    struct Coordinates coords = ConvertTileIndexToCoordinates(tileIndex, nWidth);
    int targetRegion = _GetTileFieldOnGrid(coords.x, coords.y, "region", oRealm);

    while (TRUE)
    {
        //Store the selected tile's distance
        storedDistance = _GetDistanceToTile(selectedTileIndex, tileIndex, nWidth, oRealm);
        int directionFlags = GetIntArrayElement(regDirectionsArray, selectedTileIndex, oRealm);
        int storedDirection;

        while (TRUE)
        {
            //Get the first unanalyzed direction
            int direction = _GetFirstNotAnalyzedDirection(directionFlags);
            if (direction == -1) //if all directions have been analyzed for this tile
            {
                int stackSize = GetIntArraySize(tileStackArray, oRealm);
                if (stackSize == 0) //if the stack is empty
                    return; //the end

                SetIntArrayElement(regDirectionsArray, selectedTileIndex, directionFlags, oRealm); //save the (possibly modified) directions
                selectedTileIndex = GetIntArrayElement(tileStackArray, stackSize-1, oRealm); //go back to a tile from the stack
                DeleteIntArrayElement(tileStackArray, stackSize-1, oRealm); //pop
                break; //back to the outer loop
            }
            else if (_GetPathExists(selectedTileIndex, direction, nWidth, oRealm) == FALSE) //if there are no paths in that direction
            {
                directionFlags |= direction; //mark as analyzed
                SetIntArrayElement(regDirectionsArray, selectedTileIndex, directionFlags, oRealm);
                continue; //back to the inner loop
            }
            else
            {
                directionFlags |= direction; //set the direction flag to 1
                SetIntArrayElement(regDirectionsArray, selectedTileIndex, directionFlags, oRealm); //store the directions
                AddIntArrayElement(tileStackArray, selectedTileIndex, FALSE, oRealm); //add the tile to the stack
                selectedTileIndex = _GetNeighborTileIndex(selectedTileIndex, direction, nWidth); //start analyzing the neighboring area the direction leads to
                storedDirection = direction; //store the last direction

                //Make sure we're not leaving the region
                struct Coordinates neighborCoords = ConvertTileIndexToCoordinates(selectedTileIndex, nWidth);
                if (_GetTileFieldOnGrid(neighborCoords.x, neighborCoords.y, "region", oRealm) != targetRegion)
                {
                    int stackSize = GetIntArraySize(tileStackArray, oRealm);
                    selectedTileIndex = GetIntArrayElement(tileStackArray, stackSize-1, oRealm); //go back to a tile from the stack
                    DeleteIntArrayElement(tileStackArray, stackSize-1, oRealm); //pop
                    break; //back to the outer loop
                }

                //check if the newly selected tile has a distance assigned to it
                int newDistance = _GetDistanceToTile(selectedTileIndex, tileIndex, nWidth, oRealm);
                if (newDistance == 0 && selectedTileIndex != tileIndex)
                {
                    //if not, mark the opposite direction to the stored one as analyzed and set the distance
                    directionFlags = GetIntArrayElement(regDirectionsArray, selectedTileIndex, oRealm);
                    directionFlags |= _GetOppositeDirection(storedDirection);
                    _SetDistanceToTile(selectedTileIndex, tileIndex, storedDistance+1, nWidth, oRealm);
                    SetIntArrayElement(regDirectionsArray, selectedTileIndex, directionFlags, oRealm); //store the directions
                    break; //back to the outer loop
                }
                else
                {
                    if (newDistance > storedDistance + 1) //we found a better (shorter) path, so we should overwrite the distance and re-analyze directions
                    {
                        _SetDistanceToTile(selectedTileIndex, tileIndex, storedDistance+1, nWidth, oRealm);
                        directionFlags = _GetOppositeDirection(storedDirection); //clear the analyzed directions and mark the opposite direction as analyzed
                        SetIntArrayElement(regDirectionsArray, selectedTileIndex, directionFlags, oRealm); //store the directions
                        break; //back to the outer loop
                    }
                    else //the path we found is not better, so ignore it and go back to the previous tile
                    {
                        int stackSize = GetIntArraySize(tileStackArray, oRealm);
                        selectedTileIndex = GetIntArrayElement(tileStackArray, stackSize-1, oRealm); //go back to a tile from the stack
                        DeleteIntArrayElement(tileStackArray, stackSize-1, oRealm); //pop
                        break; //back to the outer loop
                    }
                }
            }
        }
    }
}

void _FindPathsInRegion(struct Region reg, int nStep, int nWidth, int nHeight, object oRealm)
{
    if (nStep == 0)
        LogInfo("Started finding paths in region: " + IntToString(reg.id));

    //Do pathfinding to every tile in the region
    int regSize = GetIntArraySize(reg.tiles, oRealm);
    int i = nStep;
    if (i < regSize)
    {
        int tileIndex = GetIntArrayElement(reg.tiles, i, oRealm);
        _FindPathsToTileInRegion(reg, tileIndex, nWidth, nHeight, oRealm);
    }
}

void _FindPathsToRegion(struct Region reg, int nStep, int nWidth, int nHeight, object oRealm)
{
    int reg1 = -1;
    int reg2 = -1;
    int i;
    int j;

    //Create visitedRegions array and stack of reg1
    string reg1stack = "reg1stack";
    string reg2stack = "reg2stack";
    string visitedRegs = "visitedRegs";
    string regionsArray = "Regions";
    if (nStep == 0)
    {
        SetLocalInt(oRealm, "TEMP_INPROGRESS", TRUE);
        CreateIntArray(reg1stack);
        CreateIntArray(visitedRegs, 45);

        //Set the chosen region as reg1 and the first unvisited neighbor as reg2
        reg1 = reg.id;
        int neighborsNum = GetStringArraySize(reg.neighbors, oRealm);
        for (i = 0; i < neighborsNum; i++)
        {
            if (GetIntArrayElement(visitedRegs, i) == TRUE)
                continue;

            string neighboringTiles = GetStringArrayElement(reg.neighbors, i, oRealm);
            if (GetIntArraySize(neighboringTiles, oRealm) == 0 || GetIntArraySize(neighboringTiles, oRealm) > 1) //explanation for this below, starting with "we never bother"
                continue;

            reg2 = i;
            LogInfo("reg2 selected: %n", reg2);
            SetLocalInt(oRealm, "TEMP_REG1", reg1);
            SetLocalInt(oRealm, "TEMP_REG2", reg2);
            SetIntArrayElement(visitedRegs, reg1, TRUE);
            SetIntArrayElement(visitedRegs, reg2, TRUE);
            AddIntArrayElement(reg1stack, reg1);
            AddIntArrayElement(reg1stack, reg2);
            break;
        }
        if (reg2 == -1)
        {
            LogInfo("Not found any suitable neighbors, aborting");
            DeleteLocalInt(oRealm, "TEMP_INPROGRESS");
            ClearIntArray(reg1stack);
            ClearIntArray(visitedRegs);
        }
    }
    //one iteration of this block per step, think of it as: while (TRUE)
    {
        if (GetLocalInt(oRealm, "TEMP_INPROGRESS") == FALSE)
            return;
        reg1 = GetLocalInt(oRealm, "TEMP_REG1");
        reg2 = GetLocalInt(oRealm, "TEMP_REG2");

        //Get areas neighboring each other from reg1 and reg2
        string reg1neighbors = GetRegionArrayElementStringField(regionsArray, reg1, "neighbors", oRealm);
        string reg2neighbors = GetRegionArrayElementStringField(regionsArray, reg2, "neighbors", oRealm);
        reg1neighbors = GetStringArrayElement(reg1neighbors, reg2, oRealm);
        reg2neighbors = GetStringArrayElement(reg2neighbors, reg1, oRealm);
        int borderAreaOfReg1Index = GetIntArrayElement(reg1neighbors, 0, oRealm);
        int borderAreaOfReg2Index = GetIntArrayElement(reg2neighbors, 0, oRealm);

        //For every area X in reg2 set distance to every chosen region's (not necessarily reg1!) area Y like this: D(X,Y) = D(X,borderAreaOfReg2) + 1 + D(borderAreaOfReg1,Y)
        struct Coordinates coords = ConvertTileIndexToCoordinates(borderAreaOfReg1Index, nWidth);
        string borderAreaOfReg1Distances = _GetTileDistancesFieldOnGrid(coords.x, coords.y, oRealm);
        string reg2TilesArray = GetRegionArrayElementStringField(regionsArray, reg2, "tiles", oRealm);
        for (i = 0; i < GetIntArraySize(reg2TilesArray, oRealm); i++)
        {
            //Getting tileDistances array for our area X
            int areaId = GetIntArrayElement(reg2TilesArray, i, oRealm);
            coords = ConvertTileIndexToCoordinates(areaId, nWidth);
            string tileDistances = _GetTileDistancesFieldOnGrid(coords.x, coords.y, oRealm);
            for (j = 0; j < GetIntArraySize(reg.tiles, oRealm); j++)
            {
                int destinationAreaId = GetIntArrayElement(reg.tiles, j, oRealm);
                int startToBorderDistance = GetIntArrayElement(tileDistances, borderAreaOfReg2Index, oRealm); //D(X,borderAreaOfReg2)
                int borderToDestinationDistance = GetIntArrayElement(borderAreaOfReg1Distances, destinationAreaId, oRealm); //D(borderAreaOfReg1,Y)
                SetIntArrayElement(tileDistances, destinationAreaId, startToBorderDistance+1+borderToDestinationDistance, oRealm);

                //struct Coordinates destCoords = ConvertTileIndexToCoordinates(destinationAreaId, nWidth);
                //LogInfo("Distance from tile (%n,%n) to (%n,%n) is set as " + IntToString(startToBorderDistance+1+borderToDestinationDistance), coords.x, coords.y, destCoords.x, destCoords.y);
            }
        }

        //Get new region with DFS and repeat
        int foundNextNeighbor = FALSE;
        int potentialReg1 = reg2;
        while (TRUE)
        {
            string potentialReg1Neighbors = GetRegionArrayElementStringField(regionsArray, potentialReg1, "neighbors", oRealm);
            int neighborsNum = GetStringArraySize(potentialReg1Neighbors, oRealm);
            for (i = 0; i < neighborsNum; i++)
            {
                if (GetIntArrayElement(visitedRegs, i) == TRUE)
                    continue;

                string neighboringTiles = GetStringArrayElement(potentialReg1Neighbors, i, oRealm);
                int neighboringTilesSize = GetIntArraySize(neighboringTiles, oRealm);
                if (neighboringTilesSize == 0 || neighboringTilesSize > 1) //we never bother to delete potential bordering tiles for regions that eventually aren't neighbors, so more than 1 tile means there is no passage between the regions
                    continue;

                reg1 = potentialReg1;
                reg2 = i;
                AddIntArrayElement(reg1stack, reg2);
                LogInfo("reg1 selected: %n", reg1);
                LogInfo("reg2 selected: %n", reg2);
                SetLocalInt(oRealm, "TEMP_REG1", reg1);
                SetLocalInt(oRealm, "TEMP_REG2", reg2);
                SetIntArrayElement(visitedRegs, reg1, TRUE);
                SetIntArrayElement(visitedRegs, reg2, TRUE);
                foundNextNeighbor = TRUE;
                break;
            }
            if (foundNextNeighbor)
                break;

            //Delete last stack element, get the one before that and try again
            int reg1StackSize = GetIntArraySize(reg1stack);
            if (reg1StackSize == 1)
                break;
            DeleteIntArrayElement(reg1stack, reg1StackSize-1);
            potentialReg1 = GetIntArrayElement(reg1stack, reg1StackSize-2);
        }

        //If DFS is finished, so are we
        if (!foundNextNeighbor)
        {
            DeleteLocalInt(oRealm, "TEMP_INPROGRESS");
            DeleteLocalInt(oRealm, "TEMP_REG1");
            DeleteLocalInt(oRealm, "TEMP_REG2");
            ClearIntArray(reg1stack);
            ClearIntArray(visitedRegs);
        }
    }

}

void FindPaths(int nStep, object oRealm)
{
    if (nStep != 1 && nStep != GetLocalInt(OBJECT_SELF, "PROC_NEXTMINORSTEP"))
    {
        DelayCommand(0.0f, FindPaths(nStep, oRealm));
        return;
    }
    _ManageSemaphor(nStep, 1353);

    //Get width of the map
    string row = "Grid_MapRow0";
    int height = GetStringArraySize("Grid", oRealm);
    int width = GetRealmTileArraySize(row, oRealm);

    string regionsArray = "Regions";
    int regNum = GetRegionArraySize(regionsArray, oRealm);
    int i = nStep-1; //it should go from 0 to 1353; 0,2 - array creation, 3,48 - potential regions, 45*30=1350 tiles
    int regIndex = (i-3) / nMaxRegionSize;
    int internalStep = (i-3) % nMaxRegionSize;

    if (i < 3)
    {
        //Create arrays of distances for every tile
        int numberOfTiles = width * height;
        int j;
        int iterationsPerStep = 90;
        int initialIteration = i*iterationsPerStep;
        int upperBound = (initialIteration + iterationsPerStep) > numberOfTiles ? numberOfTiles : (initialIteration + iterationsPerStep);
        for (j = initialIteration; j < upperBound; j++)
        {
            struct Coordinates coords = ConvertTileIndexToCoordinates(j, width);
            string tileDistances = _GetTileDistancesFieldOnGrid(coords.x, coords.y, oRealm);
            CreateIntArray(tileDistances, numberOfTiles, oRealm);
        }
    }
    else if (regIndex < regNum)
    {
        struct Region reg = GetRegionArrayElement(regionsArray, regIndex, oRealm);
        int regSize = GetIntArraySize(reg.tiles, oRealm);
        if (regSize > 0)
            _FindPathsInRegion(reg, internalStep, width, height, oRealm);
    }

    if (regIndex == regNum && internalStep == 0)
    {
        LogInfo("Finished pathfinding");
    }
}

void FindPathsBetweenRegions(int nStep, object oRealm)
{
    if (nStep != 1 && nStep != GetLocalInt(OBJECT_SELF, "PROC_NEXTMINORSTEP"))
    {
        DelayCommand(0.0f, FindPathsBetweenRegions(nStep, oRealm));
        return;
    }
    _ManageSemaphor(nStep, 400);

    //Get width of the map
    string row = "Grid_MapRow0";
    int height = GetStringArraySize("Grid", oRealm);
    int width = GetRealmTileArraySize(row, oRealm);
    nStep--;
    int i = nStep / 20;
    int internalStep = nStep % 20;

    string regionsArray = "Regions";
    int regNum = GetRegionArraySize(regionsArray, oRealm);
    if (i < regNum && regNum > 1)
    {
        struct Region reg = GetRegionArrayElement(regionsArray, i, oRealm);
        int regSize = GetIntArraySize(reg.tiles, oRealm);
        if (regSize > 0)
        {
            if (internalStep == 0)
                LogInfo("Finding paths to region %n", reg.id);
            _FindPathsToRegion(reg, internalStep, width, height, oRealm);
        }
    }
    if (i == regNum && internalStep == 19)
    {
        LogInfo("Finished inter-region pathfinding");
    }
}

//Migrating data to a less dumb data structure than NWScript's structs...
void CreateBiomeObjectsOnRealm(object oRealm)
{
    _ManageSemaphor(1, 1);

    string regionsArray = "Regions";
    int regionsNum = GetRegionArraySize(regionsArray, oRealm);
    int i;
    int language = GetServerLanguage();
    for (i = 0; i < regionsNum; i++)
    {
        struct Region reg = GetRegionArrayElement(regionsArray, i, oRealm);
        int numberOfTiles = GetIntArraySize(reg.tiles, oRealm);
        if (numberOfTiles > 0)
        {
            object biome = CreateBiomeInstance(reg.type, oRealm, reg.id, reg.order, reg.name);
            int biomeRumorsNum = GetBiomeRumorsNumber(reg.type);
            int j;
            for (j = 0; j < biomeRumorsNum; j++)
            {
                string rumor = GetBiomeRumor(reg.type, j, language);
                AddStringArrayElement(BIOME_RUMORS_ARRAY, rumor, FALSE, biome);
            }
        }
    }
}

// ---------------------------------------------------------------------

//Functions for plot generation

void SpawnPlot(object oRealm)
{
    _ManageSemaphor(1, 1);

    string realmScript = GetRealmScript(oRealm);
    int language = GetServerLanguage();

    //Get level of final biome
    int realmStartingLvl = GetRealmStartingLevel(realmScript);
    int lvlsPerRegion = GetNumberOfLevelsPerRegion(realmScript);
    int biomesNum = GetObjectArraySize(REALM_BIOMES_ARRAY, oRealm);
    int finalBiomeLvl = realmStartingLvl + biomesNum * lvlsPerRegion;

    int plotsNum = GetNumberOfPlots(realmScript);
    int i;
    CreateStringArray("SuitablePlots");
    for (i = 0; i < plotsNum; i++)
    {
        string plotScript = GetRealmPlotScript(realmScript, i);
        int minLvl = GetPlotMinPlayerLevel(plotScript);
        int maxLvl = GetPlotMaxPlayerLevel(plotScript);

        if (finalBiomeLvl >= minLvl && finalBiomeLvl <= maxLvl)
            AddStringArrayElement("SuitablePlots", plotScript);
    }

    int suitablePlotsNum = GetStringArraySize("SuitablePlots");
    if (suitablePlotsNum == 0)
        LogWarning("No suitable plot scripts found for realm "+realmScript+", finalBiomeLvl: %n", finalBiomeLvl);

    int index = RandomNext(suitablePlotsNum);
    string suitablePlotScript = GetStringArrayElement("SuitablePlots", index);
    ClearStringArray("SuitablePlots");
    object plot = CreatePlotInstance(suitablePlotScript, oRealm);
    struct PlotJournalEntries journalEntries = GetPlotJournalEntries(plot, "default", language);
    SetPlotJournalEntries(plot, journalEntries);
    UpdatePlotForward(plot, PLOT_STATE_DOING_QUESTS, 0);

    //Add plot rumors to rumor pools of every biome
    int rumorsNum = GetPlotRumorsNumber(suitablePlotScript);
    for (i = 0; i < biomesNum; i++)
    {
        LogInfo("Started adding rumors to rumor pool of biome: %n", i);
        object biome = GetObjectArrayElement(REALM_BIOMES_ARRAY, i, oRealm);
        int j;
        for (j = 0; j < rumorsNum; j++)
        {
            LogInfo("Adding plot rumor %n to rumor pool.", j);
            string rumor = GetPlotRumor(plot, j, language);
            AddStringArrayElement(BIOME_RUMORS_ARRAY, rumor, FALSE, biome);
        }
    }
}

// ---------------------------------------------------------------------

//Functions for area spawning

void _SpawnResourcesInTile(string sSeedName, object oTile)
{
    object biome = GetTileBiome(oTile);
    int level = GetRegionStartingLevel(biome);
    string biomeScript = GetBiomeScript(biome);

    string resWps = "resWps";
    CreateObjectArray(resWps);

    int areasNum = GetObjectArraySize(TILE_AREAS_ARRAY, oTile);
    int i;
    for (i = 0; i < areasNum; i++)
    {
        object area = GetObjectArrayElement(TILE_AREAS_ARRAY, i, oTile);
        object obj = GetFirstObjectInArea(area);
        while (GetIsObjectValid(obj))
        {
            if (GetObjectType(obj) == OBJECT_TYPE_WAYPOINT && (GetTag(obj) == "wp_worldres" || GetTag(obj) == "wp_caveres"))
                AddObjectArrayElement(resWps, obj);
            obj = GetNextObjectInArea(area);
        }
    }

    int totalWaypoints = GetObjectArraySize(resWps);
    for (i = 0; i < totalWaypoints; i++)
    {
        object wp = GetObjectArrayElement(resWps, i);
        if (RandomNext(totalWaypoints, sSeedName) < 3)
        {
            int isUnderground = GetTag(wp) == "wp_caveres";
            SpawnResourcePlaceable(biomeScript, sSeedName, GetLocation(wp), level, isUnderground);
        }
    }

    ClearObjectArray(resWps);
}

string _ConvertValueToResRefPart(int nValue)
{
    string sValue = IntToString(nValue);
    if (GetStringLength(sValue) == 1)
        return "0" + sValue;
    return sValue;
}

object _SpawnTavernAreaInVillage(object oTown)
{
    string townScript = GetTownScript(oTown);
    int areasNum = GetTownTavernsNumber(townScript);
    string resref = GetTavernAreaResRef(townScript, RandomNext(areasNum));
    string tag = GetTag(oTown) + "_tavern";
    string tavernName = GetRandomTavernName(townScript, "default", GetServerLanguage());
    string areaName = GetLocalString(oTown, "Name") + " - " + tavernName;
    object tavernArea = CopyAreaWithEventScripts(resref, tag, areaName);

    AddAreaToTile(oTown, tavernArea);
    SetTavernOfTown(oTown, tavernArea);

    //Set music tracks for area
    int dayTracks = GetTavernDayTracksNumber(townScript);
    int nightTracks = GetTavernNightTracksNumber(townScript);
    int battleTracks = GetTavernBattleTracksNumber(townScript);
    int dayTrack = GetTavernDayTrack(townScript, RandomNext(dayTracks));
    int nightTrack = GetTavernNightTrack(townScript, RandomNext(nightTracks));
    int battleTrack = GetTavernBattleTrack(townScript, RandomNext(battleTracks));
    MusicBackgroundChangeDay(tavernArea, dayTrack);
    MusicBackgroundChangeNight(tavernArea, nightTrack);
    MusicBattleChange(tavernArea, battleTrack);

    return tavernArea;
}

object _SpawnStoreAreaInVillage(object oTown)
{
    string townScript = GetTownScript(oTown);
    int storesNum = GetTownStoresNumber(townScript);
    string storeScript = GetTownStoreScript(townScript, RandomNext(storesNum));
    int areasNum = GetStoreAreasNumber(storeScript);
    string resref = GetStoreAreaResRef(storeScript, RandomNext(areasNum));
    string tag = GetTag(oTown) + "_store";
    string storeName = GetRandomStoreName(storeScript, "default", GetServerLanguage());
    string areaName = GetLocalString(oTown, "Name") + " - " + storeName;
    object storeArea = CopyAreaWithEventScripts(resref, tag, areaName);

    InitializeStoreInstance(storeScript, storeArea, oTown);

    //Set music tracks for area
    int dayTracks = GetStoreDayTracksNumber(storeScript);
    int nightTracks = GetStoreNightTracksNumber(storeScript);
    int battleTracks = GetStoreBattleTracksNumber(storeScript);
    int dayTrack = GetStoreDayTrack(storeScript, RandomNext(dayTracks));
    int nightTrack = GetStoreNightTrack(storeScript, RandomNext(nightTracks));
    int battleTrack = GetStoreBattleTrack(storeScript, RandomNext(battleTracks));
    MusicBackgroundChangeDay(storeArea, dayTrack);
    MusicBackgroundChangeNight(storeArea, nightTrack);
    MusicBattleChange(storeArea, battleTrack);

    return storeArea;
}

object _SpawnFacilityAreaInVillage(object oTown)
{
    object biome = GetBiomeOfTown(oTown);
    string biomeScript = GetBiomeScript(biome);
    int level = GetRegionStartingLevel(biome);
    int language = GetServerLanguage();
    string townScript = GetTownScript(oTown);

    //Select facility appropriate for biome level
    int facilitiesNum = GetTownFacilitiesNumber(townScript);
    int i;
    CreateStringArray("SuitableFacilities");
    for (i = 0; i < facilitiesNum; i++)
    {
        string facilityScript = GetTownFacilityScript(townScript, i);
        int minLvl = GetFacilityMinPlayerLevel(facilityScript);
        int maxLvl = GetFacilityMaxPlayerLevel(facilityScript);
        if (minLvl <= level && maxLvl >= level)
            AddStringArrayElement("SuitableFacilities", facilityScript);
    }

    int suitableFacilitiesNum = GetStringArraySize("SuitableFacilities");
    if (suitableFacilitiesNum == 0)
        LogWarning("No suitable facilities found for town script "+townScript+", biome starting level: %n", level);

    int index = RandomNext(suitableFacilitiesNum);
    string selectedFacilityScript = GetStringArrayElement("SuitableFacilities", index);
    ClearStringArray("SuitableFacilities");

    //Spawn facility
    int facilityAreasNum = GetNumberOfFacilityAreas(selectedFacilityScript);
    index = RandomNext(facilityAreasNum);
    string resref = GetFacilityAreaResRef(selectedFacilityScript, index, oTown);
    string tag = GetTag(oTown) + "_facility";
    string facilityName = GetRandomFacilityName(selectedFacilityScript, "default", language);
    string areaName = GetLocalString(oTown, "Name") + " - " + facilityName;
    object area = CopyAreaWithEventScripts(resref, tag, areaName);
    AddEventScript(SUBEVENT_AREA_ON_ENTER, "are_explore", area);

    if (!GetIsObjectValid(area))
        LogWarning("Area invalid (inc_world, _SpawnFacilityAreaInVillage, area)");

    //Initialize it
    InitializeFacilityInstance(selectedFacilityScript, area, oTown);

    //Add facility rumors to the pool
    int rumors = GetFacilityRumorsNumber(selectedFacilityScript);
    for (i = 0; i < rumors; i++)
        AddStringArrayElement(BIOME_RUMORS_ARRAY, GetFacilityRumor(area, i, language), FALSE, biome);

    return area;
}

//Spawn a village area and return it
object _SpawnVillageArea(struct RealmTile tile, object oRealm)
{
    //Generate resref of the template to spawn
    int townsNum = GetBiomeTownsNumber(tile.biome);
    string townScript = GetBiomeTownScript(tile.biome, RandomNext(townsNum));
    int areasNum = GetTownAreasNumber(townScript);
    string resref = GetTownAreaResRef(townScript, RandomNext(areasNum), tile.paths);

    LogInfo("Spawning area from template: " + resref);

    //Spawn!
    string tag = "area_" + IntToString(tile.index);
    string regionName = GetRegionArrayElementStringField("Regions", tile.region, "name", oRealm);
    string tileName = GetRandomTownName(townScript, "default", GetServerLanguage());
    string areaName = regionName + " - " + tileName;
    object area = CopyAreaWithEventScripts(resref, tag, areaName);
    object biome = GetBiomeByRegionID(oRealm, tile.region);
    InitializeTileDataOnArea(area, tile, tileName, biome, oRealm);
    InitializeTownInstance(townScript, area, biome, tileName);

    if (!GetIsObjectValid(area))
        LogWarning("Area invalid (inc_world, _SpawnVillageArea, area)");

    //Set music tracks for area
    int dayTracks = GetTownDayTracksNumber(townScript);
    int nightTracks = GetTownNightTracksNumber(townScript);
    int battleTracks = GetTownBattleTracksNumber(townScript);
    int dayTrack = GetTownDayTrack(townScript, RandomNext(dayTracks));
    int nightTrack = GetTownNightTrack(townScript, RandomNext(nightTracks));
    int battleTrack = GetTownBattleTrack(townScript, RandomNext(battleTracks));
    MusicBackgroundChangeDay(area, dayTrack);
    MusicBackgroundChangeNight(area, nightTrack);
    MusicBattleChange(area, battleTrack);

    //Set skybox TODO (because it's temporary and it should depend on the weather, which in turn should depend on biome parameters)
    SetSkyBox(SKYBOX_GRASS_CLEAR, area);

    _SpawnTavernAreaInVillage(area);
    _SpawnStoreAreaInVillage(area);
    _SpawnFacilityAreaInVillage(area);
    LogInfo("Tag: " + tag);

    return area;
}

void _SpawnNormalArea(struct RealmTile tile, object oRealm)
{
    //Generate resref of the template to spawn
    int areasNum = GetBiomeRegularAreasNumber(tile.biome);
    string resref = GetBiomeRegularAreaResRef(tile.biome, RandomNext(areasNum), tile.paths);

    LogInfo("Spawning area from template: " + resref);

    //Spawn!
    string tag = "area_" + IntToString(tile.index);
    string regionName = GetRegionArrayElementStringField("Regions", tile.region, "name", oRealm);
    string tileName = GetLocalizedString("Wilderness", "Dzicz");
    string areaName = regionName + " - " + tileName;
    object biome = GetBiomeByRegionID(oRealm, tile.region);
    object area = CopyAreaWithEventScripts(resref, tag, areaName);
    InitializeTileDataOnArea(area, tile, tileName, biome, oRealm);
    SetRestEncounterCanSpawn(area);

    //Set music tracks for area
    int dayTracks = GetBiomeDayTracksNumber(tile.biome);
    int nightTracks = GetBiomeNightTracksNumber(tile.biome);
    int battleTracks = GetBiomeBattleTracksNumber(tile.biome);
    int dayTrack = GetBiomeDayTrack(tile.biome, RandomNext(dayTracks));
    int nightTrack = GetBiomeDayTrack(tile.biome, RandomNext(nightTracks));
    int battleTrack = GetBiomeBattleTrack(tile.biome, RandomNext(battleTracks));
    MusicBackgroundChangeDay(area, dayTrack);
    MusicBackgroundChangeNight(area, nightTrack);
    MusicBattleChange(area, battleTrack);

    //Set skybox TODO (because it's temporary and it should depend on the weather, which in turn should depend on biome parameters)
    SetSkyBox(SKYBOX_GRASS_CLEAR, area);

    if (!GetIsObjectValid(area))
        LogWarning("Area invalid (inc_world, _SpawnNormalArea, area)");
}

void _SpawnSpecialArea(struct RealmTile tile, object oRealm)
{
    object biome = GetBiomeByRegionID(oRealm, tile.region);
    int level = GetRegionStartingLevel(biome);
    int language = GetServerLanguage();

    //Select special area appropriate for biome level and area exits
    int specialAreasNum = GetBiomeSpecialAreasNumber(tile.biome);
    LogInfo("Special areas of biome script "+tile.biome+": %n", specialAreasNum);
    int singleExit = _GetIsNumberPowerOfTwo(tile.paths);
    int i;
    CreateStringArray("SuitableSpecialAreas");
    for (i = 0; i < specialAreasNum; i++)
    {
        string specialAreaScript = GetBiomeSpecialAreaScript(tile.biome, i);
        int minLvl = GetSpecialAreaMinPlayerLevel(specialAreaScript);
        int maxLvl = GetSpecialAreaMaxPlayerLevel(specialAreaScript);
        if (minLvl <= level && maxLvl >= level && (!singleExit || GetSpecialAreaDeadendAreas(specialAreaScript)) && (singleExit || GetSpecialAreaPassthroughAreas(specialAreaScript)) )
            AddStringArrayElement("SuitableSpecialAreas", specialAreaScript);
    }

    int suitableSpecialAreasNum = GetStringArraySize("SuitableSpecialAreas");
    if (suitableSpecialAreasNum == 0)
        LogWarning("No suitable special areas found for biome script "+tile.biome+", biome starting level: %n, area exits flag: %n", level, tile.paths);

    int index = RandomNext(suitableSpecialAreasNum);
    string selectedSpecialAreaScript = GetStringArrayElement("SuitableSpecialAreas", index);
    ClearStringArray("SuitableSpecialAreas");

    //Spawn special area
    int specialAreaVariantsNum = GetNumberOfSpecialAreas(selectedSpecialAreaScript);
    index = RandomNext(specialAreaVariantsNum);
    string resref = GetSpecialAreaResRef(selectedSpecialAreaScript, index, biome, tile.paths);
    string tag = "area_" + IntToString(tile.index);
    if (!singleExit)
        LogInfo("Multi-exit special area with tag: " + tag);
    string specialAreaName = GetRandomSpecialAreaName(selectedSpecialAreaScript, "default", language);
    string regionName = GetRegionArrayElementStringField("Regions", tile.region, "name", oRealm);
    string areaName = regionName + " - " + specialAreaName;
    object area = CopyAreaWithEventScripts(resref, tag, areaName);

    if (!GetIsObjectValid(area))
        LogWarning("Area invalid (inc_world, _SpawnSpecialArea, area)");

    //Initialize it
    InitializeTileDataOnArea(area, tile, specialAreaName, biome, oRealm);
    int lowQuestPriority = GetSpecialAreaHasLowQuestGenerationPriority(selectedSpecialAreaScript);
    InitializeSpecialAreaInstance(selectedSpecialAreaScript, lowQuestPriority, tile.roadToTown, area, biome);

    _SpawnResourcesInTile("default", area);
}

void _SpawnBossArea(struct RealmTile tile, object oRealm)
{
    object biome = GetBiomeByRegionID(oRealm, tile.region);
    object plot = GetRealmPlot(oRealm);
    string plotScript = GetPlotScript(plot);
    int level = GetRegionStartingLevel(biome);
    int language = GetServerLanguage();

    //Select boss area appropriate for biome level and area exits
    int bossAreasNum = GetNumberOfPlotBossAreas(plotScript);
    LogInfo("Boss areas of plot script "+plotScript+": %n", bossAreasNum);
    int i;
    CreateStringArray("SuitableBossAreas");
    for (i = 0; i < bossAreasNum; i++)
    {
        string bossAreaScript = GetPlotBossAreaScript(plotScript, i);
        int minLvl = GetBossAreaMinPlayerLevel(bossAreaScript);
        int maxLvl = GetBossAreaMaxPlayerLevel(bossAreaScript);
        if (minLvl <= level && maxLvl >= level)
            AddStringArrayElement("SuitableBossAreas", bossAreaScript);
    }

    int suitableBossAreasNum = GetStringArraySize("SuitableBossAreas");
    if (suitableBossAreasNum == 0)
        LogWarning("No suitable boss areas found for plot script "+plotScript+", biome starting level: %n, area exits flag: %n", level, tile.paths);

    int index = RandomNext(suitableBossAreasNum);
    string selectedBossAreaScript = GetStringArrayElement("SuitableBossAreas", index);
    ClearStringArray("SuitableBossAreas");

    //Spawn boss area
    int bossAreaVariantsNum = GetNumberOfBossAreas(selectedBossAreaScript);
    index = RandomNext(bossAreaVariantsNum);
    string resref = GetBossAreaResRef(selectedBossAreaScript, index, biome, tile.paths);
    string tag = "area_" + IntToString(tile.index);
    string bossAreaName = GetRandomBossAreaName(selectedBossAreaScript, "default", language);
    string regionName = GetRegionArrayElementStringField("Regions", tile.region, "name", oRealm);
    string areaName = regionName + " - " + bossAreaName;
    object area = CopyAreaWithEventScripts(resref, tag, areaName);

    if (!GetIsObjectValid(area))
        LogWarning("Area invalid (inc_world, _SpawnBossArea, area)");

    //Initialize it
    InitializeTileDataOnArea(area, tile, bossAreaName, biome, oRealm);
    InitializeBossAreaInstance(selectedBossAreaScript, area, biome, plot);
    InitializeBossArea("default", biome, area, level);

    //Add boss area rumors to the pool
    int rumors = GetBossAreaRumorsNumber(selectedBossAreaScript);
    for (i = 0; i < rumors; i++)
        AddStringArrayElement(BIOME_RUMORS_ARRAY, GetBossAreaRumor(area, i, language), FALSE, biome);

    _SpawnResourcesInTile("default", area);
}

void _SpawnStartingArea(struct RealmTile tile, object oRealm)
{
    LogInfo("Spawning starting village area...");
    object village = _SpawnVillageArea(tile, oRealm);
    if (!GetIsObjectValid(village))
        LogWarning("Area invalid (inc_world, _SpawnStartingArea, village)");
}

void _SpawnMapTile(struct RealmTile tile, object oRealm)
{
    switch (tile.type)
    {
        case TILE_TYPE_SETTLEMENT:
            _SpawnVillageArea(tile, oRealm);
            break;
        case TILE_TYPE_SPECIAL:
            _SpawnSpecialArea(tile, oRealm);
            break;
        case TILE_TYPE_NORMAL:
            _SpawnNormalArea(tile, oRealm);
            break;
        case TILE_TYPE_START:
            _SpawnStartingArea(tile, oRealm);
            break;
        case TILE_TYPE_BOSS:
            _SpawnBossArea(tile, oRealm);
            break;
        default:
            LogWarning("Unknown tile type - tile cannot be spawned (inc_world, SpawnMapTile, tile.type)");
            break;
    }
}

void SpawnMapTiles(int nStep, object oRealm)
{
    if (nStep != 1 && nStep != GetLocalInt(OBJECT_SELF, "PROC_NEXTMINORSTEP"))
    {
        DelayCommand(0.0f, SpawnMapTiles(nStep, oRealm));
        return;
    }
    _ManageSemaphor(nStep, 256);

    string row = "Grid_MapRow0";
    int height = GetStringArraySize("Grid", oRealm);
    int width = GetRealmTileArraySize(row, oRealm);

    int areasNum = height*width;
    int i = nStep-1;
    if (nStep > areasNum)
        return;

    struct Coordinates coords = ConvertTileIndexToCoordinates(i, width);
    struct RealmTile tile = _GetTileOnGrid(coords.x, coords.y, oRealm);
    _SpawnMapTile(tile, oRealm);
}

// ---------------------------------------------------------------------

//Functions for area initialization

void _SpawnTownStables(object oTown, object oRealm)
{
    int language = GetServerLanguage();
    object hitchPostWp = GetAreaWaypoint(oTown, "wp_hitchpost");
    CreateObject(OBJECT_TYPE_PLACEABLE, "x3_plc_hpost", GetLocation(hitchPostWp));

    object npcWp = GetAreaWaypoint(oTown, "wp_stablemaster");
    string townScript = GetTownScript(oTown);
    int num = GetTownStableMastersNumber(townScript);
    string resref = GetTownStableMasterResRef(townScript, RandomNext(num));
    object npc = CreateCreature(resref, GetLocation(npcWp));
    SetRandomSeed(RandomNextAny("default"), ObjectToString(npc));
    SetPlotFlag(npc, TRUE);
    AddEventScript(SUBEVENT_CREATURE_ON_SPAWN, "x2_def_spawn", npc);
    AddEventScript(SUBEVENT_CREATURE_ON_HEARTBEAT, "x2_def_heartbeat", npc);
    string firstName = GetRandomStableMasterFirstName(townScript, GetRacialType(npc), GetGender(npc), language, "default");
    string lastName = GetRandomStableMasterLastName(townScript, GetRacialType(npc), GetGender(npc), language, "default");
    SetName(npc, firstName + " " + lastName);
    SetLocalString(npc, "Conversation", "stablemaster");
    AddEventScript(SUBEVENT_CREATURE_ON_CONVERSATION, "cre_varconvo", npc);

    object biome = GetBiomeOfTown(oTown);
    int biomeStartingLevel = GetRegionStartingLevel(biome);
    object horseStore = SpawnStableInventory(townScript, "default", biomeStartingLevel, npcWp);
    SetLocalObject(npc, "Store", horseStore);
}

void _InitializeStartingArea(struct RealmTile tile, object oRealm)
{   
    LogInfo("Initializing starting village area: %n", tile.index);
    int language = GetServerLanguage();
    string tag = "area_" + IntToString(tile.index);
    object village = GetLocalObject(oRealm, tag);
    object startWp = GetTownRespawnWaypoint(village);
    if (!GetIsObjectValid(startWp))
        LogWarning("Waypoint invalid (inc_areas, _InitializeStartingArea, startWp)");
    SetLocalObject(oRealm, "StartWP", startWp);
    SetMapTileExplored(village);

    //Spawn facility signpost
    object signpostWp = GetAreaWaypoint(village, "wp_facsignpost");
    CreateObject(OBJECT_TYPE_PLACEABLE, "obj_facsignpost", GetLocation(signpostWp));

    //Spawn inn and store signboards
    object storesignWp = GetAreaWaypoint(village, "wp_storesign");
    location wpLocation = GetLocation(storesignWp);
    location rotatedLocation = GetRotatedLocation(wpLocation);
    CreateObject(OBJECT_TYPE_PLACEABLE, "obj_twnstoresign", rotatedLocation);

    object innsignWp = GetAreaWaypoint(village, "wp_innsign");
    wpLocation = GetLocation(innsignWp);
    rotatedLocation = GetRotatedLocation(wpLocation);
    CreateObject(OBJECT_TYPE_PLACEABLE, "obj_twninnsign", rotatedLocation);

    //Spawn stables
    _SpawnTownStables(village, oRealm);

    //Spawn tavern NPCs
    object tavern = GetTavernOfTown(village);
    string wpResRef = "wp_innkeeper";
    string townScript = GetTownScript(village);
    int innkeepersNumber = GetTavernInnkeepersNumber(townScript);
    string innkeeperResRef = GetInnkeeperResRef(townScript, RandomNext(innkeepersNumber));
    object innkeeperWp = GetAreaWaypoint(tavern, wpResRef);
    if (innkeeperWp == OBJECT_INVALID)
        LogWarning("Invalid innkeeper waypoint for town with tile tag " + tag + ", tavern area ResRef: " + GetResRef(tavern));
    object innkeeper = CreateCreature(innkeeperResRef, GetLocation(innkeeperWp));
    SetRandomSeed(RandomNextAny("default"), ObjectToString(innkeeper));
    if (innkeeper == OBJECT_INVALID)
        LogWarning("Invalid innkeeper creature for town with tile tag " + tag + ", innkeeperResRef = " + innkeeperResRef);
    SetPlotFlag(innkeeper, TRUE);
    AddEventScript(SUBEVENT_CREATURE_ON_SPAWN, "x2_def_spawn", innkeeper);
    AddEventScript(SUBEVENT_CREATURE_ON_HEARTBEAT, "x2_def_heartbeat", innkeeper);
    string firstName = GetRandomInnkeeperFirstName(townScript, GetRacialType(innkeeper), GetGender(innkeeper), language, "default");
    string lastName = GetRandomInnkeeperLastName(townScript, GetRacialType(innkeeper), GetGender(innkeeper), language, "default");
    SetName(innkeeper, firstName + " " + lastName);
    object store = SpawnTavernInventory(townScript, "default", innkeeperWp);
    if (store == OBJECT_INVALID)
        LogWarning("Invalid tavern store object for town with tile tag: " + tag);
    SetLocalString(innkeeper, "Conversation", "innkeeper");
    SetLocalObject(innkeeper, "Store", store);
    ClearCreatureEventScriptFromVariable(innkeeper, SUBEVENT_CREATURE_ON_CONVERSATION);
    AddEventScript(SUBEVENT_CREATURE_ON_CONVERSATION, "cre_varconvo", innkeeper);

    //Spawn store NPC
    object shop = GetStoreOfTown(village);
    object biome = GetBiomeOfTown(village);
    int biomeStartingLevel = GetRegionStartingLevel(biome);
    string storeScript = GetStoreScript(shop);
    wpResRef = "wp_storekeeper";
    int storekeepersNumber = GetStoreStorekeepersNumber(storeScript);
    string storekeeperResRef = GetStorekeeperResRef(storeScript, RandomNext(storekeepersNumber));
    object storekeeperWp = GetAreaWaypoint(shop, wpResRef);
    if (storekeeperWp == OBJECT_INVALID)
        LogWarning("Invalid storekeeper waypoint for town with tile tag " + tag + ", store area ResRef: " + GetResRef(shop));
    object storekeeper = CreateCreature(storekeeperResRef, GetLocation(storekeeperWp));
    SetRandomSeed(RandomNextAny("default"), ObjectToString(storekeeper));
    if (storekeeper == OBJECT_INVALID)
        LogWarning("Invalid storekeeper creature for town with tile tag " + tag + ", storekeeperResRef = " + innkeeperResRef);
    SetPlotFlag(storekeeper, TRUE);
    AddEventScript(SUBEVENT_CREATURE_ON_SPAWN, "x2_def_spawn", storekeeper);
    AddEventScript(SUBEVENT_CREATURE_ON_HEARTBEAT, "x2_def_heartbeat", storekeeper);
    firstName = GetRandomStoreOwnerFirstName(storeScript, GetRacialType(storekeeper), GetGender(storekeeper), language, "default");
    lastName = GetRandomStoreOwnerLastName(storeScript, GetRacialType(storekeeper), GetGender(storekeeper), language, "default");
    SetName(storekeeper, firstName + " " + lastName);
    store = SpawnStoreInventory(storeScript, "default", biomeStartingLevel, storekeeperWp);
    if (store == OBJECT_INVALID)
        LogWarning("Invalid shop store object for town with tile tag: " + tag);
    SetLocalString(storekeeper, "Conversation", "storekeeper");
    SetLocalObject(storekeeper, "Store", store);
    ClearCreatureEventScriptFromVariable(storekeeper, SUBEVENT_CREATURE_ON_CONVERSATION);
    AddEventScript(SUBEVENT_CREATURE_ON_CONVERSATION, "cre_varconvo", storekeeper);

    //Perform facility initialization logic
    object facility = GetFacilityOfTown(village);
    InitializeFacility("default", village, facility, biomeStartingLevel);
}

void _InitializeNormalArea(struct RealmTile tile, object oRealm)
{
    string tag = "area_" + IntToString(tile.index);
    object area = GetLocalObject(oRealm, tag);
    object biome = GetBiomeByRegionID(oRealm, tile.region);
    int language = GetServerLanguage();

    //get encounter script
    int isAdvancedEncounter = RandomNext(100, "default") < GetBiomeAdvancedEncounterChance(tile.biome);
    LogInfo("Generated encounter is advanced: %n", isAdvancedEncounter);
    int encountersNumber = isAdvancedEncounter ? GetBiomeAdvancedEncountersNumber(tile.biome) : GetBiomeBasicEncountersNumber(tile.biome);
    int i;
    CreateStringArray("SuitableEncounters");
    int lvl = GetRegionStartingLevel(biome);
    for (i = 0; i < encountersNumber; i++)
    {
        string encScript = isAdvancedEncounter ? GetBiomeAdvancedEncounterScript(tile.biome, i) : GetBiomeBasicEncounterScript(tile.biome, i);
        int minLvl = GetEncounterMinPlayerLevel(encScript);
        int maxLvl = GetEncounterMaxPlayerLevel(encScript);
        if (minLvl <= lvl && maxLvl >= lvl)
            AddStringArrayElement("SuitableEncounters", encScript);
    }

    encountersNumber = GetStringArraySize("SuitableEncounters");
    LogInfo("Encounter count before adding plot encounters: %n", encountersNumber);
    if (encountersNumber == 0)
        LogWarning("Suitable encounters not found for biome "+tile.biome+", biome starting level: %n", lvl);

    object plot = GetRealmPlot(oRealm);
    string plotScript = GetPlotScript(plot);
    encountersNumber = GetPlotEncountersNumber(plotScript);
    for (i = 0; i < encountersNumber; i++)
    {
        string encScript = GetPlotEncounterScript(plotScript, i);
        int minLvl = GetEncounterMinPlayerLevel(encScript);
        int maxLvl = GetEncounterMaxPlayerLevel(encScript);
        if (minLvl <= lvl && maxLvl >= lvl)
            AddStringArrayElement("SuitableEncounters", encScript);
    }
    encountersNumber = GetStringArraySize("SuitableEncounters");
    LogInfo("Encounter count after adding plot encounters: %n", encountersNumber);

    string encounterScript = GetStringArrayElement("SuitableEncounters", RandomNext(encountersNumber));
    int lowQuestPriority = GetEncounterHasLowQuestGenerationPriority(encounterScript);
    object encounter = CreateEncounterInstance(encounterScript, lowQuestPriority, tile.roadToTown, oRealm, area, lvl);
    SpawnEncounter(encounter, "default");

    int rumors = GetEncounterRumorsNumber(encounterScript);
    int j;
    for (j = 0; j < rumors; j++)
        AddStringArrayElement(BIOME_RUMORS_ARRAY, GetEncounterRumor(encounter, j, language), FALSE, biome);

    _SpawnResourcesInTile("default", area);

    //10% chance of a treasure chest spawn
    if (RandomNext(10) == 0)
    {
        CreateObjectArray("Waypoints");
        object wp = GetFirstObjectInArea(area);
        while (GetIsObjectValid(wp))
        {
            if (GetObjectType(wp) == OBJECT_TYPE_WAYPOINT && GetTag(wp) == "wp_treasure")
                AddObjectArrayElement("Waypoints", wp);
            wp = GetNextObjectInArea(area);
        }
        int wpIndex = RandomNext(GetObjectArraySize("Waypoints"));
        wp = GetObjectArrayElement("Waypoints", wpIndex);
        ClearObjectArray("Waypoints");

        location wpLocation = GetLocation(wp);
        location rotatedLocation = GetRotatedLocation(wpLocation);

        object chest = CreateObject(OBJECT_TYPE_PLACEABLE, "obj_lootchest", rotatedLocation);
        string biomeScript = GetBiomeScript(biome);
        int lootNum = RandomNext(3) + 2;
        int chestGold = 0;
        int totalQuality = 0; //it will be anywhere from 2 to 24, most often around 10-11.
        for (i = 0; i < lootNum; i++)
        {
            int qualityNumber = RandomNext(100);
            int quality;
            if (qualityNumber < 5)
                quality = LOOT_QUALITY_USELESS;
            else if (qualityNumber < 15)
                quality = LOOT_QUALITY_BAD;
            else if (qualityNumber < 50)
                quality = LOOT_QUALITY_AVERAGE;
            else if (qualityNumber < 85)
                quality = LOOT_QUALITY_GOOD;
            else if (qualityNumber < 95)
                quality = LOOT_QUALITY_GREAT;
            else
                quality = LOOT_QUALITY_EXTRAORDINARY;
            struct TreasureChestLoot loot = GetBiomeLoot(biomeScript, "default", lvl, quality);
            chestGold += loot.gold;
            object item = CreateItemOnObject(loot.itemResRef, chest, loot.itemStackSize);
            if (!GetIsObjectValid(item) && loot.gold == 0)
            {
                LogWarning("No item spawned in treasure chest, resRef: " + loot.itemResRef + ", area: %n", GetTileIndex(area));
            }
            SetStolenFlag(item, TRUE);
            totalQuality += quality;
        }
        CreateItemOnObject("nw_it_gold001", chest, chestGold);
        SetPlotFlag(chest, TRUE);
        int lockChance = totalQuality * 5;
        if (RandomNext(100) < lockChance)
        {
            SetLocked(chest, TRUE);
            int lockDC = totalQuality + 2 + RandomNext(5) + lvl;
            SetLockUnlockDC(chest, lockDC);
        }
        int trapChance = totalQuality * 5;
        if (RandomNext(100) < trapChance)
        {
            int trapPower;
            if (lvl <= 3)
                trapPower = TRAP_POWER_MINOR;
            else if (lvl <= 5)
                trapPower = TRAP_POWER_AVERAGE;
            else if (lvl <= 10)
                trapPower = TRAP_POWER_STRONG;
            else if (lvl <= 20)
                trapPower = TRAP_POWER_DEADLY;
            else
                trapPower = TRAP_POWER_EPIC;
            if (lvl > 2 && totalQuality > 16 && trapPower != TRAP_POWER_EPIC && (trapPower != TRAP_POWER_DEADLY || lvl > 14))
                trapPower += 1;

            int trapType;
            if (trapPower != TRAP_POWER_EPIC)
                trapType = RandomNext(10) + 1;
            else
                trapType = RandomNext(4) + 1;

            int trapConstant = GetTrapConstant(trapPower, trapType);

            int detectDC = totalQuality / 2 + lvl + RandomNext(10);
            int disarmDC = totalQuality + 2 + RandomNext(5) + lvl;

            CreateTrapOnObject(trapConstant, chest);
            SetTrapDetectable(chest);
            SetTrapDetectDC(chest, detectDC);
            SetTrapDisarmable(chest);
            SetTrapRecoverable(chest);
            SetTrapDisarmDC(chest, disarmDC);
        }
    }
}

void _InitializeSpecialArea(struct RealmTile tile, object oRealm)
{
    object biome = GetBiomeByRegionID(oRealm, tile.region);
    int level = GetRegionStartingLevel(biome);
    int language = GetServerLanguage();
    object area = GetRealmTile(oRealm, tile.index);
    string script = GetSpecialAreaScript(area);

    //Initialization
    InitializeSpecialArea("default", biome, area, level);
    SetRestEncounterCanSpawn(area);

    //Add special area rumors to the pool
    int rumors = GetSpecialAreaRumorsNumber(script);
    int i;
    for (i = 0; i < rumors; i++)
        AddStringArrayElement(BIOME_RUMORS_ARRAY, GetSpecialAreaRumor(area, i, language), FALSE, biome);
}

void _InitializeMapTile(struct RealmTile tile, object oRealm)
{
    switch (tile.type)
    {
        //case TILE_TYPE_SETTLEMENT: _InitializeVillageArea(tile, oRealm); break;
        case TILE_TYPE_SPECIAL: 
            _InitializeSpecialArea(tile, oRealm);
            break;
        case TILE_TYPE_NORMAL:
            _InitializeNormalArea(tile, oRealm);
            break;
        case TILE_TYPE_START:
            _InitializeStartingArea(tile, oRealm);
            break;
    }
}

void InitializeMapTiles(int nStep, object oRealm)
{
    if (nStep != 1 && nStep != GetLocalInt(OBJECT_SELF, "PROC_NEXTMINORSTEP"))
    {
        DelayCommand(0.0f, InitializeMapTiles(nStep, oRealm));
        return;
    }
    _ManageSemaphor(nStep, 256);

    string row = "Grid_MapRow0";
    int height = GetStringArraySize("Grid", oRealm);
    int width = GetRealmTileArraySize(row, oRealm);

    int areasNum = height*width;
    int i = nStep-1;
    if (nStep > areasNum)
        return;

    struct Coordinates coords = ConvertTileIndexToCoordinates(i, width);
    struct RealmTile tile = _GetTileOnGrid(coords.x, coords.y, oRealm);
    _InitializeMapTile(tile, oRealm);
}

// ---------------------------------------------------------------------

//Functions for spawning quests

object _SpawnQuest(object oSpecialAreaOrEncounter, object oBiome, int nWpNum, int nQuestID, int nAllowSpecialty, string questScript, int nSpecialAreaQuest)
{
    int language = GetServerLanguage();

    //Get questgiver ResRef
    object town = GetTownOfBiome(oBiome);
    int questgiversNumber = GetNumberOfQuestgivers(questScript);
    int questgiverIndex = RandomNext(questgiversNumber);
    string resref = GetQuestgiverResRef(questScript, questgiverIndex, town);

    //Get questgiver's wp
    string wpResRef;
    if (nWpNum <= 4)
        wpResRef = "wp_tquestgiver_" + IntToString(nWpNum);
    else
        wpResRef = "wp_questgiver_" + IntToString(nWpNum-4);
    object wp = GetTileWaypoint(town, wpResRef);
    if (!GetIsObjectValid(wp))
        LogWarning("Invalid questgiver waypoint " + wpResRef + " in region %n", GetRegionIDOfBiome(oBiome));

    //Create questgiver
    object questgiver = CreateCreature(resref, GetLocation(wp));
    SetRandomSeed(RandomNextAny("default"), ObjectToString(questgiver));
    SetPlotFlag(questgiver, TRUE);
    AddEventScript(SUBEVENT_CREATURE_ON_SPAWN, "x2_def_spawn", questgiver);
    AddEventScript(SUBEVENT_CREATURE_ON_HEARTBEAT, "x2_def_heartbeat", questgiver);
    SetLocalString(questgiver, "Conversation", "questgiver");
    ClearCreatureEventScriptFromVariable(questgiver, SUBEVENT_CREATURE_ON_CONVERSATION);
    AddEventScript(SUBEVENT_CREATURE_ON_CONVERSATION, "cre_varconvo", questgiver);
    if (!GetIsObjectValid(questgiver))
        LogWarning("Invalid questgiver object for quest script " + questScript + " in region %n", GetRegionIDOfBiome(oBiome));

    //Create quest
    object quest = nSpecialAreaQuest
                   ? CreateQuestInstanceFromSpecialArea(questScript, oSpecialAreaOrEncounter, nQuestID, questgiver)
                   : CreateQuestInstanceFromEncounter(questScript, oSpecialAreaOrEncounter, nQuestID, questgiver);

    //Set questgiver name
    string firstName = GetRandomQuestgiverFirstName(questScript, questgiver, "default", language);
    string lastName = GetRandomQuestgiverLastName(questScript, questgiver, "default", language);
    SetLocalString(questgiver, "FirstName", firstName);
    SetLocalString(questgiver, "LastName", lastName);
    SetName(questgiver, firstName + " " + lastName);

    //Set quest reward and dialog lines and journal
    struct QuestReward questReward = GetQuestReward(questScript, "default");
    struct QuestDialogLines questLines = GetQuestDialogLines(questScript, questReward, quest, "default", language);
    SetQuestReward(quest, questReward);
    SetQuestDialogLines(quest, questLines);

    //Add quest rumors to the pool
    int rumors = GetQuestRumorsNumber(GetQuestScript(quest));
    int i;
    for (i = 0; i < rumors; i++)
        AddStringArrayElement(BIOME_RUMORS_ARRAY, GetQuestRumor(quest, i, language), FALSE, oBiome);

    //Set journal tokens
    int token = GetQuestNameToken(nQuestID);
    SetCustomTokenEx(token, questLines.journalName);
    token = GetQuestEntryToken(nQuestID, QUEST_STATE_LINGERING);
    SetCustomTokenEx(token, questLines.journalInitial);
    token = GetQuestEntryToken(nQuestID, QUEST_STATE_COMPLETED);
    SetCustomTokenEx(token, questLines.journalSuccess);
    token = GetQuestEntryToken(nQuestID, QUEST_STATE_FAILED);
    SetCustomTokenEx(token, questLines.journalFailure);
    token = GetQuestEntryToken(nQuestID, QUEST_STATE_AFTER_COMPLETED);
    SetCustomTokenEx(token, questLines.journalSuccessFinished);
    token = GetQuestEntryToken(nQuestID, QUEST_STATE_REFUSED);
    SetCustomTokenEx(token, questLines.journalAbandoned);

    return quest;
}

object _SpawnQuestFromSpecialArea(object oSpecialArea, object oBiome, int nWpNum, int nQuestID, int nAllowSpecialty)
{
    //Get quest script
    int level = GetRegionStartingLevel(oBiome);
    string specialAreaScript = GetSpecialAreaScript(oSpecialArea);
    int specialtyQuest = nAllowSpecialty && GetNumberOfSpecialAreaSpecialtyQuests(specialAreaScript) > 0;
    CreateStringArray("SuitableQuests");
    int i;
    int questsNumber;

    //Regular quests
    questsNumber = GetNumberOfSpecialAreaRegularQuests(specialAreaScript);
    for (i = 0; i < questsNumber; i++)
    {
        string questScript = GetSpecialAreaRegularQuestScript(specialAreaScript, i);
        int minLvl = GetQuestMinPlayerLevel(questScript);
        int maxLvl = GetQuestMaxPlayerLevel(questScript);
        if (minLvl <= level && maxLvl >= level)
            AddStringArrayElement("SuitableQuests", questScript);
    }

    int suitableQuestsNum = GetStringArraySize("SuitableQuests");
    if (suitableQuestsNum == 0)
        LogWarning("No suitable regular quests found for special area script "+specialAreaScript+", biome starting level: %n", level);

    //Specialty quests
    if (specialtyQuest)
    {
        questsNumber = GetNumberOfSpecialAreaSpecialtyQuests(specialAreaScript);
        for (i = 0; i < questsNumber; i++)
        {
            string questScript = GetSpecialAreaSpecialtyQuestScript(specialAreaScript, i);
            int minLvl = GetQuestMinPlayerLevel(questScript);
            int maxLvl = GetQuestMaxPlayerLevel(questScript);
            if (minLvl <= level && maxLvl >= level)
                AddStringArrayElement("SuitableQuests", questScript);
        }
    }

    suitableQuestsNum = GetStringArraySize("SuitableQuests");
    if (suitableQuestsNum == 0)
        LogWarning("No suitable quests (including specialty) found for special area script "+specialAreaScript+", biome starting level: %n", level);

    int index = RandomNext(suitableQuestsNum);
    string selectedQuestScript = GetStringArrayElement("SuitableQuests", index);
    ClearStringArray("SuitableQuests");

    return _SpawnQuest(oSpecialArea, oBiome, nWpNum, nQuestID, nAllowSpecialty, selectedQuestScript, TRUE);
}

object _SpawnQuestFromEncounter(object oEncounter, object oBiome, int nWpNum, int nQuestID, int nAllowSpecialty)
{
    //Get quest script
    int level = GetRegionStartingLevel(oBiome);
    string encounterScript = GetEncounterScript(oEncounter);
    int specialtyQuest = nAllowSpecialty && GetNumberOfEncounterSpecialtyQuests(encounterScript) > 0;
    CreateStringArray("SuitableQuests");
    int i;
    int questsNumber;

    //Regular quests
    questsNumber = GetNumberOfEncounterRegularQuests(encounterScript);
    for (i = 0; i < questsNumber; i++)
    {
        string questScript = GetEncounterRegularQuestScript(encounterScript, i);
        int minLvl = GetQuestMinPlayerLevel(questScript);
        int maxLvl = GetQuestMaxPlayerLevel(questScript);
        if (minLvl <= level && maxLvl >= level)
            AddStringArrayElement("SuitableQuests", questScript);
    }

    int suitableQuestsNum = GetStringArraySize("SuitableQuests");
    if (suitableQuestsNum == 0)
        LogWarning("No suitable regular quests found for encounter script "+encounterScript+", biome starting level: %n", level);

    //Specialty quests
    if (specialtyQuest)
    {
        questsNumber = GetNumberOfEncounterSpecialtyQuests(encounterScript);
        for (i = 0; i < questsNumber; i++)
        {
            string questScript = GetEncounterSpecialtyQuestScript(encounterScript, i);
            int minLvl = GetQuestMinPlayerLevel(questScript);
            int maxLvl = GetQuestMaxPlayerLevel(questScript);
            if (minLvl <= level && maxLvl >= level)
                AddStringArrayElement("SuitableQuests", questScript);
        }
    }

    suitableQuestsNum = GetStringArraySize("SuitableQuests");
    if (suitableQuestsNum == 0)
        LogWarning("No suitable quests (including specialty) found for encounter script "+encounterScript+", biome starting level: %n", level);

    int index = RandomNext(suitableQuestsNum);
    string selectedQuestScript = GetStringArrayElement("SuitableQuests", index);
    ClearStringArray("SuitableQuests");

    return _SpawnQuest(oEncounter, oBiome, nWpNum, nQuestID, nAllowSpecialty, selectedQuestScript, FALSE);
}

void SpawnQuests(int nStep, object oRealm)
{
    if (nStep != 1 && nStep != GetLocalInt(OBJECT_SELF, "PROC_NEXTMINORSTEP"))
    {
        DelayCommand(0.0f, SpawnQuests(nStep, oRealm));
        return;
    }
    _ManageSemaphor(nStep, 20);

    int regionsNum = GetObjectArraySize(REALM_BIOMES_ARRAY, oRealm);
    int i = nStep - 1;

    if (i == regionsNum)
        DeleteLocalInt(oRealm, "TEMP_QUESTID");
    if (i >= regionsNum)
        return;

    int nextQuestID = i == 0 ? 1 : GetLocalInt(oRealm, "TEMP_QUESTID");

    object region = GetObjectArrayElement(REALM_BIOMES_ARRAY, i, oRealm);
    int encountersToTake = 10;
    string selectedEncountersArray = "TEMP_SELECTEDENCOUNTERS";
    CreateObjectArray(selectedEncountersArray);
    int j;
    int k;

    for (j = 0; j < encountersToTake; j++)
    {
        string arrayToUse;
        int availableEncountersNum;

        for (k = 0; k < 5; k++)
        {
            switch (k)
            {
                case 0:
                    arrayToUse = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_1;
                    break;
                case 1:
                    arrayToUse = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_2;
                    break;
                case 2:
                    arrayToUse = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_3;
                    break;
                case 3:
                    arrayToUse = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_4;
                    break;
                default:
                    if (j > 7)
                    {
                        LogInfo("No valid %nth encounter, but we have at least 8, so adding a dummy OBJECT_INVALID in its place", j+1);
                        AddObjectArrayElement(selectedEncountersArray, OBJECT_INVALID);
                    }
                    else
                        LogFatal("No valid encounters for quest generation, region id: %n (inc_generation, SpawnQuests)", GetRegionIDOfBiome(region));
                    break;
            }

            availableEncountersNum = GetObjectArraySize(arrayToUse, region);
            if (availableEncountersNum > 0)
                break;
        }

        int index = RandomNext(availableEncountersNum);
        object encounter = GetObjectArrayElement(arrayToUse, index, region);
        AddObjectArrayElement(selectedEncountersArray, encounter);
        DeleteObjectArrayElement(arrayToUse, index, region);
        string encounterScript = GetEncounterScript(encounter);
        string specialAreaScript = GetSpecialAreaScript(encounter);
        if (encounterScript != "")
            LogInfo("Selected quest encounter: " + encounterScript);
        else
            LogInfo("Selected quest special area: "+specialAreaScript+", tag: "+GetTag(encounter));
    }

    //Sort encounters by ascending distance to town hub
    string row = "Grid_MapRow0";
    int width = GetRealmTileArraySize(row, oRealm);
    object town = GetTownOfBiome(region);
    int townIndex = GetTileIndex(town);
    string sortedEncountersArray = "TEMP_SORTEDENCOUNTERSARRAY";
    CreateObjectArray(sortedEncountersArray);

    for (j = 0; j < encountersToTake; j++)
    {
        int minValueSoFar = 256;
        int minValueIndex = -1;
        object minDistanceEncounter;
        int tileIndex;
        for (k = 0; k < GetObjectArraySize(selectedEncountersArray); k++)
        {
            object encounter = GetObjectArrayElement(selectedEncountersArray, k);
            if (encounter == OBJECT_INVALID)
            {
                //this will ensure OBJECT_INVALIDs potentially added in lieu of the 9th and 10th encounter/special area will be first on the list, and thus not considered
                //(the smallest region possible is 10 tiles, so there will be minimum 8 tiles suitable for quest generation: 10 minus town and minus boss area)
                minDistanceEncounter = encounter;
                minValueSoFar = 0;
                minValueIndex = k;
                continue;
            }
            string encounterScript = GetEncounterScript(encounter);
            object tile = encounterScript != "" ? GetEncounterArea(encounter) : encounter;
            tileIndex = GetTileIndex(tile);
            int distance = _GetDistanceToTile(tileIndex, townIndex, width, oRealm);
            if (distance < minValueSoFar)
            {
                minDistanceEncounter = encounter;
                minValueSoFar = distance;
                minValueIndex = k;
            }
        }
        AddObjectArrayElement(sortedEncountersArray, minDistanceEncounter);
        DeleteObjectArrayElement(selectedEncountersArray, minValueIndex);
    }

    string questWpIndexesArray = "TEMP_QUESTWPINDEXES";
    CreateIntArray(questWpIndexesArray, 10);
    for (k = 0; k < 10; k++)
        SetIntArrayElement(questWpIndexesArray, k, k+1);

    //Generate and activate first three quests in encounters at indexes 3,4,5 (0,1,2 are ignored to avoid generating quests too close to the town hub) - allow specialty quests
    for (k = 3; k < 6; k++)
    {
        int questWpArrayElementIndex = RandomNext(GetIntArraySize(questWpIndexesArray));
        int questWpIndex = GetIntArrayElement(questWpIndexesArray, questWpArrayElementIndex);
        DeleteIntArrayElement(questWpIndexesArray, questWpArrayElementIndex);
        object encounter = GetObjectArrayElement(sortedEncountersArray, k);
        string encounterScript = GetEncounterScript(encounter);
        object quest = encounterScript != ""
                       ? _SpawnQuestFromEncounter(encounter, region, questWpIndex, nextQuestID++, TRUE)
                       : _SpawnQuestFromSpecialArea(encounter, region, questWpIndex, nextQuestID++, TRUE);

        string specialAreaScript = GetSpecialAreaScript(encounter);
        if (encounterScript != "")
            LogInfo("Spawned quest for encounter: " + encounterScript);
        else
            LogInfo("Spawned quest for special area: "+specialAreaScript+", tag: "+GetTag(encounter));
        LogInfo("Spawned and activated quest: " + GetQuestScript(quest));
        CustomQuestInitializationLogic("default", quest);
        ActivateQuest(quest);
    }

    //Generate the quest in encounter at index 6 - allow specialty quests, but don't activate them yet
    {
        int questWpArrayElementIndex = RandomNext(GetIntArraySize(questWpIndexesArray));
        int questWpIndex = GetIntArrayElement(questWpIndexesArray, questWpArrayElementIndex);
        DeleteIntArrayElement(questWpIndexesArray, questWpArrayElementIndex);
        object encounter = GetObjectArrayElement(sortedEncountersArray, 6);
        string encounterScript = GetEncounterScript(encounter);
        object quest = encounterScript != ""
                       ? _SpawnQuestFromEncounter(encounter, region, questWpIndex, nextQuestID++, TRUE)
                       : _SpawnQuestFromSpecialArea(encounter, region, questWpIndex, nextQuestID++, TRUE);

        AddObjectArrayElement(BIOME_INACTIVE_QUESTS, quest, FALSE, region);
        string specialAreaScript = GetSpecialAreaScript(encounter);
        if (encounterScript != "")
            LogInfo("Spawned quest for encounter: " + encounterScript);
        else
            LogInfo("Spawned quest for special area: "+specialAreaScript+", tag: "+GetTag(encounter));
        LogInfo("Spawned quest: " + GetQuestScript(quest));
        CustomQuestInitializationLogic("default", quest);
    }

    //Generate the last three quests in encounters at indexes 7,8,9 - disallow specialty quests, so they can be completed by everyone
    for (k = 7; k < 10; k++)
    {
        int questWpArrayElementIndex = RandomNext(GetIntArraySize(questWpIndexesArray));
        int questWpIndex = GetIntArrayElement(questWpIndexesArray, questWpArrayElementIndex);
        DeleteIntArrayElement(questWpIndexesArray, questWpArrayElementIndex);
        object encounter = GetObjectArrayElement(sortedEncountersArray, k);
        string encounterScript = GetEncounterScript(encounter);
        object quest = encounterScript != ""
                       ? _SpawnQuestFromEncounter(encounter, region, questWpIndex, nextQuestID++, FALSE)
                       : _SpawnQuestFromSpecialArea(encounter, region, questWpIndex, nextQuestID++, FALSE);

        AddObjectArrayElement(BIOME_INACTIVE_QUESTS, quest, FALSE, region);
        string specialAreaScript = GetSpecialAreaScript(encounter);
        if (encounterScript != "")
            LogInfo("Spawned non-specialty quest for encounter: " + encounterScript);
        else
            LogInfo("Spawned non-specialty quest for special area: "+specialAreaScript+", tag: "+GetTag(encounter));
        LogInfo("Spawned quest: " + GetQuestScript(quest));
        CustomQuestInitializationLogic("default", quest);
    }

    SetLocalInt(oRealm, "TEMP_QUESTID", nextQuestID);
}

// ---------------------------------------------------------------------

//Functions for generating encounter champions

void _SpawnChampion(object oEncounter, object oBiome, object oRealm)
{
    //Select champion script
    object tile = GetTile(GetEncounterArea(oEncounter));
    LogInfo("Spawning champion in tile: %n,%n", GetTileX(tile), GetTileY(tile));
    int language = GetServerLanguage();
    int level = GetRegionStartingLevel(oBiome);
    CreateStringArray("SuitableChampions");

    string encounterScript = GetEncounterScript(oEncounter);
    int championsNumber = GetNumberOfEncounterChampions(encounterScript);
    int i;
    for (i = 0; i < championsNumber; i++)
    {
        string championScript = GetEncounterChampion(oEncounter, i);
        int minLvl = GetChampionMinPlayerLevel(championScript);
        int maxLvl = GetChampionMaxPlayerLevel(championScript);
        if (minLvl <= level && maxLvl >= level)
            AddStringArrayElement("SuitableChampions", championScript);
    }

    int suitableChampionsNum = GetStringArraySize("SuitableChampions");
    if (suitableChampionsNum == 0)
        LogWarning("No suitable champions found for encounter script "+encounterScript+" despite its presence in the relevant array, biome starting level: %n", level);

    int index = RandomNext(suitableChampionsNum);
    string selectedChampionScript = GetStringArrayElement("SuitableChampions", index);

    //Spawn champion
    index = RandomNext(GetNumberOfChampionBlueprints(selectedChampionScript));
    string championResRef = GetChampionResRef(selectedChampionScript, index);
    int modifier = RandomNext(GetNumberOfChampionModifiers(selectedChampionScript));
    string championName = GetChampionName(selectedChampionScript, modifier, "default", language);
    object champion = CreateChampionInstance(selectedChampionScript, championResRef, championName, oEncounter);
    InitializeChampion(champion, oEncounter, "default");
    ApplyChampionModifier(champion, modifier, level);
    if (RandomNext(5) != 0)
    {
        int bounty = GetChampionBounty(selectedChampionScript, level, "default");
        LogInfo("Generating bounty with value %n for champion script "+selectedChampionScript+", ResRef: "+championResRef, bounty);
        string trophyResRef = GetChampionTrophyResRef(selectedChampionScript);
        string trophyName = GetTrophyName(selectedChampionScript, championName, language);
        string trophyDescription = GetTrophyDescription(selectedChampionScript, championName, language);
        string championDescription = GetChampionDescription(selectedChampionScript, championName, bounty, modifier, "default", language);
        object trophy = CreateChampionTrophy(champion, championDescription, trophyResRef, trophyName, trophyDescription, bounty, oEncounter);
    }

    //Add champion rumors to the pool
    int rumors = GetNumberOfChampionRumors(selectedChampionScript);
    for (i = 0; i < rumors; i++)
        AddStringArrayElement(BIOME_RUMORS_ARRAY, GetChampionRumor(selectedChampionScript, championName, i, modifier, language), FALSE, oBiome);

    AddObjectArrayElement(BIOME_ENCOUNTERS_WITH_CHAMPIONS, oEncounter, FALSE, oBiome);
    ClearStringArray("SuitableChampions");
}

void SpawnChampions(int nStep, object oRealm)
{
    if (nStep != 1 && nStep != GetLocalInt(OBJECT_SELF, "PROC_NEXTMINORSTEP"))
    {
        DelayCommand(0.0f, SpawnChampions(nStep, oRealm));
        return;
    }
    _ManageSemaphor(nStep, 20);

    int regionsNum = GetObjectArraySize(REALM_BIOMES_ARRAY, oRealm);
    int i = nStep - 1;

    if (i >= regionsNum)
        return;

    int maxChampionsToSpawn = 3;

    object region = GetObjectArrayElement(REALM_BIOMES_ARRAY, i, oRealm);
    int level = GetRegionStartingLevel(region);
    string encountersWithSuitableChampions = "EncountersWithSuitableChampions";
    CreateStringArray(encountersWithSuitableChampions);

    //Fill the array with all remaining encounters (not used for quests)
    int j;
    string arrayToUse;
    int availableEncountersNum;
    for (j = 0; j < 4; j++)
    {
        switch (j)
        {
            case 0:
                arrayToUse = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_1;
                break;
            case 1:
                arrayToUse = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_2;
                break;
            case 2:
                arrayToUse = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_3;
                break;
            case 3:
                arrayToUse = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_4;
                break;
        }

        availableEncountersNum = GetObjectArraySize(arrayToUse, region);
        if (availableEncountersNum == 0)
            continue;

        int k;
        for (k = 0; k < availableEncountersNum; k++)
        {
            int encounterHasSuitableChampion = FALSE;
            object encounter = GetObjectArrayElement(arrayToUse, k, region);
            string encounterScript = GetEncounterScript(encounter);
            if (encounterScript != "")
            {
                int championsNumber = GetNumberOfEncounterChampions(encounterScript);
                int l;
                for (l = 0; l < championsNumber; l++)
                {
                    string championScript = GetEncounterChampion(encounter, l);
                    int minLvl = GetChampionMinPlayerLevel(championScript);
                    int maxLvl = GetChampionMaxPlayerLevel(championScript);
                    if (minLvl <= level && maxLvl >= level)
                    {
                        encounterHasSuitableChampion = TRUE;
                        break;
                    }
                }

                if (encounterHasSuitableChampion)
                    AddObjectArrayElement(encountersWithSuitableChampions, encounter);
            }
        }
    }

    availableEncountersNum = GetObjectArraySize(encountersWithSuitableChampions);
    int championsToSpawn = availableEncountersNum < maxChampionsToSpawn ? availableEncountersNum : maxChampionsToSpawn;
    for (j = 0; j < championsToSpawn; j++)
    {
        int arrayIndex = RandomNext(GetObjectArraySize(encountersWithSuitableChampions));
        object encounter = GetObjectArrayElement(encountersWithSuitableChampions, arrayIndex);
        _SpawnChampion(encounter, region, oRealm);
        DeleteObjectArrayElement(encountersWithSuitableChampions, arrayIndex);
    }

    ClearObjectArray(encountersWithSuitableChampions);
}

// ---------------------------------------------------------------------

//Functions for generating bounty hunters

void SpawnBountyHunters(int nStep, object oRealm)
{
    if (nStep != 1 && nStep != GetLocalInt(OBJECT_SELF, "PROC_NEXTMINORSTEP"))
    {
        DelayCommand(0.0f, SpawnBountyHunters(nStep, oRealm));
        return;
    }
    _ManageSemaphor(nStep, 20);

    int language = GetServerLanguage();
    int regionsNum = GetObjectArraySize(REALM_BIOMES_ARRAY, oRealm);
    int i = nStep - 1;

    if (i >= regionsNum)
        return;

    object biome = GetObjectArrayElement(REALM_BIOMES_ARRAY, i, oRealm);
    object village = GetTownOfBiome(biome);
    string townScript = GetTownScript(village);

    //Don't spawn if there are no trophies in the region
    int j;
    int trophies = FALSE;
    for (j = 0; j < GetObjectArraySize(BIOME_ENCOUNTERS_WITH_CHAMPIONS, biome); j++)
    {
        object encounter = GetObjectArrayElement(BIOME_ENCOUNTERS_WITH_CHAMPIONS, j, biome);
        if (GetTrophyOfEncounter(encounter) != OBJECT_INVALID)
        {
            LogInfo("Trophies in region with ID %n found, spawning a bounty hunter", GetRegionIDOfBiome(biome));
            trophies = TRUE;
            break;
        }
    }
    if (trophies == FALSE)
    {
        LogInfo("No trophies in region with ID %n, not spawning any bounty hunters", GetRegionIDOfBiome(biome));
        return;
    }

    //Spawn bounty hunter NPCs
    int bountyHuntersNumber = GetTownBountyHuntersNumber(townScript);
    string bountyHunterResRef = GetTownBountyHunterResRef(townScript, RandomNext(bountyHuntersNumber));
    object hunterWp = GetAreaWaypoint(village, "wp_bountyhunter");
    object hunter = CreateCreature(bountyHunterResRef, GetLocation(hunterWp));
    SetRandomSeed(RandomNextAny("default"), ObjectToString(hunter));
    SetPlotFlag(hunter, TRUE);
    AddEventScript(SUBEVENT_CREATURE_ON_SPAWN, "x2_def_spawn", hunter);
    AddEventScript(SUBEVENT_CREATURE_ON_HEARTBEAT, "x2_def_heartbeat", hunter);

    SetLocalString(hunter, "Conversation", "bountyhunter");
    AddEventScript(SUBEVENT_CREATURE_ON_CONVERSATION, "cre_varconvo", hunter);

    string firstName = GetRandomBountyHunterFirstName(townScript, GetRacialType(hunter), GetGender(hunter), language, "default");
    string secondName = GetRandomBountyHunterLastName(townScript, GetRacialType(hunter), GetGender(hunter), language, "default");
    SetName(hunter, firstName + " " + secondName);

    effect questMarker = TagEffect(EffectVisualEffect(677), "QuestMark"); //green exclamation mark
    ApplyEffectToObject(DURATION_TYPE_PERMANENT, questMarker, hunter);

    SetBountyHunterOfTown(village, hunter);
}


// ---------------------------------------------------------------------

//Functions for spawning commoners

void SpawnCommoners(int nStep, object oRealm)
{
    if (nStep != 1 && nStep != GetLocalInt(OBJECT_SELF, "PROC_NEXTMINORSTEP"))
    {
        DelayCommand(0.0f, SpawnCommoners(nStep, oRealm));
        return;
    }
    _ManageSemaphor(nStep, 20);

    int language = GetServerLanguage();
    int regionsNum = GetObjectArraySize(REALM_BIOMES_ARRAY, oRealm);
    int i = nStep - 1;

    if (i >= regionsNum)
        return;

    object biome = GetObjectArrayElement(REALM_BIOMES_ARRAY, i, oRealm);
    object village = GetTownOfBiome(biome);
    string townScript = GetTownScript(village);
    object tavern = GetTavernOfTown(village);

    //Spawn commoner NPCs
    int j;
    int commonersNumber = GetTownCommonersNumber(townScript);
    string commonerWpResRef;
    int availableRumorsNum = GetStringArraySize(BIOME_RUMORS_ARRAY, biome);
    for (j = 0; j < 3; j++)
    {
        commonerWpResRef = "wp_tcommoner_" + IntToString(j+1);
        string commonerResRef = GetTownCommonerResRef(townScript, RandomNext(commonersNumber));
        object commonerWp = GetAreaWaypoint(tavern, commonerWpResRef);
        object commoner = CreateCreature(commonerResRef, GetLocation(commonerWp));
        SetRandomSeed(RandomNextAny("default"), ObjectToString(commoner));
        SetPlotFlag(commoner, TRUE);
        AddEventScript(SUBEVENT_CREATURE_ON_SPAWN, "x2_def_spawn", commoner);
        AddEventScript(SUBEVENT_CREATURE_ON_HEARTBEAT, "x2_def_heartbeat", commoner);
        AssignCommand(commoner, SetSpawnInCondition(NW_FLAG_IMMOBILE_AMBIENT_ANIMATIONS));
        AssignCommand(commoner, SetSpawnInCondition(NW_FLAG_AMBIENT_ANIMATIONS));
        SetLocalString(commoner, "Conversation", "npcrumor");
        ClearCreatureEventScriptFromVariable(commoner, SUBEVENT_CREATURE_ON_CONVERSATION);
        AddEventScript(SUBEVENT_CREATURE_ON_CONVERSATION, "cre_varconvo", commoner);
        string rumor = GetStringArrayElement(BIOME_RUMORS_ARRAY, RandomNext(availableRumorsNum), biome);
        SetLocalString(commoner, "Rumor", rumor);

        string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(commoner), GetGender(commoner), language, "default");
        string secondName = GetRandomCommonerLastName(townScript, GetRacialType(commoner), GetGender(commoner), language, "default");
        SetName(commoner, firstName + " " + secondName);
    }
    for (j = 0; j < 5; j++)
    {
        commonerWpResRef = "wp_commoner_" + IntToString(j+1);
        string commonerResRef = GetTownCommonerResRef(townScript, RandomNext(commonersNumber));
        object commonerWp = GetAreaWaypoint(village, commonerWpResRef);
        object commoner = CreateCreature(commonerResRef, GetLocation(commonerWp));
        SetRandomSeed(RandomNextAny("default"), ObjectToString(commoner));
        SetPlotFlag(commoner, TRUE);
        AddEventScript(SUBEVENT_CREATURE_ON_SPAWN, "x2_def_spawn", commoner);
        AddEventScript(SUBEVENT_CREATURE_ON_HEARTBEAT, "x2_def_heartbeat", commoner);
        AssignCommand(commoner, SetSpawnInCondition(NW_FLAG_IMMOBILE_AMBIENT_ANIMATIONS));
        AssignCommand(commoner, SetSpawnInCondition(NW_FLAG_AMBIENT_ANIMATIONS));
        SetLocalString(commoner, "Conversation", "npcrumor");
        AddEventScript(SUBEVENT_CREATURE_ON_CONVERSATION, "cre_varconvo", commoner);
        string rumor = GetStringArrayElement(BIOME_RUMORS_ARRAY, RandomNext(availableRumorsNum), biome);
        SetLocalString(commoner, "Rumor", rumor);

        string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(commoner), GetGender(commoner), language, "default");
        string secondName = GetRandomCommonerLastName(townScript, GetRacialType(commoner), GetGender(commoner), language, "default");
        SetName(commoner, firstName + " " + secondName);
    }
}

// ---------------------------------------------------------------------

//Functions for deleting unused areas

void DeleteTemplateAreas()
{
    LogInfo("Deleting areas..");
    int areasLeft = 0;
    int areasDeleted = 0;
    object area = GetFirstArea();
    while (GetIsObjectValid(area))
    {
        string tag = GetTag(area);
        if (!GetIsObjectValid(GetTile(area)) && tag != "companion_area" && tag != "start" && tag != "limbo" && tag != "ending")
        {
            DestroyArea(area);
            areasDeleted++;
        }
        else
            areasLeft++;

        area = GetNextArea();
    }

    object PC = GetFirstPC();
    while (GetIsObjectValid(PC))
    {
        DelayCommand(0.1, ExploreAreaForPlayer(GetArea(PC), PC, TRUE));
        PC = GetNextPC();
    }

    LogInfo("Areas deleted: %n", areasDeleted);
    LogInfo("Areas left: %n", areasLeft);
}
