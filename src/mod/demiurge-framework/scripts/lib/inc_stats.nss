#include "inc_realms"
#include "inc_arrays"
// Library containing functions for managing adventure stats that will be displayed in the ending score screen

// Returns the final point score
int GetFinalScore(object oRealm);

// Returns the DIFFICULTY_* constant representing difficulty level the adventure was played in.
// If difficulty level was changed at some point in the game, the lowest difficulty level played on will be returned.
int GetAdventureDifficulty(object oRealm);

// Returns the number of players that played in the adventure.
// If the player number changed at some point in the game, the highest number of players in-game will be returned.
int GetHighestNumberOfPlayers(object oRealm);

// Returns the number of companions that played in the adventure.
int GetNumberOfCompanions(object oRealm);

// Returns the number of tile transitions by players that happened in the adventure
int GetTileTransitions(object oRealm);

// Returns the total number of bounty missions that have been completed in the adventure
int GetBountyMissionsCompleted(object oRealm);

// Returns the total number of respawns (NOT including resurrections and raises) by players in the adventure
int GetNumberOfRespawns(object oRealm);

// Returns gold total the players and their henchmen possess
int GetTotalPartyGold();

// Returns the number of quests that were refused or failed
int GetNumberOfQuestsRefusedOrFailed(object oRealm);

// Checks if current difficulty matches the one remembered on oRealm and if the current one is lower, overwrites it.
void UpdateAdventureDifficulty(object oRealm);

// Sets the difficulty of the adventure (should be used once at realm creation)
void SetAdventureDifficulty(object oRealm, int nDifficulty);

// Checks if current number of players matches the one remembered on oRealm and if the current one is higher, overwrites it.
void UpdateNumberOfPlayers(object oRealm);

// Sets the number of companions for the adventure (should be used once at realm creation)
void SetNumberOfCompanions(object oRealm, int nNumber);

// Adds 1 to the stored number of player tile transitions
void RegisterTileTransition(object oRealm);

// Adds 1 to the stored number of completed bounty missions
void RegisterBountyMissionCompleted(object oRealm);

// Adds 1 to the stored number of players respawns
void RegisterRespawn(object oRealm);

// Adds 1 to the stored number of quests refused or failed
void RegisterFailedQuest(object oRealm);



int GetFinalScore(object oRealm)
{
    int width = GetLocalInt(oRealm, "Width");
    int regsNum = GetStringArraySize(REALM_BIOMES_ARRAY, oRealm);
    int playersNum = GetHighestNumberOfPlayers(oRealm);

    int speedScore = 100 * ( 2*width*width - GetTileTransitions(oRealm) / playersNum );
    if (speedScore < 0)
        speedScore = 0;
    int bloodlustScore = 300 * GetBountyMissionsCompleted(oRealm);
    int survivabilityScore = 500 * regsNum - 150 * GetNumberOfRespawns(oRealm) / playersNum;
    if (survivabilityScore < 0)
        survivabilityScore = 0;
    int prosperityScore = GetTotalPartyGold() / 2;
    int resourcefulnessScore = 250 * (4 * regsNum - GetNumberOfQuestsRefusedOrFailed(oRealm));
    if (resourcefulnessScore < 0)
        resourcefulnessScore = 0;

    int difficulty = GetAdventureDifficulty(oRealm);
    float multiplier = 1.0;
    switch (difficulty)
    {
        case GAME_DIFFICULTY_VERY_EASY:
            multiplier = 0.1;
            break;
        case GAME_DIFFICULTY_EASY:
            multiplier = 0.25;
            break;
        case GAME_DIFFICULTY_NORMAL:
            multiplier = 0.5;
            break;
        case GAME_DIFFICULTY_CORE_RULES:
            multiplier = 1.0;
            break;
        case GAME_DIFFICULTY_DIFFICULT:
            multiplier = 2.0;
            break;
    }

    int totalScore = FloatToInt((speedScore + bloodlustScore + survivabilityScore + prosperityScore + resourcefulnessScore) * multiplier);
    return totalScore;
}

int GetAdventureDifficulty(object oRealm)
{
    return GetLocalInt(oRealm, "REALM_DIFFICULTY");
}

int GetHighestNumberOfPlayers(object oRealm)
{
    return GetLocalInt(oRealm, "REALM_PLAYERS");
}

int GetNumberOfCompanions(object oRealm)
{
    return GetLocalInt(oRealm, "REALM_COMPANIONS");
}

int GetTileTransitions(object oRealm)
{
    return GetLocalInt(oRealm, "REALM_TRANSITIONS");
}

int GetBountyMissionsCompleted(object oRealm)
{
    return GetLocalInt(oRealm, "REALM_BOUNTIES");
}

int GetNumberOfRespawns(object oRealm)
{
    return GetLocalInt(oRealm, "REALM_RESPAWNS");
}

int GetTotalPartyGold()
{
    int gold = 0;
    object PC = GetFirstPC();
    while (GetIsObjectValid(PC))
    {
        gold += GetGold(PC);
        PC = GetNextPC();
    }
    return gold;
}

int GetNumberOfQuestsRefusedOrFailed(object oRealm)
{
    return GetLocalInt(oRealm, "REALM_FAILS");
}

void UpdateAdventureDifficulty(object oRealm)
{
    int stored = GetAdventureDifficulty(oRealm);
    int current = GetGameDifficulty();
    if (current < stored)
        SetAdventureDifficulty(oRealm, current);
}

void SetAdventureDifficulty(object oRealm, int nDifficulty)
{
    SetLocalInt(oRealm, "REALM_DIFFICULTY", nDifficulty);
}

void UpdateNumberOfPlayers(object oRealm)
{
    int stored = GetHighestNumberOfPlayers(oRealm);
    int current = 0;
    object PC = GetFirstPC();
    while (GetIsObjectValid(PC))
    {
        current++;
        PC = GetNextPC();
    }
    if (current > stored)
        SetLocalInt(oRealm, "REALM_PLAYERS", current);
}

void SetNumberOfCompanions(object oRealm, int nNumber)
{
    SetLocalInt(oRealm, "REALM_COMPANIONS", nNumber);
}

void RegisterTileTransition(object oRealm)
{
    int current = GetTileTransitions(oRealm);
    LogInfo("Registering area transition. Current value: %n", current+1);
    SetLocalInt(oRealm, "REALM_TRANSITIONS", current+1);
}

void RegisterBountyMissionCompleted(object oRealm)
{
    int current = GetBountyMissionsCompleted(oRealm);
    SetLocalInt(oRealm, "REALM_BOUNTIES", current+1);
}

void RegisterRespawn(object oRealm)
{
    int current = GetNumberOfRespawns(oRealm);
    SetLocalInt(oRealm, "REALM_RESPAWNS", current+1);
}

void RegisterFailedQuest(object oRealm)
{
    int current = GetNumberOfQuestsRefusedOrFailed(oRealm);
    SetLocalInt(oRealm, "REALM_FAILS", current+1);
}