#include "inc_language"

// Library containing functions useful when transitioning between areas

// This function can be called when transitioning a PC between areas.
// It is meant to simulate an enemy pursuiting the player and possibly dealing them some damage before they manage to get away.
// Forces a PC to perform a dexterity check if they are in combat: d20 + dex modifier vs DC = 10 + biomeStartingLevel / 4
// and in case of a fail, makes the PC suffer damage equal to biomeStartingLevel * d4.
// Can also be made to execute the same logic for the PC's henchmen.
void ApplyPursuitDamage(object oPC, int nBiomeLevel, int nApplyToHenchmen=TRUE);


void ApplyPursuitDamage(object oPC, int nBiomeLevel, int nApplyToHenchmen=TRUE)
{
    if (GetIsInCombat(oPC))
    {
        int dexModifier = GetAbilityModifier(ABILITY_DEXTERITY, oPC);
        int DC = 10 + nBiomeLevel / 4;
        int roll = d20() + dexModifier;
        if (roll < DC)
        {
            ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDamage(d4(nBiomeLevel), DAMAGE_TYPE_POSITIVE), oPC);
            SendMessageToPC(oPC, GetLocalizedString("The enemy has dealt you damage in pursuit!", "Otrzymujesz obra�enia od �cigaj�cych ci� wrog�w!"));
        }
    }

    if (!nApplyToHenchmen)
        return;

    int i = 1;
    object hench = GetHenchman(oPC, i);
    while (GetIsObjectValid(hench))
    {
        if (GetLocalString(hench, "ID") != "" && GetIsInCombat(hench))
        {
            int dexModifier = GetAbilityModifier(ABILITY_DEXTERITY, hench);
            int DC = 10 + nBiomeLevel / 4;
            int roll = d20() + dexModifier;
            if (roll < DC)
            {
                ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDamage(d4(nBiomeLevel), DAMAGE_TYPE_POSITIVE), hench);
                string henchName = GetName(hench);
                SendMessageToPC(oPC, GetLocalizedString("The enemy has dealt damage to "+henchName+" in pursuit!", henchName+" otrzymuje obra�enia od �cigaj�cych was wrog�w!"));
            }
        }

        hench = GetHenchman(oPC, ++i);
    }
}