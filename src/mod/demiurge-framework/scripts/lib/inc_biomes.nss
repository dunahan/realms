#include "inc_debug"
#include "inc_hndvars"
#include "inc_arrays"
#include "inc_realms"
#include "inc_lootconsts"

const string BIOME_REGULAR_TILES_ARRAY = "BIOME_AREAS";
const string BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_1 = "BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_1";
const string BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_2 = "BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_2";
const string BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_3 = "BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_3";
const string BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_4 = "BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_4";
const string BIOME_INACTIVE_QUESTS = "BIOME_INACTIVE_QUESTS";
const string BIOME_RUMORS_ARRAY = "BIOME_RUMORS";
const string BIOME_ENCOUNTERS_WITH_CHAMPIONS = "BIOME_ENCOUNTERS_WITH_CHAMPIONS";

//Library for using various biome implementations dynamically

// Getter for the biome script's fogColorRed field
int GetBiomeFogColorRed(string sBiomeScript);

// Getter for the biome script's fogColorGreen field
int GetBiomeFogColorGreen(string sBiomeScript);

// Getter for the biome script's fogColorBlue field
int GetBiomeFogColorBlue(string sBiomeScript);

// Getter for the biome script's fogNightColorRed field
int GetBiomeFogNightColorRed(string sBiomeScript);

// Getter for the biome script's fogNightColorGreen field
int GetBiomeFogNightColorGreen(string sBiomeScript);

// Getter for the biome script's fogNightColorBlue field
int GetBiomeFogNightColorBlue(string sBiomeScript);

// Getter for the biome script's weatherChanceClear field
int GetBiomeWeatherChanceClear(string sBiomeScript);

// Getter for the biome script's weatherChanceRain field
int GetBiomeWeatherChanceRain(string sBiomeScript);

// Getter for the biome script's weatherChanceSnow field
int GetBiomeWeatherChanceSnow(string sBiomeScript);

// Getter for the biome script's skyboxClear field
int GetBiomeSkyboxClear(string sBiomeScript);

// Getter for the biome script's skyboxRain field
int GetBiomeSkyboxRain(string sBiomeScript);

// Getter for the biome script's skyboxSnow field
int GetBiomeSkyboxSnow(string sBiomeScript);

// Getter for the biome script's mapRed field
int GetBiomeMapColorRed(string sBiomeScript);

// Getter for the biome script's mapGreen field
int GetBiomeMapColorGreen(string sBiomeScript);

// Getter for the biome script's mapBlue field
int GetBiomeMapColorBlue(string sBiomeScript);

// Getter for the biome script's areasId field
string GetBiomeAreasIdentifier(string sBiomeScript);

// Getter for the biome script's regularAreasNumber field
int GetBiomeRegularAreasNumber(string sBiomeScript);

// Getter for the biome script's restEncounterChance field
float GetBiomeRestEncounterChance(string sBiomeScript);

// Getter for the biome script's basicEncountersNumber field
int GetBiomeBasicEncountersNumber(string sBiomeScript);

// Getter for the biome script's advancedEncountersNumber field
int GetBiomeAdvancedEncountersNumber(string sBiomeScript);

// Getter for the biome script's advancedEncounterChance field
int GetBiomeAdvancedEncounterChance(string sBiomeScript);

// Getter for the biome script's specialAreasNumber field
int GetBiomeSpecialAreasNumber(string sBiomeScript);

// Getter for the biome script's townsNumber field
int GetBiomeTownsNumber(string sBiomeScript);

// Getter for the biome script's dayTracksNumber field
int GetBiomeDayTracksNumber(string sBiomeScript);

// Getter for the biome script's nightTracksNumber field
int GetBiomeNightTracksNumber(string sBiomeScript);

// Getter for the biome script's battleTracksNumber field
int GetBiomeBattleTracksNumber(string sBiomeScript);

// Getter for the biome script's rumorsNumber field
int GetBiomeRumorsNumber(string sBiomeScript);

// Returns a ResRef of a regular area to spawn corresponding to the index nIndex in range [0, regularAreasNumber-1] and an exits flag representing which exits should be available for a given biome script
string GetBiomeRegularAreaResRef(string sBiomeScript, int nIndex, int nExitsFlag);

// Returns a biome's basic encounter script's name corresponding to the index nIndex in range [0, basicEncountersNumber-1]
string GetBiomeBasicEncounterScript(string sBiomeScript, int nIndex);

// Returns a biome's advanced encounter script's name corresponding to the index nIndex in range [0, advancedEncountersNumber-1]
string GetBiomeAdvancedEncounterScript(string sBiomeScript, int nIndex);

// Returns a biome's special area script's name corresponding to the index nIndex in range [0, specialAreasNumber-1]
string GetBiomeSpecialAreaScript(string sBiomeScript, int nIndex);

// Returns a biome's town script's name corresponding to the index nIndex in range [0, townsNumber-1]
string GetBiomeTownScript(string sBiomeScript, int nIndex);

// Returns a biome's day music track's constant (TRACK_*, but it can also be an integer that has no constant assigned) corresponding to the index nIndex in range [0, dayTracksNumber-1]
int GetBiomeDayTrack(string sBiomeScript, int nIndex);

// Returns a biome's night music track's constant (TRACK_*, but it can also be an integer that has no constant assigned) corresponding to the index nIndex in range [0, nightTracksNumber-1]
int GetBiomeNightTrack(string sBiomeScript, int nIndex);

// Returns a biome's battle music track's constant (TRACK_*, but it can also be an integer that has no constant assigned) corresponding to the index nIndex in range [0, battleTracksNumber-1]
int GetBiomeBattleTrack(string sBiomeScript, int nIndex);

// Returns a biome's rumor corresponding to the index nIndex in range [0, rumorsNumber-1] for a given LANGUAGE_* constant from inc_language
string GetBiomeRumor(string sBiomeScript, int nIndex, int nLanguage);

// Returns a random name for a region of the given biome, based on a LANGUAGE_* constant from inc_language
string GetRandomBiomeRegionName(string sBiomeScript, string sSeedName, int nLanguage);

// Returns random loot for a given biome script according to PC/region's level and the loot's relative quality for that level (LOOT_QUALITY_* constant)
struct TreasureChestLoot GetBiomeLoot(string sBiomeScript, string sSeedName, int nLevel, int nLootQuality);

// Prepares a rest encounter for a given biome script and expected level of the PC party
void PrepareRestEncounter(string sBiomeScript, int nLevel);

// Spawns a resource placeable at lResourceLocation appropriate for the given biome level and resource location (underground or not) for a given biome script
object SpawnResourcePlaceable(string sBiomeScript, string sSeedName, location lResourceLocation, int nLevel, int nIsUndergroundResource);

//Instance functions

// Biome object constructor - creates a biome (region) object
object CreateBiomeInstance(string sBiomeScript, object oRealm, int nRegionID, int nRegionOrder, string sRegionName);

// Returns the given biome's script name
string GetBiomeScript(object oBiome);

// Returns the biome's town object
object GetTownOfBiome(object oBiome);

// Sets the biome's town object
void SetTownOfBiome(object oBiome, object oTown);

// Returns the biome's unique region ID
int GetRegionIDOfBiome(object oBiome);

// Returns the biome's sequential number (starting with 1 for the starting region)
int GetRegionOrderNumber(object oBiome);

// Returns the level PCs should start the region with
int GetRegionStartingLevel(object oBiome);

// Returns the counter of completed quests in the biome
int GetRegionCompletedQuests(object oBiome);

// Increases the counter of completed quests in the biome by 1
void IncrementRegionCompletedQuests(object oBiome);

// Returns the counter of completed, failed or refused quests in the biome
int GetRegionPastQuests(object oBiome);

// Increases the counter of completed, failed or refused quests in the biome by 1
void IncrementRegionPastQuests(object oBiome);

// Returns the name of the biome instance
string GetRegionName(object oBiome);



//Script functions
int GetBiomeFogColorRed(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 0);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0 || result > 255)
        LogWarning("Biome " + sBiomeScript + " returned invalid fogColorRed value: %n", result);
    return result;
}

int GetBiomeFogColorGreen(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 1);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0 || result > 255)
        LogWarning("Biome " + sBiomeScript + " returned invalid fogColorGreen value: %n", result);
    return result;
}

int GetBiomeFogColorBlue(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 2);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0 || result > 255)
        LogWarning("Biome " + sBiomeScript + " returned invalid fogColorBlue value: %n", result);
    return result;
}

int GetBiomeFogNightColorRed(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 40);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0 || result > 255)
        LogWarning("Biome " + sBiomeScript + " returned invalid fogNightColorRed value: %n", result);
    return result;
}

int GetBiomeFogNightColorGreen(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 41);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0 || result > 255)
        LogWarning("Biome " + sBiomeScript + " returned invalid fogNightColorGreen value: %n", result);
    return result;
}

int GetBiomeFogNightColorBlue(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 42);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0 || result > 255)
        LogWarning("Biome " + sBiomeScript + " returned invalid fogNightColorBlue value: %n", result);
    return result;
}

int GetBiomeWeatherChanceClear(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 3);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0 || result > 100)
        LogWarning("Biome " + sBiomeScript + " returned invalid weatherChanceClear value: %n", result);
    return result;
}

int GetBiomeWeatherChanceRain(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 4);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0 || result > 100)
        LogWarning("Biome " + sBiomeScript + " returned invalid weatherChanceRain value: %n", result);
    return result;
}

int GetBiomeWeatherChanceSnow(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 5);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0 || result > 100)
        LogWarning("Biome " + sBiomeScript + " returned invalid weatherChanceSnow value: %n", result);
    return result;
}

int GetBiomeSkyboxClear(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 6);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetBiomeSkyboxRain(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 7);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetBiomeSkyboxSnow(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 8);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetBiomeMapColorRed(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 9);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0 || result > 255)
        LogWarning("Biome " + sBiomeScript + " returned invalid mapRed value: %n", result);
    return result;
}

int GetBiomeMapColorGreen(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 10);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0 || result > 255)
        LogWarning("Biome " + sBiomeScript + " returned invalid mapGreen value: %n", result);
    return result;
}

int GetBiomeMapColorBlue(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 11);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0 || result > 255)
        LogWarning("Biome " + sBiomeScript + " returned invalid mapBlue value: %n", result);
    return result;
}

string GetBiomeAreasIdentifier(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 12);
    ExecuteScript(sBiomeScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogFatal("Biome " + sBiomeScript + " returned empty areasId value");
    return result;
}

int GetBiomeRegularAreasNumber(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 13);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogFatal("Biome " + sBiomeScript + " returned invalid regularAreasNumber value: %n", result);
    return result;
}

int GetBiomeUndergroundResourcesNumber(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 14);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 1)
        LogWarning("Biome " + sBiomeScript + " returned invalid undergroundResourcesNumber value: %n", result);
    return result;
}

int GetBiomeOverworldResourcesNumber(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 15);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 1)
        LogWarning("Biome " + sBiomeScript + " returned invalid overworldResourcesNumber value: %n", result);
    return result;
}

int GetBiomeBasicEncountersNumber(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 16);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Biome " + sBiomeScript + " returned invalid basicEncountersNumber value: %n", result);
    return result;
}

int GetBiomeAdvancedEncountersNumber(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 44);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Biome " + sBiomeScript + " returned invalid advancedEncountersNumber value: %n", result);
    return result;
}

int GetBiomeAdvancedEncounterChance(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 45);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0 || result > 100)
        LogWarning("Biome " + sBiomeScript + " returned invalid advancedEncounterChance value: %n", result);
    return result;
}

int GetBiomeSpecialAreasNumber(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 17);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Biome " + sBiomeScript + " returned invalid specialAreasNumber value: %n", result);
    return result;
}

int GetBiomeTownsNumber(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 18);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Biome " + sBiomeScript + " returned invalid townsNumber value: %n", result);
    return result;
}

int GetBiomeDayTracksNumber(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 19);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Biome " + sBiomeScript + " returned invalid dayTracksNumber value: %n", result);
    return result;
}

int GetBiomeNightTracksNumber(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 20);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Biome " + sBiomeScript + " returned invalid nightTracksNumber value: %n", result);
    return result;
}

int GetBiomeBattleTracksNumber(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 21);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Biome " + sBiomeScript + " returned invalid battleTracksNumber value: %n", result);
    return result;
}

int GetBiomeRumorsNumber(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 22);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 0)
        LogWarning("Biome " + sBiomeScript + " returned negative rumorsNumber value: %n", result);
    return result;
}

string GetBiomeRegularAreaResRef(string sBiomeScript, int nIndex, int nExitsFlag)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 25);
    SetLocalInt(mod, funcArg1, nIndex);
    SetLocalInt(mod, funcArg2, nExitsFlag);
    ExecuteScript(sBiomeScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Biome " + sBiomeScript + " returned invalid regular area ResRef, nIndex = %n, nExitsFlag = %n", nIndex, nExitsFlag);
    return result;
}

string GetBiomeBasicEncounterScript(string sBiomeScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 27);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sBiomeScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Biome " + sBiomeScript + " returned invalid basic encounter script name, nIndex = %n", nIndex);
    return result;
}

string GetBiomeAdvancedEncounterScript(string sBiomeScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 43);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sBiomeScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Biome " + sBiomeScript + " returned invalid advanced encounter script name, nIndex = %n", nIndex);
    return result;
}

string GetBiomeSpecialAreaScript(string sBiomeScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 28);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sBiomeScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Biome " + sBiomeScript + " returned invalid special area script name, nIndex = %n", nIndex);
    return result;
}

string GetBiomeTownScript(string sBiomeScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 29);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sBiomeScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Biome " + sBiomeScript + " returned invalid town script name, nIndex = %n", nIndex);
    return result;
}

int GetBiomeDayTrack(string sBiomeScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 30);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetBiomeNightTrack(string sBiomeScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 31);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetBiomeBattleTrack(string sBiomeScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 32);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sBiomeScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

string GetBiomeRumor(string sBiomeScript, int nIndex, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 33);
    SetLocalInt(mod, funcArg1, nIndex);
    SetLocalInt(mod, funcArg2, nLanguage);
    ExecuteScript(sBiomeScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Biome " + sBiomeScript + " returned invalid rumor, nIndex = %n, nLanguage = %n", nIndex, nLanguage);
    return result;
}

string GetRandomBiomeRegionName(string sBiomeScript, string sSeedName, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 34);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalInt(mod, funcArg2, nLanguage);
    ExecuteScript(sBiomeScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid region name returned in realm " + sBiomeScript + ", sSeedName = " + sSeedName + ", nLanguage = %n", nLanguage);
    return result;
}

struct TreasureChestLoot GetBiomeLoot(string sBiomeScript, string sSeedName, int nLevel, int nLootQuality)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 35);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalInt(mod, funcArg2, nLevel);
    SetLocalInt(mod, funcArg3, nLootQuality);
    ExecuteScript(sBiomeScript);
    struct TreasureChestLoot result;
    result.gold = GetLocalInt(mod, funcResult);
    result.itemResRef = GetLocalString(mod, funcResult2);
    result.itemStackSize = GetLocalInt(mod, funcResult3);
    return result;
}

void PrepareRestEncounter(string sBiomeScript, int nLevel)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 36);
    SetLocalInt(mod, funcArg1, nLevel);
    ExecuteScript(sBiomeScript);
}

float GetBiomeRestEncounterChance(string sBiomeScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 37);
    ExecuteScript(sBiomeScript);
    float result = GetLocalFloat(mod, funcResult);
    return result;
}

object SpawnResourcePlaceable(string sBiomeScript, string sSeedName, location lResourceLocation, int nLevel, int nIsUndergroundResource)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 39);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalLocation(mod, funcArg2, lResourceLocation);
    SetLocalInt(mod, funcArg3, nLevel);
    SetLocalInt(mod, funcArg4, nIsUndergroundResource);
    ExecuteScript(sBiomeScript);
    object result = GetLocalObject(mod, funcResult);
    return result;
}



// Instance functions

int _CalculateStartingLevel(object oRealm, int nRegionOrder)
{
    string realmScript = GetRealmScript(oRealm);
    int levelsPerRegion = GetNumberOfLevelsPerRegion(realmScript);
    int realmStartingLvl = GetRealmStartingLevel(realmScript);
    int biomeStartingLvl = realmStartingLvl + (nRegionOrder-1) * levelsPerRegion;
    return biomeStartingLvl;
}

//Constructor
object CreateBiomeInstance(string sBiomeScript, object oRealm, int nRegionID, int nRegionOrder, string sRegionName)
{
    object biomeObject = CreateObject(OBJECT_TYPE_PLACEABLE, "obj_invisible", GetStartingLocation());
    SetLocalString(biomeObject, "BIOME_SCRIPT", sBiomeScript);
    SetLocalString(biomeObject, "BIOME_NAME", sRegionName);
    AddObjectArrayElement(REALM_BIOMES_ARRAY, biomeObject, FALSE, oRealm);
    SetLocalObject(oRealm, "REALM_BIOME_" + IntToString(nRegionID), biomeObject);
    SetLocalObject(biomeObject, "BIOME_REALM", oRealm);
    SetLocalInt(biomeObject, "BIOME_ID", nRegionID);
    SetLocalInt(biomeObject, "BIOME_ORDER", nRegionOrder);
    CreateObjectArray(BIOME_REGULAR_TILES_ARRAY, 0, biomeObject);
    CreateStringArray(BIOME_RUMORS_ARRAY, 0, biomeObject);
    CreateObjectArray(BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_1, 0, biomeObject);
    CreateObjectArray(BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_2, 0, biomeObject);
    CreateObjectArray(BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_3, 0, biomeObject);
    CreateObjectArray(BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_4, 0, biomeObject);
    CreateObjectArray(BIOME_INACTIVE_QUESTS, 0, biomeObject);
    CreateObjectArray(BIOME_ENCOUNTERS_WITH_CHAMPIONS, 0, biomeObject);

    //calculate starting level
    int startingLvl = _CalculateStartingLevel(oRealm, nRegionOrder);
    SetLocalInt(biomeObject, "BIOME_STARTINGLVL", startingLvl);

    return biomeObject;
}

//Instance functions
string GetBiomeScript(object oBiome)
{
    return GetLocalString(oBiome, "BIOME_SCRIPT");
}

object GetTownOfBiome(object oBiome)
{
    return GetLocalObject(oBiome, "BIOME_TOWN");
}

int GetRegionIDOfBiome(object oBiome)
{
    return GetLocalInt(oBiome, "BIOME_ID");
}

int GetRegionOrderNumber(object oBiome)
{
    return GetLocalInt(oBiome, "BIOME_ORDER");
}

void SetTownOfBiome(object oBiome, object oTown)
{
    SetLocalObject(oBiome, "BIOME_TOWN", oTown);
}

int GetRegionStartingLevel(object oBiome)
{
    return GetLocalInt(oBiome, "BIOME_STARTINGLVL");
}

string GetRegionName(object oBiome)
{
    return GetLocalString(oBiome, "BIOME_NAME");
}

int GetRegionCompletedQuests(object oBiome)
{
    return GetLocalInt(oBiome, "BIOME_COMPLETEDQUESTS");
}

void IncrementRegionCompletedQuests(object oBiome)
{
    int counter = GetRegionCompletedQuests(oBiome);
    SetLocalInt(oBiome, "BIOME_COMPLETEDQUESTS", counter+1);
}

int GetRegionPastQuests(object oBiome)
{
    return GetLocalInt(oBiome, "BIOME_PASTQUESTS");
}

void IncrementRegionPastQuests(object oBiome)
{
    int counter = GetRegionPastQuests(oBiome);
    SetLocalInt(oBiome, "BIOME_PASTQUESTS", counter+1);
}