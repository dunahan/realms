#include "inc_debug"
#include "inc_hndvars"
#include "inc_arrays"
#include "inc_tiles"
#include "inc_towns"

//Library for using various facility implementations dynamically

// Getter for the facility script's areasNumber field
int GetNumberOfFacilityAreas(string sFacilityScript);

// Getter for the facility script's minPlayerLevel field
int GetFacilityMinPlayerLevel(string sFacilityScript);

// Getter for the facility script's maxPlayerLevel field
int GetFacilityMaxPlayerLevel(string sFacilityScript);

// Getter for the facility's rumorsNumber field
int GetFacilityRumorsNumber(string sFacilityScript);

// Returns the facility's area ResRef corresponding to index nIndex in range [0, areasNumber-1] and based on town the facility is in
// (so that a snow biome will spawn a different [snowy] area than a plains biome, for example)
string GetFacilityAreaResRef(string sFacilityScript, int nIndex, object oTown);

// Initialize the facility (whose main area is oFacility)
void InitializeFacility(string sSeedName, object oTown, object oFacility, int nBiomeStartingLevel);

// Returns the facility's rumor corresponding to index nIndex in range [0, rumorsNumber-1]
string GetFacilityRumor(object oFacility, int nIndex, int nLanguage);

// Returns a random facility name for a given facility script, based on a LANGUAGE_* constant from inc_language
string GetRandomFacilityName(string sFacilityScript, string sSeedName, int nLanguage);

//Instance functions

// Facility area initialization (InitializeFacility in inc_facilities needs to be called, too, to perform custom facility actions)
void InitializeFacilityInstance(string sFacilityScript, object oFacility, object oTown);

// Returns the given facility's script name
string GetFacilityScript(object oFacility);

// Returns the facility's town
object GetTownOfFacility(object oFacility);



//Script functions
int GetNumberOfFacilityAreas(string sFacilityScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 0);
    ExecuteScript(sFacilityScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Facility " + sFacilityScript + " returned invalid areasNumber value: %n", result);
    return result;
}

int GetFacilityMinPlayerLevel(string sFacilityScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 1);
    ExecuteScript(sFacilityScript);
    return GetLocalInt(mod, funcResult);
}

int GetFacilityMaxPlayerLevel(string sFacilityScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 2);
    ExecuteScript(sFacilityScript);
    return GetLocalInt(mod, funcResult);
}

int GetFacilityRumorsNumber(string sFacilityScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 3);
    ExecuteScript(sFacilityScript);
    return GetLocalInt(mod, funcResult);
}

string GetFacilityAreaResRef(string sFacilityScript, int nIndex, object oTown)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 4);
    SetLocalInt(mod, funcArg1, nIndex);
    SetLocalObject(mod, funcArg2, oTown);
    ExecuteScript(sFacilityScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid area resref returned in facility " + sFacilityScript + ", nIndex = %n", nIndex);
    return result;
}

void InitializeFacility(string sSeedName, object oTown, object oFacility, int nBiomeStartingLevel)
{
    object mod = GetModule();
    string facilityScript = GetFacilityScript(oFacility);
    SetLocalInt(mod, funcHandler, 5);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalObject(mod, funcArg2, oTown);
    SetLocalObject(mod, funcArg3, oFacility);
    SetLocalInt(mod, funcArg4, nBiomeStartingLevel);
    ExecuteScript(facilityScript);
}

string GetFacilityRumor(object oFacility, int nIndex, int nLanguage)
{
    object mod = GetModule();
    string facilityScript = GetFacilityScript(oFacility);
    SetLocalInt(mod, funcHandler, 6);
    SetLocalObject(mod, funcArg1, oFacility);
    SetLocalInt(mod, funcArg2, nIndex);
    SetLocalInt(mod, funcArg3, nLanguage);
    ExecuteScript(facilityScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid rumor returned in facility " + facilityScript + ", nIndex = %n, nLanguage = %n", nIndex, nLanguage);
    return result;
}

string GetRandomFacilityName(string sFacilityScript, string sSeedName, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 7);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalInt(mod, funcArg2, nLanguage);
    ExecuteScript(sFacilityScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid facility name returned in " + sFacilityScript + ", sSeedName = " + sSeedName + ", nLanguage = %n", nLanguage);
    return result;
}

//Instance functions

//Initializator
void InitializeFacilityInstance(string sFacilityScript, object oFacility, object oTown)
{
    SetLocalObject(oFacility, "FACILITY_TOWN", oTown);
    SetFacilityOfTown(oTown, oFacility);
    AddAreaToTile(oTown, oFacility);
    SetLocalString(oFacility, "FACILITY_SCRIPT", sFacilityScript);   
}

//Instance functions
string GetFacilityScript(object oFacility)
{
    return GetLocalString(oFacility, "FACILITY_SCRIPT");
}

object GetTownOfFacility(object oFacility)
{
    return GetLocalObject(oFacility, "FACILITY_TOWN");
}