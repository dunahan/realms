#include "inc_random"
#include "inc_biomes"
#include "inc_language"
#include "inc_debug"

// This library contains functions for generating random names of people, taverns, towns and other places


// Constants

const int PL_TO = 1;
const int PL_TEN = 2;
const int PL_TA = 3;
const int PL_TE = 4;

// Headers

// Returns a randomly generated realm name based on a given random seed.
string RandomRealmName(string sSeedName="default");

// Returns a randomly generated name of a region based on a given random seed, taking its biome into account.
string RandomRegionName(string sBiomeScript, string sSeedName="default");

// Returns a randomly generated town name based on a given random seed.
string RandomTownName(string sSeedName="default");


// Definitions

int plVariant;

string _GetPolishAdjectiveVariant(string sWord)
{
    string wordEnding = GetStringRight(sWord, 1);
    string wordWithoutEnding = GetStringLeft(sWord, GetStringLength(sWord)-1);
    if (wordEnding == "y")
    {
        switch (plVariant)
        {
            case PL_TO: wordEnding = "e"; break;
            case PL_TEN: wordEnding = "y"; break;
            case PL_TA: wordEnding = "a"; break;
            case PL_TE: wordEnding = "e"; break;
        }
    }
    else if (wordEnding == "i")
    {
        switch (plVariant)
        {
            case PL_TO: wordEnding = "ie"; break;
            case PL_TEN: wordEnding = "i"; break;
            case PL_TA: wordEnding = "a"; break;
            case PL_TE: wordEnding = "ie"; break;
        }
    }
    return wordWithoutEnding + wordEnding;
}

string RandomRealmName(string sSeedName="default")
{
    //First part of the main name
    string firstPart;
    switch (RandomNext(35, sSeedName))
    {
        case 0: firstPart = "Blar"; break;
        case 1: firstPart = "Reo"; break;
        case 2: firstPart = "Chim"; break;
        case 3: firstPart = "Kle"; break;
        case 4: firstPart = "Vri"; break;
        case 5: firstPart = "Bre"; break;
        case 6: firstPart = "Ixi"; break;
        case 7: firstPart = "Ro"; break;
        case 8: firstPart = "Eom"; break;
        case 9: firstPart = "Glio"; break;
        case 10: firstPart = "Yea"; break;
        case 11: firstPart = "Cu"; break;
        case 12: firstPart = "Da"; break;
        case 13: firstPart = "E"; break;
        case 14: firstPart = "A"; break;
        case 15: firstPart = "I"; break;
        case 16: firstPart = "U"; break;
        case 17: firstPart = "Se"; break;
        case 18: firstPart = "Ia"; break;
        case 19: firstPart = "Bre"; break;
        case 20: firstPart = "Fea"; break;
        case 21: firstPart = "Grac"; break;
        case 22: firstPart = "Ste"; break;
        case 23: firstPart = "Iod"; break;
        case 24: firstPart = "Ia"; break;
        case 25: firstPart = "Ta"; break;
        case 26: firstPart = "Me"; break;
        case 27: firstPart = "Stre"; break;
        case 28: firstPart = "Awe"; break;
        case 29: firstPart = "Oclo"; break;
        case 30: firstPart = "Wo"; break;
        case 31: firstPart = "Leo"; break;
        case 32: firstPart = "Vre"; break;
        case 33: firstPart = "Pio"; break;
        case 34: firstPart = "Xo"; break;
    }

    //Second part of the main name
    string secondPart;
    switch (RandomNext(35, sSeedName))
    {
        case 0: secondPart = "ri"; break;
        case 1: secondPart = "sa"; break;
        case 2: secondPart = "mu"; break;
        case 3: secondPart = "xo"; break;
        case 4: secondPart = "gath"; break;
        case 5: secondPart = "beon"; break;
        case 6: secondPart = "da"; break;
        case 7: secondPart = "ve"; break;
        case 8: secondPart = "mio"; break;
        case 9: secondPart = "bea"; break;
        case 10: secondPart = "kel"; break;
        case 11: secondPart = "kle"; break;
        case 12: secondPart = "xio"; break;
        case 13: secondPart = "shi"; break;
        case 14: secondPart = "me"; break;
        case 15: secondPart = "bro"; break;
        case 16: secondPart = "wea"; break;
        case 17: secondPart = "alli"; break;
        case 18: secondPart = "lled"; break;
        case 19: secondPart = "con"; break;
        case 20: secondPart = "kame"; break;
        case 21: secondPart = "ri"; break;
        case 22: secondPart = "rui"; break;
        case 23: secondPart = "te"; break;
        case 24: secondPart = "fio"; break;
        case 25: secondPart = "teo"; break;
        case 26: secondPart = "shea"; break;
        case 27: secondPart = "ofe"; break;
        case 28: secondPart = "ada"; break;
        case 29: secondPart = "tu"; break;
        case 30: secondPart = "bar"; break;
        case 31: secondPart = "griar"; break;
        case 32: secondPart = "axi"; break;
        case 33: secondPart = "bbi"; break;
        case 34: secondPart = "ga"; break;
    }

    //Third part of the main name
    string thirdPart;
    switch (RandomNext(35, sSeedName))
    {
        case 0: thirdPart = "mos"; break;
        case 1: thirdPart = "sos"; break;
        case 2: thirdPart = "mar"; break;
        case 3: thirdPart = "rah"; break;
        case 4: thirdPart = "aer"; break;
        case 5: thirdPart = "ara"; break;
        case 6: thirdPart = "lar"; break;
        case 7: thirdPart = "ron"; break;
        case 8: thirdPart = "lan"; break;
        case 9: thirdPart = "ran"; break;
        case 10: thirdPart = "ion"; break;
        case 11: thirdPart = "riel"; break;
        case 12: thirdPart = "dran"; break;
        case 13: thirdPart = "ran"; break;
        case 14: thirdPart = "gon"; break;
        case 15: thirdPart = "gan"; break;
        case 16: thirdPart = "man"; break;
        case 17: thirdPart = "mis"; break;
        case 18: thirdPart = "mes"; break;
        case 19: thirdPart = "rial"; break;
        case 20: thirdPart = "non"; break;
        case 21: thirdPart = "dell"; break;
        case 22: thirdPart = "ven"; break;
        case 23: thirdPart = "nem"; break;
        case 24: thirdPart = "qar"; break;
        case 25: thirdPart = "lon"; break;
        case 26: thirdPart = "zan"; break;
        case 27: thirdPart = "thra"; break;
        case 28: thirdPart = "nem"; break;
        case 29: thirdPart = "ary"; break;
        case 30: thirdPart = "ene"; break;
        case 31: thirdPart = "ona"; break;
        case 32: thirdPart = "que"; break;
        case 33: thirdPart = "nys"; break;
        case 34: thirdPart = "cia"; break;
    }

    AssertStringNotEqual("", firstPart, "(inc_names, RandomRealmName, firstPart)");
    AssertStringNotEqual("", secondPart, "(inc_names, RandomRealmName, secondPart)");
    AssertStringNotEqual("", thirdPart, "(inc_names, RandomRealmName, thirdPart)");

    return firstPart + secondPart + thirdPart;
}

string RandomRegionName(string sBiomeScript, string sSeedName="default")
{
    //First part
    int randResult = RandomNext(16, sSeedName);
    string firstPart;
    if (randResult < 10)
    {
        switch (randResult)
        {
            case 0: firstPart = GetLocalizedString("Expanse", "Roz��g"); plVariant = PL_TEN; break;
            case 1: firstPart = GetLocalizedString("Country", "Ziemia"); plVariant = PL_TA; break;
            case 2: firstPart = GetLocalizedString("Territories", "Terytorium"); plVariant = PL_TO; break;
            case 3: firstPart = GetLocalizedString("Province", "Prowincja"); plVariant = PL_TA; break;
            case 4: firstPart = GetLocalizedString("Domain", "Domena"); plVariant = PL_TA; break;
            case 5: firstPart = GetLocalizedString("Land", "L�d"); plVariant = PL_TEN; break;
            case 6: firstPart = GetLocalizedString("Region", "Region"); plVariant = PL_TEN; break;
            case 7: firstPart = GetLocalizedString("Sanctum", "Sanktuarium"); plVariant = PL_TO; break;
            case 8: firstPart = GetLocalizedString("Dominion", "Dominium"); plVariant = PL_TO; break;
            case 9: firstPart = GetLocalizedString("Reach", "Tereny"); plVariant = PL_TE; break;
        }
    }
    else if (sBiomeScript == "bio_rural")
    {
        switch (randResult)
        {
            case 10: firstPart = GetLocalizedString("Hills", "Wzg�rza"); plVariant = PL_TE; break;
            case 11: firstPart = GetLocalizedString("Valley", "Dolina"); plVariant = PL_TA; break;
            case 12: firstPart = GetLocalizedString("Lowlands", "Niziny"); plVariant = PL_TE; break;
            case 13: firstPart = GetLocalizedString("Fields", "Pola"); plVariant = PL_TE; break;
            case 14: firstPart = GetLocalizedString("Plains", "R�wniny"); plVariant = PL_TE; break;
            case 15: firstPart = GetLocalizedString("Grasslands", "��ki"); plVariant = PL_TE; break;
        }
    }
    else if (sBiomeScript == "")
    {
        switch (randResult)
        {
            case 10: firstPart = GetLocalizedString("Forest", "Las"); plVariant = PL_TEN; break;
            case 11: firstPart = GetLocalizedString("Wilderness", "Puszcza"); plVariant = PL_TA; break;
            case 12: firstPart = GetLocalizedString("Woods", "Knieja"); plVariant = PL_TA; break;
            case 13: firstPart = GetLocalizedString("Groves", "Gaj"); plVariant = PL_TEN; break;
            case 14: firstPart = GetLocalizedString("Woodland", "B�r"); plVariant = PL_TEN; break;
            case 15: firstPart = GetLocalizedString("Thicket", "Ost�py"); plVariant = PL_TE; break;
        }
    }
    else if (sBiomeScript == "")
    {
        switch (randResult)
        {
            case 10: firstPart = GetLocalizedString("Highlands", "Wy�yny"); plVariant = PL_TE; break;
            case 11: firstPart = GetLocalizedString("Mountains", "G�ry"); plVariant = PL_TE; break;
            case 12: firstPart = GetLocalizedString("Snowland", "�niegi"); plVariant = PL_TE; break;
            case 13: firstPart = GetLocalizedString("Taiga", "Tajga"); plVariant = PL_TA; break;
            case 14: firstPart = GetLocalizedString("Peaks", "Szczyty"); plVariant = PL_TE; break;
            case 15: firstPart = GetLocalizedString("Ridge", "Gra�"); plVariant = PL_TA; break;
        }
    }
    else if (sBiomeScript == "")
    {
        switch (randResult)
        {
            case 10: firstPart = GetLocalizedString("Sands", "Piaski"); plVariant = PL_TE; break;
            case 11: firstPart = GetLocalizedString("Desert", "Pustynia"); plVariant = PL_TA; break;
            case 12: firstPart = GetLocalizedString("Desolation", "Pustkowie"); plVariant = PL_TO; break;
            case 13: firstPart = GetLocalizedString("Seclusion", "Odludzie"); plVariant = PL_TO; break;
            case 14: firstPart = GetLocalizedString("Canyons", "Kaniony"); plVariant = PL_TE; break;
            case 15: firstPart = GetLocalizedString("Dunes", "Wydmy"); plVariant = PL_TE; break;
        }
    }
    else if (sBiomeScript == "") //more to add + underdark
    {
        switch (randResult)
        {
            case 10: firstPart = GetLocalizedString("Jungle", "D�ungla"); plVariant = PL_TA; break;
            case 11: firstPart = GetLocalizedString("Rainforest", "Tropik"); plVariant = PL_TEN; break;
            case 12: firstPart = GetLocalizedString("Bushes", "G�szcz"); plVariant = PL_TEN; break;
        }
    }


    //Second part
    string secondPart;
    switch (RandomNext(48, sSeedName))
    {
        case 0: secondPart = GetLocalizedString("of the Night", "Nocy"); break;
        case 1: secondPart = GetLocalizedString("of Swords", "Mieczy"); break;
        case 2: secondPart = GetLocalizedString("of Life", "�ycia"); break;
        case 3: secondPart = GetLocalizedString("of Death", "�mierci"); break;
        case 4: secondPart = GetLocalizedString("of Fire", "Ognia"); break;
        case 5: secondPart = GetLocalizedString("of Water", "Wody"); break;
        case 6: secondPart = GetLocalizedString("of Earth", "Ziemi"); break;
        case 7: secondPart = GetLocalizedString("of Wind", "Wiatru"); break;
        case 8: secondPart = GetLocalizedString("of Dragons", "Smok�w"); break;
        case 9: secondPart = GetLocalizedString("of the Ancients", "Staro�ytnych"); break;
        case 10: secondPart = GetLocalizedString("of Thieves", "Z�odziei"); break;
        case 11: secondPart = GetLocalizedString("of Rogues", "�otr�w"); break;
        case 12: secondPart = GetLocalizedString("of Gods", "Bog�w"); break;
        case 13: secondPart = GetLocalizedString("of Trickery", "Podst�p�w"); break;
        case 14: secondPart = GetLocalizedString("of Forgotten Battles", "Zapomnianych Bitew"); break;
        case 15: secondPart = GetLocalizedString("of Cloudy Sky", "Zachmurzonego Nieba"); break;
        case 16: secondPart = GetLocalizedString("of the Alchemist", "Alchemika"); break;
        case 17: secondPart = GetLocalizedString("of Mystics", "Mistyk�w"); break;
        case 18: secondPart = GetLocalizedString("of Outlaws", "Banit�w"); break;
        case 19: secondPart = GetLocalizedString("of Stone", "Kamienia"); break;
        case 20: secondPart = GetLocalizedString("of Tears", "�ez"); break;
        case 21: secondPart = GetLocalizedString("of Honor", "Honoru"); break;
        case 22: secondPart = GetLocalizedString("of Merchants", "Kupc�w"); break;
        case 23: secondPart = GetLocalizedString("of the Faithful", "Wiernych"); break;
        case 24: secondPart = GetLocalizedString("of Mercenaries", "Najemnik�w"); break;
        case 25: secondPart = GetLocalizedString("of the Bird", "Ptaka"); break;
        case 26: secondPart = GetLocalizedString("of Beasts", "Bestii"); break;
        case 27: secondPart = GetLocalizedString("of the Moon", "Ksi�yca"); break;
        case 28: secondPart = GetLocalizedString("of the Sun", "S�o�ca"); break;
        case 29: secondPart = GetLocalizedString("of Courage", "M�stwa"); break;
        case 30: secondPart = GetLocalizedString("of Blood", "Krwi"); break;
        case 31: secondPart = GetLocalizedString("of the Nobles", "Mo�nych"); break;
        case 32: secondPart = GetLocalizedString("of Warriors", "Wojownik�w"); break;
        case 33: secondPart = GetLocalizedString("of Heroes", "Bohater�w"); break;
        case 34: secondPart = GetLocalizedString("of Light", "�wiat�a"); break;
        case 35: secondPart = GetLocalizedString("of Darkness", "Ciemno�ci"); break;
        case 36: secondPart = GetLocalizedString("of the Oracle", "Wyroczni"); break;
        case 37: secondPart = GetLocalizedString("of Visions", "Wizji"); break;
        case 38: secondPart = GetLocalizedString("of the Edge", "Kra�ca"); break;
        case 39: secondPart = GetLocalizedString("of the Spirit", "Duszy"); break;
        case 40: secondPart = GetLocalizedString("of the Specter", "Ber�a"); break;
        case 41: secondPart = GetLocalizedString("of Cinder", "Popio�u"); break;
        case 42: secondPart = GetLocalizedString("of Ashes", "Proch�w"); break;
        case 43: secondPart = GetLocalizedString("of Enigma", "Tajemnicy"); break;
        case 44: secondPart = GetLocalizedString("of the Eclipse", "Za�mienia"); break;
        case 45: secondPart = GetLocalizedString("of the Echo", "Echa"); break;
        case 46: secondPart = GetLocalizedString("of Wonders", "Cud�w"); break;
        case 47: secondPart = GetLocalizedString("of Destiny", "Przeznaczenia"); break;
    }

    //Adjective
    string adjective;
    switch (RandomNext(73, sSeedName))
    {
        case 0: adjective = GetLocalizedString("Red", _GetPolishAdjectiveVariant("Czerwony")); break;
        case 1: adjective = GetLocalizedString("Blue", _GetPolishAdjectiveVariant("Niebieski")); break;
        case 2: adjective = GetLocalizedString("Black", _GetPolishAdjectiveVariant("Czarny")); break;
        case 3: adjective = GetLocalizedString("White", _GetPolishAdjectiveVariant("Bia�y")); break;
        case 4: adjective = GetLocalizedString("Green", _GetPolishAdjectiveVariant("Zielony")); break;
        case 5: adjective = GetLocalizedString("Crystal", _GetPolishAdjectiveVariant("Kryszta�owy")); break;
        case 6: adjective = GetLocalizedString("Diamond", _GetPolishAdjectiveVariant("Diamentowy")); break;
        case 7: adjective = GetLocalizedString("Starry", _GetPolishAdjectiveVariant("Gwiezdny")); break;
        case 8: adjective = GetLocalizedString("Shining", _GetPolishAdjectiveVariant("L�ni�cy")); break;
        case 9: adjective = GetLocalizedString("Murky", _GetPolishAdjectiveVariant("Mroczny")); break;
        case 10: adjective = GetLocalizedString("Great", _GetPolishAdjectiveVariant("Wielki")); break;
        case 11: adjective = GetLocalizedString("Primeval", _GetPolishAdjectiveVariant("Pradawny")); break;
        case 12: adjective = GetLocalizedString("Azure", _GetPolishAdjectiveVariant("Azurowy")); break;
        case 13: adjective = GetLocalizedString("Crimson", _GetPolishAdjectiveVariant("Szkar�atny")); break;
        case 14: adjective = GetLocalizedString("Shattered", _GetPolishAdjectiveVariant("Zniszczony")); break;
        case 15: adjective = GetLocalizedString("Savage", _GetPolishAdjectiveVariant("Brutalny")); break;
        case 16: adjective = GetLocalizedString("Ebon", _GetPolishAdjectiveVariant("Hebanowy")); break;
        case 17: adjective = GetLocalizedString("Treacherous", _GetPolishAdjectiveVariant("Zdradliwy")); break;
        case 18: adjective = GetLocalizedString("Misty", _GetPolishAdjectiveVariant("Mglisty")); break;
        case 19: adjective = GetLocalizedString("Hidden", _GetPolishAdjectiveVariant("Ukryty")); break;
        case 20: adjective = GetLocalizedString("Merciless", _GetPolishAdjectiveVariant("Bezlitosny")); break;
        case 21: adjective = GetLocalizedString("Fading", _GetPolishAdjectiveVariant("Zanikaj�cy")); break;
        case 22: adjective = GetLocalizedString("Shivering", _GetPolishAdjectiveVariant("Dr��cy")); break;
        case 23: adjective = GetLocalizedString("Phantom", _GetPolishAdjectiveVariant("Widmowy")); break;
        case 24: adjective = GetLocalizedString("Severed", _GetPolishAdjectiveVariant("Od�amany")); break;
        case 25: adjective = GetLocalizedString("Immortal", _GetPolishAdjectiveVariant("Nie�miertelny")); break;
        case 26: adjective = GetLocalizedString("Dormant", _GetPolishAdjectiveVariant("Drzemi�cy")); break;
        case 27: adjective = GetLocalizedString("Dreaming", _GetPolishAdjectiveVariant("�ni�cy")); break;
        case 28: adjective = GetLocalizedString("Maroon", _GetPolishAdjectiveVariant("Kasztanowy")); break;
        case 29: adjective = GetLocalizedString("Delusion", _GetPolishAdjectiveVariant("Z�udny")); break;
        case 30: adjective = GetLocalizedString("Forged", _GetPolishAdjectiveVariant("Wykuty")); break;
        case 31: adjective = GetLocalizedString("Lonely", _GetPolishAdjectiveVariant("Samotny")); break;
        case 32: adjective = GetLocalizedString("Boiling", _GetPolishAdjectiveVariant("Kipi�cy")); break;
        case 33: adjective = GetLocalizedString("Iron", _GetPolishAdjectiveVariant("�elazny")); break;
        case 34: adjective = GetLocalizedString("Shadow", _GetPolishAdjectiveVariant("Cienisty")); break;
        case 35: adjective = GetLocalizedString("Summer", _GetPolishAdjectiveVariant("Letni")); break;
        case 36: adjective = GetLocalizedString("Elder", _GetPolishAdjectiveVariant("Starszy")); break;
        case 37: adjective = GetLocalizedString("Old", _GetPolishAdjectiveVariant("Stary")); break;
        case 38: adjective = GetLocalizedString("New", _GetPolishAdjectiveVariant("Nowy")); break;
        case 39: adjective = GetLocalizedString("Fresh", _GetPolishAdjectiveVariant("�wie�y")); break;
        case 40: adjective = GetLocalizedString("Rabid", _GetPolishAdjectiveVariant("W�ciek�y")); break;
        case 41: adjective = GetLocalizedString("Sacred", _GetPolishAdjectiveVariant("�wi�ty")); break;
        case 42: adjective = GetLocalizedString("Flaming", _GetPolishAdjectiveVariant("Jaskrawy")); break;
        case 43: adjective = GetLocalizedString("Sterile", _GetPolishAdjectiveVariant("Ja�owy")); break;
        case 44: adjective = GetLocalizedString("Sapphire", _GetPolishAdjectiveVariant("Szafirowy")); break;
        case 45: adjective = GetLocalizedString("Emerald", _GetPolishAdjectiveVariant("Szmaragdowy")); break;
        case 46: adjective = GetLocalizedString("Twin", _GetPolishAdjectiveVariant("Bli�niaczy")); break;
        case 47: adjective = GetLocalizedString("Jade", _GetPolishAdjectiveVariant("Jadeitowy")); break;
        case 48: adjective = GetLocalizedString("Void", _GetPolishAdjectiveVariant("Pr�ny")); break;
        case 49: adjective = GetLocalizedString("Amber", _GetPolishAdjectiveVariant("Bursztynowy")); break;
        case 50: adjective = GetLocalizedString("Golden", _GetPolishAdjectiveVariant("Z�oty")); break;
        case 51: adjective = GetLocalizedString("Silver", _GetPolishAdjectiveVariant("Srebrny")); break;
        case 52: adjective = GetLocalizedString("Calm", _GetPolishAdjectiveVariant("Spokojny")); break;
        case 53: adjective = GetLocalizedString("Transient", _GetPolishAdjectiveVariant("Przemijaj�cy")); break;
        case 54: adjective = GetLocalizedString("Fortune", _GetPolishAdjectiveVariant("Obfity")); break;
        case 55: adjective = GetLocalizedString("Shifting", _GetPolishAdjectiveVariant("Ruchomy")); break;
        case 56: adjective = GetLocalizedString("Rune", _GetPolishAdjectiveVariant("Runiczny")); break;
        case 57: adjective = GetLocalizedString("Conjured", _GetPolishAdjectiveVariant("Powo�any")); break;
        case 58: adjective = GetLocalizedString("Hollow", _GetPolishAdjectiveVariant("Pusty")); break;
        case 59: adjective = GetLocalizedString("Pure", _GetPolishAdjectiveVariant("Czysty")); break;
        case 60: adjective = GetLocalizedString("Argent", _GetPolishAdjectiveVariant("Srebrzysty")); break;
        case 61: adjective = GetLocalizedString("Slumbering", _GetPolishAdjectiveVariant("�pi�cy")); break;
        case 62: adjective = GetLocalizedString("Tamed", _GetPolishAdjectiveVariant("Okie�znany")); break;
        case 63: adjective = GetLocalizedString("Celestial", _GetPolishAdjectiveVariant("Niebia�ski")); break;
        case 64: adjective = GetLocalizedString("Glowing", _GetPolishAdjectiveVariant("Rozjarzony")); break;
        case 65: adjective = GetLocalizedString("Malachite", _GetPolishAdjectiveVariant("Malachitowy")); break;
        case 66: adjective = GetLocalizedString("Enchanted", _GetPolishAdjectiveVariant("Zakl�ty")); break;
        case 67: adjective = GetLocalizedString("Lustrous", _GetPolishAdjectiveVariant("B�yszcz�cy")); break;
        case 68: adjective = GetLocalizedString("Ethereal", _GetPolishAdjectiveVariant("Eteryczny")); break;
        case 69: adjective = GetLocalizedString("Injured", _GetPolishAdjectiveVariant("Ranny")); break;
        case 70: adjective = GetLocalizedString("Barren", _GetPolishAdjectiveVariant("Suchy")); break;
        case 71: adjective = GetLocalizedString("Howling", _GetPolishAdjectiveVariant("Wyj�cy")); break;
        case 72: adjective = GetLocalizedString("Mad", _GetPolishAdjectiveVariant("Szalony")); break;
    }

    AssertStringNotEqual("", adjective, "(inc_names, RandomRegionName, adjective)");
    AssertStringNotEqual("", firstPart, "(inc_names, RandomRegionName, firstPart)");
    AssertStringNotEqual("", secondPart, "(inc_names, RandomRegionName, secondPart)");

    return adjective + " " + firstPart + " " + secondPart;
}

string RandomTownName(string sSeedName="default")
{
    //First part
    string firstPart;
    switch (RandomNext(67, sSeedName))
    {
        case 0: firstPart = "Daven"; break;
        case 1: firstPart = "Er"; break;
        case 2: firstPart = "Pant"; break;
        case 3: firstPart = "Cew"; break;
        case 4: firstPart = "Crest"; break;
        case 5: firstPart = "Card"; break;
        case 6: firstPart = "Aqua"; break;
        case 7: firstPart = "Cael"; break;
        case 8: firstPart = "Ta"; break;
        case 9: firstPart = "Wim"; break;
        case 10: firstPart = "Oz"; break;
        case 11: firstPart = "Middles"; break;
        case 12: firstPart = "As"; break;
        case 13: firstPart = "Pen"; break;
        case 14: firstPart = "Whaelr"; break;
        case 15: firstPart = "Ash"; break;
        case 16: firstPart = "Ayles"; break;
        case 17: firstPart = "Taed"; break;
        case 18: firstPart = "Drum"; break;
        case 19: firstPart = "Coal"; break;
        case 20: firstPart = "Lu"; break;
        case 21: firstPart = "Sher"; break;
        case 22: firstPart = "Baler"; break;
        case 23: firstPart = "Dove"; break;
        case 24: firstPart = "Dun"; break;
        case 25: firstPart = "Cul"; break;
        case 26: firstPart = "East"; break;
        case 27: firstPart = "Clac"; break;
        case 28: firstPart = "Rom"; break;
        case 29: firstPart = "A"; break;
        case 30: firstPart = "Kin"; break;
        case 31: firstPart = "Wolf"; break;
        case 32: firstPart = "Wake"; break;
        case 33: firstPart = "Ilfra"; break;
        case 34: firstPart = "Wills"; break;
        case 35: firstPart = "Tylwaer"; break;
        case 36: firstPart = "Ivy"; break;
        case 37: firstPart = "Eros"; break;
        case 38: firstPart = "Dews"; break;
        case 39: firstPart = "Accring"; break;
        case 40: firstPart = "Bell"; break;
        case 41: firstPart = "Dom"; break;
        case 42: firstPart = "Blen"; break;
        case 43: firstPart = "Skarg"; break;
        case 44: firstPart = "Yarl"; break;
        case 45: firstPart = "Black"; break;
        case 46: firstPart = "Spal"; break;
        case 47: firstPart = "Aelin"; break;
        case 48: firstPart = "Alderr"; break;
        case 49: firstPart = "Haern"; break;
        case 50: firstPart = "Hi"; break;
        case 51: firstPart = "Wan"; break;
        case 52: firstPart = "Kirk"; break;
        case 53: firstPart = "North"; break;
        case 54: firstPart = "Tarn"; break;
        case 55: firstPart = "Sky"; break;
        case 56: firstPart = "Jed"; break;
        case 57: firstPart = "Helm"; break;
        case 58: firstPart = "Ponty"; break;
        case 59: firstPart = "Pol"; break;
        case 60: firstPart = "Kamo"; break;
        case 61: firstPart = "Kil"; break;
        case 62: firstPart = "Abing"; break;
        case 63: firstPart = "Swin"; break;
        case 64: firstPart = "Runs"; break;
        case 65: firstPart = "Balla"; break;
        case 66: firstPart = "Bam"; break;

    }

    //Second part
    string secondPart;
    switch (RandomNext(63, sSeedName))
    {
        case 0: secondPart = "ast"; break;
        case 1: secondPart = "mawr"; break;
        case 2: secondPart = "mann"; break;
        case 3: secondPart = "hill"; break;
        case 4: secondPart = "end"; break;
        case 5: secondPart = "rin"; break;
        case 6: secondPart = "kirk"; break;
        case 7: secondPart = "ewe"; break;
        case 8: secondPart = "borne"; break;
        case 9: secondPart = "ryn"; break;
        case 10: secondPart = "brough"; break;
        case 11: secondPart = "ton"; break;
        case 12: secondPart = "dle"; break;
        case 13: secondPart = "bourne"; break;
        case 14: secondPart = "bury"; break;
        case 15: secondPart = "morden"; break;
        case 16: secondPart = "drake"; break;
        case 17: secondPart = "chapel"; break;
        case 18: secondPart = "fell"; break;
        case 19: secondPart = "field"; break;
        case 20: secondPart = "no"; break;
        case 21: secondPart = "wich"; break;
        case 22: secondPart = "cheth"; break;
        case 23: secondPart = "hallow"; break;
        case 24: secondPart = "sey"; break;
        case 25: secondPart = "comb"; break;
        case 26: secondPart = "allen"; break;
        case 27: secondPart = "water"; break;
        case 28: secondPart = "combe"; break;
        case 29: secondPart = "den"; break;
        case 30: secondPart = "dreath"; break;
        case 31: secondPart = "wood"; break;
        case 32: secondPart = "tey"; break;
        case 33: secondPart = "moral"; break;
        case 34: secondPart = "burton"; break;
        case 35: secondPart = "calgo"; break;
        case 36: secondPart = "ness"; break;
        case 37: secondPart = "ford"; break;
        case 38: secondPart = "pool"; break;
        case 39: secondPart = "ding"; break;
        case 40: secondPart = "miley"; break;
        case 41: secondPart = "deen"; break;
        case 42: secondPart = "dean"; break;
        case 43: secondPart = "rane"; break;
        case 44: secondPart = "wall"; break;
        case 45: secondPart = "stead"; break;
        case 46: secondPart = "borourgh"; break;
        case 47: secondPart = "firth"; break;
        case 48: secondPart = "pridd"; break;
        case 49: secondPart = "perro"; break;
        case 50: secondPart = "uraska"; break;
        case 51: secondPart = "kenny"; break;
        case 52: secondPart = "don"; break;
        case 53: secondPart = "mar"; break;
        case 54: secondPart = "garth"; break;
        case 55: secondPart = "wick"; break;
        case 56: secondPart = "ter"; break;
        case 57: secondPart = "burgh"; break;
        case 58: secondPart = "byder"; break;
        case 59: secondPart = "worth"; break;
        case 60: secondPart = "dide"; break;
        case 61: secondPart = "roth"; break;
        case 62: secondPart = "mere"; break;
    }

    return firstPart+secondPart;
}