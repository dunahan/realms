#include "inc_tokens"
#include "inc_language"
#include "inc_quests"
#include "inc_realms"
#include "inc_tiles"
#include "inc_realmsdb"
#include "inc_companions"
#include "inc_ruleset"
#include "inc_stats"

// This library contains setters for tokens used by Demiurge Framework.

// DF uses 9-digit tokens of the formula: XXAABBBCC
// XX - Demiurge Framework's token identifier, which is always 46
// AA - 2-digit number denoting the token's type (03 for journal entries, for example)
// BBB - 3-digit number classifying the token (all tokens used in a single conversation, for example)
// CC - final 2-digit subclassification
// The last two parts may be used in conjunction for cases where such nested classification is not needed

// List of AA values
// 01 -
// 02 - conversation tokens
// 03 - journal tokens
// 04 - other tokens

// Returns a token containing the name of the given quest ID
int GetQuestNameToken(int nQuestId);

// Returns a token containing the content of the quest entry of the given quest ID and state
int GetQuestEntryToken(int nQuestId, int nQuestState);

// Prepares main quest's (plot) journal string tokens
void SetPlotJournalTokens(string sEntryName, string sEntryFirstParagraph, string sEntrySecondParagraph);

// Prepares conversation tokens for a quest giver
void SetQuestGiverTokens(object oQuestGiver);

// Prepares conversation tokens for a map dialogue
void SetMapDetailsTokens(object oPC);

// Prepares sMessage as the last message token's value
void SetLastMessageToken(string sMessage);

// Prepares companion info tokens based on a struct provided
void SetCompanionInformationTokens(struct CompanionInformation companionInfo);

// Prepares list of display names of companions in a party at module start dialogue
void SetPartyCompanionsTokens(string sCompanionNamesArray, string sCompanionIdsArray);

// Prepares conversation tokens for a customized ruleset
void SetCustomRulesetTokens(object oMapGeneratorDoor);

// Prepares the conversation token with a champion description
void SetChampionDescriptionToken(string sDescription);

// Prepares the conversation token with a reputation description
void SetReputationDescriptionToken(string sDescription);

// Prepares the conversation tokens for crafting a spell item
void SetSpellCraftingTokens(int nSpell, int nNeededResources, int nAddedResources);

string _PadWithZeros(int number, int desiredDigits)
{
    string result = IntToString(number);
    int length = GetStringLength(result);
    int i;
    for (i = length; i < desiredDigits; i++)
        result = "0" + result;
    return result;
}

int GetQuestNameToken(int nQuestId)
{
    if (nQuestId > 999)
        return -1;
    string token = "4603" + _PadWithZeros(nQuestId, 3) + "00";
    return StringToInt(token);
}

int GetQuestEntryToken(int nQuestId, int nQuestState)
{
    if (nQuestState < 1 || nQuestState > 5)
        return -1;
    if (nQuestState == 6)
        nQuestState = 3; //We use the same journal entry for FAILED and AFTER_FAILED
    string token = "4603" + _PadWithZeros(nQuestId, 3) + _PadWithZeros(nQuestState, 2);
    return StringToInt(token);
}

void SetQuestGiverTokens(object oQuestGiver)
{
    object oQuest = GetLocalObject(oQuestGiver, "QUEST");
    struct QuestDialogLines questLines = GetQuestInstanceDialogLines(oQuest);
    SetCustomTokenEx(460200101, questLines.npcIntroLine);
    SetCustomTokenEx(460200102, questLines.npcContentLine);
    SetCustomTokenEx(460200103, questLines.npcRewardLine);
    SetCustomTokenEx(460200104, questLines.npcBeforeCompletionLine);
    SetCustomTokenEx(460200105, questLines.playerReportingSuccess);
    SetCustomTokenEx(460200106, questLines.playerReportingFailure);
    SetCustomTokenEx(460200107, questLines.npcReceivedSuccess);
    SetCustomTokenEx(460200108, questLines.npcReceivedFailure);
    SetCustomTokenEx(460200109, questLines.npcAfterSuccess);
    SetCustomTokenEx(460200110, questLines.npcAfterFailure);
    SetCustomTokenEx(460200111, questLines.npcAfterRefusal);
}

void SetPlotJournalTokens(string sEntryName, string sEntryFirstParagraph, string sEntrySecondParagraph)
{
    SetCustomTokenEx(460399900, sEntryName);
    SetCustomTokenEx(460399901, sEntryFirstParagraph);
    SetCustomTokenEx(460399902, sEntrySecondParagraph);
}

void SetMapDetailsTokens(object oPC)
{
    object realm = GetRealm();
    object tile = GetTile(oPC);
    object biome = GetTileBiome(tile);
    object trackedQuest = GetTrackedQuest(oPC);

    int x = GetTileX(tile);
    int y = GetTileY(tile);

    string realmName = GetRealmName(realm);
    string seed = GetRealmSeedString(realm);
    string biomeName = GetRegionName(biome);
    string coordinates = "("+IntToString(x)+","+IntToString(y)+")";
    string quest = GetLocalizedString("None", "�adne");
    string mainQuestLocalized = GetLocalizedString("Main quest", "Zadanie g��wne");
    string plotScript = GetLocalString(trackedQuest, "PLOT_SCRIPT"); //not using inc_plo_instanc due to circular references...
    if (trackedQuest != OBJECT_INVALID)
        quest = plotScript == "" ? GetQuestNameWithQuestgiver(trackedQuest) : mainQuestLocalized + " - \"" + GetLocalString(trackedQuest, "PLOT_JOURNALENTRIES1") + "\"";

    SetCustomTokenEx(460200201, realmName);
    SetCustomTokenEx(460200202, seed);
    SetCustomTokenEx(460200203, biomeName);
    SetCustomTokenEx(460200204, coordinates);
    SetCustomTokenEx(460200205, quest);
}

void SetPlayerSettingsTokens(object oPC)
{
    string playerName = GetPCPlayerName(oPC);
    struct PlayerSettings settings = GetPlayerSettings(oPC);
    string imageSize;
    switch (settings.mapImageSize)
    {
        case MAP_IMAGE_SIZE_SMALL: imageSize = GetLocalizedString("Small", "Ma�y"); break;
        case MAP_IMAGE_SIZE_MEDIUM: imageSize = GetLocalizedString("Medium", "�redni"); break;
        case MAP_IMAGE_SIZE_LARGE: imageSize = GetLocalizedString("Large", "Du�y"); break;
        default: LogWarning("Invalid map image size for player "+playerName+" with value %n", settings.mapImageSize);
    }
    SetCustomTokenEx(460200301, imageSize);
}

void SetLastMessageToken(string sMessage)
{
    SetCustomTokenEx(460400001, sMessage);
}

void SetCompanionInformationTokens(struct CompanionInformation companionInfo)
{
    SetCustomTokenEx(460200401, companionInfo.id);
    SetCustomTokenEx(460200402, companionInfo.firstName);
    SetCustomTokenEx(460200403, companionInfo.lastName);
    SetCustomTokenEx(460200404, companionInfo.description);
    SetCustomTokenEx(460200405, GetClassString(companionInfo.class));
    SetCustomTokenEx(460200406, GetRaceString(companionInfo.race));
    SetCustomTokenEx(460200407, GetFullAlignmentString(companionInfo.alignmentGood, companionInfo.alignmentLawful));
    SetCustomTokenEx(460200408, companionInfo.author);

    string fullName = companionInfo.lastName == "" ? companionInfo.firstName : companionInfo.firstName+"_"+companionInfo.lastName;
    string fileName = "rlm_char_"+GetStringLowerCase(fullName)+".sqlite3";
    SetCustomTokenEx(460200409, fileName);
}

void SetPartyCompanionsTokens(string sCompanionNamesArray, string sCompanionIdsArray)
{
    int companionsNum = GetStringArraySize(sCompanionNamesArray);
    int i;
    for (i = 0; i < 5; i++)
    {
        string displayString = "-";
        if (i < companionsNum)
        {
            string id = GetStringArrayElement(sCompanionIdsArray, i);
            displayString = GetCompanionDisplayString(id, TRUE);
        }
        SetCustomTokenEx(460200501+i, displayString);
    }
}

void SetCustomRulesetTokens(object oMapGeneratorDoor)
{
    SetCustomTokenEx(460200601, IntToString(GetLocalInt(oMapGeneratorDoor, "STARTING_GOLD"))+"%");
    SetCustomTokenEx(460200602, IntToString(GetLocalInt(oMapGeneratorDoor, "QUEST_GOLD"))+"%");
    SetCustomTokenEx(460200603, IntToString(GetLocalInt(oMapGeneratorDoor, "BOUNTY_GOLD"))+"%");
    SetCustomTokenEx(460200604, IntToString(GetLocalInt(oMapGeneratorDoor, "STARTING_LIVES"))+"%");
}

void SetHenchmanExperienceTokens(object oHenchman)
{
    int currentXp = GetXP(oHenchman);
    int currentLvl = GetHitDice(oHenchman);
    int neededXp = GetExperienceForLevel(currentLvl+1) - currentXp;
    SetCustomTokenEx(460200701, IntToString(currentXp));
    SetCustomTokenEx(460200702, IntToString(neededXp));
}

void SetScoreScreenTokens(object oRealm)
{
    int width = GetLocalInt(oRealm, "Width");
    string mapSize;
    switch (width)
    {
        case 4: mapSize = GetLocalizedString("Tiny", "Bardzo ma�a"); break;
        case 6: mapSize = GetLocalizedString("Small", "Ma�a"); break;
        case 8: mapSize = GetLocalizedString("Medium", "�rednia"); break;
        case 10: mapSize = GetLocalizedString("Large", "Du�a"); break;
        case 12: mapSize = GetLocalizedString("Extra large", "Bardzo du�a"); break;
        case 14: mapSize = GetLocalizedString("Huge", "Ogromna"); break;
        case 16: mapSize = GetLocalizedString("Giant", "Gigantyczna"); break;
    }

    struct Ruleset ruleset = GetRuleset(oRealm);
    int customRulesBool = FALSE;
    if (ruleset.startingGold != 1.0
        || ruleset.questGold != 1.0
        || ruleset.bountyGold != 1.0
        || ruleset.startingSoulstones != 1.0)
        customRulesBool = TRUE;
    string customRules = customRulesBool ? GetLocalizedString("Yes", "Tak") : GetLocalizedString("No", "Nie");

    int difficultyInt = GetAdventureDifficulty(oRealm);
    string difficulty;
    switch (difficultyInt)
    {
        case GAME_DIFFICULTY_VERY_EASY: difficulty = GetLocalizedString("Very easy", "Bardzo �atwy"); break;
        case GAME_DIFFICULTY_EASY: difficulty = GetLocalizedString("Easy", "�atwy"); break;
        case GAME_DIFFICULTY_NORMAL: difficulty = GetLocalizedString("Normal", "Normalny"); break;
        case GAME_DIFFICULTY_CORE_RULES: difficulty = GetLocalizedString("Hardcore D&D", "Hardcore D&D"); break;
        case GAME_DIFFICULTY_DIFFICULT: difficulty = GetLocalizedString("Very difficult", "Bardzo trudny"); break;
    }

    SetCustomTokenEx(460200801, GetRealmSeedString(oRealm));
    SetCustomTokenEx(460200802, mapSize);
    SetCustomTokenEx(460200803, customRules);
    SetCustomTokenEx(460200804, IntToString(GetHighestNumberOfPlayers(oRealm)));
    SetCustomTokenEx(460200805, IntToString(GetNumberOfCompanions(oRealm)));
    SetCustomTokenEx(460200806, IntToString(GetTileTransitions(oRealm)));
    SetCustomTokenEx(460200807, IntToString(GetBountyMissionsCompleted(oRealm)));
    SetCustomTokenEx(460200808, IntToString(GetNumberOfRespawns(oRealm)));
    SetCustomTokenEx(460200809, IntToString(GetTotalPartyGold()));
    SetCustomTokenEx(460200810, IntToString(GetNumberOfQuestsRefusedOrFailed(oRealm)));
    SetCustomTokenEx(460200811, difficulty);
    SetCustomTokenEx(460200812, IntToString(GetFinalScore(oRealm)));

    SetCustomTokenEx(460200813, IntToString(FloatToInt(ruleset.startingGold*100))+"%");
    SetCustomTokenEx(460200814, IntToString(FloatToInt(ruleset.questGold*100))+"%");
    SetCustomTokenEx(460200815, IntToString(FloatToInt(ruleset.bountyGold*100))+"%");
    SetCustomTokenEx(460200816, IntToString(FloatToInt(ruleset.startingSoulstones*100))+"%");
}

void SetChampionDescriptionToken(string sDescription)
{
    SetCustomTokenEx(460200901, sDescription);
}

void SetReputationDescriptionToken(string sDescription)
{
    SetCustomTokenEx(460201001, sDescription);
}

void SetSpellCraftingTokens(int nSpell, int nNeededResources, int nAddedResources)
{
    string sSpellName = GetStringByStrRef(StringToInt(Get2DAString("spells", "Name", nSpell)));
    SetCustomTokenEx(460201101, sSpellName);
    SetCustomTokenEx(460201102, IntToString(nNeededResources));
    SetCustomTokenEx(460201103, IntToString(nAddedResources));
}
