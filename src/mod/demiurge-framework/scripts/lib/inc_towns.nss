#include "inc_debug"
#include "inc_hndvars"
#include "inc_tiles"
#include "inc_biomes"
#include "inc_scriptevents"

//Library for using various town implementations dynamically

// Getter for the town script's areasId field
string GetTownAreasIdentifier(string sTownScript);

// Getter for the town script's areasNumber field
int GetTownAreasNumber(string sTownScript);

// Getter for the town script's commonersNumber field
int GetTownCommonersNumber(string sTownScript);

// Getter for the town script's bountyHuntersNumber field
int GetTownBountyHuntersNumber(string sTownScript);

// Getter for the town script's stableMastersNumber field
int GetTownStableMastersNumber(string sTownScript);

// Getter for the town script's tavernsNumber field
int GetTownTavernsNumber(string sTownScript);

// Getter for the town script's storesNumber field
int GetTownStoresNumber(string sTownScript);

// Getter for the town script's facilitiesNumber field
int GetTownFacilitiesNumber(string sTownScript);

// Getter for the town script's dayTracksNumber field
int GetTownDayTracksNumber(string sTownScript);

// Getter for the town script's nightTracksNumber field
int GetTownNightTracksNumber(string sTownScript);

// Getter for the town script's battleTracksNumber field
int GetTownBattleTracksNumber(string sTownScript);

// Getter for the town script's tavernDayTracksNumber field
int GetTavernDayTracksNumber(string sTownScript);

// Getter for the town script's tavernNightTracksNumber field
int GetTavernNightTracksNumber(string sTownScript);

// Getter for the town script's tavernBattleTracksNumber field
int GetTavernBattleTracksNumber(string sTownScript);

// Returns a ResRef of a commoner creature to spawn corresponding to the index nIndex in range [0, commonersNumber-1]
string GetTownCommonerResRef(string sTownScript, int nIndex);

// Returns a ResRef of a commoner creature to spawn corresponding to the index nIndex in range [0, bountyHuntersNumber-1]
string GetTownBountyHunterResRef(string sTownScript, int nIndex);

// Returns a ResRef of a commoner creature to spawn corresponding to the index nIndex in range [0, stableMastersNumber-1]
string GetTownStableMasterResRef(string sTownScript, int nIndex);

// Returns a ResRef of a town area to spawn corresponding to the index nIndex in range [0, areasNumber-1] and an exits flag representing which exits should be available for a given biome script
string GetTownAreaResRef(string sTownScript, int nIndex, int nExitsFlag);

// Returns a ResRef of a tavern area to spawn corresponding to the index nIndex in range [0, tavernAreasNumber-1]
string GetTavernAreaResRef(string sTownScript, int nIndex);

// Returns a town's day music track's constant (TRACK_*, but it can also be an integer that has no constant assigned) corresponding to the index nIndex in range [0, dayTracksNumber-1]
int GetTownDayTrack(string sTownScript, int nIndex);

// Returns a town's night music track's constant (TRACK_*, but it can also be an integer that has no constant assigned) corresponding to the index nIndex in range [0, nightTracksNumber-1]
int GetTownNightTrack(string sTownScript, int nIndex);

// Returns a town's battle music track's constant (TRACK_*, but it can also be an integer that has no constant assigned) corresponding to the index nIndex in range [0, battleTracksNumber-1]
int GetTownBattleTrack(string sTownScript, int nIndex);

// Returns a tavern's day music track's constant (TRACK_*, but it can also be an integer that has no constant assigned) corresponding to the index nIndex in range [0, tavernDayTracksNumber-1]
int GetTavernDayTrack(string sTownScript, int nIndex);

// Returns a tavern's night music track's constant (TRACK_*, but it can also be an integer that has no constant assigned) corresponding to the index nIndex in range [0, tavernNightTracksNumber-1]
int GetTavernNightTrack(string sTownScript, int nIndex);

// Returns a tavern's battle music track's constant (TRACK_*, but it can also be an integer that has no constant assigned) corresponding to the index nIndex in range [0, tavernBattleTracksNumber-1]
int GetTavernBattleTrack(string sTownScript, int nIndex);

// Returns a random name for a given town script, based on a LANGUAGE_* constant from inc_language
string GetRandomTownName(string sTownScript, string sSeedName, int nLanguage);

// Returns a random tavern name for a given town script, based on a LANGUAGE_* constant from inc_language
string GetRandomTavernName(string sTownScript, string sSeedName, int nLanguage);

// Getter for the town script's innkeepersNumber field
int GetTavernInnkeepersNumber(string sTownScript);

// Returns a ResRef of an innkeeper creature to spawn corresponding to the index nIndex in range [0, innkeepersNumber-1]
string GetInnkeeperResRef(string sTownScript, int nIndex);

// Creates and returns a tavern store object based on a given town script and a randomness seed at oDestinationWaypoint
object SpawnTavernInventory(string sTownScript, string sSeedName, object oDestinationWaypoint);

// Creates and returns a tavern store object at oDestinationWaypoint based on a given town script, a randomness seed and (possibly) the biome starting level
object SpawnStableInventory(string sTownScript, string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint);

// Returns a town's store script's name corresponding to the index nIndex in range [0, storesNumber-1]
string GetTownStoreScript(string sTownScript, int nIndex);

// Returns a town's store script's name corresponding to the index nIndex in range [0, facilitiesNumber-1]
string GetTownFacilityScript(string sTownScript, int nIndex);

// Returns a random town's commoner first name
string GetRandomCommonerFirstName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName);

// Returns a random town's commoner last name
string GetRandomCommonerLastName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName);

// Returns a random town's innkeeper first name
string GetRandomInnkeeperFirstName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName);

// Returns a random town's innkeeper last name
string GetRandomInnkeeperLastName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName);

// Returns a random town's bounty hunter first name
string GetRandomBountyHunterFirstName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName);

// Returns a random town's bounty hunter last name
string GetRandomBountyHunterLastName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName);

// Returns a random town's stable master first name
string GetRandomStableMasterFirstName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName);

// Returns a random town's stable master last name
string GetRandomStableMasterLastName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName);

//Instance functions

// Town area initialization
void InitializeTownInstance(string sTownScript, object oTown, object oBiome, string sTownName);

// Returns the given town's script name
string GetTownScript(object oTown);

// Returns the town's biome
object GetBiomeOfTown(object oTown);

// Returns the tavern waypoint used for PC spawning and respawning
object GetTownRespawnWaypoint(object oTown);

// Sets oTown as the town in which the PC should respawn
void SetPCRespawn(object oPC, object oTown);

// Returns the waypoint designated as the PC's respawn
object GetPCRespawnWaypoint(object oPC);

// Returns the general store area of the town
object GetStoreOfTown(object oTown);

// Sets the tavern's area as the town's tavern
void SetTavernOfTown(object oTown, object oTavern);

// Returns the tavern area of town
object GetTavernOfTown(object oTown);

// Sets the facility's area as the town's facility
void SetFacilityOfTown(object oTown, object oFacility);

// Returns the facility area of town
object GetFacilityOfTown(object oTown);

// Sets the bounty hunter NPC of oTown
void SetBountyHunterOfTown(object oTown, object oBountyHunter);

// Returns the bounty hunter NPC of oTown
object GetBountyHunterOfTown(object oTown);

// Returns the name of oTown
string GetTownName(object oTown);


string GetTownAreasIdentifier(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 0);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Town " + sTownScript + " returned empty areasId value");
    return result;
}

int GetTownAreasNumber(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 1);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Town " + sTownScript + " returned invalid areasNumber value: %n", result);
    return result;
}

int GetTownCommonersNumber(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 2);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Town " + sTownScript + " returned invalid commonersNumber value: %n", result);
    return result;
}

int GetTownBountyHuntersNumber(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 39);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Town " + sTownScript + " returned invalid bountyHuntersNumber value: %n", result);
    return result;
}

int GetTownStableMastersNumber(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 49);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Town " + sTownScript + " returned invalid stableMastersNumber value: %n", result);
    return result;
}

int GetTownTavernsNumber(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 3);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Town " + sTownScript + " returned invalid tavernsNumber value: %n", result);
    return result;
}

int GetTownStoresNumber(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 4);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Town " + sTownScript + " returned invalid storesNumber value: %n", result);
    return result;
}

int GetTownDayTracksNumber(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 5);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Town " + sTownScript + " returned invalid dayTracksNumber value: %n", result);
    return result;
}

int GetTownNightTracksNumber(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 6);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Town " + sTownScript + " returned invalid nightTracksNumber value: %n", result);
    return result;
}

int GetTownBattleTracksNumber(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 7);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Town " + sTownScript + " returned invalid battleTracksNumber value: %n", result);
    return result;
}

int GetTavernDayTracksNumber(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 8);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Town " + sTownScript + " returned invalid tavernDayTracksNumber value: %n", result);
    return result;
}

int GetTavernNightTracksNumber(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 9);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Town " + sTownScript + " returned invalid tavernNightTracksNumber value: %n", result);
    return result;
}

int GetTavernBattleTracksNumber(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 10);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Town " + sTownScript + " returned invalid tavernBattleTracksNumber value: %n", result);
    return result;
}

string GetTownCommonerResRef(string sTownScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 14);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Town " + sTownScript + " returned invalid commoner creature ResRef");
    return result;
}

string GetTownBountyHunterResRef(string sTownScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 40);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Town " + sTownScript + " returned invalid bounty hunter creature ResRef");
    return result;
}

string GetTownStableMasterResRef(string sTownScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 50);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Town " + sTownScript + " returned invalid stable master creature ResRef");
    return result;
}

string GetTownAreaResRef(string sTownScript, int nIndex, int nExitsFlag)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 15);
    SetLocalInt(mod, funcArg1, nIndex);
    SetLocalInt(mod, funcArg2, nExitsFlag);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Town " + sTownScript + " returned invalid town area ResRef");
    return result;
}

string GetTavernAreaResRef(string sTownScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 16);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Town " + sTownScript + " returned invalid tavern area ResRef");
    return result;
}

int GetTownDayTrack(string sTownScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 18);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetTownNightTrack(string sTownScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 19);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetTownBattleTrack(string sTownScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 20);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetTavernDayTrack(string sTownScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 21);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetTavernNightTrack(string sTownScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 22);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetTavernBattleTrack(string sTownScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 23);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

string GetRandomTownName(string sTownScript, string sSeedName, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 27);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalInt(mod, funcArg2, nLanguage);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid town name returned in town " + sTownScript + ", sSeedName = " + sSeedName + ", nLanguage = %n", nLanguage);
    return result;
}

string GetRandomTavernName(string sTownScript, string sSeedName, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 28);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalInt(mod, funcArg2, nLanguage);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid tavern name returned in town " + sTownScript + ", sSeedName = " + sSeedName + ", nLanguage = %n", nLanguage);
    return result;
}

int GetTavernInnkeepersNumber(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 30);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Town " + sTownScript + " returned invalid innkeepersNumber value: %n", result);
    return result;
}

string GetInnkeeperResRef(string sTownScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 32);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid innkeeper creature ResRef returned in town " + sTownScript + ", nIndex = %n", nIndex);
    return result;
}

object SpawnTavernInventory(string sTownScript, string sSeedName, object oDestinationWaypoint)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 33);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalObject(mod, funcArg2, oDestinationWaypoint);
    ExecuteScript(sTownScript);
    object result = GetLocalObject(mod, funcResult);
    if (result == OBJECT_INVALID)
        LogWarning("Invalid tavern inventory store object returned in town " + sTownScript + ", sSeedName = " + sSeedName);
    return result;
}

object SpawnStableInventory(string sTownScript, string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 51);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalInt(mod, funcArg2, nBiomeStartingLevel);
    SetLocalObject(mod, funcArg3, oDestinationWaypoint);
    ExecuteScript(sTownScript);
    object result = GetLocalObject(mod, funcResult);
    if (result == OBJECT_INVALID)
        LogWarning("Invalid stable master inventory store object returned in town " + sTownScript + ", sSeedName = " + sSeedName);
    return result;
}

string GetTownStoreScript(string sTownScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 36);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid store script returned in town " + sTownScript + ", nIndex = %n", nIndex);
    return result;
}

int GetTownFacilitiesNumber(string sTownScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 37);
    ExecuteScript(sTownScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Town " + sTownScript + " returned invalid facilitiesNumber value: %n", result);
    return result;
}

string GetTownFacilityScript(string sTownScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 38);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid facility script returned in town " + sTownScript + ", nIndex = %n", nIndex);
    return result;
}

string GetRandomCommonerFirstName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 41);
    SetLocalInt(mod, funcArg1, nRacialType);
    SetLocalInt(mod, funcArg2, nGender);
    SetLocalInt(mod, funcArg3, nLanguage);
    SetLocalString(mod, funcArg4, sSeedName);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid commoner first name returned in town " + sTownScript + ", nRacialType = %n, nGender = %n, nLanguage = %n", nRacialType, nGender, nLanguage);
    return result;
}

string GetRandomCommonerLastName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 42);
    SetLocalInt(mod, funcArg1, nRacialType);
    SetLocalInt(mod, funcArg2, nGender);
    SetLocalInt(mod, funcArg3, nLanguage);
    SetLocalString(mod, funcArg4, sSeedName);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid commoner last name returned in town " + sTownScript + ", nRacialType = %n, nGender = %n, nLanguage = %n", nRacialType, nGender, nLanguage);
    return result;
}

string GetRandomInnkeeperFirstName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 43);
    SetLocalInt(mod, funcArg1, nRacialType);
    SetLocalInt(mod, funcArg2, nGender);
    SetLocalInt(mod, funcArg3, nLanguage);
    SetLocalString(mod, funcArg4, sSeedName);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid innkeeper first name returned in town " + sTownScript + ", nRacialType = %n, nGender = %n, nLanguage = %n", nRacialType, nGender, nLanguage);
    return result;
}

string GetRandomInnkeeperLastName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 44);
    SetLocalInt(mod, funcArg1, nRacialType);
    SetLocalInt(mod, funcArg2, nGender);
    SetLocalInt(mod, funcArg3, nLanguage);
    SetLocalString(mod, funcArg4, sSeedName);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid innkeeper last name returned in town " + sTownScript + ", nRacialType = %n, nGender = %n, nLanguage = %n", nRacialType, nGender, nLanguage);
    return result;
}

string GetRandomBountyHunterFirstName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 45);
    SetLocalInt(mod, funcArg1, nRacialType);
    SetLocalInt(mod, funcArg2, nGender);
    SetLocalInt(mod, funcArg3, nLanguage);
    SetLocalString(mod, funcArg4, sSeedName);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid bounty hunter first name returned in town " + sTownScript + ", nRacialType = %n, nGender = %n, nLanguage = %n", nRacialType, nGender, nLanguage);
    return result;
}

string GetRandomBountyHunterLastName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 46);
    SetLocalInt(mod, funcArg1, nRacialType);
    SetLocalInt(mod, funcArg2, nGender);
    SetLocalInt(mod, funcArg3, nLanguage);
    SetLocalString(mod, funcArg4, sSeedName);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid bounty hunter last name returned in town " + sTownScript + ", nRacialType = %n, nGender = %n, nLanguage = %n", nRacialType, nGender, nLanguage);
    return result;
}

string GetRandomStableMasterFirstName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 47);
    SetLocalInt(mod, funcArg1, nRacialType);
    SetLocalInt(mod, funcArg2, nGender);
    SetLocalInt(mod, funcArg3, nLanguage);
    SetLocalString(mod, funcArg4, sSeedName);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid stable master first name returned in town " + sTownScript + ", nRacialType = %n, nGender = %n, nLanguage = %n", nRacialType, nGender, nLanguage);
    return result;
}

string GetRandomStableMasterLastName(string sTownScript, int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 48);
    SetLocalInt(mod, funcArg1, nRacialType);
    SetLocalInt(mod, funcArg2, nGender);
    SetLocalInt(mod, funcArg3, nLanguage);
    SetLocalString(mod, funcArg4, sSeedName);
    ExecuteScript(sTownScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid stable master last name returned in town " + sTownScript + ", nRacialType = %n, nGender = %n, nLanguage = %n", nRacialType, nGender, nLanguage);
    return result;
}

//Instance functions

//Initializator
void InitializeTownInstance(string sTownScript, object oTown, object oBiome, string sTownName)
{
    SetLocalObject(oTown, "TOWN_BIOME", oBiome);
    SetTownOfBiome(oBiome, oTown);
    SetLocalString(oTown, "TOWN_SCRIPT", sTownScript);
    SetLocalString(oTown, "TOWN_NAME", sTownName);
    AddEventScript(SUBEVENT_AREA_ON_ENTER, "are_twnenter", oTown);
}

//Instance functions
string GetTownScript(object oTown)
{
    return GetLocalString(oTown, "TOWN_SCRIPT");
}

object GetBiomeOfTown(object oTown)
{
    return GetLocalObject(oTown, "TOWN_BIOME");
}

object GetTownRespawnWaypoint(object oTown)
{
    return GetTileWaypoint(oTown, "wp_tavernspawn");
}

void SetPCRespawn(object oPC, object oTown)
{
    object wp = GetTownRespawnWaypoint(oTown);
    SetLocalObject(oPC, "RESPAWN_WP", wp);
}

object GetPCRespawnWaypoint(object oPC)
{
    return GetLocalObject(oPC, "RESPAWN_WP");
}

object GetStoreOfTown(object oTown)
{
    return GetLocalObject(oTown, "TOWN_STORE");
}

void SetTavernOfTown(object oTown, object oTavern)
{
    SetLocalObject(oTown, "TOWN_TAVERN", oTavern);
}

object GetTavernOfTown(object oTown)
{
    return GetLocalObject(oTown, "TOWN_TAVERN");
}

void SetFacilityOfTown(object oTown, object oFacility)
{
    SetLocalObject(oTown, "TOWN_FACILITY", oFacility);
}

object GetFacilityOfTown(object oTown)
{
    return GetLocalObject(oTown, "TOWN_FACILITY");
}

void SetBountyHunterOfTown(object oTown, object oBountyHunter)
{
    SetLocalObject(oTown, "TOWN_BOUNTYHUNTER", oBountyHunter);
}

object GetBountyHunterOfTown(object oTown)
{
    return GetLocalObject(oTown, "TOWN_BOUNTYHUNTER");
}

string GetTownName(object oTown)
{
    return GetLocalString(oTown, "TOWN_NAME");
}