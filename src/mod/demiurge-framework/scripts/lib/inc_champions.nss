#include "inc_ruleset"
#include "inc_debug"
#include "inc_hndvars"
#include "inc_encounters"
#include "inc_scriptevents"

//Library for using various encounter implementations dynamically

// Getter for the champion script's blueprintsNumber field
int GetNumberOfChampionBlueprints(string sChampionScript);

// Getter for the champion script's rumorsNumber field
int GetNumberOfChampionRumors(string sChampionScript);

// Getter for the champion script's modifiersNumber field
int GetNumberOfChampionModifiers(string sChampionScript);

// Getter for the champion script's minPlayerLevel field
int GetChampionMinPlayerLevel(string sChampionScript);

// Getter for the champion script's maxPlayerLevel field
int GetChampionMaxPlayerLevel(string sChampionScript);

// Getter for the champion script's trophyResRef field
string GetChampionTrophyResRef(string sChampionScript);

// Returns the champion's ResRef corresponding to index nIndex in range [0, blueprintsNumber-1]
string GetChampionResRef(string sChampionScript, int nIndex);

// Returns the champion's trophy's name based on the champion's full name and the LANGUAGE_* constant
string GetTrophyName(string sChampionScript, string sChampionName, int nLanguage);

// Returns the champion's trophy's description based on the champion's full name and the LANGUAGE_* constant
string GetTrophyDescription(string sChampionScript, string sChampionName, int nLanguage);

// Returns the (potentially randomized to some extent) bounty the champion's trophy is worth to the bounty hunter
int GetChampionBounty(string sChampionScript, int nLevel, string sSeedName);

// Initializes the champion
void InitializeChampion(object oChampion, object oEncounter, string sSeedName);

// Applies a modifier to the champion based on the modifier index and (possibly) the encounter's biome's level
void ApplyChampionModifier(object oChampion, int nModifierIndex, int nLevel);

// Returns a champion's name based on the modifier index (to include a modifier-related title, for example)
string GetChampionName(string sChampionScript, int nModifierIndex, string sSeedName, int nLanguage);

// Returns a champion's description to be spoken by a bounty hunter NPC (if there is a bounty on the champion)
string GetChampionDescription(string sChampionScript, string sChampionName, int nBounty, int nModifierIndex, string sSeedName, int nLanguage);

// Returns the encounter's rumor corresponding to rumor index nRumorIndex in range [0, rumorsNumber-1] and modifier index nModifierIndex in range [0, modifiersNumber-1]
string GetChampionRumor(string sChampionScript, string sChampionName, int nRumorIndex, int nModifierIndex, int nLanguage);

//Instance functions

// Creates the champion creature
object CreateChampionInstance(string sChampionScript, string sChampionResRef, string sChampionName, object oEncounter);

// Returns the encounter object this champion was spawned in
object GetEncounterOfChampion(object oChampion);

// Returns the given champion's script name
string GetChampionScript(object oChampion);

// Returns the trophy item associated with oChampion (OBJECT_INVALID if there is no bounty on oChampion)
object GetTrophyOfChampion(object oChampion);

// Creates the trophy item
object CreateChampionTrophy(object oChampion, string sChampionDescription, string sTrophyItemResRef, string sTrophyName, string sTrophyDescription, int nBounty, object oEncounter);

// Returns the champion creature associated with oTrophyItem (it may have already been killed and destroyed)
object GetChampionOfTrophy(object oTrophyItem);

// Returns the encounter object the champion associated with this trophy item was spawned in
object GetEncounterOfTrophy(object oTrophyItem);

// Returns the value of the bounty a PC can earn by returning oTrophyItem to the bounty hunter NPC
int GetBountyOfTrophy(object oTrophyItem);

// Returns the name of the champion associated with this trophy (useful if the creature doesn't exist anymore)
string GetChampionNameOfTrophy(object oTrophyItem);

// Returns the description of the champion associated with this trophy to be spoken by a bounty hunter (useful if the creature doesn't exist anymore)
string GetChampionDescriptionOfTrophy(object oTrophyItem);



//Script functions
int GetNumberOfChampionBlueprints(string sChampionScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 0);
    ExecuteScript(sChampionScript);
    return GetLocalInt(mod, funcResult);
}

int GetChampionMinPlayerLevel(string sChampionScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 1);
    ExecuteScript(sChampionScript);
    return GetLocalInt(mod, funcResult);
}

int GetChampionMaxPlayerLevel(string sChampionScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 2);
    ExecuteScript(sChampionScript);
    return GetLocalInt(mod, funcResult);
}

int GetNumberOfChampionRumors(string sChampionScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 3);
    ExecuteScript(sChampionScript);
    return GetLocalInt(mod, funcResult);
}

int GetNumberOfChampionModifiers(string sChampionScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 4);
    ExecuteScript(sChampionScript);
    return GetLocalInt(mod, funcResult);
}

string GetChampionTrophyResRef(string sChampionScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 5);
    ExecuteScript(sChampionScript);
    return GetLocalString(mod, funcResult);
}

string GetChampionResRef(string sChampionScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 6);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sChampionScript);
    return GetLocalString(mod, funcResult);
}

string GetTrophyName(string sChampionScript, string sChampionName, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 7);
    SetLocalString(mod, funcArg1, sChampionName);
    SetLocalInt(mod, funcArg2, nLanguage);
    ExecuteScript(sChampionScript);
    return GetLocalString(mod, funcResult);
}

string GetTrophyDescription(string sChampionScript, string sChampionName, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 8);
    SetLocalString(mod, funcArg1, sChampionName);
    SetLocalInt(mod, funcArg2, nLanguage);
    ExecuteScript(sChampionScript);
    return GetLocalString(mod, funcResult);
}

int GetChampionBounty(string sChampionScript, int nLevel, string sSeedName)
{
    object mod = GetModule();
    object realm = GetRealm();
    struct Ruleset rules = GetRuleset(realm);
    SetLocalInt(mod, funcHandler, 9);
    SetLocalInt(mod, funcArg1, nLevel);
    SetLocalString(mod, funcArg2, sSeedName);
    ExecuteScript(sChampionScript);
    int result = FloatToInt(GetLocalInt(mod, funcResult) * rules.bountyGold);
    return result;
}

void InitializeChampion(object oChampion, object oEncounter, string sSeedName)
{
    object mod = GetModule();
    string sChampionScript = GetChampionScript(oChampion);
    SetLocalInt(mod, funcHandler, 10);
    SetLocalObject(mod, funcArg1, oChampion);
    SetLocalObject(mod, funcArg2, oEncounter);
    SetLocalString(mod, funcArg3, sSeedName);
    ExecuteScript(sChampionScript);
}

void ApplyChampionModifier(object oChampion, int nModifierIndex, int nLevel)
{
    object mod = GetModule();
    string sChampionScript = GetChampionScript(oChampion);
    SetLocalInt(mod, funcHandler, 11);
    SetLocalObject(mod, funcArg1, oChampion);
    SetLocalInt(mod, funcArg2, nModifierIndex);
    SetLocalInt(mod, funcArg3, nLevel);
    ExecuteScript(sChampionScript);
}

string GetChampionName(string sChampionScript, int nModifierIndex, string sSeedName, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 12);
    SetLocalInt(mod, funcArg1, nModifierIndex);
    SetLocalString(mod, funcArg2, sSeedName);
    SetLocalInt(mod, funcArg3, nLanguage);
    ExecuteScript(sChampionScript);
    return GetLocalString(mod, funcResult);
}

string GetChampionRumor(string sChampionScript, string sChampionName, int nRumorIndex, int nModifierIndex, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 13);
    SetLocalString(mod, funcArg1, sChampionName);
    SetLocalInt(mod, funcArg2, nRumorIndex);
    SetLocalInt(mod, funcArg3, nModifierIndex);
    SetLocalInt(mod, funcArg4, nLanguage);
    ExecuteScript(sChampionScript);
    return GetLocalString(mod, funcResult);
}

string GetChampionDescription(string sChampionScript, string sChampionName, int nBounty, int nModifierIndex, string sSeedName, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 14);
    SetLocalString(mod, funcArg1, sChampionName);
    SetLocalInt(mod, funcArg2, nBounty);
    SetLocalInt(mod, funcArg3, nModifierIndex);
    SetLocalString(mod, funcArg4, sSeedName);
    SetLocalInt(mod, funcArg5, nLanguage);
    ExecuteScript(sChampionScript);
    return GetLocalString(mod, funcResult);
}

//Constructor
object CreateChampionInstance(string sChampionScript, string sChampionResRef, string sChampionName, object oEncounter)
{
    location loc = GetLocationWithinEncounterBounds(oEncounter, 6.5, 6.5); //encounter center
    object champion = CreateCreature(sChampionResRef, loc);
    SetRandomSeed(RandomNextAny("default"), ObjectToString(champion));
    
    if (champion == OBJECT_INVALID)
        LogWarning("Invalid champion creature with resref: " + sChampionResRef);
    SetName(champion, sChampionName);

    SetChampionOfEncounter(oEncounter, champion);
    SetLocalString(champion, "CHAMPION_SCRIPT", sChampionScript);
    SetLocalObject(champion, "CHAMPION_ENCOUNTER", oEncounter);

    return champion;
}

object CreateChampionTrophy(object oChampion, string sChampionDescription, string sTrophyItemResRef, string sTrophyName, string sTrophyDescription, int nBounty, object oEncounter)
{
    object trophy = CreateItemOnObject(sTrophyItemResRef, oChampion);
    if (trophy == OBJECT_INVALID)
        LogWarning("Invalid champion trophy with resref: " + sTrophyItemResRef);

    SetPickpocketableFlag(trophy, FALSE);
    SetDroppableFlag(trophy, TRUE);
    SetName(trophy, sTrophyName);
    SetDescription(trophy, sTrophyDescription, TRUE);

    SetLocalInt(trophy, "TROPHY_BOUNTY", nBounty);
    SetLocalString(trophy, "TROPHY_CHAMPIONNAME", GetName(oChampion));
    SetLocalString(trophy, "TROPHY_CHAMPIONDESCRIPTION", sChampionDescription);
    SetLocalObject(trophy, "TROPHY_CHAMPION", oChampion);
    SetLocalObject(trophy, "TROPHY_ENCOUNTER", oEncounter);

    SetLocalObject(oChampion, "CHAMPION_TROPHY", trophy);
    SetLocalObject(oEncounter, "ENCOUNTER_TROPHY", trophy);

    return trophy;
}

//Instance functions
object GetEncounterOfChampion(object oChampion)
{
    return GetLocalObject(oChampion, "CHAMPION_ENCOUNTER");
}

string GetChampionScript(object oChampion)
{
    return GetLocalString(oChampion, "CHAMPION_SCRIPT");
}

object GetTrophyOfChampion(object oChampion)
{
    return GetLocalObject(oChampion, "CHAMPION_TROPHY");
}

object GetChampionOfTrophy(object oTrophyItem)
{
    return GetLocalObject(oTrophyItem, "TROPHY_CHAMPION");
}

object GetEncounterOfTrophy(object oTrophyItem)
{
    return GetLocalObject(oTrophyItem, "TROPHY_ENCOUNTER");
}

int GetBountyOfTrophy(object oTrophyItem)
{
    return GetLocalInt(oTrophyItem, "TROPHY_BOUNTY");
}

string GetChampionNameOfTrophy(object oTrophyItem)
{
    return GetLocalString(oTrophyItem, "TROPHY_CHAMPIONNAME");
}

string GetChampionDescriptionOfTrophy(object oTrophyItem)
{
    return GetLocalString(oTrophyItem, "TROPHY_CHAMPIONDESCRIPTION");
}