#include "inc_common"
#include "inc_debug"
//Library containing functions related to the module's ruleset and difficulty ("Very Difficult" difficulty modifications, to be precise)

struct Ruleset {
    float startingGold;
    float questGold;
    float bountyGold;
    float startingSoulstones;
};

int nStartingGold = 2400;
float fStartingSoulstonesPerMapTile = 0.375f;

// Recalculates difficulty buffs to be applied to all NPCs in the module - should be called on difficulty change to and from Very Difficult
void RecalculateDifficultyBuffs(object oCreature);

// Sets the ruleset modifiers on the realm object
void SetRuleset(object oRealm, int nStartingGoldPercent, int nQuestGoldPercent, int nBountyGoldPercent, int nStartingSoulstonesPercent);

// Returns the struct with ruleset modifiers applicable to the realm
struct Ruleset GetRuleset(object oRealm);

int _DamageBonusToConst(int nDamageBonusValue)
{
    if (nDamageBonusValue <= 5)
        return nDamageBonusValue;

    if (nDamageBonusValue <= 20)
        return nDamageBonusValue+10;

    return -1;
}

effect _VeryDifficultNPCBuffsEffect(object oCreature)
{
    effect ef = EffectACIncrease(2);
    ef = EffectLinkEffects(EffectAttackIncrease(2), ef);

    int extraDmg = GetHitDice(oCreature) / 2;
    if (extraDmg > 20)
        extraDmg = 20;

    if (extraDmg > 0)
        ef = EffectLinkEffects(EffectDamageIncrease(_DamageBonusToConst(extraDmg), DAMAGE_TYPE_POSITIVE), ef);

    ef = TagEffect(ef, "VERY_DIFFICULT_BUFFS");
    ef = SupernaturalEffect(ef);

    return ef;
}

void RecalculateDifficultyBuffs(object oCreature)
{
    effect ef = GetEffectByTag(oCreature, "VERY_DIFFICULT_BUFFS");
    if (GetIsEffectValid(ef))
        RemoveEffect(oCreature, ef);

    if (GetGameDifficulty() == GAME_DIFFICULTY_DIFFICULT)
    {
        ef = _VeryDifficultNPCBuffsEffect(oCreature);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, ef, oCreature);
    }
}

void SetRuleset(object oRealm, int nStartingGoldPercent, int nQuestGoldPercent, int nBountyGoldPercent, int nStartingSoulstonesPercent)
{
    float startingGold = nStartingGoldPercent / 100.0f;
    float questGold = nQuestGoldPercent / 100.0f;
    float bountyGold = nBountyGoldPercent / 100.0f;
    float startingSoulstones = nStartingSoulstonesPercent / 100.0f;

    SetLocalFloat(oRealm, "RULESET_GOLD", startingGold);
    SetLocalFloat(oRealm, "RULESET_REWARD", questGold);
    SetLocalFloat(oRealm, "RULESET_BOUNTY", bountyGold);
    SetLocalFloat(oRealm, "RULESET_LIVES", startingSoulstones);
}

struct Ruleset GetRuleset(object oRealm)
{
    struct Ruleset result;
    result.startingGold = GetLocalFloat(oRealm, "RULESET_GOLD");
    result.questGold = GetLocalFloat(oRealm, "RULESET_REWARD");
    result.bountyGold = GetLocalFloat(oRealm, "RULESET_BOUNTY");
    result.startingSoulstones = GetLocalFloat(oRealm, "RULESET_LIVES");
    return result;
}
