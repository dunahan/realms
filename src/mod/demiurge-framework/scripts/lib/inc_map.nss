#include "inc_generation"
#include "inc_arrays"
#include "inc_tiles"
#include "inc_realms"
#include "inc_encounters"
#include "inc_quests"
#include "inc_biomes"
#include "inc_debug"

// -------------------------
// -- Function prototypes --
// -------------------------

// Returns the shortest path's distance between tiles of given coordinates
int GetDistanceToTile(int nSourceX, int nSourceY, int nTargetX, int nTargetY, object oRealm);

// Returns the DIRECTION_* constant representing the direction of the next tile in the shortest path between tiles of given coordinates
float GetShortestPathNextDirection(int nSourceX, int nSourceY, int nTargetX, int nTargetY, object oRealm);

// Displays the map of the realm saved on oRealm to oPC
void DisplayMap(object oPC, object oRealm, int nPoliticalMode=FALSE, int nTargetX=-1, int nTargetY=-1, int nFullMap=FALSE, int userOffsetX=0, int userOffsetY=0, int imgSize=3);

// Stop displaying the map
void CancelMapDisplay(object oPC);

// -----------------------------
// -- Function implementation --
// -----------------------------

int GetDistanceToTile(int nSourceX, int nSourceY, int nTargetX, int nTargetY, object oRealm)
{
    if (nSourceX == nTargetX && nSourceY == nTargetY)
        return 0;

    int width = GetRealmTileArraySize("Grid_MapRow0", oRealm);
    int sourceIndex = ConvertTileCoordinatesToIndex(nSourceX, nSourceY, width);
    int targetIndex = ConvertTileCoordinatesToIndex(nTargetX, nTargetY, width);
    int distance = _GetDistanceToTile(sourceIndex, targetIndex, width, oRealm);
    return distance;
}

int GetShortestPathNextDirectionPath(int nSourceX, int nSourceY, int nTargetX, int nTargetY, object oRealm)
{
    if (nSourceX == nTargetX && nSourceY == nTargetY)
        return 0;

    int height = GetStringArraySize("Grid", oRealm);
    int width = GetRealmTileArraySize("Grid_MapRow0", oRealm);
    int targetIndex = ConvertTileCoordinatesToIndex(nTargetX, nTargetY, width);
    int sourcePaths = _GetTileFieldOnGrid(nSourceX, nSourceY, "paths", oRealm);

    int i;
    int shortestDistance = 99999;
    int finalDirection = 0;
    for (i = 0; i < 4; i++)
    {
        int x = nSourceX;
        int y = nSourceY;
        int pathDirection;
        switch (i)
        {
            case 0:
                pathDirection = PATH_NORTH;
                y--;
                break;
            case 1:
                pathDirection = PATH_EAST;
                x++;
                break;
            case 2:
                pathDirection = PATH_SOUTH;
                y++;
                break;
            case 3:
                pathDirection = PATH_WEST;
                x--;
                break;
        }

        if ((sourcePaths & pathDirection) == FALSE)
            continue;

        if (x < 0 || x >= width || y < 0 || y >= height)
            continue;

        string distancesArray = _GetStringTileFieldOnGrid(x, y, "distances", oRealm);
        int distance = GetIntArrayElement(distancesArray, targetIndex, oRealm);
        if (distance < shortestDistance)
        {
            shortestDistance = distance;
            finalDirection = pathDirection;
        }
    }
    LogInfo("Distance from (%n,%n): %n", nSourceX, nSourceY, shortestDistance);
    return finalDirection;
}

float GetShortestPathNextDirection(int nSourceX, int nSourceY, int nTargetX, int nTargetY, object oRealm)
{
    int finalDirection = GetShortestPathNextDirectionPath(nSourceX, nSourceY, nTargetX, nTargetY, oRealm);
    switch (finalDirection)
    {
        case PATH_NORTH:
            return DIRECTION_NORTH;
        case PATH_EAST:
            return DIRECTION_EAST;
        case PATH_SOUTH:
            return DIRECTION_SOUTH;
        case PATH_WEST:
            return DIRECTION_WEST;
    }
    return 0.0f;
}

string GetPathSymbol(int originPath, int exitPath)
{
    int bothPaths = originPath | exitPath;
    switch (bothPaths)
    {
        case 10:
            return "d";
        case 5:
            return "e";
        case 3:
            return "f";
        case 6:
            return "g";
        case 12:
            return "h";
        case 9:
            return "i";
        case 8:
            return "j";
        case 1:
            return "k";
        case 2:
            return "l";
        case 4:
            return "m";
        default:
            return "n";
    }
    return "n";
}

void GetPathArray(string sArrayName, int nWidth, int nSourceX, int nSourceY, int nTargetX, int nTargetY, object oRealm)
{
    CreateStringArray(sArrayName, nWidth*nWidth);
    int i;
    for (i = 0; i < nWidth*nWidth; i++)
        SetStringArrayElement(sArrayName, i, "n");

    if (nTargetX == -1 || nTargetY == -1)
        return;

    int originPath = 0;
    int exitPath;

    while (TRUE)
    {
        exitPath = GetShortestPathNextDirectionPath(nSourceX, nSourceY, nTargetX, nTargetY, oRealm);
        int index = ConvertTileCoordinatesToIndex(nSourceX, nSourceY, nWidth);
        SetStringArrayElement(sArrayName, index, GetPathSymbol(originPath, exitPath));

        if (exitPath == 0)
            return;

        switch (exitPath)
        {
            case PATH_NORTH:
                nSourceY--;
                originPath = PATH_SOUTH;
                break;
            case PATH_SOUTH:
                nSourceY++;
                originPath = PATH_NORTH;
                break;
            case PATH_WEST:
                nSourceX--;
                originPath = PATH_EAST;
                break;
            case PATH_EAST:
                nSourceX++;
                originPath = PATH_WEST;
                break;
        }
    }
}

string GetMapTile(int directions)
{
    switch (directions)
    {
        case 1:
            return "a";
        case 2:
            return "b";
        case 4:
            return "c";
        case 8:
            return "d";
        case 10:
            return "e";
        case 5:
            return "f";
        case 12:
            return "g";
        case 9:
            return "h";
        case 3:
            return "i";
        case 6:
            return "j";
        case 11:
            return "k";
        case 14:
            return "l";
        case 13:
            return "m";
        case 7:
            return "n";
        case 15:
            return "o";
        default:
            return "p";
    }
    return "";
}

string GetMapSymbol(int symbol)
{
    switch (symbol)
    {
        case 1:
            return "a"; //settlement
        case 2:
            return "b"; //quest
        case 3:
            return "c"; //PC
        default:
            return "p";
    }
    return "";
}

string GetColoredMapTile(int directions, int red=255, int green=255, int blue=255)
{
    string mapTile = GetMapTile(directions);
    return ColorString(mapTile, red, green, blue);
}

string GetStringColoredByBiome(string stringToColor, object area)
{
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    string script = GetBiomeScript(biome);
    int r = GetBiomeMapColorRed(script);
    int g = GetBiomeMapColorGreen(script);
    int b = GetBiomeMapColorBlue(script);
    return ColorString(stringToColor, r, g, b);
}

string GetRowWithCharacterAtPosition(string sCharacter, int nPosition, int nRowLength)
{
    string row = "";
    int i;
    for (i = 0; i < nPosition; i++)
        row += "p";
    row += sCharacter;
    for (i = 0; i < nRowLength-nPosition-1; i++)
        row += "p";
    return row;
}

string GetStringColoredByRegionId(string stringToColor, int regionNum)
{
    int r,g,b;
    switch (regionNum)
    {
        case 1:
            r = 41;
            g = 115;
            b = 109;
            break;
        case 2:
            r = 88;
            g = 117;
            b = 82;
            break;
        case 3:
            r = 182;
            g = 79;
            b = 39;
            break;
        case 4:
            r = 96;
            g = 111;
            b = 124;
            break;
        case 5:
            r = 247;
            g = 196;
            b = 93;
            break;
        case 6:
            r = 63;
            g = 72;
            b = 51;
            break;
        case 7:
            r = 128;
            g = 25;
            b = 29;
            break;
        case 8:
            r = 73;
            g = 76;
            b = 32;
            break;
        case 9:
            r = 121;
            g = 86;
            b = 63;
            break;
        case 10:
            r = 94;
            g = 47;
            b = 44;
            break;
        case 11:
            r = 247;
            g = 244;
            b = 214;
            break;
        case 12:
            r = 123;
            g = 44;
            b = 66;
            break;
        case 13:
            r = 152;
            g = 153;
            b = 82;
            break;
        case 14:
            r = 181;
            g = 132;
            b = 43;
            break;
        case 15:
            r = 46;
            g = 37;
            b = 25;
            break;
        case 16:
            r = 196;
            g = 133;
            b = 101;
            break;
        case 17:
            r = 157;
            g = 130;
            b = 163;
            break;
        case 18:
            r = 130;
            g = 145;
            b = 175;
            break;
        case 19:
            r = 165;
            g = 68;
            b = 68;
            break;
        case 20:
            r = 1;
            g = 91;
            b = 85;
            break;
        case 21:
            r = 1;
            g = 91;
            b = 56;
            break;
    }
    return ColorString(stringToColor, r, g, b);
}

void GetArrayOfAcceptedQuestTileIndexes(string sArrayName, object oRealm, int nSize)
{
    CreateIntArray(sArrayName, nSize*nSize);
    int questsNum = GetObjectArraySize(REALM_QUESTS_ARRAY, oRealm);
    LogInfo("Quests array size: %n", questsNum);
    int i;
    for (i = 0; i < questsNum; i++)
    {
        object quest = GetObjectArrayElement(REALM_QUESTS_ARRAY, i, oRealm);
        int questState = GetQuestState(quest);
        if (questState == QUEST_STATE_LINGERING && GetIsQuestAccepted(quest))
        {
            object encounter = GetQuestEncounter(quest);
            object specialArea = GetQuestSpecialArea(quest);
            object area = GetIsObjectValid(encounter) ? GetEncounterArea(encounter) : specialArea;
            object tile = GetTile(area);
            int tileIndex = GetTileIndex(tile);
            SetIntArrayElement(sArrayName, tileIndex, 1);
            LogInfo("Quest at tile index: %n", tileIndex);
        }
    }
}

void DisplayMap(object oPC, object oRealm, int nPoliticalMode=FALSE, int nTargetX=-1, int nTargetY=-1, int nFullMap=FALSE, int userOffsetX=0, int userOffsetY=0, int imgSize=3)
{
    string fileSuffix;
    switch (imgSize)
    {
        case 1: fileSuffix = "_s"; break;
        case 2: fileSuffix = "_m"; break;
        case 3: fileSuffix = ""; break;
    }

    object tile = GetTile(oPC);
    int sourceX = GetTileX(tile);
    int sourceY = GetTileY(tile);
    LogInfo("source x,y: %n,%n", sourceX, sourceY);

    int size = GetStringArraySize("Grid", oRealm);
    if (nTargetX >= size || nTargetY >= size)
    {
        LogWarning("Target coordinates out of bounds: (%n,%n) for map size %n", nTargetX, nTargetY, size);
        return;
    }
    string mapbg = size == 16 ? "mapbg_large"+fileSuffix : "mapbg"+fileSuffix;
    string maptiles = "maptiles"+fileSuffix;
    string mapsymbols = "mapsymbols"+fileSuffix;
    int y;
    int x;
    float time = 86400.0f;
    int currentRegion = 1; //used in political mode
    if (nPoliticalMode)
    {
        CreateIntArray("TEMP_ARRAY", 50);
        int i;
        for (i = 0; i < 50; i++)
            SetIntArrayElement("TEMP_ARRAY", i, -1);
    }

    GetPathArray("PathToTarget", size, sourceX, sourceY, nTargetX, nTargetY, oRealm);
    GetArrayOfAcceptedQuestTileIndexes("AcceptedQuestTileIndexes", oRealm, size);
    int xOffset, yOffset;

    for (y = 0; y < size; y++)
    {
        string symbols = "";
        string pcPositions = "";

        xOffset = FloatToInt(-(size/2)*5.5);
        yOffset;
        switch (imgSize)
        {
            case 1: yOffset = 1; break;
            case 2: yOffset = 2; break;
            case 3: yOffset = 4; break;
        }
        yOffset *= (y-(size/2));

        //add user offset
        xOffset += userOffsetX;
        yOffset += userOffsetY;

        for (x = 0; x < size; x++)
        {
            int index = ConvertTileCoordinatesToIndex(x, y, size);
            object area = GetRealmTile(oRealm, index);

            //move on if this tile is unexplored and we're not in full map mode
            int explored = nFullMap || GetIsMapTileExplored(area);

            int directions = _GetTileFieldOnGrid(x, y, "paths", oRealm);
            int tileType = _GetTileFieldOnGrid(x, y, "type", oRealm);

            //settlements
            if (explored && (tileType == TILE_TYPE_START || tileType == TILE_TYPE_SETTLEMENT))
                symbols += GetMapSymbol(1);
            else
                symbols += GetMapSymbol(4);

            //pc position
            if (sourceX == x && sourceY == y)
                pcPositions += GetMapSymbol(3);
            else
                pcPositions += GetMapSymbol(4);

            //to get political mode
            object biome = GetTileBiome(area);
            int regionId = GetRegionIDOfBiome(biome);

            //In political mode (for GetStringColoredByRegionNum) we need to use arbitrary region numbers and not region IDs, because region IDs are not necessary sequential
            int regNum;
            if (nPoliticalMode)
            {
                regNum = GetIntArrayElement("TEMP_ARRAY", regionId);
                if (regNum == -1)
                {
                    regNum = currentRegion;
                    SetIntArrayElement("TEMP_ARRAY", regionId, regNum);
                    currentRegion++;
                }
            }

            //we don't need to post that at all if the tile is unexplored
            if (!explored)
                continue;

            string row = nPoliticalMode
                         ? GetStringColoredByRegionId(GetRowWithCharacterAtPosition(GetMapTile(directions), x, size), regNum)
                         : GetStringColoredByBiome(GetRowWithCharacterAtPosition(GetMapTile(directions), x, size), area);

            PostString(oPC, row, xOffset, yOffset, SCREEN_ANCHOR_CENTER, time, 0x00000000, 0x000000FF, 50000+100*x+y, maptiles);
        }

        string pathRow;
        for (x = 0; x < size; x++)
            pathRow += GetStringArrayElement("PathToTarget", ConvertTileCoordinatesToIndex(x, y, size));
        PostString(oPC, pathRow, xOffset, yOffset, SCREEN_ANCHOR_CENTER, time, 0xFFFFFFFF, 0xFFFFFFFF, 40000+y, mapsymbols);

        string questsRow;
        for (x = 0; x < size; x++)
            questsRow += GetIntArrayElement("AcceptedQuestTileIndexes", ConvertTileCoordinatesToIndex(x, y, size)) ? "b" : "p";
        PostString(oPC, questsRow, xOffset, yOffset, SCREEN_ANCHOR_CENTER, time, 0xFFFFFFFF, 0xFFFFFFFF, 30100+y, mapsymbols);

        PostString(oPC, pcPositions, xOffset, yOffset, SCREEN_ANCHOR_CENTER, time, 0xFFFFFFFF, 0xFFFFFFFF, 20000+y, mapsymbols);
        PostString(oPC, symbols, xOffset, yOffset, SCREEN_ANCHOR_CENTER, time, 0xFFFFFFFF, 0xFFFFFFFF, 30000+y, mapsymbols);
    }

    //in order for top left corner of the background to match the 0,0 tile when map is of maximum (16) size, bg's xOffset should be -44 and yOffset -32 (for large map display), so:
    //correct for large: -68, -37 (-44-24, -32-5)
    //correct for medium: -56, -19 (-44-12, -16-3)
    //correct for small: -50, -9 (-44-6, -8-1)
    //...but the above is correct only for 16x16 maps. I have no idea where the numbers below come from, but they work. If anyone finds a formula connecting them together, let me know.
    switch (imgSize)
    {
        case 1: yOffset = -9; break;
        case 2: yOffset = -19; break;
        case 3: yOffset = -37; break;
    }
    if (imgSize == 1)
    {
        switch (size)
        {
            case 4: xOffset = -25; break;
            case 8: xOffset = -34; break;
            case 12: xOffset = -42; break;
            case 16: xOffset = -50; break;
        }
    }
    else if (imgSize == 2)
    {
        switch (size)
        {
            case 4: xOffset = -39; break;
            case 8: xOffset = -45; break;
            case 12: xOffset = -50; break;
            case 16: xOffset = -56; break;
        }
    }
    else if (imgSize == 3)
    {
        xOffset = -68;
    }
    xOffset += userOffsetX;
    yOffset += userOffsetY;
    PostString(oPC, "a", xOffset, yOffset, SCREEN_ANCHOR_CENTER, time, 0xFFFFFFFF, 0xFFFFFFFF, 60000, mapbg);
    ClearIntArray("TEMP_ARRAY");
}

void CancelMapDisplay(object oPC)
{
    float time = 0.001f;
    int x, y;
    for (y = 0; y < 16; y++)
    {
        for (x = 0; x < 16; x++)
            PostString(oPC, "", 0, 0, SCREEN_ANCHOR_CENTER, time, 0x00000000, 0x000000FF, 50000+100*x+y);

        PostString(oPC, "", 0, 0, SCREEN_ANCHOR_CENTER, time, 0xFFFFFFFF, 0xFFFFFFFF, 40000+y);
        PostString(oPC, "", 0, 0, SCREEN_ANCHOR_CENTER, time, 0xFFFFFFFF, 0xFFFFFFFF, 30100+y);
        PostString(oPC, "", 0, 0, SCREEN_ANCHOR_CENTER, time, 0xFFFFFFFF, 0xFFFFFFFF, 20000+y);
        PostString(oPC, "", 0, 0, SCREEN_ANCHOR_CENTER, time, 0xFFFFFFFF, 0xFFFFFFFF, 30000+y);
    }
    PostString(oPC, "", 0, 0, SCREEN_ANCHOR_TOP_LEFT, time, 0xFFFFFFFF, 0xFFFFFFFF, 60000);
}