#include "inc_realmsdb"
#include "inc_experience"
#include "inc_debug"
#include "inc_common"
#include "inc_language"
#include "inc_scriptevents"
#include "x0_i0_assoc"
#include "hench_i0_equip"
#include "x0_i0_henchman"
//Library containing functions for management of companions/henchmen

// Removes companion from its master's command and party. Use this instead of RemoveHenchman for companions.
void RemoveCompanion(object oCompanion);

// Returns TRUE if oCreature is a valid companion and FALSE otherwise.
// Associates that are invalid companions include summons and henchmen that haven't been created with the inc_companions library.
int GetIsCreatureCompanion(object oCompanion);

// Sets a companion's amount of XP points - core SetXP function should never be called on companions, use this instead
// This will also change the companion's level if their XP becomes high or low enough.
// The function will return the companion object (due to the possibility of replacing the companion instance).
object SetCompanionXP(object oCompanion, int nXP);

// Adjusts companion's alignment - core AdjustAlignment function should never be called on companions, use this instead
// The parameters are the same as in AdjustAlignment, but it doesn't accept either ALIGNMENT_NEUTRAL or ALIGNMENT_ALL
void AdjustCompanionAlignment(object oCompanion, int nAlignment, int nShift);

// Sets an integer value on a companion;
// avoid local variables on companions, as they are cleared on every companion level-up
void SetCompanionInt(object oCompanion, string sVarName, int nValue);

// Gets an integer value on a companion;
// avoid local variables on companions, as they are cleared on every companion level-up
int GetCompanionInt(object oCompanion, string sVarName);

// Sets a string value on a companion;
// avoid local variables on companions, as they are cleared on every companion level-up
void SetCompanionString(object oCompanion, string sVarName, string sValue);

// Gets a string value on a companion;
// avoid local variables on companions, as they are cleared on every companion level-up
string GetCompanionString(object oCompanion, string sVarName);

// Sets a float value on a companion;
// avoid local variables on companions, as they are cleared on every companion level-up
void SetCompanionFloat(object oCompanion, string sVarName, float fValue);

// Gets a float value on a companion;
// avoid local variables on companions, as they are cleared on every companion level-up
float GetCompanionFloat(object oCompanion, string sVarName);

// Sets an object (by reference) on a companion;
// avoid local variables on companions, as they are cleared on every companion level-up
void SetCompanionObject(object oCompanion, string sVarName, object oValue);

// Gets an object (by reference) on a companion;
// avoid local variables on companions, as they are cleared on every companion level-up
object GetCompanionObject(object oCompanion, string sVarName);

// Spawns a companion with the given ID at first level
object SpawnCompanion(string sCompanionId, location lLocation);


int _GetCompanionVariableEntryExists(object oCompanion, string sVarName)
{
    string id = GetLocalString(oCompanion, "ID");
    string sql =
        "SELECT COUNT(*) FROM CompanionVariables "+
        "   WHERE VarName = @varName "+
        "   AND CompanionId = @id; "
        ;
    sqlquery query = SqlPrepareQueryObject(GetModule(), sql);
    SqlBindString(query, "@id", id);
    SqlBindString(query, "@varName", sVarName);
    SqlStep(query);
    if (SqlGetError(query) != "")
        LogWarning("Companion variable check error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, _GetCompanionVariableEntryExists)");
    int result = SqlGetInt(query, 0);
    return result;
}

void RemoveCompanion(object oCompanion)
{
    object master = GetPCMaster(oCompanion);
    if (GetIsPC(master))
        FireHenchman(master, oCompanion);
    AssignCommand(oCompanion, ClearAllActions());
}

int GetIsCreatureCompanion(object oCompanion)
{
    if (GetLocalString(oCompanion, "ID") != "")
        return TRUE;
    return FALSE;
}

void SetCompanionInt(object oCompanion, string sVarName, int nValue)
{
    int varExists = _GetCompanionVariableEntryExists(oCompanion, sVarName);
    string id = GetLocalString(oCompanion, "ID");
    string sql;
    sqlquery query;
    if (varExists)
    {
        sql =
            "UPDATE CompanionVariables "+
            "   SET Int = @value "+
            "   WHERE VarName = @varName "+
            "   AND CompanionId = @id; "
            ;
        query = SqlPrepareQueryObject(GetModule(), sql);
        SqlBindString(query, "@id", id);
        SqlBindString(query, "@varName", sVarName);
        SqlBindInt(query, "@value", nValue);
        SqlStep(query);
        if (SqlGetError(query) != "")
            LogWarning("Companion int update error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, SetCompanionInt)");
    }
    else
    {
        sql =
            "INSERT INTO CompanionVariables (CompanionId, VarName, Int) VALUES "+
            "   (@id, @varName, @value); "
            ;
        query = SqlPrepareQueryObject(GetModule(), sql);
        SqlBindString(query, "@id", id);
        SqlBindString(query, "@varName", sVarName);
        SqlBindInt(query, "@value", nValue);
        SqlStep(query);
        if (SqlGetError(query) != "")
            LogWarning("Companion int insert error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, SetCompanionInt)");
    }
}

int GetCompanionInt(object oCompanion, string sVarName)
{
    string id = GetLocalString(oCompanion, "ID");
    string sql =
        "SELECT Int FROM CompanionVariables "+
        "   WHERE VarName = @varName "+
        "   AND CompanionId = @id; "
        ;
    sqlquery query = SqlPrepareQueryObject(GetModule(), sql);
    SqlBindString(query, "@id", id);
    SqlBindString(query, "@varName", sVarName);
    SqlStep(query);
    if (SqlGetError(query) != "")
        LogWarning("Companion int retrieval error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, GetCompanionInt)");
    int result = SqlGetInt(query, 0);
    if (SqlStep(query) == TRUE)
        LogWarning("Multiple companion int values error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, GetCompanionInt)");
    return result;
}

void SetCompanionString(object oCompanion, string sVarName, string sValue)
{
    int varExists = _GetCompanionVariableEntryExists(oCompanion, sVarName);
    string id = GetLocalString(oCompanion, "ID");
    string sql;
    sqlquery query;
    if (varExists)
    {
        sql =
            "UPDATE CompanionVariables "+
            "   SET String = @value "+
            "   WHERE VarName = @varName "+
            "   AND CompanionId = @id; "
            ;
        query = SqlPrepareQueryObject(GetModule(), sql);
        SqlBindString(query, "@id", id);
        SqlBindString(query, "@varName", sVarName);
        SqlBindString(query, "@value", sValue);
        SqlStep(query);
        if (SqlGetError(query) != "")
            LogWarning("Companion string update error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, SetCompanionString)");
    }
    else
    {
        sql =
            "INSERT INTO CompanionVariables (CompanionId, VarName, String) VALUES "+
            "   (@id, @varName, @value); "
            ;
        query = SqlPrepareQueryObject(GetModule(), sql);
        SqlBindString(query, "@id", id);
        SqlBindString(query, "@varName", sVarName);
        SqlBindString(query, "@value", sValue);
        SqlStep(query);
        if (SqlGetError(query) != "")
            LogWarning("Companion string insert error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, SetCompanionString)");
    }
}

string GetCompanionString(object oCompanion, string sVarName)
{
    string id = GetLocalString(oCompanion, "ID");
    string sql =
        "SELECT String FROM CompanionVariables "+
        "   WHERE VarName = @varName "+
        "   AND CompanionId = @id; "
        ;
    sqlquery query = SqlPrepareQueryObject(GetModule(), sql);
    SqlBindString(query, "@id", id);
    SqlBindString(query, "@varName", sVarName);
    SqlStep(query);
    if (SqlGetError(query) != "")
        LogWarning("Companion string retrieval error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, GetCompanionString)");
    string result = SqlGetString(query, 0);
    if (SqlStep(query) == TRUE)
        LogWarning("Multiple companion string values error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, GetCompanionString)");
    return result;
}

void SetCompanionFloat(object oCompanion, string sVarName, float fValue)
{
    int varExists = _GetCompanionVariableEntryExists(oCompanion, sVarName);
    string id = GetLocalString(oCompanion, "ID");
    string sql;
    sqlquery query;
    if (varExists)
    {
        sql =
            "UPDATE CompanionVariables "+
            "   SET Float = @value "+
            "   WHERE VarName = @varName "+
            "   AND CompanionId = @id; "
            ;
        query = SqlPrepareQueryObject(GetModule(), sql);
        SqlBindString(query, "@id", id);
        SqlBindString(query, "@varName", sVarName);
        SqlBindFloat(query, "@value", fValue);
        SqlStep(query);
        if (SqlGetError(query) != "")
            LogWarning("Companion float update error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, SetCompanionFloat)");
    }
    else
    {
        sql =
            "INSERT INTO CompanionVariables (CompanionId, VarName, Float) VALUES "+
            "   (@id, @varName, @value); "
            ;
        query = SqlPrepareQueryObject(GetModule(), sql);
        SqlBindString(query, "@id", id);
        SqlBindString(query, "@varName", sVarName);
        SqlBindFloat(query, "@value", fValue);
        SqlStep(query);
        if (SqlGetError(query) != "")
            LogWarning("Companion float insert error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, SetCompanionFloat)");
    }
}

float GetCompanionFloat(object oCompanion, string sVarName)
{
    string id = GetLocalString(oCompanion, "ID");
    string sql =
        "SELECT Float FROM CompanionVariables "+
        "   WHERE VarName = @varName "+
        "   AND CompanionId = @id; "
        ;
    sqlquery query = SqlPrepareQueryObject(GetModule(), sql);
    SqlBindString(query, "@id", id);
    SqlBindString(query, "@varName", sVarName);
    SqlStep(query);
    if (SqlGetError(query) != "")
        LogWarning("Companion float retrieval error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, GetCompanionFloat)");
    float result = SqlGetFloat(query, 0);
    if (SqlStep(query) == TRUE)
        LogWarning("Multiple companion float values error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, GetCompanionFloat)");
    return result;
}

void SetCompanionObject(object oCompanion, string sVarName, object oValue)
{
    int varExists = _GetCompanionVariableEntryExists(oCompanion, sVarName);
    string id = GetLocalString(oCompanion, "ID");
    string objectRef = ObjectToString(oValue);
    string sql;
    sqlquery query;
    if (varExists)
    {
        sql =
            "UPDATE CompanionVariables "+
            "   SET Object = @value "+
            "   WHERE VarName = @varName "+
            "   AND CompanionId = @id; "
            ;
        query = SqlPrepareQueryObject(GetModule(), sql);
        SqlBindString(query, "@id", id);
        SqlBindString(query, "@varName", sVarName);
        SqlBindString(query, "@value", objectRef);
        SqlStep(query);
        if (SqlGetError(query) != "")
            LogWarning("Companion object reference update error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, SetCompanionObject)");
    }
    else
    {
        sql =
            "INSERT INTO CompanionVariables (CompanionId, VarName, Object) VALUES "+
            "   (@id, @varName, @value); "
            ;
        query = SqlPrepareQueryObject(GetModule(), sql);
        SqlBindString(query, "@id", id);
        SqlBindString(query, "@varName", sVarName);
        SqlBindString(query, "@value", objectRef);
        SqlStep(query);
        if (SqlGetError(query) != "")
            LogWarning("Companion object insert error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, SetCompanionObject)");
    }
}

object GetCompanionObject(object oCompanion, string sVarName)
{
    string id = GetLocalString(oCompanion, "ID");
    string sql =
        "SELECT Object FROM CompanionVariables "+
        "   WHERE VarName = @varName "+
        "   AND CompanionId = @id; "
        ;
    sqlquery query = SqlPrepareQueryObject(GetModule(), sql);
    SqlBindString(query, "@id", id);
    SqlBindString(query, "@varName", sVarName);
    SqlStep(query);
    if (SqlGetError(query) != "")
        LogWarning("Companion object reference retrieval error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, GetCompanionObject)");
    string reference = SqlGetString(query, 0);
    object result = StringToObject(reference);
    if (SqlStep(query) == TRUE)
        LogWarning("Multiple companion object reference values error (ID: "+id+", sVarName: "+sVarName+") (inc_companions, GetCompanionObject)");
    return result;
}

int _GetStartingAlignmentPointsValue(int nAlignment)
{
    switch (nAlignment)
    {
        case ALIGNMENT_CHAOTIC:
        case ALIGNMENT_EVIL:
            return 15;
        case ALIGNMENT_NEUTRAL:
            return 50;
        case ALIGNMENT_LAWFUL:
        case ALIGNMENT_GOOD:
            return 85;
    }
    LogWarning("Incorrect alignment constant: %n (inc_companions, _GetStartingAlignmentPointsValue, nAlignment)", nAlignment);
    return 0;
}

int _GetAlignmentByPoints(int nAlignmentPoints, int nLawAxis=FALSE)
{
    if (nAlignmentPoints >= 0 && nAlignmentPoints <= 30)
    {
        if (nLawAxis)
            return ALIGNMENT_CHAOTIC;
        return ALIGNMENT_EVIL;
    }
    else if (nAlignmentPoints <= 69)
        return ALIGNMENT_NEUTRAL;
    else if (nLawAxis)
        return ALIGNMENT_LAWFUL;
    return ALIGNMENT_GOOD;
}

void _RefreshAlignment(object oCompanion)
{
    int goodPoints = GetCompanionInt(oCompanion, "ALIGNMENT_GOOD");
    int lawfulPoints = GetCompanionInt(oCompanion, "ALIGNMENT_LAWFUL");

    //"Reset" alignment to neutral, will be easier
    AdjustAlignment(oCompanion, ALIGNMENT_NEUTRAL, 100, FALSE);

    //Get alignment constants
    int goodAlignment = _GetAlignmentByPoints(goodPoints, FALSE);
    int lawfulAlignment = _GetAlignmentByPoints(lawfulPoints, TRUE);

    //Set correct alignments (but not alignment points yet)
    if (goodAlignment != ALIGNMENT_NEUTRAL)
        AdjustAlignment(oCompanion, goodAlignment, 50, FALSE);
    if (lawfulAlignment != ALIGNMENT_NEUTRAL)
        AdjustAlignment(oCompanion, lawfulAlignment, 50, FALSE);

    //Add or subtract alignments points within brackets (without changing the alignment set earlier)
    int currentGoodPoints = _GetStartingAlignmentPointsValue(goodAlignment);
    int currentLawfulPoints = _GetStartingAlignmentPointsValue(lawfulAlignment);
    AdjustAlignment(oCompanion, ALIGNMENT_GOOD, goodPoints-currentGoodPoints, FALSE);
    AdjustAlignment(oCompanion, ALIGNMENT_LAWFUL, lawfulPoints-currentLawfulPoints, FALSE);
}

void AdjustCompanionAlignment(object oCompanion, int nAlignment, int nShift)
{
    if (nAlignment == ALIGNMENT_ALL || nAlignment == ALIGNMENT_NEUTRAL)
        LogWarning("Incorrect alignment constant: %n (inc_companions, AdjustCompanionAlignment, nAlignment)", nAlignment);

    int goodPoints = GetCompanionInt(oCompanion, "ALIGNMENT_GOOD");
    int lawfulPoints = GetCompanionInt(oCompanion, "ALIGNMENT_LAWFUL");

    int currentGoodConstant = _GetAlignmentByPoints(goodPoints, FALSE);
    int currentLawfulConstant = _GetAlignmentByPoints(lawfulPoints, TRUE);

    if (nAlignment == ALIGNMENT_GOOD)
        goodPoints += nShift;
    if (nAlignment == ALIGNMENT_EVIL)
        goodPoints -= nShift;
    if (nAlignment == ALIGNMENT_LAWFUL)
        lawfulPoints += nShift;
    if (nAlignment == ALIGNMENT_CHAOTIC)
        lawfulPoints -= nShift;

    int newGoodConstant = _GetAlignmentByPoints(goodPoints, FALSE);
    int newLawfulConstant = _GetAlignmentByPoints(lawfulPoints, TRUE);

    if (newGoodConstant != currentGoodConstant)
        goodPoints = _GetStartingAlignmentPointsValue(newGoodConstant);
    if (newLawfulConstant != currentLawfulConstant)
        lawfulPoints = _GetStartingAlignmentPointsValue(newLawfulConstant);

    SetCompanionInt(oCompanion, "ALIGNMENT_GOOD", goodPoints);
    SetCompanionInt(oCompanion, "ALIGNMENT_LAWFUL", lawfulPoints);
    _RefreshAlignment(oCompanion);
}

void _StubbornDestroyObject(object obj)
{
    if (GetIsObjectValid(obj))
    {
        AssignCommand(obj, SetIsDestroyable(TRUE, TRUE, TRUE));
        DestroyObject(obj);
        DelayCommand(0.1, _StubbornDestroyObject(obj));
    }
}

void _AfterInventoryTransferActions(object companion, object newCompanion, object master, location originalLocation)
{
    int i;
    float delay = 0.5f;
    object item = GetFirstItemInInventory(companion);
    if (GetIsObjectValid(item))
    {
        DelayCommand(delay, _AfterInventoryTransferActions(companion, newCompanion, master, originalLocation));
        return;
    }

    for (i = 0; i <= 14; i++)
    {
        //we want to check creature skin, too
        if (i == 14)
            i = INVENTORY_SLOT_CARMOUR;

        item = GetItemInSlot(i, companion);
        if (GetIsObjectValid(item))
        {
            DelayCommand(delay, _AfterInventoryTransferActions(companion, newCompanion, master, originalLocation));
            return;
        }
    }

    //if we got that far, time to wrap things up
    AssignCommand(newCompanion, ClearAllActions());
    AssignCommand(newCompanion, ActionJumpToLocation(originalLocation));
    if (master != OBJECT_INVALID)
        AddHenchman(master, newCompanion);
    _StubbornDestroyObject(companion);
    string plMsg = GetGender(newCompanion) == GENDER_MALE ? "Awansowałem na następny poziom." : "Awansowałam na następny poziom.";
    DelayCommand(1.0, AssignCommand(newCompanion, SpeakString(GetLocalizedString("I've gained a level.", plMsg))));
}

void _GetInventoryFromCreature(object oGiver)
{
    object item = GetFirstItemInInventory(oGiver);
    while (GetIsObjectValid(item))
    {
        ActionTakeItem(item, oGiver);
        item = GetNextItemInInventory(oGiver);
    }

    int i;
    for (i = 0; i <= 14; i++)
    {
        //we want to transfer creature skin, too
        if (i == 14)
            i = INVENTORY_SLOT_CARMOUR;

        item = GetItemInSlot(i, oGiver);
        ActionTakeItem(item, oGiver);
        ActionEquipItem(item, i);
    }
}

struct CompanionTacticsSettings {
    int defendMaster;
    int rangedWeapon;
    int dualWield;
    int lightOffhand;
    float range;
    int stopCasting;
    float spellChallenge;
    int dontCastMelee;
    int dontSummon;
    int dontDispel;
    int distance2;
    int distance4;
    int distance6;
    int heal75;
    int heal50;
    int heal25;
    int dontHealMelee;
    int stayStealthy;
    int dontDisarm;
    int recoverTraps;
    int helpWithLocks;
    int autoOpenLocks;
    int pickItems;
    int autoOpenChests;
    int preferredCombatMode;
};

struct CompanionTacticsSettings _GetCompanionTacticsSettings(object oCompanion)
{
    struct CompanionTacticsSettings x;
    x.defendMaster = GetAssociateState(NW_ASC_MODE_DEFEND_MASTER, oCompanion);
    x.rangedWeapon = GetAssociateState(NW_ASC_USE_RANGED_WEAPON, oCompanion);
    x.dualWield = GetLocalInt(oCompanion, "DualWieldState");
    x.lightOffhand = GetLocalInt(oCompanion, "LightOffHand");
    x.range = GetLocalFloat(oCompanion, "HenchRange");
    x.stopCasting = GetLocalInt(oCompanion, "X2_L_STOPCASTING");
    x.spellChallenge = GetLocalFloat(oCompanion, "NewHenchChallenge");
    x.dontCastMelee = GetLocalInt(oCompanion, "DoNotCastMelee");
    x.dontSummon = GetLocalInt(oCompanion, "DontSummon");
    x.dontDispel = GetLocalInt(oCompanion, "X2_HENCH_DO_NOT_DISPEL");
    x.distance2 = GetAssociateState(NW_ASC_DISTANCE_2_METERS, oCompanion);
    x.distance4 = GetAssociateState(NW_ASC_DISTANCE_4_METERS, oCompanion);
    x.distance6 = GetAssociateState(NW_ASC_DISTANCE_6_METERS, oCompanion);
    x.heal75 = GetAssociateState(NW_ASC_HEAL_AT_75, oCompanion);
    x.heal50 = GetAssociateState(NW_ASC_HEAL_AT_50, oCompanion);
    x.heal25 = GetAssociateState(NW_ASC_HEAL_AT_25, oCompanion);
    x.dontHealMelee = GetLocalInt(oCompanion, "DoNotHealMelee");
    x.stayStealthy = GetLocalInt(oCompanion, "X2_HENCH_STEALTH_MODE");
    x.dontDisarm = GetLocalInt(oCompanion, "NoDisarmTraps");
    x.recoverTraps = GetLocalInt(oCompanion, "AutoRecoverTraps");
    x.helpWithLocks = GetAssociateState(NW_ASC_RETRY_OPEN_LOCKS, oCompanion);
    x.autoOpenLocks = GetLocalInt(oCompanion, "AutoOpenLocks");
    x.pickItems = GetLocalInt(oCompanion, "AutoPickup");
    x.autoOpenChests = GetLocalInt(oCompanion, "AutoOpenChest");
    x.preferredCombatMode = GetLocalInt(oCompanion, "PreferredCombatMode");
    return x;
}

void _SetCompanionTacticsSettings(object oCompanion, struct CompanionTacticsSettings settings)
{
    SetAssociateState(NW_ASC_MODE_DEFEND_MASTER, settings.defendMaster, oCompanion);
    SetAssociateState(NW_ASC_USE_RANGED_WEAPON, settings.rangedWeapon, oCompanion);
    SetLocalInt(oCompanion, "DualWieldState", settings.dualWield);
    SetLocalInt(oCompanion, "LightOffHand", settings.lightOffhand);
    SetLocalFloat(oCompanion, "HenchRange", settings.range);
    SetLocalInt(oCompanion, "X2_L_STOPCASTING", settings.stopCasting);
    SetLocalFloat(oCompanion, "NewHenchChallenge", settings.spellChallenge);
    SetLocalInt(oCompanion, "DoNotCastMelee", settings.dontCastMelee);
    SetLocalInt(oCompanion, "DontSummon", settings.dontSummon);
    SetLocalInt(oCompanion, "X2_HENCH_DO_NOT_DISPEL", settings.dontDispel);
    SetAssociateState(NW_ASC_DISTANCE_2_METERS, settings.distance2, oCompanion);
    SetAssociateState(NW_ASC_DISTANCE_4_METERS, settings.distance4, oCompanion);
    SetAssociateState(NW_ASC_DISTANCE_6_METERS, settings.distance6, oCompanion);
    SetAssociateState(NW_ASC_HEAL_AT_75, settings.heal75, oCompanion);
    SetAssociateState(NW_ASC_HEAL_AT_50, settings.heal50, oCompanion);
    SetAssociateState(NW_ASC_HEAL_AT_25, settings.heal25, oCompanion);
    SetLocalInt(oCompanion, "DoNotHealMelee", settings.dontHealMelee);
    SetLocalInt(oCompanion, "X2_HENCH_STEALTH_MODE", settings.stayStealthy);
    SetLocalInt(oCompanion, "NoDisarmTraps", settings.dontDisarm);
    SetLocalInt(oCompanion, "AutoRecoverTraps", settings.recoverTraps);
    SetAssociateState(NW_ASC_RETRY_OPEN_LOCKS, settings.helpWithLocks, oCompanion);
    SetLocalInt(oCompanion, "AutoOpenLocks", settings.autoOpenLocks);
    SetLocalInt(oCompanion, "AutoPickup", settings.pickItems);
    SetLocalInt(oCompanion, "AutoOpenChest", settings.autoOpenChests);
    SetLocalInt(oCompanion, "PreferredCombatMode", settings.preferredCombatMode);

    ClearWeaponStates(oCompanion);
    HenchEquipDefaultWeapons(oCompanion, TRUE);
    DelayCommand(1.0, SetActionMode(OBJECT_SELF, ACTION_MODE_STEALTH, settings.stayStealthy));
}

object _SetCompanionLevel(object oCompanion, int nLevel)
{
    string id = GetLocalString(oCompanion, "ID");

    struct CompanionInformation info = GetCompanionInformation(id);
    object updatedCompanion = CreateCompanionInstance(id, nLevel, Limbo());

    //Remove from the party (if any)
    object master = GetMaster(oCompanion);
    if (GetIsObjectValid(master))
        RemoveHenchman(master, oCompanion);

    //Get the companion's original location
    location originalLocation = GetLocation(oCompanion);

    //Send old companion to limbo
    AssignCommand(oCompanion, JumpToLocation(Limbo()));

    //Update the name
    string fullName = info.lastName == "" ? info.firstName : info.firstName + " " + info.lastName;
    SetName(updatedCompanion, fullName);

    //Set typical local vars
    SetLocalString(updatedCompanion, "FIRSTNAME", info.firstName);
    SetLocalString(updatedCompanion, "LASTNAME", info.lastName);
    SetLocalString(updatedCompanion, "ID", info.id);

    //Refresh alignment and XP
    _RefreshAlignment(updatedCompanion);
    int xp = GetCompanionInt(oCompanion, "XP");
    SetXP(updatedCompanion, xp);

    //Change faction to "Companions"
    object companionsFactionObject = CreateObject(OBJECT_TYPE_CREATURE, "cre_facneutral", Limbo());
    ChangeFaction(updatedCompanion, companionsFactionObject);
    DestroyObject(companionsFactionObject);

    //Copy the horse tail and horse-related variables
    SetScriptParam("oMaster", ObjectToString(master));
    SetScriptParam("oOld", ObjectToString(oCompanion));
    SetScriptParam("oNew", ObjectToString(updatedCompanion));
    ExecuteScript("exe_hentranhorse");

    //Copy scripts stored in vars
    SetLocalString(updatedCompanion, SUBEVENT_CREATURE_ON_PHYSICAL_ATTACKED, GetLocalString(oCompanion, SUBEVENT_CREATURE_ON_PHYSICAL_ATTACKED));
    SetLocalString(updatedCompanion, SUBEVENT_CREATURE_ON_BLOCKED, GetLocalString(oCompanion, SUBEVENT_CREATURE_ON_BLOCKED));
    SetLocalString(updatedCompanion, SUBEVENT_CREATURE_ON_SPELL_CAST_AT, GetLocalString(oCompanion, SUBEVENT_CREATURE_ON_SPELL_CAST_AT));
    SetLocalString(updatedCompanion, SUBEVENT_CREATURE_ON_CONVERSATION, GetLocalString(oCompanion, SUBEVENT_CREATURE_ON_CONVERSATION));
    SetLocalString(updatedCompanion, SUBEVENT_CREATURE_ON_DAMAGED, GetLocalString(oCompanion, SUBEVENT_CREATURE_ON_DAMAGED));
    SetLocalString(updatedCompanion, SUBEVENT_CREATURE_ON_DEATH, GetLocalString(oCompanion, SUBEVENT_CREATURE_ON_DEATH));
    SetLocalString(updatedCompanion, SUBEVENT_CREATURE_ON_DISTURBED, GetLocalString(oCompanion, SUBEVENT_CREATURE_ON_DISTURBED));
    SetLocalString(updatedCompanion, SUBEVENT_CREATURE_ON_HEARTBEAT, GetLocalString(oCompanion, SUBEVENT_CREATURE_ON_HEARTBEAT));
    SetLocalString(updatedCompanion, SUBEVENT_CREATURE_ON_PERCEPTION, GetLocalString(oCompanion, SUBEVENT_CREATURE_ON_PERCEPTION));
    SetLocalString(updatedCompanion, SUBEVENT_CREATURE_ON_RESTED, GetLocalString(oCompanion, SUBEVENT_CREATURE_ON_RESTED));
    SetLocalString(updatedCompanion, SUBEVENT_CREATURE_ON_COMBAT_ROUND_END, GetLocalString(oCompanion, SUBEVENT_CREATURE_ON_COMBAT_ROUND_END));
    SetLocalString(updatedCompanion, SUBEVENT_CREATURE_ON_SPAWN, GetLocalString(oCompanion, SUBEVENT_CREATURE_ON_SPAWN));
    SetLocalString(updatedCompanion, SUBEVENT_CREATURE_ON_USER_DEFINED, GetLocalString(oCompanion, SUBEVENT_CREATURE_ON_USER_DEFINED));

    //Transfer tactics variables
    struct CompanionTacticsSettings settings = _GetCompanionTacticsSettings(oCompanion);
    _SetCompanionTacticsSettings(updatedCompanion, settings);

    //Set event scripts
    int i;
    for (i = 5000; i < 5012; i++)
    {
        string script = GetEventScript(oCompanion, i);
        SetEventScript(updatedCompanion, i, script);
    }

    //Add death event handler for custom quest death logic
    AddEventScript(SUBEVENT_CREATURE_ON_DEATH, "cre_que_death", updatedCompanion);

    //Destroy creature skin on the new companion, if any
    object skin = GetItemInSlot(INVENTORY_SLOT_CARMOUR, updatedCompanion);
    DestroyObject(skin);

    //Transfer inventory and equipment
    AssignCommand(updatedCompanion, _GetInventoryFromCreature(oCompanion));

    //Destroy the old companion after inventory is transferred, destroy dummy companion and send updated one back to the original location
    _AfterInventoryTransferActions(oCompanion, updatedCompanion, master, originalLocation);

    return updatedCompanion;
}

object SetCompanionXP(object oCompanion, int nXP)
{
    SetXP(oCompanion, nXP);
    string id = GetLocalString(oCompanion, "ID");
    SetCompanionInt(oCompanion, "XP", nXP);

    int expectedLevel = GetLevelForExperience(nXP);
    int currentLevel = GetHitDice(oCompanion);
    if (currentLevel != expectedLevel)
        oCompanion = _SetCompanionLevel(oCompanion, expectedLevel);

    return oCompanion;
}

object SpawnCompanion(string sCompanionId, location lLocation)
{
    struct CompanionInformation info = GetCompanionInformation(sCompanionId);
    object companion = CreateCompanionInstance(sCompanionId, 1, lLocation);

    string fullName = info.lastName == "" ? info.firstName : info.firstName + " " + info.lastName;
    SetName(companion, fullName);

    SetLocalString(companion, "FIRSTNAME", info.firstName);
    SetLocalString(companion, "LASTNAME", info.lastName);
    SetLocalString(companion, "ID", info.id);
    SetCompanionInt(companion, "ALIGNMENT_GOOD", _GetStartingAlignmentPointsValue(info.alignmentGood));
    SetCompanionInt(companion, "ALIGNMENT_LAWFUL", _GetStartingAlignmentPointsValue(info.alignmentLawful));

    //Change faction to "Companions"
    object companionsFactionObject = CreateObject(OBJECT_TYPE_CREATURE, "cre_facneutral", Limbo());
    ChangeFaction(companion, companionsFactionObject);
    DestroyObject(companionsFactionObject);

    //Give the companion starting clothes based on class
    string clothes;
    switch (info.class)
    {
        case CLASS_TYPE_PALADIN:
            clothes = "nw_cloth010";
            break;
        case CLASS_TYPE_BARBARIAN:
            clothes = "nw_cloth015";
            break;
        case CLASS_TYPE_MONK:
            clothes = "nw_cloth016";
            break;
        case CLASS_TYPE_BARD:
            clothes = "nw_cloth021";
            break;
        case CLASS_TYPE_FIGHTER:
            clothes = "nw_cloth025";
            break;
        case CLASS_TYPE_DRUID:
            clothes = "nw_cloth001";
            break;
        case CLASS_TYPE_ROGUE:
            clothes = "nw_cloth004";
            break;
        case CLASS_TYPE_WIZARD:
            clothes = "nw_cloth005";
            break;
        case CLASS_TYPE_SORCERER:
            clothes = "nw_cloth008";
            break;
        case CLASS_TYPE_CLERIC:
            clothes = "x2_cloth008";
            break;
        case CLASS_TYPE_RANGER:
            clothes = "nw_cloth006";
            break;
        default:
            LogWarning("Invalid companion starting class: %n (SpawnCompanion, inc_companions)", info.class);
    }
    object item = CreateItemOnObject(clothes, companion);
    AssignCommand(companion, ActionEquipItem(item, INVENTORY_SLOT_CHEST));

    //Set companion event scripts
    SetEventScript(companion, EVENT_SCRIPT_CREATURE_ON_BLOCKED_BY_DOOR, "hench_ch_block");
    SetEventScript(companion, EVENT_SCRIPT_CREATURE_ON_DAMAGED, "hench_ch_damage");
    SetEventScript(companion, EVENT_SCRIPT_CREATURE_ON_DEATH, "hench_ch_death");
    SetEventScript(companion, EVENT_SCRIPT_CREATURE_ON_DIALOGUE, "hench_ch_conv");
    SetEventScript(companion, EVENT_SCRIPT_CREATURE_ON_DISTURBED, "hench_ch_distrb");
    SetEventScript(companion, EVENT_SCRIPT_CREATURE_ON_END_COMBATROUND, "hench_ch_combat");
    SetEventScript(companion, EVENT_SCRIPT_CREATURE_ON_HEARTBEAT, "hench_ch_heart");
    SetEventScript(companion, EVENT_SCRIPT_CREATURE_ON_MELEE_ATTACKED, "hench_ch_attack");
    SetEventScript(companion, EVENT_SCRIPT_CREATURE_ON_NOTICE, "hench_ch_percep");
    SetEventScript(companion, EVENT_SCRIPT_CREATURE_ON_RESTED, "hench_ch_rest");
    SetEventScript(companion, EVENT_SCRIPT_CREATURE_ON_SPAWN_IN, "hench_ch_spawn");
    SetEventScript(companion, EVENT_SCRIPT_CREATURE_ON_SPELLCASTAT, "hench_ch_spell");
    SetEventScript(companion, EVENT_SCRIPT_CREATURE_ON_USER_DEFINED_EVENT, "hench_ch_usrdef");

    //And now use master scripts
    SetCreatureMasterScripts(companion);

    //Add death event handler for custom quest death logic
    AddEventScript(SUBEVENT_CREATURE_ON_DEATH, "cre_que_death", companion);

    return companion;
}
