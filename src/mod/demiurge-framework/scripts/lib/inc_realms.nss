#include "inc_debug"
#include "inc_hndvars"
#include "inc_arrays"
#include "inc_random"

const int REALM_SIZE_TINY = 1;
const int REALM_SIZE_SMALL = 2;
const int REALM_SIZE_MEDIUM = 3;
const int REALM_SIZE_LARGE = 4;

const int ITEM_POWER_USELESS = 1;
const int ITEM_POWER_BAD = 2;
const int ITEM_POWER_AVERAGE = 3;
const int ITEM_POWER_GOOD = 4;
const int ITEM_POWER_GREAT = 5;
const int ITEM_POWER_EXTRAORDINARY = 6;

const string REALM_TILES_ARRAY = "REALM_TILES";
const string REALM_BIOMES_ARRAY = "REALM_BIOMES";
const string REALM_QUESTS_ARRAY = "REALM_QUESTS";

// Finishes the current adventure and takes all the players to the score screen.
// This function should be called somewhere in the boss area after overcoming the presented challenge.
void FinishAdventure();

// Sets the given realm script as the current one and creates (and registers) a new realm object, which is then returned;
// should be called only once, at some point before map generation procedure
object SetRealmScript(string sRealmScript);

// Getter for the realm script's overworldBiomeNumber field
int GetNumberOfOverworldBiomes(string sRealmScript);

// Getter for the realm script's levelsPerRegion field
int GetNumberOfLevelsPerRegion(string sRealmScript);

// Getter for the realm script's craftableNumber field
//int GetNumberOfCraftables(string sRealmScript);

// Getter for the realm script's plotsNumber field
int GetNumberOfPlots(string sRealmScript);

// Returns the realm's overworld biome script name corresponding to index nIndex in range [0, overworldBiomeNumber-1]
string GetOverworldBiomeScript(string sRealmScript, int nIndex);

// Returns a random name for a realm based on a realm script
string GetRandomRealmName(string sRealmScript, string sSeedName);

// Returns a plot script name corresponding to the index nIndex in range [0, plotsNumber-1] for a given realm script
string GetRealmPlotScript(string sRealmScript, int nIndex);

// Getter for the realm script's startingLevel field
int GetRealmStartingLevel(string sRealmScript);

// Returns the number of resource units required to craft a wand with the given spell (of a given spell level)
int GetResourceUnitsRequiredToCraftWand(string sRealmScript, int nSpell, int nSpellLevel);

// Returns the number of resource units required to brew a potion with the given spell (of a given spell level)
int GetResourceUnitsRequiredToBrewPotion(string sRealmScript, int nSpell, int nSpellLevel);

// Returns the number of resource units required to scribe a scroll with the given spell (of a given spell level)
int GetResourceUnitsRequiredToScribeScroll(string sRealmScript, int nSpell, int nSpellLevel);

// Returns the value of an item in resource points for crafting a wand
int GetResourceUnitsOfItemForWandCrafting(string sRealmScript, string sItemTag);

// Returns the value of an item in resource points for brewing a potion
int GetResourceUnitsOfItemForPotionBrewing(string sRealmScript, string sItemTag);

// Returns the value of an item in resource points for scribing a scroll
int GetResourceUnitsOfItemForScrollScribing(string sRealmScript, string sItemTag);

//Instance functions

// Realm object constructor - creates a realm object; doesn't generate the realm, which is the task of the cnv_mg_generate script
object CreateRealmInstance(string sRealmScript);

// Returns the module's realm object
object GetRealm();

// Returns a realm's biome object by its region ID
object GetBiomeByRegionID(object oRealm, int nRegionID);

// Returns a realm's name
string GetRealmName(object oRealm);

// Returns a realm's size constant (REALM_SIZE_*)
int GetRealmSize(object oRealm);

// Returns the plot object generated for this realm
object GetRealmPlot(object oRealm);

// Returns the seed used for the realm generation
int GetRealmSeed(object oRealm);

// Returns a realm's script's name
string GetRealmScript(object oRealm);

// Sets a realm's name, should be called only once, during map generation;
// any further calls will log a warning
void SetRealmName(object oRealm, string sName);

// Sets a realm's size (REALM_SIZE_* constants), should be called only once, during map generation;
// any further calls will log a warning
void SetRealmSize(object oRealm, int nSizeConst);

// Sets a realm's plot, should be called only once, during map generation
void SetRealmPlot(object oRealm, object oPlot);


//Script functions
void FinishAdventure()
{
    object PC = GetFirstPC();
    object wp = GetWaypointByTag("WP_ENDING");
    while (GetIsObjectValid(PC))
    {
        AssignCommand(PC, JumpToObject(wp));
        PC = GetNextPC();
    }
}

object SetRealmScript(string sRealmScript)
{
    object realm = CreateRealmInstance(sRealmScript);
    SetLocalObject(OBJECT_SELF, "MAPOBJECT", realm);
    return realm;
}

int GetNumberOfOverworldBiomes(string sRealmScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 0);
    ExecuteScript(sRealmScript);
    int result = GetLocalInt(mod, funcResult);
    if (result == 0)
        LogFatal("Realm " + sRealmScript + " has no overworld biomes!");
    return result;
}

int GetNumberOfLevelsPerRegion(string sRealmScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 1);
    ExecuteScript(sRealmScript);
    int result = GetLocalInt(mod, funcResult);
    if (result == 0)
        LogFatal("Realm " + sRealmScript + " has 0 levels per regions!");
    return result;
}

int GetNumberOfPlots(string sRealmScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 7);
    ExecuteScript(sRealmScript);
    int result = GetLocalInt(mod, funcResult);
    if (result == 0)
        LogFatal("Realm " + sRealmScript + " has no plots to generate!");
    return result;
}

string GetOverworldBiomeScript(string sRealmScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 2);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sRealmScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid overworld biome script name returned in realm " + sRealmScript + ", nIndex = %n", nIndex);
    return result;
}

string GetRandomRealmName(string sRealmScript, string sSeedName)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 4);
    SetLocalString(mod, funcArg1, sSeedName);
    ExecuteScript(sRealmScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid realm name returned in realm " + sRealmScript + ", sSeedName = " + sSeedName);
    return result;
}

string GetRealmPlotScript(string sRealmScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 8);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sRealmScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid plot script name returned in realm " + sRealmScript + ", nIndex = %n", nIndex);
    return result;
}

int GetRealmStartingLevel(string sRealmScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 9);
    ExecuteScript(sRealmScript);
    int result = GetLocalInt(mod, funcResult);
    if (result < 1)
        LogFatal("Realm " + sRealmScript + " has starting level set to %n, which is lower than 1", result);
    return result;
}

int GetResourceUnitsRequiredToCraftWand(string sRealmScript, int nSpell, int nSpellLevel)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 10);
    SetLocalInt(mod, funcArg1, nSpell);
    SetLocalInt(mod, funcArg2, nSpellLevel);
    ExecuteScript(sRealmScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetResourceUnitsRequiredToBrewPotion(string sRealmScript, int nSpell, int nSpellLevel)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 11);
    SetLocalInt(mod, funcArg1, nSpell);
    SetLocalInt(mod, funcArg2, nSpellLevel);
    ExecuteScript(sRealmScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetResourceUnitsRequiredToScribeScroll(string sRealmScript, int nSpell, int nSpellLevel)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 12);
    SetLocalInt(mod, funcArg1, nSpell);
    SetLocalInt(mod, funcArg2, nSpellLevel);
    ExecuteScript(sRealmScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetResourceUnitsOfItemForWandCrafting(string sRealmScript, string sItemTag)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 13);
    SetLocalString(mod, funcArg1, sItemTag);
    ExecuteScript(sRealmScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetResourceUnitsOfItemForPotionBrewing(string sRealmScript, string sItemTag)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 14);
    SetLocalString(mod, funcArg1, sItemTag);
    ExecuteScript(sRealmScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetResourceUnitsOfItemForScrollScribing(string sRealmScript, string sItemTag)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 15);
    SetLocalString(mod, funcArg1, sItemTag);
    ExecuteScript(sRealmScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

//Instance functions

//Constructor
object CreateRealmInstance(string sRealmScript)
{
    object realmObject = CreateObject(OBJECT_TYPE_PLACEABLE, "obj_invisible", GetStartingLocation());
    SetLocalString(realmObject, "REALM_SCRIPT", sRealmScript);
    CreateObjectArray(REALM_BIOMES_ARRAY, 0, realmObject);
    CreateObjectArray(REALM_TILES_ARRAY, 0, realmObject);
    CreateObjectArray(REALM_QUESTS_ARRAY, 0, realmObject);
    SetLocalObject(GetModule(), "REALM", realmObject);
    return realmObject;
}

//Instance functions
object GetRealm()
{
    return GetLocalObject(GetModule(), "REALM");
}

string GetRealmName(object oRealm)
{
    return GetLocalString(oRealm, "REALM_NAME");
}

int GetRealmSize(object oRealm)
{
    return GetLocalInt(oRealm, "REALM_SIZE");
}

int GetRealmSeed(object oRealm)
{
    return GetLocalInt(oRealm, "REALM_SEED");
}

string GetRealmSeedString(object oRealm)
{
    return GetLocalString(oRealm, "REALM_SEED_TEXT");
}

string GetRealmScript(object oRealm)
{
    return GetLocalString(oRealm, "REALM_SCRIPT");
}

void SetRealmName(object oRealm, string sName)
{
    if (GetRealmName(oRealm) == "")
        SetLocalString(oRealm, "REALM_NAME", sName);
    else
        LogWarning("Multiple calls to SetRealmName");
}

void SetRealmSize(object oRealm, int nSizeConst)
{
    if (GetRealmSize(oRealm) == 0)
        SetLocalInt(oRealm, "REALM_SIZE", nSizeConst);
    else
        LogWarning("Multiple calls to SetRealmSize");
}

void SetRealmSeed(object oRealm, string sSeed)
{
    if (GetRealmSeed(oRealm) == 0)
    {
        SetLocalInt(oRealm, "REALM_SEED", GetSeedFromString(sSeed));
        SetLocalString(oRealm, "REALM_SEED_TEXT", sSeed);
    }
    else
        LogWarning("Multiple calls to SetRealmSeed");
}

object GetBiomeByRegionID(object oRealm, int nRegionID)
{
    return GetLocalObject(oRealm, "REALM_BIOME_" + IntToString(nRegionID));
}

void SetRealmPlot(object oRealm, object oPlot)
{
    SetLocalObject(oRealm, "REALM_PLOT", oPlot);
}

object GetRealmPlot(object oRealm)
{
    return GetLocalObject(oRealm, "REALM_PLOT");
}