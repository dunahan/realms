#include "inc_arrays"
#include "inc_str_rtile"

//Headers
void CreateRegionArray(string sName, int nArraySize=0, object oObject=OBJECT_INVALID);
void ClearRegionArray(string sName, object oObject=OBJECT_INVALID);
void AddRegionArrayElement(string sName, struct Region reg, int nBeginning=FALSE, object oObject=OBJECT_INVALID);
void DeleteRegionArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID);
void SetRegionArrayElement(string sName, int nIndex, struct Region reg, object oObject=OBJECT_INVALID);
struct Region GetRegionArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID);
int GetRegionArrayElementIntField(string sName, int nIndex, string sField, object oObject=OBJECT_INVALID);
string GetRegionArrayElementStringField(string sName, int nIndex, string sField, object oObject=OBJECT_INVALID);
int GetRegionArraySize(string sName, object oObject=OBJECT_INVALID);
void SetRegionArrayElementIntField(string sName, int nIndex, string sField, int nValue, object oObject=OBJECT_INVALID);

//Struct
struct Region
{
    int id; //region unique ID
    string type; //region's biome (represented as the biome's script name)
    int minlvl; //region's areas' minimum level
    int maxlvl; //region's areas' maximum level
    int fogdcol; //fog's day color in the region
    int fogncol; //fog's night color in the region
    string name; //region's in-game name
    string tiles; //name of an int array of this region's tiles' IDs
    string neighbors; //name of a string array of int arrays; 
                      //the array's indexes correspond to region IDs and elements of int arrays are IDs of tiles bordering those regions; 
                      //all elements are later on deleted from these lists apart from a single tile (the one with the pathway to the other region)
    int order; //number denoting the region's sequential order in a map; regions will be traversed in the order specified by these values, starting from 1  
};

//Definitions
void CreateRegionArray(string sName, int nArraySize=0, object oObject=OBJECT_INVALID)
{
    sName = sName+"_reg";
    CreateIntArray(sName+"_id", nArraySize, oObject);
    CreateStringArray(sName+"_regtype", nArraySize, oObject);
    CreateIntArray(sName+"_minlvl", nArraySize, oObject);
    CreateIntArray(sName+"_maxlvl", nArraySize, oObject);
    CreateIntArray(sName+"_fogdcol", nArraySize, oObject);
    CreateIntArray(sName+"_fogncol", nArraySize, oObject);
    CreateStringArray(sName+"_name", nArraySize, oObject);
    CreateStringArray(sName+"_tiles", nArraySize, oObject);
    CreateStringArray(sName+"_neighbors", nArraySize, oObject);
    CreateIntArray(sName+"_order", nArraySize, oObject);
}

void ClearRegionArray(string sName, object oObject=OBJECT_INVALID)
{
    sName = sName+"_reg";
    ClearIntArray(sName+"_id", oObject);
    ClearStringArray(sName+"_regtype", oObject);
    ClearIntArray(sName+"_minlvl", oObject);
    ClearIntArray(sName+"_maxlvl", oObject);
    ClearIntArray(sName+"_fogdcol", oObject);
    ClearIntArray(sName+"_fogncol", oObject);
    ClearStringArray(sName+"_name", oObject);
    ClearStringArray(sName+"_tiles", oObject);
    ClearStringArray(sName+"_neighbors", oObject);
    ClearIntArray(sName+"_order", oObject);
}

void AddRegionArrayElement(string sName, struct Region reg, int nBeginning=FALSE, object oObject=OBJECT_INVALID)
{
    sName = sName+"_reg";
    AddIntArrayElement(sName+"_id", reg.id, nBeginning, oObject);
    AddStringArrayElement(sName+"_regtype", reg.type, nBeginning, oObject);
    AddIntArrayElement(sName+"_minlvl", reg.minlvl, nBeginning, oObject);
    AddIntArrayElement(sName+"_maxlvl", reg.maxlvl, nBeginning, oObject);
    AddIntArrayElement(sName+"_fogdcol", reg.fogdcol, nBeginning, oObject);
    AddIntArrayElement(sName+"_fogncol", reg.fogncol, nBeginning, oObject);
    AddStringArrayElement(sName+"_name", reg.name, nBeginning, oObject);
    AddStringArrayElement(sName+"_tiles", reg.tiles, nBeginning, oObject);
    AddStringArrayElement(sName+"_neighbors", reg.neighbors, nBeginning, oObject);
    AddIntArrayElement(sName+"_order", reg.order, nBeginning, oObject);
}

void DeleteRegionArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    sName = sName+"_reg";
    DeleteIntArrayElement(sName+"_id", nIndex, oObject);
    DeleteStringArrayElement(sName+"_regtype", nIndex, oObject);
    DeleteIntArrayElement(sName+"_minlvl", nIndex, oObject);
    DeleteIntArrayElement(sName+"_maxlvl", nIndex, oObject);
    DeleteIntArrayElement(sName+"_fogdcol", nIndex, oObject);
    DeleteIntArrayElement(sName+"_fogncol", nIndex, oObject);
    DeleteStringArrayElement(sName+"_name", nIndex, oObject);
    DeleteStringArrayElement(sName+"_tiles", nIndex, oObject);
    DeleteStringArrayElement(sName+"_neighbors", nIndex, oObject);
    DeleteIntArrayElement(sName+"_order", nIndex, oObject);
}

void SetRegionArrayElement(string sName, int nIndex, struct Region reg, object oObject=OBJECT_INVALID)
{
    sName = sName+"_reg";
    SetIntArrayElement(sName+"_id", nIndex, reg.id, oObject);
    SetStringArrayElement(sName+"_regtype", nIndex, reg.type, oObject);
    SetIntArrayElement(sName+"_minlvl", nIndex, reg.minlvl, oObject);
    SetIntArrayElement(sName+"_maxlvl", nIndex, reg.maxlvl, oObject);
    SetIntArrayElement(sName+"_fogdcol", nIndex, reg.fogdcol, oObject);
    SetIntArrayElement(sName+"_fogncol", nIndex, reg.fogncol, oObject);
    SetStringArrayElement(sName+"_name", nIndex, reg.name, oObject);
    SetStringArrayElement(sName+"_tiles", nIndex, reg.tiles, oObject);
    SetStringArrayElement(sName+"_neighbors", nIndex, reg.neighbors, oObject);
    SetIntArrayElement(sName+"_order", nIndex, reg.order, oObject);
}

struct Region GetRegionArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    struct Region reg;
    sName = sName+"_reg";
    reg.id = GetIntArrayElement(sName+"_id", nIndex, oObject);
    reg.type = GetStringArrayElement(sName+"_regtype", nIndex, oObject);
    reg.minlvl = GetIntArrayElement(sName+"_minlvl", nIndex, oObject);
    reg.maxlvl = GetIntArrayElement(sName+"_maxlvl", nIndex, oObject);
    reg.fogdcol = GetIntArrayElement(sName+"_fogdcol", nIndex, oObject);
    reg.fogncol = GetIntArrayElement(sName+"_fogncol", nIndex, oObject);
    reg.name = GetStringArrayElement(sName+"_name", nIndex, oObject);
    reg.tiles = GetStringArrayElement(sName+"_tiles", nIndex, oObject);
    reg.neighbors = GetStringArrayElement(sName+"_neighbors", nIndex, oObject);
    reg.order = GetIntArrayElement(sName+"_order", nIndex, oObject);

    return reg;
}

int GetRegionArrayElementIntField(string sName, int nIndex, string sField, object oObject=OBJECT_INVALID)
{
    sName = sName+"_reg";
    return GetIntArrayElement(sName+"_"+sField, nIndex, oObject);
}

string GetRegionArrayElementStringField(string sName, int nIndex, string sField, object oObject=OBJECT_INVALID)
{
    sName = sName+"_reg";
    return GetStringArrayElement(sName+"_"+sField, nIndex, oObject);
}

int GetRegionArraySize(string sName, object oObject=OBJECT_INVALID)
{
    sName = sName+"_reg";
    return GetIntArraySize(sName+"_id", oObject);
}

void SetRegionArrayElementIntField(string sName, int nIndex, string sField, int nValue, object oObject=OBJECT_INVALID)
{
    sName = sName+"_reg";
    SetIntArrayElement(sName+"_"+sField, nIndex, nValue, oObject);
}