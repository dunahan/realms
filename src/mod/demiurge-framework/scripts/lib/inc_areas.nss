#include "inc_debug"

// Returns an area by resref - requires the mod_regareas script to be run on module's initial startup.
// Because only areas existing at the module's start are registered, this function won't return any instances created later on, but that's intended.
object GetAreaByResRef(string sResRef);

// A CopyArea wrapper that also copies the event scripts (and has API similar to CreateArea) - requires the mod_regareas script to be run on module's initial startup
object CopyAreaWithEventScripts(string sResRef, string sNewTag="", string sNewName="");


object GetAreaByResRef(string sResRef)
{
    object mod = GetModule();
    object area = GetLocalObject(mod, "STORED_AREA_"+sResRef);
    if (!GetIsObjectValid(area))
        LogWarning("Invalid area retrieved (inc_areas, GetAreaByResRef, "+sResRef+")");
    return area;
}

object CopyAreaWithEventScripts(string sResRef, string sNewTag="", string sNewName="")
{
    object original = GetAreaByResRef(sResRef);
    object new = CopyArea(original);
    if (sNewTag != "")
        SetTag(new, sNewTag);
    if (sNewName != "")
        SetName(new, sNewName);

    SetEventScript(new, EVENT_SCRIPT_AREA_ON_ENTER, GetEventScript(original, EVENT_SCRIPT_AREA_ON_ENTER));
    SetEventScript(new, EVENT_SCRIPT_AREA_ON_EXIT, GetEventScript(original, EVENT_SCRIPT_AREA_ON_EXIT));
    SetEventScript(new, EVENT_SCRIPT_AREA_ON_HEARTBEAT, GetEventScript(original, EVENT_SCRIPT_AREA_ON_HEARTBEAT));
    SetEventScript(new, EVENT_SCRIPT_AREA_ON_USER_DEFINED_EVENT, GetEventScript(original, EVENT_SCRIPT_AREA_ON_USER_DEFINED_EVENT));

    return new;
}