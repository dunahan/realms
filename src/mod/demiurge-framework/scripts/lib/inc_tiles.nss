#include "inc_arrays"
#include "inc_biomes"
#include "inc_str_rtile"

const string TILE_ENCOUNTERS_ARRAY = "TILE_ENCOUNTERS";
const string TILE_AREAS_ARRAY = "TileAreas";

struct Coordinates
{
    int x;
    int y;
};

// Converts coordinates to a single number index based on width of the realm grid
int ConvertTileCoordinatesToIndex(int x, int y, int nWidth);

// Converts a single number index to realm grid coordinates of a tile based on width of the realm grid
struct Coordinates ConvertTileIndexToCoordinates(int nIndex, int nWidth);

// Regular tile instance constructor
//object CreateTileInstance(object oBiome, string sResRef, string sTag, string sAreaName);

// Makes a tile out of an area
void InitializeTileDataOnArea(object oArea, struct RealmTile tile, string sTileName, object oBiome, object oRealm);

// Returns the tile's biome
object GetTileBiome(object oTile);

// Adds oArea to oTile as its area
void AddAreaToTile(object oTile, object oArea);

// Returns the tile oObject is in (oObject can also be an area, but can't be an item)
object GetTile(object oObject);

// Returns the tile's unique index
int GetTileIndex(object oTile);

// Returns the tile's X coordinate on the map
int GetTileX(object oTile);

// Returns the tile's Y coordinate on the map
int GetTileY(object oTile);

// Returns the tile's name
string GetTimeName(object oTile);

// Returns the spawned tile's (main) area (requires map width to be stored on the module as "Width")
object GetTileArea(int x, int y, object oRealm);

// Returns the first waypoint with the specified tag in the given area
object GetAreaWaypoint(object oArea, string sWaypointTag);

// Returns the first waypoint with the specified tag in the given tile (which includes all the tile's areas)
object GetTileWaypoint(object oTileArea, string sWaypointTag);

// Moves the PC to the generated map's starting point
void MovePCToStart(object oPC, object oRealm);

// Returns a tile object of oRealm with the given nIndex
object GetRealmTile(object oRealm, int nIndex);

// Marks tile as explored on the realm map
void SetMapTileExplored(object oTile, int nExplored=TRUE);

// Returns TRUE if the tile has been marked as explored on the realm map
int GetIsMapTileExplored(object oTile);

// Returns the tile exit waypoint of an area nearest to oTarget,
// returns OBJECT_INVALID if oTarget's area does not have any tile exit waypoints.
object GetNearestTileExitWaypoint(object oTarget);


int ConvertTileCoordinatesToIndex(int x, int y, int nWidth)
{
    return y * nWidth + x;
}

struct Coordinates ConvertTileIndexToCoordinates(int nIndex, int nWidth)
{
    int y = 0;
    while (nIndex >= (y+1)*nWidth)
        y++;
    int x = nIndex - y*nWidth;

    struct Coordinates result;
    result.x = x;
    result.y = y;
    return result;
}

object GetTileBiome(object oTile)
{
    return GetLocalObject(oTile, "TILE_BIOME");
}

void InitializeTileDataOnArea(object oArea, struct RealmTile tile, string sTileName, object oBiome, object oRealm)
{
    CreateObjectArray(TILE_AREAS_ARRAY, 1, oArea);
    SetObjectArrayElement(TILE_AREAS_ARRAY, 0, oArea, oArea); //this is the main area of this tile
    SetLocalInt(oArea, "X", tile.x);
    SetLocalInt(oArea, "Y", tile.y);
    SetLocalInt(oArea, "Index", tile.index);
    SetLocalString(oArea, "Name", sTileName);
    SetLocalObject(oArea, "Tile", oArea);
    SetLocalObject(oArea, "TILE_BIOME", oBiome);
    string array = BIOME_REGULAR_TILES_ARRAY;
    AddObjectArrayElement(array, oArea, FALSE, oBiome);
    CreateObjectArray(TILE_ENCOUNTERS_ARRAY, 0, oArea);
    string tag = "area_" + IntToString(tile.index);
    SetLocalObject(oRealm, tag, oArea);
}

void AddAreaToTile(object oTile, object oArea)
{
    SetLocalObject(oArea, "Tile", oTile);
    AddObjectArrayElement("TileAreas", oArea, TRUE, oTile);
}

object GetTile(object oObject)
{
    object area = GetArea(oObject);
    return GetLocalObject(area, "Tile");
}

int GetTileIndex(object oTile)
{
    return GetLocalInt(oTile, "Index");
}

int GetTileX(object oTile)
{
    return GetLocalInt(oTile, "X");
}

int GetTileY(object oTile)
{
    return GetLocalInt(oTile, "Y");
}

string GetTimeName(object oTile)
{
    return GetLocalString(oTile, "Name");
}

//Return the spawned tile's (main) area (requires map width to be stored on the realm as "Width")
object GetTileArea(int x, int y, object oRealm)
{
    int width = GetLocalInt(oRealm, "Width");
    int index = ConvertTileCoordinatesToIndex(x, y, width);
    object result = GetRealmTile(oRealm, index);
    return result;
}

//Return the first waypoint with the specified tag in the given area
object GetAreaWaypoint(object oArea, string sWaypointTag)
{
    object obj = GetFirstObjectInArea(oArea);
    while (GetIsObjectValid(obj))
    {
        if (GetObjectType(obj) == OBJECT_TYPE_WAYPOINT && GetTag(obj) == sWaypointTag)
            return obj;
        obj = GetNextObjectInArea(oArea);
    }
    return OBJECT_INVALID;
}

//Return the first waypoint with the specified tag in the given tile (which includes all the tile's areas)
object GetTileWaypoint(object oTile, string sWaypointTag)
{
    int areaNum = GetObjectArraySize("TileAreas", oTile);
    int i;
    for (i = 0; i < areaNum; i++)
    {
        object area = GetObjectArrayElement("TileAreas", i, oTile);
        object waypoint = GetAreaWaypoint(area, sWaypointTag);
        if (waypoint != OBJECT_INVALID)
            return waypoint;
    }
    return OBJECT_INVALID;
}

void MovePCToStart(object oPC, object oRealm)
{
    object startWp = GetLocalObject(oRealm, "StartWP");
    if (!GetIsObjectValid(startWp))
        LogFatal("Map starting point waypoint is invalid (inc_areas, MovePCToStart, startWp)");

    AssignCommand(oPC, ClearAllActions());
    AssignCommand(oPC, JumpToObject(startWp));
}

object GetRealmTile(object oRealm, int nIndex)
{
    string tag = "area_" + IntToString(nIndex);
    object area = GetLocalObject(oRealm, tag);
    return area;
}

void SetMapTileExplored(object oTile, int nExplored=TRUE)
{
    SetLocalInt(oTile, "TILE_EXPLORED", nExplored);
}

int GetIsMapTileExplored(object oTile)
{
    return GetLocalInt(oTile, "TILE_EXPLORED");
}

object GetNearestTileExitWaypoint(object oTarget)
{
    object area = GetArea(oTarget);
    float minDistance = 10000.0;
    object selectedWp = OBJECT_INVALID;

    float distance;
    object waypoint;

    waypoint = GetAreaWaypoint(area, "wp_north");
    distance = GetDistanceBetween(waypoint, oTarget);
    if (distance > 0.0f && distance < minDistance)
    {
        minDistance = distance;
        selectedWp = waypoint;
    }

    waypoint = GetAreaWaypoint(area, "wp_south");
    distance = GetDistanceBetween(waypoint, oTarget);
    if (distance > 0.0f && distance < minDistance)
    {
        minDistance = distance;
        selectedWp = waypoint;
    }

    waypoint = GetAreaWaypoint(area, "wp_east");
    distance = GetDistanceBetween(waypoint, oTarget);
    if (distance > 0.0f && distance < minDistance)
    {
        minDistance = distance;
        selectedWp = waypoint;
    }

    waypoint = GetAreaWaypoint(area, "wp_west");
    distance = GetDistanceBetween(waypoint, oTarget);
    if (distance > 0.0f && distance < minDistance)
    {
        minDistance = distance;
        selectedWp = waypoint;
    }

    return waypoint;
}