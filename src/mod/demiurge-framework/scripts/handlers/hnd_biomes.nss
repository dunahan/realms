#include "inc_hndvars"
#include "inc_biomes"
#include "inc_lootconsts"

string OnGetBiomeRegularAreaResRef(int nIndex, int nExitsFlag);
string OnGetBiomeBasicEncounterScript(int nIndex);
string OnGetBiomeAdvancedEncounterScript(int nIndex);
string OnGetBiomeSpecialAreaScript(int nIndex);
string OnGetBiomeTownScript(int nIndex);
int OnGetBiomeDayTrack(int nIndex);
int OnGetBiomeNightTrack(int nIndex);
int OnGetBiomeBattleTrack(int nIndex);
string OnGetBiomeRumor(int nIndex, int nLanguage);
string OnGetRandomBiomeRegionName(string sSeedName, int nLanguage);
struct TreasureChestLoot OnGetBiomeLoot(string sSeedName, int nLevel, int nLootQuality);
void OnPrepareRestEncounter(int nLevel);
object OnSpawnResourcePlaceable(string sSeedName, location lResourceLocation, int nLevel, int nIsUndergroundResource);

void ScriptHandler()
{
    object mod = GetModule();
    int handler = GetLocalInt(mod, funcHandler);

    if (handler == 0) //GetBiomeFogColorRed
    {        
        SetLocalInt(mod, funcResult, fogColorRed);
    }
    else if (handler == 1) //GetBiomeFogColorGreen
    {        
        SetLocalInt(mod, funcResult, fogColorGreen);
    }
    else if (handler == 2) //GetBiomeFogColorBlue
    {
        SetLocalInt(mod, funcResult, fogColorBlue);
    }
    else if (handler == 3) //GetBiomeWeatherChanceClear
    {
        SetLocalInt(mod, funcResult, weatherChanceClear);
    }
    else if (handler == 4) //GetBiomeWeatherChanceRain
    {
        SetLocalInt(mod, funcResult, weatherChanceRain);
    }
    else if (handler == 5) //GetBiomeWeatherChanceSnow
    {
        SetLocalInt(mod, funcResult, weatherChanceSnow);
    }
    else if (handler == 6) //GetBiomeSkyboxClear
    {
        SetLocalInt(mod, funcResult, skyboxClear);
    }
    else if (handler == 7) //GetBiomeSkyboxRain
    {
        SetLocalInt(mod, funcResult, skyboxRain);
    }
    else if (handler == 8) //GetBiomeSkyboxSnow
    {
        SetLocalInt(mod, funcResult, skyboxSnow);
    }
    else if (handler == 9) //GetBiomeMapColorRed
    {
        SetLocalInt(mod, funcResult, mapRed);
    }
    else if (handler == 10) //GetBiomeMapColorGreen
    {
        SetLocalInt(mod, funcResult, mapGreen);
    }
    else if (handler == 11) //GetBiomeMapColorBlue
    {
        SetLocalInt(mod, funcResult, mapBlue);
    }
    else if (handler == 12) //GetBiomeAreasIdentifier
    {
        SetLocalString(mod, funcResult, areasId);
    }
    else if (handler == 13) //GetBiomeRegularAreasNumber
    {
        SetLocalInt(mod, funcResult, regularAreasNumber);
    }
    else if (handler == 16) //GetBiomeBasicEncountersNumber
    {
        SetLocalInt(mod, funcResult, basicEncountersNumber);
    }
    else if (handler == 44) //GetBiomeAdvancedEncountersNumber
    {
        SetLocalInt(mod, funcResult, advancedEncountersNumber);
    }
    else if (handler == 45) //GetBiomeAdvancedEncounterChance
    {
        SetLocalInt(mod, funcResult, advancedEncounterChance);
    }
    else if (handler == 17) //GetBiomeSpecialAreasNumber
    {
        SetLocalInt(mod, funcResult, specialAreasNumber);
    }
    else if (handler == 18) //GetBiomeTownsNumber
    {
        SetLocalInt(mod, funcResult, townsNumber);
    }
    else if (handler == 19) //GetBiomeDayTracksNumber
    {
        SetLocalInt(mod, funcResult, dayTracksNumber);
    }
    else if (handler == 20) //GetBiomeNightTracksNumber
    {
        SetLocalInt(mod, funcResult, nightTracksNumber);
    }
    else if (handler == 21) //GetBiomeBattleTracksNumber
    {
        SetLocalInt(mod, funcResult, battleTracksNumber);
    }
    else if (handler == 22) //GetBiomeRumorsNumber
    {
        SetLocalInt(mod, funcResult, rumorsNumber);
    }
    else if (handler == 25) //GetBiomeRegularAreaResRef
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        int nExitsFlag = GetLocalInt(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetBiomeRegularAreaResRef(nIndex, nExitsFlag));
    }
    else if (handler == 27) //GetBiomeBasicEncounterScript
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetBiomeBasicEncounterScript(nIndex));
    }
    else if (handler == 43) //GetBiomeAdvancedEncounterScript
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetBiomeAdvancedEncounterScript(nIndex));
    }
    else if (handler == 28) //GetBiomeSpecialAreaScript
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetBiomeSpecialAreaScript(nIndex));
    }
    else if (handler == 29) //GetBiomeTownScript
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetBiomeTownScript(nIndex));
    }
    else if (handler == 30) //GetBiomeDayTrack
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetBiomeDayTrack(nIndex));
    }
    else if (handler == 31) //GetBiomeNightTrack
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetBiomeNightTrack(nIndex));
    }
    else if (handler == 32) //GetBiomeBattleTrack
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetBiomeBattleTrack(nIndex));
    }
    else if (handler == 33) //GetBiomeRumor
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        int nLanguage = GetLocalInt(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetBiomeRumor(nIndex, nLanguage));
    }
    else if (handler == 34) //GetRandomBiomeRegionName
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        int nLanguage = GetLocalInt(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetRandomBiomeRegionName(sSeedName, nLanguage));
    }
    else if (handler == 35) //GetBiomeLoot
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        int nLevel = GetLocalInt(mod, funcArg2);
        int nLootQuality = GetLocalInt(mod, funcArg3);
        struct TreasureChestLoot result = OnGetBiomeLoot(sSeedName, nLevel, nLootQuality);
        SetLocalInt(mod, funcResult, result.gold);
        SetLocalString(mod, funcResult2, result.itemResRef);
        SetLocalInt(mod, funcResult3, result.itemStackSize);
    }
    else if (handler == 36) //PrepareRestEncounter
    {
        int nLevel = GetLocalInt(mod, funcArg1);
        OnPrepareRestEncounter(nLevel);
    }
    else if (handler == 37) //GetBiomeRestEncounterChance
    {
        SetLocalFloat(mod, funcResult, restEncounterChance);
    }
    else if (handler == 39) //SpawnResourcePlaceable
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        location lResourceLocation = GetLocalLocation(mod, funcArg2);
        int nLevel = GetLocalInt(mod, funcArg3);
        int nIsUndergroundResource = GetLocalInt(mod, funcArg4);
        SetLocalObject(mod, funcResult, OnSpawnResourcePlaceable(sSeedName, lResourceLocation, nLevel, nIsUndergroundResource));
    }
    else if (handler == 40) //GetBiomeFogNightColorRed
    {        
        SetLocalInt(mod, funcResult, fogNightColorRed);
    }
    else if (handler == 41) //GetBiomeFogNightColorGreen
    {        
        SetLocalInt(mod, funcResult, fogNightColorGreen);
    }
    else if (handler == 42) //GetBiomeFogNightColorBlue
    {
        SetLocalInt(mod, funcResult, fogNightColorBlue);
    }
}

/*
string GetLootItemResRef(string sRealmScript, string sSeedName, int nLevel, int nItemPower)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 7);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalInt(mod, funcArg2, nLevel);
    SetLocalInt(mod, funcArg3, nItemPower);
    ExecuteScript(sRealmScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid loot item resref returned in realm " + sRealmScript + ", sSeedName = " + sSeedName + ", nLevel = %n, nItemPower = %n", nLevel, nItemPower);
    return result;
}
*/