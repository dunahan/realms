#include "inc_hndvars"

string OnGetSpecialAreaResRef(int nIndex, object oBiome, int nExitsFlag);
void OnInitializeSpecialArea(string sSeedName, object oBiome, object oSpecialArea, int nBiomeStartingLevel);
string OnGetSpecialAreaRumor(object oSpecialArea, int nIndex, int nLanguage);
string OnGetRandomSpecialAreaName(string sSeedName, int nLanguage);
string OnGetSpecialAreaRegularQuestScript(int nIndex);
string OnGetSpecialAreaSpecialtyQuestScript(int nIndex);

void ScriptHandler()
{
    object mod = GetModule();
    int handler = GetLocalInt(mod, funcHandler);

    if (handler == 0) //GetNumberOfSpecialAreas
    {
        SetLocalInt(mod, funcResult, areasNumber);
    }
    else if (handler == 1) //GetSpecialAreaMinPlayerLevel
    {
        SetLocalInt(mod, funcResult, minPlayerLevel);
    }
    else if (handler == 2) //GetSpecialAreaMaxPlayerLevel
    {
        SetLocalInt(mod, funcResult, maxPlayerLevel);
    }
    else if (handler == 3) //GetSpecialAreaRumorsNumber
    {
        SetLocalInt(mod, funcResult, rumorsNumber);
    }
    else if (handler == 8) //GetSpecialAreaDeadendAreas
    {
        SetLocalInt(mod, funcResult, deadendAreas);
    }
    else if (handler == 9) //GetSpecialAreaPassthroughAreas
    {
        SetLocalInt(mod, funcResult, passThroughAreas);
    }
    else if (handler == 4) //GetSpecialAreaResRef
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        object oBiome = GetLocalObject(mod, funcArg2);
        int nExitsFlag = GetLocalInt(mod, funcArg3);
        SetLocalString(mod, funcResult, OnGetSpecialAreaResRef(nIndex, oBiome, nExitsFlag));
    }
    else if (handler == 5) //InitializeSpecialArea
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        object oBiome = GetLocalObject(mod, funcArg2);
        object oSpecialArea = GetLocalObject(mod, funcArg3);
        int nBiomeStartingLevel = GetLocalInt(mod, funcArg4);
        OnInitializeSpecialArea(sSeedName, oBiome, oSpecialArea, nBiomeStartingLevel);
    }
    else if (handler == 6) //GetSpecialAreaRumor
    {
        object oSpecialArea = GetLocalObject(mod, funcArg1);
        int nIndex = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        SetLocalString(mod, funcResult, OnGetSpecialAreaRumor(oSpecialArea, nIndex, nLanguage));
    }
    else if (handler == 7) //GetRandomSpecialAreaName
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        int nLanguage = GetLocalInt(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetRandomSpecialAreaName(sSeedName, nLanguage));
    }
    else if (handler == 13) //GetNumberOfSpecialAreaRegularQuests
    {
        SetLocalInt(mod, funcResult, regularQuestsNumber);
    }
    else if (handler == 14) //GetNumberOfSpecialAreaSpecialtyQuests
    {
        SetLocalInt(mod, funcResult, specialtyQuestsNumber);
    }
    else if (handler == 10) //GetSpecialAreaHasLowQuestGenerationPriority
    {
        SetLocalInt(mod, funcResult, lowQuestGenerationPriority);
    }
    else if (handler == 11) //GetSpecialAreaRegularQuestScript
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetSpecialAreaRegularQuestScript(nIndex));
    }
    else if (handler == 12) //GetSpecialAreaSpecialtyQuestScript
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetSpecialAreaSpecialtyQuestScript(nIndex));
    }
}