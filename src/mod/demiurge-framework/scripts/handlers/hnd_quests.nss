#include "inc_hndvars"
#include "inc_quests"


string OnGetQuestgiverResRef(int nIndex, object oTown);
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage);
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage);
struct QuestReward OnGetQuestReward(string sSeedName);
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage);
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage);
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC);
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC);
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC);
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest);
void OnCustomQuestDeathLogic(object oQuest, object oDead);
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC);


void ScriptHandler()
{
    object mod = GetModule();
    int handler = GetLocalInt(mod, funcHandler);

    if (handler == 0) //GetNumberOfQuestgivers
    {
        SetLocalInt(mod, funcResult, questgiversNumber);
    }
    else if (handler == 1) //GetQuestgiverResRef
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        object oTown = GetLocalObject(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetQuestgiverResRef(nIndex, oTown));
    }
    else if (handler == 2) //GetRandomQuestgiverFirstName
    {
        object oQuestGiver = GetLocalObject(mod, funcArg1);
        string sSeedName = GetLocalString(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        SetLocalString(mod, funcResult, OnGetRandomQuestgiverFirstName(oQuestGiver, sSeedName, nLanguage));
    }
    else if (handler == 3) //GetRandomQuestgiverLastName
    {
        object oQuestGiver = GetLocalObject(mod, funcArg1);
        string sSeedName = GetLocalString(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        SetLocalString(mod, funcResult, OnGetRandomQuestgiverLastName(oQuestGiver, sSeedName, nLanguage));
    }
    else if (handler == 4) //GetQuestReward
    {
        string sSeedName = GetLocalString(mod, funcArg1);

        struct QuestReward result = OnGetQuestReward(sSeedName);
        SetLocalInt(mod, funcResult, result.gold);
        SetLocalString(mod, funcResult2, result.itemResRef);
        SetLocalInt(mod, funcResult3, result.itemStackSize);
    }
    else if (handler == 5) //GetQuestDialogLines
    {
        struct QuestReward questReward;
        questReward.gold = GetLocalInt(mod, funcArg1);
        questReward.itemResRef = GetLocalString(mod, funcArg2);
        questReward.itemStackSize = GetLocalInt(mod, funcArg3);
        object oQuest = GetLocalObject(mod, funcArg4);
        string sSeedName = GetLocalString(mod, funcArg5);
        int nLanguage = GetLocalInt(mod, funcArg6);

        struct QuestDialogLines result = OnGetQuestDialogLines(questReward, oQuest, sSeedName, nLanguage);
        SetLocalString(mod, funcResult, result.journalInitial);
        SetLocalString(mod, funcResult2, result.journalSuccess);
        SetLocalString(mod, funcResult3, result.journalFailure);
        SetLocalString(mod, funcResult4, result.journalSuccessFinished);
        SetLocalString(mod, funcResult16, result.journalAbandoned);
        SetLocalString(mod, funcResult5, result.npcIntroLine);
        SetLocalString(mod, funcResult6, result.npcContentLine);
        SetLocalString(mod, funcResult7, result.npcRewardLine);
        SetLocalString(mod, funcResult8, result.npcBeforeCompletionLine);
        SetLocalString(mod, funcResult9, result.playerReportingSuccess);
        SetLocalString(mod, funcResult10, result.playerReportingFailure);
        SetLocalString(mod, funcResult11, result.npcReceivedSuccess);
        SetLocalString(mod, funcResult12, result.npcReceivedFailure);
        SetLocalString(mod, funcResult13, result.npcAfterSuccess);
        SetLocalString(mod, funcResult14, result.npcAfterFailure);
        SetLocalString(mod, funcResult17, result.npcAfterRefusal);
        SetLocalString(mod, funcResult15, result.journalName);
    }
    else if (handler == 6) //GetQuestRumorsNumber
    {
        SetLocalInt(mod, funcResult, rumorsNumber);
    }
    else if (handler == 7) //GetQuestRumor
    {
        object oQuest = GetLocalObject(mod, funcArg1);
        int nIndex = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        SetLocalString(mod, funcResult, OnGetQuestRumor(oQuest, nIndex, nLanguage));
    }
    /*else if (handler == 8) //GetRandomQuestName
    {
        object oQuest = GetLocalObject(mod, funcArg1);
        string sSeedName = GetLocalString(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        SetLocalString(mod, funcResult, GetRandomQuestName(oQuest, sSeedName, nLanguage));
    }*/
    else if (handler == 9) //CustomQuestTakingLogic
    {
        object oQuest = GetLocalObject(mod, funcArg1);
        object oConversingPC = GetLocalObject(mod, funcArg2);
        OnCustomQuestTakingLogic(oQuest, oConversingPC);
    }
    else if (handler == 10) //CustomQuestCompletionLogic
    {
        object oQuest = GetLocalObject(mod, funcArg1);
        object oConversingPC = GetLocalObject(mod, funcArg2);
        OnCustomQuestCompletionLogic(oQuest, oConversingPC);
    }
    else if (handler == 11) //CustomQuestFailureLogic
    {
        object oQuest = GetLocalObject(mod, funcArg1);
        object oConversingPC = GetLocalObject(mod, funcArg2);
        OnCustomQuestFailureLogic(oQuest, oConversingPC);
    }
    else if (handler == 12) //CustomQuestInitializationLogic
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        object oQuest = GetLocalObject(mod, funcArg2);
        OnCustomQuestInitializationLogic(sSeedName, oQuest);
    }
    else if (handler == 13) //GetQuestMinPlayerLevel
    {
        SetLocalInt(mod, funcResult, minPlayerLevel);
    }
    else if (handler == 14) //GetQuestMaxPlayerLevel
    {
        SetLocalInt(mod, funcResult, maxPlayerLevel);
    }
    else if (handler == 15) //CustomQuestDeathLogic
    {
        object oQuest = GetLocalObject(mod, funcArg1);
        object oDead = GetLocalObject(mod, funcArg2);
        OnCustomQuestDeathLogic(oQuest, oDead);
    }
    else if (handler == 16) //CustomQuestRespawnLogic
    {
        object oQuest = GetLocalObject(mod, funcArg1);
        object oRespawningPC = GetLocalObject(mod, funcArg2);
        OnCustomQuestRespawnLogic(oQuest, oRespawningPC);
    }
}