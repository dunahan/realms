#include "inc_hndvars"
#include "inc_encounters"

const string ENCOUNTER_WP_NORTHWEST = "nw";
const string ENCOUNTER_WP_NORTHEAST = "ne";
const string ENCOUNTER_WP_NORTH = "n";
const string ENCOUNTER_WP_SOUTH = "s";
const string ENCOUNTER_WP_SOUTHWEST = "sw";
const string ENCOUNTER_WP_SOUTHEAST = "se";
const string ENCOUNTER_WP_WEST = "w";
const string ENCOUNTER_WP_EAST = "e";

void OnSpawnEncounter(object oEncounter, string sSeedName);
string OnGetEncounterRegularQuestScript(int nIndex);
string OnGetEncounterSpecialtyQuestScript(int nIndex);
string OnGetEncounterRumor(object oEncounter, int nIndex, int nLanguage);
string OnGetEncounterChampion(object oEncounter, int nIndex);

void ScriptHandler()
{
    object mod = GetModule();
    int handler = GetLocalInt(mod, funcHandler);

    if (handler == 0) //GetNumberOfEncounterRegularQuests
    {
        SetLocalInt(mod, funcResult, regularQuestsNumber);
    }
    else if (handler == 1) //GetEncounterMinPlayerLevel
    {
        SetLocalInt(mod, funcResult, minPlayerLevel);
    }
    else if (handler == 2) //GetEncounterMaxPlayerLevel
    {
        SetLocalInt(mod, funcResult, maxPlayerLevel);
    }
    else if (handler == 3) //SpawnEncounter
    {
        object oEncounter = GetLocalObject(mod, funcArg1);
        string sSeedName = GetLocalString(mod, funcArg2);
        OnSpawnEncounter(oEncounter, sSeedName);
    }
    else if (handler == 4) //GetEncounterRegularQuestScript
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetEncounterRegularQuestScript(nIndex));
    }
    else if (handler == 5) //GetEncounterRumorsNumber
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalInt(mod, funcResult, rumorsNumber);
    }
    else if (handler == 6) //GetEncounterRumor
    {
        object oEncounter = GetLocalObject(mod, funcArg1);
        int nIndex = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        SetLocalString(mod, funcResult, OnGetEncounterRumor(oEncounter, nIndex, nLanguage));
    }
    else if (handler == 7) //GetEncounterHasLowQuestGenerationPriority
    {
        SetLocalInt(mod, funcResult, lowQuestGenerationPriority);
    }
    else if (handler == 8) //GetNumberOfEncounterSpecialtyQuests
    {
        SetLocalInt(mod, funcResult, specialtyQuestsNumber);
    }
    else if (handler == 9) //GetEncounterSpecialtyQuestScript
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetEncounterSpecialtyQuestScript(nIndex));
    }
    else if (handler == 10) //GetNumberOfEncounterChampions
    {
        SetLocalInt(mod, funcResult, championsNumber);
    }
    else if (handler == 11) //GetEncounterChampion
    {
        object oEncounter = GetLocalObject(mod, funcArg1);
        int nIndex = GetLocalInt(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetEncounterChampion(oEncounter, nIndex));
    }
}
