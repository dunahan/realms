#include "inc_hndvars"

string OnGetFacilityAreaResRef(int nIndex, object oTown);
void OnInitializeFacility(string sSeedName, object oTown, object oFacility, int nBiomeStartingLevel);
string OnGetFacilityRumor(object oFacility, int nIndex, int nLanguage);
string OnGetRandomFacilityName(string sSeedName, int nLanguage);

void ScriptHandler()
{
    object mod = GetModule();
    int handler = GetLocalInt(mod, funcHandler);

    if (handler == 0) //GetNumberOfFacilityAreas
    {
        SetLocalInt(mod, funcResult, areasNumber);
    }
    else if (handler == 1) //GetFacilityMinPlayerLevel
    {
        SetLocalInt(mod, funcResult, minPlayerLevel);
    }
    else if (handler == 2) //GetFacilityMaxPlayerLevel
    {
        SetLocalInt(mod, funcResult, maxPlayerLevel);
    }
    else if (handler == 3) //GetFacilityRumorsNumber
    {
        SetLocalInt(mod, funcResult, rumorsNumber);
    }
    else if (handler == 4) //GetFacilityAreaResRef
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        object oTown = GetLocalObject(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetFacilityAreaResRef(nIndex, oTown));
    }
    else if (handler == 5) //InitializeFacility
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        object oTown = GetLocalObject(mod, funcArg2);
        object oFacility = GetLocalObject(mod, funcArg3);
        int nBiomeStartingLevel = GetLocalInt(mod, funcArg4);
        OnInitializeFacility(sSeedName, oTown, oFacility, nBiomeStartingLevel);
    }
    else if (handler == 6) //GetFacilityRumor
    {
        object oFacility = GetLocalObject(mod, funcArg1);
        int nIndex = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        SetLocalString(mod, funcResult, OnGetFacilityRumor(oFacility, nIndex, nLanguage));
    }
    else if (handler == 7) //GetRandomFacilityName
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        int nLanguage = GetLocalInt(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetRandomFacilityName(sSeedName, nLanguage));
    }
}