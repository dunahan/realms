#include "inc_hndvars"

string OnGetChampionResRef(int nIndex);
string OnGetTrophyName(string sChampionName, int nLanguage);
string OnGetTrophyDescription(string sChampionName, int nLanguage);
int OnGetChampionBounty(int nLevel, string sSeedName);
void OnInitializeChampion(object oChampion, object oEncounter, string sSeedName);
void OnApplyChampionModifier(object oChampion, int nModifierIndex, int nLevel);
string OnGetChampionName(int nModifierIndex, string sSeedName, int nLanguage);
string OnGetChampionRumor(string sChampionName, int nRumorIndex, int nModifierIndex, int nLanguage);
string OnGetChampionDescription(string sChampionName, int nBounty, int nModifierIndex, string sSeedName, int nLanguage);

void ScriptHandler()
{
    object mod = GetModule();
    int handler = GetLocalInt(mod, funcHandler);

    if (handler == 0) //GetNumberOfChampionBlueprints
    {
        SetLocalInt(mod, funcResult, blueprintsNumber);
    }
    else if (handler == 1) //GetChampionMinPlayerLevel
    {
        SetLocalInt(mod, funcResult, minPlayerLevel);
    }
    else if (handler == 2) //GetChampionMaxPlayerLevel
    {
        SetLocalInt(mod, funcResult, maxPlayerLevel);
    }
    else if (handler == 3) //GetNumberOfChampionRumors
    {
        SetLocalInt(mod, funcResult, rumorsNumber);
    }
    else if (handler == 4) //GetNumberOfChampionModifiers
    {
        SetLocalInt(mod, funcResult, modifiersNumber);
    }
    else if (handler == 5) //GetChampionTrophyResRef
    {
        SetLocalString(mod, funcResult, trophyResRef);
    }
    else if (handler == 6) //GetChampionResRef
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetChampionResRef(nIndex));
    }
    else if (handler == 7) //GetTrophyName
    {
        string sChampionName = GetLocalString(mod, funcArg1);
        int nLanguage = GetLocalInt(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetTrophyName(sChampionName, nLanguage));
    }
    else if (handler == 8) //GetTrophyDescription
    {
        string sChampionName = GetLocalString(mod, funcArg1);
        int nLanguage = GetLocalInt(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetTrophyDescription(sChampionName, nLanguage));
    }
    else if (handler == 9) //GetChampionBounty
    {
        int nLevel = GetLocalInt(mod, funcArg1);
        string sSeedName = GetLocalString(mod, funcArg2);
        SetLocalInt(mod, funcResult, OnGetChampionBounty(nLevel, sSeedName));
    }
    else if (handler == 10) //InitializeChampion
    {
        object oChampion = GetLocalObject(mod, funcArg1);
        object oEncounter = GetLocalObject(mod, funcArg2);
        string sSeedName = GetLocalString(mod, funcArg3);
        OnInitializeChampion(oChampion, oEncounter, sSeedName);
    }
    else if (handler == 11) //ApplyChampionModifier
    {
        object oChampion = GetLocalObject(mod, funcArg1);
        int nModifierIndex = GetLocalInt(mod, funcArg2);
        int nLevel = GetLocalInt(mod, funcArg3);
        OnApplyChampionModifier(oChampion, nModifierIndex, nLevel);
    }
    else if (handler == 12) //GetChampionName
    {
        int nModifierIndex = GetLocalInt(mod, funcArg1);
        string sSeedName = GetLocalString(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        SetLocalString(mod, funcResult, OnGetChampionName(nModifierIndex, sSeedName, nLanguage));
    }
    else if (handler == 13) //GetChampionRumor
    {
        string sChampionName = GetLocalString(mod, funcArg1);
        int nRumorIndex = GetLocalInt(mod, funcArg2);
        int nModifierIndex = GetLocalInt(mod, funcArg3);
        int nLanguage = GetLocalInt(mod, funcArg4);
        SetLocalString(mod, funcResult, OnGetChampionRumor(sChampionName, nRumorIndex, nModifierIndex, nLanguage));
    }
    else if (handler == 14) //GetChampionDescription
    {
        string sChampionName = GetLocalString(mod, funcArg1);
        int nBounty = GetLocalInt(mod, funcArg2);
        int nModifierIndex = GetLocalInt(mod, funcArg3);
        string sSeedName = GetLocalString(mod, funcArg4);
        int nLanguage = GetLocalInt(mod, funcArg5);
        SetLocalString(mod, funcResult, OnGetChampionDescription(sChampionName, nBounty, nModifierIndex, sSeedName, nLanguage));
    }
}
