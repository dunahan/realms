#include "inc_hndvars"
#include "inc_plots"

struct PlotJournalEntries OnGetPlotJournalEntries(object oPlot, string sSeedName, int nLanguage);
string OnGetPlotBossAreaScript(int nIndex);
string OnGetPlotRumor(object oPlot, int nIndex, int nLanguage);
string OnGetPlotEncounterScript(int nIndex);

void ScriptHandler()
{
    object mod = GetModule();
    int handler = GetLocalInt(mod, funcHandler);

    if (handler == 0) //GetNumberOfPlotBossAreas
    {
        SetLocalInt(mod, funcResult, bossAreasNumber);
    }
    else if (handler == 1) //GetPlotMinPlayerLevel
    {
        SetLocalInt(mod, funcResult, minPlayerLevel);
    }
    else if (handler == 2) //GetPlotMaxPlayerLevel
    {
        SetLocalInt(mod, funcResult, maxPlayerLevel);
    }
    else if (handler == 3) //GetPlotRumorsNumber
    {
        SetLocalInt(mod, funcResult, rumorsNumber);
    }
    else if (handler == 4) //GetPlotJournalEntries
    {
        object oPlot = GetLocalObject(mod, funcArg1);
        string sSeedName = GetLocalString(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        
        struct PlotJournalEntries result = OnGetPlotJournalEntries(oPlot, sSeedName, nLanguage);
        SetLocalString(mod, funcResult, result.journalName);
        SetLocalString(mod, funcResult2, result.journalPlotDescription);
        SetLocalString(mod, funcResult3, result.journalStart);
        SetLocalString(mod, funcResult4, result.journalFinishedRegionQuests);
        SetLocalString(mod, funcResult5, result.journalFailedRegionQuests);
        SetLocalString(mod, funcResult6, result.journalReachedNextVillage);
        SetLocalString(mod, funcResult7, result.journalFinishedLastRegionQuests);
        SetLocalString(mod, funcResult8, result.journalFailedLastRegionQuests);
    }
    else if (handler == 5) //GetPlotBossAreaScript
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetPlotBossAreaScript(nIndex));
    }    
    else if (handler == 6) //GetPlotRumor
    {
        object oPlot = GetLocalObject(mod, funcArg1);
        int nIndex = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        SetLocalString(mod, funcResult, OnGetPlotRumor(oPlot, nIndex, nLanguage));
    }
    if (handler == 7) //GetPlotEncountersNumber
    {
        SetLocalInt(mod, funcResult, encountersNumber);
    }
    if (handler == 8) //GetPlotEncounterScript
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetPlotEncounterScript(nIndex));
    }
}
