#include "inc_generation"
void main()
{
    object oPC = GetClickingObject();
    object storeArea = GetArea(OBJECT_SELF);
    object tile = GetLocalObject(storeArea, "Tile");
    object destinationWp = GetTileWaypoint(tile, "wp_store");
    AssignCommand(oPC, JumpToObject(destinationWp));
}
