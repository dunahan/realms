#include "inc_realmsdb"
void main() 
{
    object area = GetArea(OBJECT_SELF);
    if (area == GetAreaFromLocation(GetStartingLocation()) || GetTag(area) == "companion_area")
    {
        string msg = GetLocalizedString("You can only use the map after starting the adventure!", "Mo�esz u�y� mapy dopiero po rozpocz�ciu przygody!");
        FloatingTextStringOnCreature(msg, OBJECT_SELF, FALSE);
        return;
    }

    ClearAllActions();
    ActionStartConversation(OBJECT_SELF, "settings", TRUE, FALSE);
}