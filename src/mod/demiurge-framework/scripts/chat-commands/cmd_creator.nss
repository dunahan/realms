#include "inc_language"
#include "inc_common"
void main()
{
    object wp = GetWaypointByTag("wp_creatorarea");

    if (GetArea(OBJECT_SELF) != GetAreaFromLocation(GetStartingLocation()))
        return;

    string errorEng = "Companion creator is available only in single-player mode!";
    string errorPl = "Tryb kreatora towarzyszy dost�pny jest tylko w grze jednoosobowej!";
    if (!GetIsSinglePlayer())
    {
        SendMessageToPC(OBJECT_SELF, GetLocalizedString(errorEng, errorPl));
        return;
    }

    JumpToObject(wp);
}
