#include "inc_chat"
#include "inc_debug"
#include "inc_realms"
#include "inc_generation"
#include "inc_tiles"
void main()
{
    string params = GetChatCommandParameters();

    int spaceIndex = FindSubString(params, " ");
    if (spaceIndex == -1)
    {
        SendMessageToPC(OBJECT_SELF, "The command requires X and Y realm coordinates, example: /teleport 0 3");
        return;
    }
    string xPart = GetStringLeft(params, spaceIndex);
    string yPart = GetSubString(params, spaceIndex+1, 9999);
    
    int x = StringToInt(xPart);
    int y = StringToInt(yPart);
    if ((x == 0 && xPart != "0") || (y == 0 && yPart != "0"))
    {
        SendMessageToPC(OBJECT_SELF, "Bad parameters, the command expects two numbers");
        return;
    }

    object realm = GetRealm();
    object tile = GetTileArea(x, y, realm);
    if (!GetIsObjectValid(tile))
    {
        SendMessageToPC(OBJECT_SELF, "No tile with coordinates ("+ xPart +","+ yPart +")");
        return;
    }

    object targetWp;
    int i;
    for (i = 0; i < 5; i++)
    {
        if (i == 4)
            LogWarning("Tile with coordinates (%n,%n) contains no exit waypoints", x, y);

        string wpTag;
        switch (i)
        {
            case 0: wpTag = "wp_south"; break;
            case 1: wpTag = "wp_north"; break;
            case 2: wpTag = "wp_east"; break;
            case 3: wpTag = "wp_west"; break;
        }
        targetWp = GetTileWaypoint(tile, wpTag);
        if (GetIsObjectValid(targetWp))
            break;
    }

    AssignCommand(OBJECT_SELF, ClearAllActions());
    AssignCommand(OBJECT_SELF, JumpToObject(targetWp));
    SetMapTileExplored(tile);
}