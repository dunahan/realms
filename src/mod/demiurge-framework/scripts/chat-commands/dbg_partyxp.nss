#include "inc_chat"
#include "inc_experience"
#include "inc_companions"

void SetCompanionXPVoid(object companion, int xp)
{
    SetCompanionXP(companion, xp);
}

void main()
{
    string param = GetChatCommandParameters();
    int xp = StringToInt(param);
    if (xp == 0)
        return;

    int finalXp = GetPenaltyAdjustedExperience(OBJECT_SELF, xp);
    GiveXPToCreature(OBJECT_SELF, finalXp);

    int henchNum = 1;
    object henchman = GetHenchman(OBJECT_SELF, henchNum);
    while (GetIsObjectValid(henchman))
    {
        //we're only interested with henchmen with ID to avoid giving xp to horses
        if (GetLocalString(henchman, "ID") != "")
        {
            finalXp = GetPenaltyAdjustedExperience(henchman, xp);
            int xp = GetXP(henchman) + finalXp;
            //delay is there because the henchman may be replaced with another instance and we don't want that to interfere with the looping
            DelayCommand(0.1, SetCompanionXPVoid(henchman, xp));
            LogInfo("Henchman "+GetName(henchman)+" received %n experience (out of base %n)", finalXp, xp);
        }
        henchman = GetHenchman(OBJECT_SELF, ++henchNum);
    }
}