#include "inc_generation"
#include "inc_realms"
#include "inc_tiles"
#include "inc_towns"
#include "inc_arrays"
#include "inc_ruleset"
#include "inc_companions"
#include "inc_stats"
void main()
{
    object PC = GetClickingObject();
    object realm = GetRealm();
    MovePCToStart(PC, realm);

    //Set respawn
    object startWp = GetLocalObject(realm, "StartWP");
    object tile = GetTile(startWp);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    SetPCRespawn(PC, town);

    //for the first PC only
    if (GetLocalInt(OBJECT_SELF, "PCEntered") == FALSE)
    {
        //Spawn henchmen
        int i;
        string valuesArray = "CompanionIds";
        int companions = GetStringArraySize(valuesArray);
        SetNumberOfCompanions(realm, companions);

        for (i = 0; i < companions; i++)
        {
            string id = GetStringArrayElement(valuesArray, i);
            object companion = SpawnCompanion(id, GetLocation(PC));
            AddHenchman(PC, companion);
        }

        //Give starting gold
        struct Ruleset ruleset = GetRuleset(realm);
        int gold = FloatToInt(ruleset.startingGold * nStartingGold);
        GiveGoldToCreature(PC, gold);

        //Give starting soulstones
        int size = GetLocalInt(OBJECT_SELF, "MAPSIZE");
        size *= size;
        int soulstones = FloatToInt(ruleset.startingSoulstones * fStartingSoulstonesPerMapTile * size);
        object soulstonesStack = CreateItemOnObject("it_soulstone", PC, soulstones);
        SetPlotFlag(soulstonesStack, TRUE);

        SetLocalInt(OBJECT_SELF, "PCEntered", TRUE);
    }
}
