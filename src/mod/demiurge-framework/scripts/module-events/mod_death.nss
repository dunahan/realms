#include "inc_language"

void main()
{
    object PC = GetLastPlayerDied();
    SetLocalLocation(PC, "LastDeathLocation", GetLocation(PC));

    object soulstone = GetItemPossessedBy(PC, "it_soulstone");
    int canRespawn = soulstone != OBJECT_INVALID;

    string plYouDied = GetGender(PC) == GENDER_MALE ? "Zgin��e�" : "Zgin�a�";
    string plYouLost = GetGender(PC) == GENDER_MALE ? "przegra�e�" : "przegra�a�";
    string enMsg = canRespawn 
        ? "You have died. You can wait for help or use a soulstone to respawn in the last settlement visited." 
        : "You have died and have no soulstones left. Unless there is anyone who can help you, you have lost the game.";
    string plMsg = canRespawn 
        ? plYouDied+". Mo�esz czeka� na pomoc, albo u�y� Kamienia Duszy, aby odrodzi� si� w ostatniej odwiedzonej wiosce."
        : plYouDied+" i nie posiadasz �adnych Kamieni Dusz, kt�re pozwoli�yby ci si� odrodzi�. Je�eli nie ma nikogo, kto m�g�by ci pom�c, "+plYouLost+" gr�.";
    string msg = GetServerLanguage() == LANGUAGE_POLISH ? plMsg  : enMsg;

    DelayCommand(2.5, PopUpDeathGUIPanel(PC, canRespawn, TRUE, 0, msg));
}