// This is an OnPlayerTarget script that is necessary for the EnterTargetingModeToExecuteScript function from inc_targeting to work.

void main()
{
    object PC = GetLastPlayerToSelectTarget();
    string script = GetLocalString(PC, "TARGET_SCRIPT");
    DeleteLocalString(PC, "TARGET_SCRIPT");
    ExecuteScript(script);
}