#include "inc_quests"
#include "inc_realms"
#include "inc_arrays"
// OnRespawn handler that fires custom respawn logic of every quest currently in progress

void main()
{
    object PC = GetLastRespawnButtonPresser();
    int i;
    object realm = GetRealm();
    int questsNum = GetObjectArraySize(REALM_QUESTS_ARRAY, realm);

    for (i = 0; i < questsNum; i++)
    {
        object quest = GetObjectArrayElement(REALM_QUESTS_ARRAY, i, realm);
        if (!GetIsQuestAccepted(quest) || GetQuestState(quest) != QUEST_STATE_LINGERING)
            continue;
        
        string questScript = GetQuestScript(quest);
        CustomQuestRespawnLogic(quest, PC);
    }
}