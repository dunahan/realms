#include "inc_debug"
void main()
{
    object oCaster = OBJECT_SELF; //get caster
    object oItem = GetSpellCastItem(); //get item from which was spell cast
    int nSpellId = GetSpellId(); //get spell id
    int nClass = GetLastSpellCastClass();
    int origCasterLevel = GetCasterLevel(oCaster); //get original caster level
    int origDC = GetSpellSaveDC(); //get original DC
    int origMeta = GetMetaMagicFeat(); //get original metamagic feat

    //if the spell was cast from item then exit because the bonuses shown here are not applicable for items
    if (oItem != OBJECT_INVALID)
        return;

    int newDC = origDC;
    int newMeta = origMeta;
    int newCasterLevel = origCasterLevel;

    //Get modifiers or overrides from inc_spellhook
    int overrideDC = GetLocalInt(OBJECT_SELF, "SPELLHOOK_DC");
    int modifierDC = GetLocalInt(OBJECT_SELF, "SPELLHOOK_MOD_DC");
    int overrideMeta = GetLocalInt(OBJECT_SELF, "SPELLHOOK_META");
    int modifierMeta = GetLocalInt(OBJECT_SELF, "SPELLHOOK_MOD_META");
    int overrideLvl = GetLocalInt(OBJECT_SELF, "SPELLHOOK_LVL");
    int modifierLvl = GetLocalInt(OBJECT_SELF, "SPELLHOOK_MOD_LVL");
    if (overrideDC != 0)
        newDC = overrideDC;
    else if (modifierDC != 0)
        newDC += modifierDC;
    if (overrideMeta != 0)
        newMeta = overrideMeta;
    else if (modifierMeta != 0)
        newMeta |= modifierMeta;
    if (overrideLvl != 0)
        newCasterLevel = overrideLvl;
    else if (modifierLvl != 0)
        newCasterLevel += modifierLvl;

    //Very Difficult setting: increase NPCs' spell DC by 2 except for save-or-die spells, make all spells automatically empowered and extended
    if (GetGameDifficulty() == GAME_DIFFICULTY_DIFFICULT && !GetIsPC(oCaster))
    {
        newMeta = origMeta | METAMAGIC_EMPOWER | METAMAGIC_EXTEND;

        switch (nSpellId)
        {
            case SPELL_BANISHMENT:
            case SPELL_IMPLOSION:
            case SPELL_PHANTASMAL_KILLER:
            case SPELL_WEIRD:
            case SPELL_WAIL_OF_THE_BANSHEE:
            case SPELL_CIRCLE_OF_DEATH:
            case SPELL_CLOUDKILL:
            case SPELL_DESTRUCTION:
            case SPELL_FINGER_OF_DEATH:
            case SPELL_UNDEATH_TO_DEATH:
            case SPELL_SLAY_LIVING:
                break;
            default:
                newDC += 2;
                break;
        }
    }

    //now when we are done, set the variables
    SetLocalInt(oCaster, "SPELL_DC_OVERRIDE", newDC);
    SetLocalInt(oCaster, "SPELL_CASTER_LEVEL_OVERRIDE", newCasterLevel);
    SetLocalInt(oCaster, "SPELL_METAMAGIC_OVERRIDE", newMeta);
}
