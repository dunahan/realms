#include "inc_arrays"
#include "inc_tiles"
#include "inc_towns"
#include "inc_biomes"
#include "inc_language"
#include "inc_resting"
#include "inc_common"

void main()
{
    object mod = GetModule();
    string restingVar = "RESTING_IN_PROGRESS";
    object PC = GetLastPCRested();
    object area = GetArea(PC);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    string biomeScript = GetBiomeScript(biome);
    object town = GetTownOfBiome(biome);
    int eventType = GetLastRestEventType();
    int restStarted = eventType == REST_EVENTTYPE_REST_STARTED;

    //Disallow resting if the area is marked as disallowing resting
    if (restStarted && GetIsRestDisallowed(area))
    {
        string customMsg = GetCustomRestAttemptMessage(area);
        string msg = customMsg != "" ? customMsg : GetLocalizedString("You can't rest in this area at this time.", "W tej chwili nie mo�esz odpoczywa� na tym obszarze.");

        AssignCommand(PC, ClearAllActions(TRUE));
        FloatingTextStringOnCreature(msg, PC, FALSE);
        return;
    }

    //Disallow resting in town areas: main area, store area, facility area (their templates should have the "No Rest" flag checked, but just in case)
    string tag = GetTag(area);
    if (restStarted && tile == town && tag != "companion_area" && tag != "start")
    {
        if (area != GetTavernOfTown(town) || GetLocalInt(area, "CanRest") == FALSE)
        {
            AssignCommand(PC, ClearAllActions(TRUE));
            FloatingTextStringOnCreature(GetLocalizedString("You must pay the innkeeper if you want to rest here.", "Musisz zap�aci� karczmarzowi, aby m�c tu odpoczywa�."), PC, FALSE);
            return;
        }
    }

    //Attempt to spawn a rest encounter
    if (restStarted && GetCanRestEncounterSpawn(area))
    {
        //Check if area has defined encounters and use them if so
        int areaRestEncounters = GetStringArraySize("RestEncounters", area);
        if (areaRestEncounters > 0)
        {
            string encounterArray = GetStringArrayElement("RestEncounters", Random(areaRestEncounters), area);
            CopyStringArray(encounterArray, "RestEncounter", area, OBJECT_INVALID);
        }
        //Use biome rest encounter preparation function instead
        else
        {
            int level = GetRegionStartingLevel(biome);
            PrepareRestEncounter(biomeScript, level);
        }

        float encounterChance;
        //Check if area has overriden the rest encounter chance
        if (GetAreaHasOwnRestEncounterChance(area))
        {
            encounterChance = GetAreaSpecificRestEncounterChance(area);
        }
        else
        {
            encounterChance = GetBiomeRestEncounterChance(biomeScript);
        }

        float interruptionTime = IntToFloat(Random(8)+1);
        int encounterWillSpawn = RandomlySpawnRestEncounter(PC, "RestEncounter", encounterChance, interruptionTime);
        if (encounterWillSpawn)
            return;
    }

    //Passing time on successful rest, but if multiple PCs start resting, we don't want everyone finishing to cause a time skip
    if (restStarted)
        SetLocalInt(mod, restingVar, TRUE);

    if (eventType == REST_EVENTTYPE_REST_FINISHED && GetLocalInt(mod, restingVar))
    {
        DeleteLocalInt(mod, restingVar);
        FloatingTextStringOnAllPCs(GetLocalizedString("6 hours have passed.", "Min�o 6 godzin."));
        SetTime(GetTimeHour()+6, GetTimeMinute(), GetTimeSecond(), GetTimeMillisecond());
    }

    //If we can rest, go ahead and execute regular rest script
    ExecuteScript("x2_mod_def_rest");
}