#include "x2_inc_switches"
#include "inc_crafting"
#include "inc_language"
#include "inc_debug"

void main()
{
    object item = GetItemActivated();
    if (GetLocalString(item, "CRAFT_RESREF") == "")
        return;

    object user = GetItemActivator();
    int result = CraftItemFromRecipe(user, item);
    switch (result)
    {
        case 0:
            FloatingTextStringOnCreature(GetLocalizedString("Invalid recipe item!", "Niepoprawny przedmiot schematu!"), user);
            LogWarning("Invalid recipe item with resref: " + GetResRef(item));
            break;
        case 1:
            FloatingTextStringOnCreature(GetLocalizedString("You don't have all the materials required!", "Nie masz wszystkich wymaganych materia��w!"), user);
            break;
        case 2:
            FloatingTextStringOnCreature(GetLocalizedString("Crafting ended in failure!", "Wytwarzanie przedmiotu zako�czone pora�k�!"), user);
            break;
        case 3:
            FloatingTextStringOnCreature(GetLocalizedString("Crafting succeeded!", "Wytwarzanie przedmiotu zako�czone powodzeniem!"), user);
            break;
    }
}