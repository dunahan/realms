void CountInstructions()
{
    int instructionCount = 0;
    for (instructionCount = 0; instructionCount < 44000; instructionCount++)
        SetLocalInt(OBJECT_SELF, "INSTRUCTION_COUNT", instructionCount);
    SetLocalInt(OBJECT_SELF, "TMI_INCREASED", TRUE);
}

void main()
{
    CountInstructions();
}