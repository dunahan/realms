#include "inc_colors"
#include "inc_realmsdb"
#include "inc_scriptevents"

void StubbornTeleportPC(object oPC, object oWP)
{
    if (GetArea(oPC) == GetArea(oWP))
        AssignCommand(oPC, JumpToObject(oWP, FALSE));
    else
        DelayCommand(0.1, StubbornTeleportPC(oPC, oWP));
}

void main()
{
    string pcsJoinedVar = "PCS_JOINED"; //this is actually just the number of times any PC connected to the game in this session, but no matter
    object PC = GetEnteringObject();
    object module = GetModule();
    SetPCEventScripts(PC);
    int playersJoined = GetLocalInt(module, pcsJoinedVar);
    playersJoined++;

    //Simply don't teleport a new player into any starting area's room if there are too many
    if (playersJoined < 6 && GetLocalInt(PC, "JOINED") == FALSE)
    {
        SetLocalInt(PC, "JOINED", TRUE);
        SetLocalInt(GetModule(), pcsJoinedVar, playersJoined);
        string num = IntToString(playersJoined);
        object wp = GetWaypointByTag("wp_start" + num);
        object door = GetObjectByTag("door_start" + num);
        SetLocked(door, FALSE);

        StubbornTeleportPC(PC, wp);
    }

    //Give the player a map and a guide if they don't have one yet (well, they shouldn't, they are supposed to play a new character! :< )
    object item = GetItemPossessedBy(PC, "it_map");
    if (!GetIsObjectValid(item))
        CreateItemOnObject("it_map", PC);
    item = GetItemPossessedBy(PC, "it_guide");
    if (!GetIsObjectValid(item))
        CreateItemOnObject("it_guide", PC);

    //Don't do anything else if the TMI limit is set too low
    int tmiIncreased = GetLocalInt(OBJECT_SELF, "TMI_INCREASED");
    if (!tmiIncreased)
        return;

    //Verify if the player exists in the Realms database
    int newPlayer = RegisterPlayer(PC);
    if (newPlayer)
        DelayCommand(1.0f, AssignCommand(PC, ActionStartConversation(OBJECT_SELF, "guide", TRUE, FALSE)));
}
