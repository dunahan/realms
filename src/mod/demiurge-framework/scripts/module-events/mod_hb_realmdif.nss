#include "inc_realms"
#include "inc_stats"

// OnHeartbeat script that keeps track of the adventure difficulty in Realms

void main()
{
    object realm = GetRealm();
    UpdateAdventureDifficulty(realm);
}