#include "inc_stats"
#include "inc_realms"

// OnClientEnter script that keeps track of the player count in Realms

void main()
{
    object realm = GetRealm();
    UpdateNumberOfPlayers(realm);
}