// This is an ItemOnAcquire script used only in the companion creator mode.
// It's registered in sto_wizscrollsop.
#include "inc_debug"
#include "inc_language"
void main()
{
    object item = GetModuleItemAcquired();
    object PC = GetModuleItemAcquiredBy();
    int boughtEarlier = GetLocalInt(item, "Bought");
    if (!boughtEarlier && GetBaseItemType(item) == BASE_ITEM_SPELLSCROLL)
    {
        SetLocalInt(item, "Bought", TRUE);

        int scrollsBought = GetLocalInt(PC, "CREATOR_SCROLLSBOUGHT");
        scrollsBought++;
        SetLocalInt(PC, "CREATOR_SCROLLSBOUGHT", scrollsBought);
        if (scrollsBought >= 1)
        {
            AssignCommand(PC, TakeGoldFromCreature(GetGold(PC), PC, TRUE));

            //The PC could try to be clever and drop some gold before the purchase
            object area = GetArea(PC);
            item = GetFirstObjectInArea(area);
            while (GetIsObjectValid(item))
            {
                if (GetObjectType(item) == OBJECT_TYPE_ITEM && GetBaseItemType(item) == BASE_ITEM_GOLD)
                    DestroyObject(item);
                item = GetNextObjectInArea(area);
            }

            FloatingTextStringOnCreature(GetLocalizedString(
                "You have selected a scroll. Study it to learn the spell and prepare the spells you want your character to have at this level. You will be able to select another scroll on the next level.",
                "Wybrano zw�j. Naucz si� z niego zakl�cia i przygotuj czary, kt�re twoja posta� ma umie� na tym poziomie. Kolejny zw�j b�dzie dost�pny po awansie."
                ), PC, FALSE);
        }
    }
}
