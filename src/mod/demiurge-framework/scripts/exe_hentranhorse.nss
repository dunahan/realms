#include "inc_horses"

// This script exists only to contain a single function that would otherwise be
// in inc_companions, but including "inc_horses" in that script causes
// a "too many includes" error on compilation of some scripts, so... here we go.
// It's executed in inc_companions in lieu of the function itself.

void _TransferHorseFromOldHenchmanToNew(object oOld, object oNew, object oMaster=OBJECT_INVALID)
{
    if (HorseGetIsMounted(oOld))
    {
        string horseResRef = GetSkinString(oOld,"sX3_HorseResRef");
        int tail = GetCreatureTailType(oOld);
        HorseInstantMount(oNew, tail, FALSE, horseResRef);
        RecalculateRidingEffects(oNew);
    }
    else if (GetIsObjectValid(HorseGetMyHorse(oOld)))
    {
        object horse = HorseGetMyHorse(oOld);
        HorseRemoveOwner(horse);
        if (GetFactionEqual(horse, oOld))
            HorseSetOwner(horse, oNew, TRUE);
        else
            HorseSetOwnerWithoutAddingToParty(horse, oNew, TRUE);
        
        if (GetIsObjectValid(oMaster) && GetArea(horse) == GetArea(oMaster))
            AddHenchman(oMaster, horse);

        SetHorseName(horse, oNew);
    }
}

void main()
{
    object oMaster = StringToObject(GetScriptParam("oMaster"));
    object oOld = StringToObject(GetScriptParam("oOld"));
    object oNew = StringToObject(GetScriptParam("oNew"));
    _TransferHorseFromOldHenchmanToNew(oOld, oNew, oMaster);
}
