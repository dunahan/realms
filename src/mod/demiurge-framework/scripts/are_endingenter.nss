void main()
{
    object PC = GetEnteringObject();
    if (!GetIsPC(PC))
        return;

    FadeToBlack(PC, FADE_SPEED_FASTEST);
    SetCutsceneMode(PC);
    AssignCommand(PC, ActionStartConversation(PC, "scorescreen", TRUE, FALSE));
}
