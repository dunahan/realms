#include "inc_debug"
#include "inc_epicspells"

void main()
{
    object PC = GetLastPlayerToSelectTarget();
    object caster = GetLocalObject(PC, "SpellCasterCompanion");
    object target = GetTargetingModeSelectedObject();
    vector targetPosition = GetTargetingModeSelectedPosition();
    int spell = GetLocalInt(caster, "Selected_Spell");

    AssignCommand(caster, ClearAllActions());
    if (GetObjectType(target) != 0) //0 means an area or a module
        AssignCommand(caster, ActionCastSpellAtObjectIncludingEpic(spell, target));
    else
        AssignCommand(caster, ActionCastSpellAtLocationIncludingEpic(spell, Location(target, targetPosition, 0.0f)));
}
