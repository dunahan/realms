#include "inc_quests"
#include "inc_realms"
#include "inc_arrays"
// OnDeath handler for henchmen that fires custom death logic of every quest currently in progress

void main()
{
    int i;
    object realm = GetRealm();
    int questsNum = GetObjectArraySize(REALM_QUESTS_ARRAY, realm);

    for (i = 0; i < questsNum; i++)
    {
        object quest = GetObjectArrayElement(REALM_QUESTS_ARRAY, i, realm);
        int state = GetQuestState(quest);
        if (!GetIsQuestAccepted(quest) || (state != QUEST_STATE_LINGERING && state != QUEST_STATE_COMPLETED))
            continue;
        
        string questScript = GetQuestScript(quest);
        CustomQuestDeathLogic(quest, OBJECT_SELF);
    }
}