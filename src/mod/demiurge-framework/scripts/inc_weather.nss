#include "inc_realms"
#include "inc_biomes"
#include "inc_arrays"
#include "inc_tiles"
#include "inc_debug"

// Library with functions for weather management

// Updates weather in every region with a randomly selected new one
void UpdateRealmWeather();

// Restores weather, should be called on save file load
void RestoreRealmWeather();

// Sets the fog in all regions in the realm
void SetRealmFog();

struct WindParams
{
    vector direction;
    float magnitude;
    float yaw;
    float pitch;
};

struct WindParams _RandomizeWind()
{
    struct WindParams result;
    int strength = Random(4);
    if (strength < 2)
    {
        result.direction = Vector(1.0f, 1.0f, 0.0f);
        result.magnitude = 0.0f;
        result.yaw = 0.0f;
        result.pitch = 0.0f;
        LogInfo("No wind");
    }
    else if (strength == 2)
    {
        result.direction = Vector(1.0f, 1.0f, 0.0f);
        result.magnitude = 1.0f;
        result.yaw = 100.0f;
        result.pitch = 3.0f;
        LogInfo("Light wind");
    }
    else if (strength == 3)
    {
        result.direction = Vector(1.0f, 1.0f, 0.0f);
        result.magnitude = 2.0f;
        result.yaw = 150.0f;
        result.pitch = 5.0f;
        LogInfo("Heavy wind");
    }
    return result;
}

void RestoreRealmWeather()
{
    int weather = GetLocalInt(GetModule(), "CURR_WEATHER");
    object realm = GetRealm();
    int i;
    int biomesNum = GetObjectArraySize(REALM_BIOMES_ARRAY, realm);
    for (i = 0; i < biomesNum; i++)
    {
        object biome = GetObjectArrayElement(REALM_BIOMES_ARRAY, i, realm);
        int j;
        int tilesNum = GetObjectArraySize(BIOME_REGULAR_TILES_ARRAY, biome);
        for (j = 0; j < tilesNum; j++)
        {
            object tile = GetObjectArrayElement(BIOME_REGULAR_TILES_ARRAY, j, biome);
            int k;
            int areasNum = GetObjectArraySize(TILE_AREAS_ARRAY, tile);
            for (k = 0; k < areasNum; k++)
            {
                object area = GetObjectArrayElement(TILE_AREAS_ARRAY, k, tile);
                if (GetIsAreaAboveGround(area) && !GetIsAreaInterior(area))
                {
                    SetWeather(area, weather);
                }
            }
        }
    }
}

void UpdateRealmWeather()
{
    object realm = GetRealm();
    int i;
    int biomesNum = GetObjectArraySize(REALM_BIOMES_ARRAY, realm);
    for (i = 0; i < biomesNum; i++)
    {
        object biome = GetObjectArrayElement(REALM_BIOMES_ARRAY, i, realm);
        string biomeScript = GetBiomeScript(biome);
        int clearChance = GetBiomeWeatherChanceClear(biomeScript);
        int rainChance = GetBiomeWeatherChanceRain(biomeScript);
        int snowChance = GetBiomeWeatherChanceSnow(biomeScript);
        int rand = Random(100);
        int weather = WEATHER_INVALID;
        int skybox; 
        if (rand < clearChance)
        {
            weather = WEATHER_CLEAR;
            skybox = GetBiomeSkyboxClear(biomeScript);
        }
        else if (rand < clearChance + rainChance)
        {
            weather = WEATHER_RAIN;
            skybox = GetBiomeSkyboxRain(biomeScript);
        }
        else
        {
            weather = WEATHER_SNOW;
            skybox = GetBiomeSkyboxSnow(biomeScript);
        }
        struct WindParams wind = _RandomizeWind();

        LogInfo("Weather: %n", weather);

        int j;
        int tilesNum = GetObjectArraySize(BIOME_REGULAR_TILES_ARRAY, biome);
        for (j = 0; j < tilesNum; j++)
        {
            object tile = GetObjectArrayElement(BIOME_REGULAR_TILES_ARRAY, j, biome);
            int k;
            int areasNum = GetObjectArraySize(TILE_AREAS_ARRAY, tile);
            for (k = 0; k < areasNum; k++)
            {
                object area = GetObjectArrayElement(TILE_AREAS_ARRAY, k, tile);
                if (GetIsAreaAboveGround(area) && !GetIsAreaInterior(area))
                {
                    SetWeather(area, weather);
                    SetLocalInt(GetModule(), "CURR_WEATHER", weather);
                    SetSkyBox(skybox, area);
                    AmbientSoundSetDayVolume(area, 25);
                    AmbientSoundSetNightVolume(area, 25);
                    SetAreaWind(area, wind.direction, wind.magnitude, wind.yaw, wind.pitch);
                }
            }
        }
    }
}

int GetCurrentWeather()
{
    return GetLocalInt(GetModule(), "CURR_WEATHER");
}

void SetRealmFog()
{
    object realm = GetRealm();
    int i;
    int biomesNum = GetObjectArraySize(REALM_BIOMES_ARRAY, realm);
    for (i = 0; i < biomesNum; i++)
    {
        object biome = GetObjectArrayElement(REALM_BIOMES_ARRAY, i, realm);
        string biomeScript = GetBiomeScript(biome);

        int fogDayRed = GetBiomeFogColorRed(biomeScript);
        int fogDayGreen = GetBiomeFogColorGreen(biomeScript);
        int fogDayBlue = GetBiomeFogColorBlue(biomeScript);
        int fogNightRed = GetBiomeFogNightColorRed(biomeScript);
        int fogNightGreen = GetBiomeFogNightColorGreen(biomeScript);
        int fogNightBlue = GetBiomeFogNightColorBlue(biomeScript);

        int fogColorDay = 256*256*fogDayRed + 256*fogDayGreen + fogDayBlue;
        int fogColorNight = 256*256*fogNightRed + 256*fogNightGreen + fogNightBlue;

        int j;
        int tilesNum = GetObjectArraySize(BIOME_REGULAR_TILES_ARRAY, biome);
        for (j = 0; j < tilesNum; j++)
        {
            object tile = GetObjectArrayElement(BIOME_REGULAR_TILES_ARRAY, j, biome);
            int k;
            int areasNum = GetObjectArraySize(TILE_AREAS_ARRAY, tile);
            for (k = 0; k < areasNum; k++)
            {
                object area = GetObjectArrayElement(TILE_AREAS_ARRAY, k, tile);
                if (GetIsAreaAboveGround(area) && !GetIsAreaInterior(area))
                {
                    SetFogColor(FOG_TYPE_SUN, fogColorDay, area);
                    SetFogColor(FOG_TYPE_MOON, fogColorNight, area);
                }
            }
        }
    }
}