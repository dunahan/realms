#include "inc_chat"
#include "inc_random"

int StartingConditional()
{
    string lastMsg = GetLastMessage(GetPCSpeaker());
    if (GetStringLength(lastMsg) > 20)
        return TRUE;
    int seed = GetSeedFromString(lastMsg);
    if (seed == 0)
        return TRUE;
    return FALSE;
}
