#include "inc_convoptions"
#include "inc_debug"
void main()
{
    int selectedValue = StringToInt(GetSelectedOptionValue());

    if (selectedValue != 0)
        SetLocalInt(OBJECT_SELF, "STARTLEVEL", selectedValue);

    LogInfo("Starting level: %n", GetLocalInt(OBJECT_SELF, "STARTLEVEL"));
}
