#include "inc_dftokens"
#include "inc_convoptions"
#include "inc_realmsdb"
int StartingConditional()
{
    string id = GetSelectedOptionValue();
    SetLocalString(OBJECT_SELF, "CompanionId", id);
    struct CompanionInformation info = GetCompanionInformation(id);
    SetCompanionInformationTokens(info);
    return TRUE;
}
