#include "inc_arrays"
void main()
{
    string optionsArray = "CompanionNames";
    string valuesArray = "CompanionIds";
    int param = StringToInt(GetScriptParam("Slot"));

    string name = GetStringArrayElement(optionsArray, param);
    string id = GetStringArrayElement(valuesArray, param);

    DeleteStringArrayElement(optionsArray, param);
    DeleteStringArrayElement(valuesArray, param);

    AddStringArrayElement("SelectableNames", name);
    AddStringArrayElement("SelectableIds", id);
}
