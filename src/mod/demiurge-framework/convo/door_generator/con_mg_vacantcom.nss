#include "inc_arrays"
int StartingConditional()
{
    string optionsArray = "CompanionNames";
    
    int PCs = 0;
    object PC = GetFirstPC();
    while (GetIsObjectValid(PC))
    {
        PCs++;
        PC = GetNextPC();
    }

    if (GetStringArraySize(optionsArray) + PCs < 5)
        return TRUE;
    return FALSE;
}
