#include "inc_generation"
#include "inc_common"
#include "inc_language"
#include "inc_debug"
#include "inc_realms"
#include "inc_ruleset"
#include "inc_stats"
#include "inc_weather"

// This is a realm generating procedure

int height;
int width;
string descriptor;
string seed;
int players;
object mapObject;

int _GetNextMajorStep()
{
    return GetLocalInt(OBJECT_SELF, "PROC_NEXTMAJORSTEP");
}

void GenMap()
{
    SendMessageToAll(GetLocalizedString("Generating the map...", "Generowanie mapy..."));
    int i;
    for (i = 1; i <= 151; i++)
        DelayCommand(0.0f, GenerateRealmMap(width, height, i, mapObject));
}

void GenPaths()
{
    if (_GetNextMajorStep() < 1)
    {
        DelayCommand(0.1, GenPaths());
        return;
    }

    SendMessageToAll(GetLocalizedString("Generating area transitions...", "Generowanie przej�� mi�dzy obszarami..."));
    int i;
    for (i = 1; i <= 46; i++)
        DelayCommand(0.0f, GeneratePaths(i, mapObject));
}

void Pathfinding()
{
    if (_GetNextMajorStep() < 2)
    {
        DelayCommand(0.1, Pathfinding());
        return;
    }
    
    SendMessageToAll(GetLocalizedString("Pathfinding...", "Szukanie �cie�ek..."));
    int i;
    for (i = 1; i <= 1353; i++)
        DelayCommand(0.0f, FindPaths(i, mapObject));
}

void InterRegionPathfinding()
{
    if (_GetNextMajorStep() < 3)
    {
        DelayCommand(0.1, InterRegionPathfinding());
        return;
    }
    
    SendMessageToAll(GetLocalizedString("Inter-region pathfinding...", "Szukanie �cie�ek mi�dzy regionami..."));
    int i;
    for (i = 1; i <= 400; i++)
        DelayCommand(0.0f, FindPathsBetweenRegions(i, mapObject));
}

void GenSpecialTiles()
{
    if (_GetNextMajorStep() < 4)
    {
        DelayCommand(0.1, GenSpecialTiles());
        return;
    }
    
    SendMessageToAll(GetLocalizedString("Special area generation...", "Generowanie obszar�w specjalnych..."));
    int i;
    for (i = 1; i <= 46; i++)
        DelayCommand(0.0f, GenerateSpecialTiles(i, mapObject));
}

void DataMigration()
{
    if (_GetNextMajorStep() < 5)
    {
        DelayCommand(0.1, DataMigration());
        return;
    }

    SendMessageToAll(GetLocalizedString("Data migration...", "Migrowanie danych..."));
    DelayCommand(0.0f, CreateBiomeObjectsOnRealm(mapObject));
}

void PlotGeneration()
{
    if (_GetNextMajorStep() < 6)
    {
        DelayCommand(0.1, PlotGeneration());
        return;
    }

    int seed = GetLocalInt(GetModule(), "_custom_seed_default");
    LogInfo("Current seed value: %n", seed);

    SendMessageToAll(GetLocalizedString("Plot creation...", "Tworzenie scenariusza..."));
    SpawnPlot(mapObject);
}

void Spawn()
{
    if (_GetNextMajorStep() < 7)
    {
        DelayCommand(0.1, Spawn());
        return;
    }
    
    int seed = GetLocalInt(GetModule(), "_custom_seed_default");
    LogInfo("Current seed value: %n", seed);

    SendMessageToAll(GetLocalizedString("Areas creation...", "Tworzenie obszar�w..."));
    int i;
    for (i = 1; i <= 256; i++)
        DelayCommand(0.0f, SpawnMapTiles(i, mapObject));
}

void Initialize()
{
    if (_GetNextMajorStep() < 8)
    {
        DelayCommand(0.3, Initialize());
        return;
    }
    
    int seed = GetLocalInt(GetModule(), "_custom_seed_default");
    LogInfo("Current seed value: %n", seed);

    SendMessageToAll(GetLocalizedString("Areas initialization...", "Inicjalizacja obszar�w..."));

    int areasNum = height*width;
    int i;
    for (i = 1; i <= 256; i++)
        DelayCommand(0.0f, InitializeMapTiles(i, mapObject));
}

void Quests()
{
    if (_GetNextMajorStep() < 9)
    {
        DelayCommand(0.3, Quests());
        return;
    }
    
    int seed = GetLocalInt(GetModule(), "_custom_seed_default");
    LogInfo("Current seed value: %n", seed);

    SendMessageToAll(GetLocalizedString("Quest generation...", "Generowanie zada�..."));
    int i;
    for (i = 1; i <= 20; i++)
        DelayCommand(0.0f, SpawnQuests(i, mapObject));
}

void Champions()
{
    if (_GetNextMajorStep() < 10)
    {
        DelayCommand(0.3, Champions());
        return;
    }
    
    int seed = GetLocalInt(GetModule(), "_custom_seed_default");
    LogInfo("Current seed value: %n", seed);

    SendMessageToAll(GetLocalizedString("Encounter champions generation...", "Generowanie czempion�w..."));
    int i;
    for (i = 1; i <= 20; i++)
        DelayCommand(0.0f, SpawnChampions(i, mapObject));
}

void Commoners()
{
    if (_GetNextMajorStep() < 11)
    {
        DelayCommand(0.3, Commoners());
        return;
    }
    
    int seed = GetLocalInt(GetModule(), "_custom_seed_default");
    LogInfo("Current seed value: %n", seed);

    SendMessageToAll(GetLocalizedString("Spawning commoners...", "Tworzenie NPCt�w..."));
    int i;
    for (i = 1; i <= 20; i++)
        DelayCommand(0.0f, SpawnCommoners(i, mapObject));
}

void BountyHunters()
{
    if (_GetNextMajorStep() < 12)
    {
        DelayCommand(0.3, BountyHunters());
        return;
    }

    int seed = GetLocalInt(GetModule(), "_custom_seed_default");
    LogInfo("Current seed value: %n", seed);

    SendMessageToAll(GetLocalizedString("Spawning bounty hunters...", "Tworzenie �owc�w g��w..."));
    int i;
    for (i = 1; i <= 20; i++)
        DelayCommand(0.0f, SpawnBountyHunters(i, mapObject));
}

void SetDoorTransition()
{
    SendMessageToAll(GetLocalizedString("Realms successfully generated!", "Kraina pomy�lnie wygenerowana!"));
    int checksum = GetLocalInt(GetModule(), "MAP_CHECKSUM");
    LogInfo("Generated map's checksum: %n", checksum);
    SetEventScript(OBJECT_SELF, EVENT_SCRIPT_DOOR_ON_CLICKED, "obj_gotorealm");
    ActionResumeConversation();
}

void DeleteTemplates()
{
    if (_GetNextMajorStep() < 13)
    {
        DelayCommand(0.3, DeleteTemplates());
        return;
    }

    DeleteTemplateAreas();
    UpdateRealmWeather();
    SetRealmFog();
    SetDoorTransition();
}

void _TransferRulesetModifiers(object oRealm)
{
    int startingGold = GetLocalInt(OBJECT_SELF, "STARTING_GOLD");
    int questGold = GetLocalInt(OBJECT_SELF, "QUEST_GOLD");
    int bountyGold = GetLocalInt(OBJECT_SELF, "BOUNTY_GOLD");
    int startingSoulstones = GetLocalInt(OBJECT_SELF, "STARTING_LIVES");
    SetRuleset(oRealm, startingGold, questGold, bountyGold, startingSoulstones);
}

int postDelay = FALSE;

void main()
{
    //small delay is necessary to display the conversation node at all
    if (!postDelay)
    {
        ActionPauseConversation();
        postDelay = TRUE;
        DelayCommand(0.1f, main());
        return;
    }

    int size = GetLocalInt(OBJECT_SELF, "MAPSIZE");
    height = size;
    width = size;
    descriptor = "World";
    mapObject = GetRealm();
    seed = GetLocalString(OBJECT_SELF, "SEED"); //1488417218;
    players = 1;

    SetLocalInt(mapObject, "Width", width);
    int difficulty = GetGameDifficulty();
    SetAdventureDifficulty(mapObject, difficulty);
    UpdateNumberOfPlayers(mapObject);

    _TransferRulesetModifiers(mapObject);

    SetRealmParameters(size, players, seed, mapObject);
    SetEventScript(OBJECT_SELF, EVENT_SCRIPT_DOOR_ON_CLICKED, "");
    GenMap();
    DelayCommand(0.0f, GenPaths());
    DelayCommand(0.0f, Pathfinding());
    DelayCommand(0.0f, InterRegionPathfinding());
    DelayCommand(0.0f, GenSpecialTiles());
    DelayCommand(0.0f, DataMigration());
    DelayCommand(0.0f, PlotGeneration());
    DelayCommand(0.0f, Spawn());
    DelayCommand(0.0f, Initialize());
    DelayCommand(0.0f, Quests());
    DelayCommand(0.0f, Champions());
    DelayCommand(0.0f, Commoners());
    DelayCommand(0.0f, BountyHunters());
    DelayCommand(0.0f, DeleteTemplates());
}
