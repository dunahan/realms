#include "inc_arrays"
void main()
{
    string optionsArray = "SelectableNames";
    string valuesArray = "SelectableIds";
    string id = GetLocalString(OBJECT_SELF, "CompanionId");

    int index = GetStringArrayIndexByValue(valuesArray, id);

    string name = GetStringArrayElement(optionsArray, index);

    DeleteStringArrayElement(optionsArray, index);
    DeleteStringArrayElement(valuesArray, index);

    AddStringArrayElement("CompanionNames", name);
    AddStringArrayElement("CompanionIds", id);
}
