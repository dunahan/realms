#include "inc_quests"
#include "inc_realms"
#include "inc_experience"
#include "inc_companions"
#include "inc_plots"

void SetCompanionXPVoid(object companion, int xp)
{
    SetCompanionXP(companion, xp);
}

void main()
{
    object oQuest = GetLocalObject(OBJECT_SELF, "QUEST");
    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    object encounter = GetQuestEncounter(oQuest);
    object specialArea = GetQuestSpecialArea(oQuest);
    object area = GetIsObjectValid(encounter) ? GetEncounterArea(encounter) : specialArea;
    object biome = GetTileBiome(area);

    UpdateQuestState(oQuest, QUEST_STATE_AFTER_COMPLETED);
    UntrackQuest(oQuest);
    CustomQuestCompletionLogic(oQuest, GetPCSpeaker());

    struct QuestReward reward = GetQuestInstanceReward(oQuest);
    object oPC = GetPCSpeaker();

    GiveGoldToCreature(oPC, reward.gold);
    CreateItemOnObject(reward.itemResRef, oPC, reward.itemStackSize);

    //Update plot if all quests are finished
    int biomeQuestCounter = GetRegionCompletedQuests(biome);
    if (biomeQuestCounter == 3)
    {
        int biomeIndex = GetRegionOrderNumber(biome)-1;
        object plot = GetRealmPlot(realm);
        UpdatePlotForward(plot, PLOT_STATE_DONE_QUESTS, biomeIndex);
    }
    else
    {
        IncrementRegionCompletedQuests(biome);
        IncrementRegionPastQuests(biome);
    }

    int biomeStartingLevel = GetRegionStartingLevel(biome);
    int levelsPerRegion = GetNumberOfLevelsPerRegion(realmScript);
    int startingExpAmount = GetExperienceForLevel(biomeStartingLevel);
    int finalExpAmount = GetExperienceForLevel(biomeStartingLevel+levelsPerRegion);
    int baseExpReward = (finalExpAmount-startingExpAmount) / 3 + 1; //one extra XP point to ensure three quests are enough to advance
    oPC = GetFirstPC();
    while (GetIsObjectValid(oPC))
    {
        int expToGive = GetPenaltyAdjustedExperience(oPC, baseExpReward);
        GiveXPToCreature(oPC, expToGive);

        int henchNum = 1;
        object henchman = GetHenchman(oPC, henchNum);
        while (GetIsObjectValid(henchman))
        {
            //we're only interested with henchmen with ID to avoid giving xp to horses
            if (GetLocalString(henchman, "ID") != "")
            {
                expToGive = GetPenaltyAdjustedExperience(henchman, baseExpReward);
                int xp = GetXP(henchman) + expToGive;
                //delay is there because the henchman may be replaced with another instance and we don't want that to interfere with the looping
                DelayCommand(0.1, SetCompanionXPVoid(henchman, xp));
                LogInfo("Henchman "+GetName(henchman)+" received %n experience (out of base %n)", expToGive, baseExpReward);
            }
            henchman = GetHenchman(oPC, ++henchNum);
        }

        oPC = GetNextPC();
    }
}
