#include "inc_quests"
#include "inc_biomes"
#include "inc_plots"
#include "inc_stats"

void main()
{
    //Get biome
    object realm = GetRealm();
    object quest = GetLocalObject(OBJECT_SELF, "QUEST");
    object encounter = GetQuestEncounter(quest);
    object specialArea = GetQuestSpecialArea(quest);
    object area = GetIsObjectValid(encounter) ? GetEncounterArea(encounter) : specialArea;
    object biome = GetTileBiome(area);

    //Activate next inactive quest
    int inactiveQuestsNum = GetObjectArraySize(BIOME_INACTIVE_QUESTS, biome);
    if (inactiveQuestsNum > 0)
    {
        object nextQuest = GetObjectArrayElement(BIOME_INACTIVE_QUESTS, 0, biome);
        ActivateQuest(nextQuest);
        DeleteObjectArrayElement(BIOME_INACTIVE_QUESTS, 0, biome);
    }
    
    IncrementRegionPastQuests(biome);
    int pastQuests = GetRegionPastQuests(biome);
    if (pastQuests == 7)
    {
        int biomeIndex = GetRegionOrderNumber(biome)-1;
        object realm = GetRealm();
        object plot = GetRealmPlot(realm);
        UpdatePlotForward(plot, PLOT_STATE_FAILED_QUESTS, biomeIndex);
    }

    int questState = GetQuestState(quest);
    UpdateQuestState(quest, QUEST_STATE_REFUSED);
    UntrackQuest(quest);
    if (questState != QUEST_STATE_NONE)
        CustomQuestFailureLogic(quest, GetPCSpeaker());
    RegisterFailedQuest(realm);
}
