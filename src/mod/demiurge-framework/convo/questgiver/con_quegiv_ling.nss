#include "inc_quests"

int StartingConditional()
{
    object oQuest = GetLocalObject(OBJECT_SELF, "QUEST");
    int questState = GetQuestState(oQuest);

    if (questState == QUEST_STATE_LINGERING
        || questState == QUEST_STATE_COMPLETED
        || questState == QUEST_STATE_FAILED)
        return TRUE;
    return FALSE;
}
