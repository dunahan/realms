#include "inc_quests"

int StartingConditional()
{
    object oQuest = GetLocalObject(OBJECT_SELF, "QUEST");
    int questState = GetQuestState(oQuest);

    if (questState == QUEST_STATE_AFTER_COMPLETED)
        return TRUE;
    return FALSE;
}
