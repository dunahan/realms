#include "inc_dftokens"
#include "inc_quests"

int StartingConditional()
{
    SetQuestGiverTokens(OBJECT_SELF);
    object oQuest = GetLocalObject(OBJECT_SELF, "QUEST");

    if (GetIsQuestAccepted(oQuest))
        return FALSE;
    return TRUE;
}
