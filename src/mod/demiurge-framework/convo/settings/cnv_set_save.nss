#include "inc_realmsdb"
void main()
{
    struct PlayerSettings settings;
    object PC = GetPCSpeaker();
    settings.mapOffsetX = GetLocalInt(PC, "TEMP_MAP_X");
    settings.mapOffsetY = GetLocalInt(PC, "TEMP_MAP_Y");
    settings.mapImageSize = GetLocalInt(PC, "TEMP_MAP_SIZE");
    SetPlayerSettings(PC, settings);
}
