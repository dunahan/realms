#include "inc_map"
#include "inc_realms"
void FlashMap(object oPC)
{
    int mapFlashing = GetLocalInt(oPC, "MAP_FLASH");
    if (!mapFlashing)
        return;

    object realm = GetRealm();
    float period = 1.0f;

    int xOffset = GetLocalInt(oPC, "TEMP_MAP_X");
    int yOffset = GetLocalInt(oPC, "TEMP_MAP_Y");
    int imgSize = GetLocalInt(oPC, "TEMP_MAP_SIZE");

    DisplayMap(oPC, realm, TRUE, -1, -1, FALSE, xOffset, yOffset, imgSize);
    DelayCommand(period/2, CancelMapDisplay(oPC));
    DelayCommand(period, FlashMap(oPC));
}

void main()
{
    object PC = GetPCSpeaker();
    int mapFlashing = GetLocalInt(PC, "MAP_FLASH");
    if (mapFlashing)
        return;

    SetLocalInt(PC, "MAP_FLASH", TRUE);
    FlashMap(PC);
}
