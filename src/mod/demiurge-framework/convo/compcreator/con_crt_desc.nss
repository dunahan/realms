#include "inc_dftokens"
#include "inc_chat"
int StartingConditional()
{
    object PC = GetPCSpeaker();
    string lastMsg = GetLastMessage(PC);
    SetLocalString(PC, "CREATOR_DESCRIPTION", lastMsg);
    SetLastMessageToken(lastMsg);
    return TRUE;
}
