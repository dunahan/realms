int StartingConditional()
{
    object PC = GetPCSpeaker();
    int creationInProgress = GetLocalInt(PC, "CREATOR_PROGRESS");
    int expectedLevel = GetLocalInt(PC, "CREATOR_LVL");
    int isWizard = GetLocalInt(PC, "CREATOR_WIZARD");

    if (creationInProgress == FALSE)
        return FALSE;

    if (isWizard && expectedLevel > GetHitDice(PC))
        return TRUE;

    return FALSE;
}
