#include "inc_companions"
#include "inc_experience"
void RegisterFirstSnapshot(object PC)
{
    string firstName = GetLocalString(PC, "CREATOR_FIRSTNAME");
    string lastName = GetLocalString(PC, "CREATOR_LASTNAME");
    string description = GetLocalString(PC, "CREATOR_DESCRIPTION");

    SetXP(PC, 0);

    string id = StartNewCompanionCreationAndReturnID(PC, firstName, lastName, description);
    ForceRest(PC);
    AddCompanionSnapshot(id, 1, PC);

    if (GetLevelByClass(CLASS_TYPE_WIZARD, PC) > 0)
    {
        SetLocalInt(PC, "CREATOR_WIZARD", TRUE);
    }
    SetLocalString(PC, "CREATOR_ID", id);
    SetLocalInt(PC, "CREATOR_LVL", 2);
    SetLocalInt(PC, "CREATOR_PROGRESS", TRUE);
    SetLocalInt(PC, "CREATOR_SCROLLSBOUGHT", 0);

    int newXP = GetExperienceForLevel(2);
    SetXP(PC, newXP);
}

void main()
{
    object PC = GetPCSpeaker();

    //delete all items of the PC
    object item = GetFirstItemInInventory(PC);
    while (GetIsObjectValid(item))
    {
        DestroyObject(item);
        item = GetNextItemInInventory(PC);
    }
    int i;
    for (i = 0; i <= 13; i++)
    {
        item = GetItemInSlot(i, PC);
        DestroyObject(item);
    }

    //...and in the area
    object area = GetArea(PC);
    item = GetFirstObjectInArea(area);
    while (GetIsObjectValid(item))
    {
        if (GetObjectType(item) == OBJECT_TYPE_ITEM)
            DestroyObject(item);
        item = GetNextObjectInArea(area);
    }

    //Take their gold, too
    TakeGoldFromCreature(GetGold(PC), PC, TRUE);

    DelayCommand(0.1, RegisterFirstSnapshot(PC));
}
