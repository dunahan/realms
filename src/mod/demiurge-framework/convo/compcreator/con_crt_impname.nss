#include "inc_dftokens"
#include "inc_chat"
int StartingConditional()
{
    object PC = GetPCSpeaker();
    string lastMsg = GetLastMessage(PC);
    lastMsg = GetStringLowerCase(lastMsg);
    SetLocalString(PC, "CREATOR_IMPORTNAME", lastMsg);
    SetLastMessageToken(lastMsg);
    return TRUE;
}
