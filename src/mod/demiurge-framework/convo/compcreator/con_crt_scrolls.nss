int StartingConditional()
{
    string level = GetScriptParam("Level");
    int spellLvl = StringToInt(level);
    int wizLvl = GetLevelByClass(CLASS_TYPE_WIZARD, GetPCSpeaker());
    int minLvl;
    switch (spellLvl)
    {
        case 0: minLvl = 1; break;
        case 1: minLvl = 1; break;
        case 2: minLvl = 3; break;
        case 3: minLvl = 5; break;
        case 4: minLvl = 7; break;
        case 5: minLvl = 9; break;
        case 6: minLvl = 11; break;
        case 7: minLvl = 13; break;
        case 8: minLvl = 15; break;
        case 9: minLvl = 17; break;
    }
    return wizLvl >= minLvl;
}
