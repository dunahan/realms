#include "inc_companions"
#include "inc_experience"
void main()
{
    object PC = GetPCSpeaker();
    if (!GetIsObjectValid(PC))
        PC = GetLocalObject(OBJECT_SELF, "PC"); //workaround for GetPCSpeaker() not working in ActionTaken of a single-node convo
    LogInfo(GetName(PC));
    string id = GetLocalString(PC, "CREATOR_ID");
    int currentLvl = GetHitDice(PC);
    int nextLvl = currentLvl+1;

    //delete all remainder scrolls of the PC (we don't want companions to spawn with them)
    object item = GetFirstItemInInventory(PC);
    while (GetIsObjectValid(item))
    {
        DestroyObject(item);
        item = GetNextItemInInventory(PC);
    }

    ForceRest(PC);
    AddCompanionSnapshot(id, currentLvl, PC);
    SetLocalInt(PC, "CREATOR_LVL", nextLvl);
    SetLocalInt(PC, "CREATOR_SCROLLSBOUGHT", 0);

    int newXP = GetExperienceForLevel(nextLvl);
    SetXP(PC, newXP);
}
