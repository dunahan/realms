#include "inc_dftokens"
#include "inc_convoptions"
#include "inc_realmsdb"
int StartingConditional()
{
    string id = GetSelectedOptionValue();
    if (id == "")
        id = GetLocalString(OBJECT_SELF, "COMPANION_ID");
    else
        SetLocalString(OBJECT_SELF, "COMPANION_ID", id);
    struct CompanionInformation info = GetCompanionInformation(id);
    SetCompanionInformationTokens(info);
    return TRUE;
}
