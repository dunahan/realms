int StartingConditional()
{
    object PC = GetPCSpeaker();
    int creationInProgress = GetLocalInt(PC, "CREATOR_PROGRESS");
    int level = GetHitDice(PC);

    if (creationInProgress == FALSE)
        return FALSE;

    if (level == 40)
        return TRUE;

    return FALSE;
}
