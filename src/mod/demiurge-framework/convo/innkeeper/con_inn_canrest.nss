int StartingConditional()
{
    object area = GetArea(OBJECT_SELF);
    return GetLocalInt(area, "CanRest");
}
