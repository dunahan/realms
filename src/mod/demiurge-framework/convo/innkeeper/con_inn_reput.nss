#include "inc_reputation"
#include "inc_dftokens"

int StartingConditional()
{
    string text = GetDescriptivePartyReputation();
    SetReputationDescriptionToken(text);
    return TRUE;
}
