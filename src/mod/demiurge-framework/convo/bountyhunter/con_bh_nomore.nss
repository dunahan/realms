#include "inc_biomes"
#include "inc_towns"
#include "inc_encounters"
#include "inc_tiles"
#include "inc_common"

int StartingConditional()
{
    object area = GetArea(OBJECT_SELF);
    object tile = GetTile(area);
    object biome = GetBiomeOfTown(tile);
    
    int i;
    for (i = 0; i < GetObjectArraySize(BIOME_ENCOUNTERS_WITH_CHAMPIONS, biome); i++)
    {
        object encounter = GetObjectArrayElement(BIOME_ENCOUNTERS_WITH_CHAMPIONS, i, biome);
        object trophy = GetTrophyOfEncounter(encounter);
        if (GetIsObjectValid(trophy) && GetLocalInt(OBJECT_SELF, ObjectToString(trophy)) == FALSE)
            return FALSE;
    }

    SetLocalInt(OBJECT_SELF, "NoMoreBounties", TRUE);
    effect questMark = GetEffectByTag(OBJECT_SELF, "QuestMark");
    RemoveEffect(OBJECT_SELF, questMark);
    return TRUE;
}
