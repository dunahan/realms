#include "inc_convoptions"

void main()
{
    int option = StringToInt(GetScriptParam("Option"));
    SetConversationOptionValue(option);

    string description = GetSelectedOptionValue();
    SetLocalString(OBJECT_SELF, "ChampionDescription", description);
}
