int StartingConditional()
{
    if (GetLocalInt(OBJECT_SELF, "AlreadyTalked") == TRUE)
        return FALSE;

    SetLocalInt(OBJECT_SELF, "AlreadyTalked", TRUE);
    return TRUE;
}
