#include "inc_champions"
#include "inc_encounters"
#include "inc_realms"
#include "inc_tiles"
#include "inc_biomes"
#include "inc_arrays"
#include "inc_common"
#include "inc_stats"

void main()
{
    object thisArea = GetArea(OBJECT_SELF);
    object thisTile = GetTile(thisArea);
    object thisBiome = GetTileBiome(thisTile);
    object PC = GetPCSpeaker();
    object item = GetFirstItemInInventory(PC);
    while (GetIsObjectValid(item))
    {
        object encounter = GetEncounterOfTrophy(item);
        if (encounter != OBJECT_INVALID)
        {
            object area = GetEncounterArea(encounter);
            object tile = GetTile(area);
            object biome = GetTileBiome(tile);
            if (biome == thisBiome)
            {
                int bounty = GetBountyOfTrophy(item);
                GiveGoldToCreature(PC, bounty);
                SetLocalInt(OBJECT_SELF, ObjectToString(item), TRUE);
                DestroyObject(item);
                RegisterBountyMissionCompleted(GetRealm());
            }
        }
        item = GetNextItemInInventory(PC);
    }

    //Update quest marker to exclamation mark if no other PC has any trophies
    int i;
    for (i = 0; i < GetObjectArraySize(BIOME_ENCOUNTERS_WITH_CHAMPIONS, thisBiome); i++)
    {
        object encounter = GetObjectArrayElement(BIOME_ENCOUNTERS_WITH_CHAMPIONS, i, thisBiome);
        object biomeTrophy = GetTrophyOfEncounter(encounter);
        if (GetLocalInt(OBJECT_SELF, ObjectToString(biomeTrophy)) == TRUE)
            continue;

        object owner = GetItemPossessor(biomeTrophy);
        if (GetIsObjectValid(owner) && GetIsPC(owner))
            return;
    }

    effect questMarker = GetEffectByTag(OBJECT_SELF, "QuestMark");
    RemoveEffect(OBJECT_SELF, questMarker);
    effect newQuestMarker = TagEffect(EffectVisualEffect(677), "QuestMark"); //green exclamation mark
    ApplyEffectToObject(DURATION_TYPE_PERMANENT, newQuestMarker, OBJECT_SELF);
}
