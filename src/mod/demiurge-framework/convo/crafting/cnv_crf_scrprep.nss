#include "inc_convoptions"
#include "inc_common"
#include "inc_colors"
#include "inc_crafting"
#include "inc_debug"
#include "inc_dftokens"
#include "inc_realms"

void main()
{
    object PC = GetPCSpeaker();
    object crafter = GetCrafterCreature(PC);
    int resourcesInPool = GetScrollWritingPoolValue(PC);

    string spell = GetSelectedOptionValue();
    int spellId = spell != "" ? StringToInt(spell) : GetLocalInt(PC, "Crafted_Spell");
    SetLocalInt(PC, "Crafted_Spell", spellId);

    struct CasterClassAndSpellLevel classAndSpellLvl = _GuessCasterClassAndSpellLevel(crafter, spellId);
    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    int requiredResources = GetResourceUnitsRequiredToScribeScroll(realmScript, spellId, classAndSpellLvl.spellLvl);
    SetSpellCraftingTokens(spellId, requiredResources, resourcesInPool);
}
