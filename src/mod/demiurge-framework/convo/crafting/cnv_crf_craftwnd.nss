#include "inc_crafting"

void main()
{
    object PC = GetPCSpeaker();
    object crafter = GetCrafterCreature(PC);
    int spellId = GetLocalInt(PC, "Crafted_Spell");
    CraftWand(PC, crafter, spellId);
}
