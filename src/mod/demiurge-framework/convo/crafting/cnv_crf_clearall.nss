#include "inc_crafting"
#include "inc_convoptions"

void main()
{
    object PC = GetPCSpeaker();
    ClearPotionBrewingPool(PC);
    ClearScrollWritingPool(PC);
    ClearWandCraftingPool(PC);
    SetCrafterCreature(PC, PC);
    ClearConversationOptions();
}
