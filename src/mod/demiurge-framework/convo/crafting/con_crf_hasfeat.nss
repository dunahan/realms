#include "inc_debug"
#include "inc_common"
#include "inc_crafting"
int StartingConditional()
{
    string param = GetScriptParam("Feat");
    if (!GetIsStringInt(param))
    {
        LogWarning("Invalid feat constant provided (con_crf_hasfeat)");
        return FALSE;
    }

    int feat = StringToInt(param);
    object crafter = GetCrafterCreature(GetPCSpeaker());

    return GetHasFeat(feat, crafter);
}
