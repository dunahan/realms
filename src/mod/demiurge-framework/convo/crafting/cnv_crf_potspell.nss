#include "inc_convoptions"
#include "inc_common"
#include "inc_colors"
#include "inc_crafting"
#include "inc_debug"
#include "inc_dftokens"

void main()
{
    object PC = GetPCSpeaker();
    object crafter = GetCrafterCreature(PC);

    if (GetConversationOptionsInitialized())
        return;

    string optionsArray = "optionsArray";
    string valuesArray = "valuesArray";

    CreateStringArray(optionsArray);
    CreateStringArray(valuesArray);

    int spellId = 0;
    for (spellId = 0; spellId < 1000; spellId++)
    {
        string nameStringRef = Get2DAString("spells", "Name", spellId);
        int isSubSpell = Get2DAString("spells", "Master", spellId) != "";
        int hasSubSpells = Get2DAString("spells", "SubRadSpell1", spellId) != "";

        if (GetIsStringInt(nameStringRef) && !isSubSpell && CanBrewPotion(PC, crafter, spellId, FALSE))
        {
            string spellName = GetStringByStrRef(StringToInt(nameStringRef));

            string value = IntToString(spellId);
            string option = "["+spellName+"]";
            option = ColorStringWithStoredColor(option, "10");
            AddStringArrayElement(optionsArray, option);
            AddStringArrayElement(valuesArray, value);
        }
    }

    LoadConversationOptions(optionsArray, valuesArray);
}
