#include "inc_crafting"
int StartingConditional()
{
    object PC = GetPCSpeaker();
    object crafter = GetCrafterCreature(PC);
    int spell = GetLocalInt(PC, "Crafted_Spell");
    return CanWriteScroll(PC, crafter, spell);
}
