#include "x3_inc_horse"

int StartingConditional()
{
    object owner = HorseGetOwner(OBJECT_SELF);
    object PC = GetPCSpeaker();
    if (owner == PC || GetMaster(owner) == PC)
        return TRUE;
    return FALSE;
}
