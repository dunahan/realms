void main()
{
    object PC = GetLocalObject(OBJECT_SELF, "PC");
    TakeGoldFromCreature(GetGold(PC), PC, TRUE);

    //The PC could try to be clever and drop some gold before the purchase
    object area = GetArea(PC);
    object item = GetFirstObjectInArea(area);
    while (GetIsObjectValid(item))
    {
        if (GetObjectType(item) == OBJECT_TYPE_ITEM && GetBaseItemType(item) == BASE_ITEM_GOLD)
            DestroyObject(item);
        item = GetNextObjectInArea(area);
    }
}
